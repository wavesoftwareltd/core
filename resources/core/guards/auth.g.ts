import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '@resources/core/services/auth.service';
 import {Observable} from 'rxjs/Observable';


@Injectable()
export class AuthGuard implements CanActivate {
 
    constructor(private router: Router,private auth: AuthService) { }
 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>{


        if (this.auth.isAuthenticated()) {
            // logged in so return true 
            return true; 
        } 
 
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
    
}  