import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '@resources/core/services/auth.service';
 import {Observable} from 'rxjs/Observable';


@Injectable()
export class LoginGuard implements CanActivate {
 
    constructor(private router: Router,private auth: AuthService) { }
 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>{


        if (this.auth.isAuthenticated()) {
         // redirect login user to homepage
         console.log(state);
         console.log(route);
         console.log(this.router);
        this.router.navigate(['/']);
        return false;
        }else{
            // not logged in so return true 
            return true; 
        }
 
      
    }
    
}  