import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

@Directive({
  selector: '[email-validation]',
  providers: [{provide: NG_VALIDATORS, useExisting: EmailValidatorDirective, multi: true}]
})
export class EmailValidatorDirective implements Validator {

  validate(control: AbstractControl): {[key: string]: any} {
    return this.emailValidator()(control);
  }

  checkEmail(email : string): boolean {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

  emailValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const isValid = this.checkEmail(control.value);
    return !isValid ? {'emailValidator': {value: control.value}} : null;
  };
}
}