import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

@Directive({
  selector: '[min]',
  providers: [{provide: NG_VALIDATORS, useExisting: MinValidatorDirective, multi: true}]
})
export class MinValidatorDirective implements Validator,OnChanges {
  @Input() min = 0;
  currentControl: AbstractControl;

 
  validate(control: AbstractControl): {[key: string]: any} {
    this.currentControl = control;
    return this.matchFields()(control);
  }

  matchFields(): ValidatorFn {
      var that = this;
  return (control: AbstractControl): {[key: string]: any} => {
    const isValid = control.value ? (that.min <= control.value.length) : false;
    return !isValid ? {'confirmField': {value: control.value}} : null;
  };
}

 ngOnChanges(changes: SimpleChanges) {
     if(this.currentControl){
     this.currentControl.updateValueAndValidity();
     }
  }

} 