import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';
import { parse, format, isValidNumber,CountryCode } from 'libphonenumber-js'

@Directive({
  selector: '[phone-validation]',
  providers: [{provide: NG_VALIDATORS, useExisting: PhoneValidatorDirective, multi: true}]
})
export class PhoneValidatorDirective implements Validator {

  protected DEFAULT_COUNTRY_CODE:CountryCode = 'IL';

  validate(control: AbstractControl): {[key: string]: any} {
    return this.phoneValidator()(control);
  }

  checkPhone(phone): boolean {
  return isValidNumber(phone,this.DEFAULT_COUNTRY_CODE);
}

  phoneValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const isValid = this.checkPhone(control.value);
    return !isValid ? {'phoneValidator': {value: control.value}} : null;
  };
}
}