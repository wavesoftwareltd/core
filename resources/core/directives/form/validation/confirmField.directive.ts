import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

@Directive({
  selector: '[confirm-field-validation]',
  providers: [{provide: NG_VALIDATORS, useExisting: ConfirmFieldValidatorDirective, multi: true}]
})
export class ConfirmFieldValidatorDirective implements Validator,OnChanges {
  @Input() target;
  currentControl: AbstractControl;

 
  validate(control: AbstractControl): {[key: string]: any} {
    this.currentControl = control;
    return this.matchFields()(control);
  }

  matchFields(): ValidatorFn {
      var that = this;
  return (control: AbstractControl): {[key: string]: any} => {
    const isValid = (that.target == control.value);
    return !isValid ? {'confirmField': {value: control.value}} : null;
  };
}

 ngOnChanges(changes: SimpleChanges) {
     if(this.currentControl){
     this.currentControl.updateValueAndValidity();
     }
  }

} 