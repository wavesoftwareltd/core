import { Directive, Input, OnChanges, SimpleChanges, ElementRef } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators, NgModel } from '@angular/forms';

declare var $: any;
declare var CURRENT_LANG: any;

@Directive({
  selector: '[data-date-format]',
  providers: [NgModel],
  host: {
    '(ngModelChange)': 'onInputChange($event)'
  }
})
export class DatetimepickerDirective {

  constructor(private model: NgModel, private elementRef: ElementRef) {
    var that = this;
    $(this.elementRef.nativeElement).datetimepicker({
      autoclose: true,
      // componentIcon: '.s7-date',
      navIcons: {
        rightIcon: 's7-angle-right',
        leftIcon: 's7-angle-left'
      },
      language: CURRENT_LANG ? CURRENT_LANG : 'en'
    }) ;
    $(this).addClass("hasDatepicker");
    
    $(elementRef.nativeElement).on('change', function () {
      that.model.valueAccessor.writeValue($(this).val());
      that.model.viewToModelUpdate($(this).val());

      console.log(that.model);
    });

  }

  onInputChange(event) {
    //this.model.valueAccessor.writeValue(event.toUpperCase());
  }
}