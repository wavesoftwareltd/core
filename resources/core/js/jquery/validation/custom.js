jQuery.validator.addMethod("phone-validation", function (value, element) {
    return this.optional(element) || value.search(/\b\d{3}[-.]?\d{3}[-.]?\d{4}\b/g) > -1;
}, "Please fill currect phone number");

$.validator.addMethod("valueNotEquals", function (value, element, arg) {
    return arg !== value;
}, "Please select a option");

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};