

declare var numeral:any;
declare var CURRENT_CURRENCY:any;
declare var jQuery: any;
declare var $: any;

export class Helper {

    static getUniqueString() {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
        this.s4() + '-' + this.s4() + this.s4() + this.s4();
    }

    static s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    static shuffleArray(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
      }

      static swap(json){
        var ret = {};
        for(var key in json){
          ret[json[key]] = key;
        }
        return ret;
      }

      static moveArrayElemnts(array,steps){
        var result = [];
        var length = array.length;
        array.forEach(function(element,index) {
            var current = steps + index;
            if(current >= length){
                current = length - current; 
            }
            result[index] = array[current];
        });
        return result;
      }

      /**
       * Reload page after x seconed of non action
       * @param seconds 
       */
      static reloadAfterNonAction(seconds:number){
        var miliSeconds = seconds * 1000;
        var time = new Date().getTime();
        $(document.body).bind("mousemove keypress", function(e) {
            time = new Date().getTime();
        });
   
        function refresh() {
            if(new Date().getTime() - time >= miliSeconds) 
                window.location.reload(true);
            else 
                setTimeout(refresh, 10000);
        }
   
        setTimeout(refresh, 10000);
      }

      static convertToCurrency(number){
        return numeral(number).format('0,0') + " " + CURRENT_CURRENCY.symbol + " " ;
      }

      static getFormData(formSelector) {//serialize data function
        var formArray = jQuery(formSelector).serializeArray();
          var returnArray = {};
          for (var i = 0; i < formArray.length; i++){
            returnArray[formArray[i]['name']] = formArray[i]['value'];
          }
          return returnArray;
        }

        static parseDatetime(value) {
          var a = /^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/.exec(value);
          if (a) {
              return new Date(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]);
          }
          return null;
      }
      

        static convertToArray(object){
          var result = [];
          for(var i in object){
            result.push(object);
          }
          return result;
        }
        
        static convertToArrayOfKeys(object){
          var result = [];
          for(var i in object){
            result.push(i);
          }
          return result;
        }
        
        static convertToMultiArray(object){
          var result = [];
          for(var i in object){
            result.push(i);
          }
          return {
            'keys' : this.convertToArrayOfKeys(object),
            'values' : this.convertToMultiArray(object)
          };
        }
} 