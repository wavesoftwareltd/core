import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { InfoService } from '@resources/core/services/info.service';
import { AuthService } from '@resources/core/services/auth.service';


@Component({
  selector: 'register',
  templateUrl: 'register.template.html',
  styleUrls: ['register.style.scss']
})

export class RegisterComponent implements OnInit {

  public name;
  public email;
  public phone;
  public password;
  public confirmPassword;
  public isAgree;
  protected successCallback = null;
  protected errorCallback = null;
  public isSend = false;
  public isSubmiting = false;
  public hasError = false;
  public errorMessage = 'Error';
  constructor(public info: InfoService, private auth: AuthService) {
  }

  ngOnInit() {
    console.log('register component init');
  }

  register() {
    var that = this;
    if (this.isAgree) {
      this.isSubmiting = true;
      this.auth.register(this.getParams(), (res) => {
        if(res.action == 'error'){
          that.hasError = true;
          that.errorMessage = res.message;
        }
        that.isSend = true;
        this.isSubmiting = false;
        if (this.successCallback) {
          this.successCallback(res);
        }
      }, this.errorCallback);
    }
  }


  getParams() {
    return {
      name: this.name,
      email: this.email,
      password: this.password,
      phone: this.phone,
      is_agree: this.isAgree
    };
  }

  onSubmit(form) {
    console.log(form);
    console.log(form.valid);
  }
}
