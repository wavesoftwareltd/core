import { Component,Input,Output,EventEmitter,OnInit } from '@angular/core';
import { InfoService } from '@resources/core/services/info.service';
import { AuthService } from '@resources/core/services/auth.service';


@Component({
  selector: 'forgot',
  templateUrl:'forgot.template.html',
  styleUrls : ['forgot.style.scss']
})

export class ForgotPasswordComponent implements OnInit {

  @Input() email ; 
  readonly SUCCESS_STATUS = 'success';
  readonly ERROR_STATUS = 'error';
	public responseStatus; 
  public isSubmiting = false;

  constructor(public info : InfoService,protected auth: AuthService){
  }

  ngOnInit(){
      console.log('forgot component init');
  }

   send() {
     var that = this;
     this.isSubmiting = true;
        this.auth.forgotPassword(<string>this.email,()=>{
          that.responseStatus = that.SUCCESS_STATUS;
          this.isSubmiting = false;
          that.successCallback();
        },()=>{
          that.responseStatus = that.ERROR_STATUS;
          this.isSubmiting = false;
          that.errorCallback();
        });
    }

    successCallback(){
      
    }

    errorCallback(){
      
    }
 }
      