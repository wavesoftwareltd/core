import { Component,Input,Output,EventEmitter,OnInit } from '@angular/core';
import { InfoService } from '@resources/core/services/info.service';
import { AuthService } from '@resources/core/services/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'changePassword',
  templateUrl:'changePassword.template.html',
  styleUrls : ['changePassword.style.scss']
})

export class ChangePasswordComponent implements OnInit {
 
  public password;
  public token;
  readonly SUCCESS_STATUS = 'success';
  readonly ERROR_STATUS = 'error';
	public responseStatus; 
  public isSubmiting = false;
  public isChecked = false;
  public confirmPassword;
  public isValid = false;
  public isChanged = false;
  public isWasSend = false;
  public change;
  public onSubmit;

  constructor(
    public info : InfoService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    protected auth: AuthService){
  }

  
  public ngOnInit(): any {
    var that = this;
    console.log('ChangePasswordComponent  init');
    this.activeRoute.params.subscribe(params => {
      if (params['token']) {
        that.token = params['token']; 
        that.auth.verifyPasswordToken(that.token,(res) => {
            that.isChecked = true;
            that.isValid = res.status;
            console.log(res);
        })
        console.log(that.token);
      } 
    });  
  }

   send() {
     var that = this;
     this.isSubmiting = true;
        this.auth.changePassword(<string>this.token,<string>this.password,(res)=>{
          that.responseStatus = that.SUCCESS_STATUS;
          this.isSubmiting = false;
          that.isChanged = res.status;
          that.isWasSend = true;
          that.successCallback();
        },()=>{
          that.responseStatus = that.ERROR_STATUS;
          this.isSubmiting = false;
          that.isWasSend = true;
          that.errorCallback();
        });
    }

    successCallback(){
      this.router.navigate(['/']);
    }

    errorCallback(){
      
    }
 } 
      