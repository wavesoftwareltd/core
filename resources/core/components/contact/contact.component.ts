import { Component,Input,Output,EventEmitter,OnInit,ViewChild } from '@angular/core';
import { InfoService } from '@resources/core/services/info.service';
import { AuthService } from '@resources/core/services/auth.service';
import { ContactService } from '@resources/core/services/contact.service';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'contact',
  templateUrl:'contact.template.html',
  styleUrls : ['contact.style.scss']
})

export class ContactComponent implements OnInit {
 
  public password;
  public token;
  readonly SUCCESS_STATUS = 'success';
  readonly ERROR_STATUS = 'error';
	public responseStatus; 
  public isSubmiting = false;
  public isChecked = false;
  public confirmPassword;
  public isValid = false;
  public isChanged = false;
  public isWasSend = false;
  public change;
  public onSubmit; 
  public isFormTested = false;
  @ViewChild('form') public form: NgForm;

  constructor(
    public info : InfoService,
    protected activeRoute: ActivatedRoute,
    public contactService: ContactService, 
    protected sb: MatSnackBar,
    protected auth: AuthService){
  }

  
  public ngOnInit(): any {
    var that = this;
    console.log('Contact us init'); 
  }

   send() {
     var that = this;
     var that = this;
     if (!this.form.valid) {
       console.log('Not Valid!');
       this.isFormTested = true;
     } else {
       this.isFormTested = false;
       this.isSubmiting = true;
        this.contactService.submit(()=>{
          this.successCallback();
          this.contactService.reset();
        }
        ,()=>{
          this.errorCallback();
          this.contactService.reset();
        });
     }
     
    }

    successCallback(){
      this.sb.open('The message was successfully sent!', 'close', { duration: 3000 });
      
    }

    errorCallback(){
      this.sb.open('There was an error with the request!', 'close', { duration: 3000 });
    }
 } 
      