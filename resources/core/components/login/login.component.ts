import { Component,Input,Output,EventEmitter,OnInit } from '@angular/core';
import { InfoService } from '@resources/core/services/info.service';
import { AuthService } from '@resources/core/services/auth.service';


@Component({
  selector: 'login',
  templateUrl:'login.template.html',
  styleUrls : ['login.style.scss']
})

export class LoginComponent implements OnInit {

  @Input() email ; 
  @Input() password ; 
  public isSubmiting = false;

  constructor(public info : InfoService,protected auth: AuthService){
  }

  ngOnInit(){
      console.log('login component init');
  }

   login() {
     var that = this;
     this.isSubmiting = true;
        this.auth.login(<string>this.email,<string>this.password,()=>{
          that.isSubmiting = false;
        },()=>{
          that.isSubmiting = false;
        });
    }
 }
      