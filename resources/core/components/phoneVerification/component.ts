import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { InfoService } from '@resources/core/services/info.service';
import { AuthService } from '@resources/core/services/auth.service';
import { ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'phone_verification',
  templateUrl: 'template.html'
})

export class PhoneVerificationComponent implements OnInit {

  public isChecked = false;
  public registrationCode;
  public isValid = false;
  public isSubmiting = false;
  public isCheckingRegistrationInfo = true;
  public isWatingForVerification = false;
  public hasError = false;
  public digit1;
  public digit2;
  public digit3;
  public digit4;
  public isVerified = false;
  public isRegistrationNotValid = false;
  public phone;
  protected redirectToPhoneVerificaiton = true;

  constructor(
    public info: InfoService,
    protected auth: AuthService,
    private activeRoute: ActivatedRoute
  ) {
  }

  public ngOnInit(): any {
    var that = this;
    console.log('phone verification init');
    this.skipToNext();
    this.activeRoute.params.subscribe(params => {
      if (params['registration_code']) {
        that.registrationCode = params['registration_code'];  
        this.auth.getRegistationInfo(that.registrationCode,(res)=>{
          if(res.status){
            that.isVerified = true;
          }else if(res.is_expired || !res.is_exist){
            that.isRegistrationNotValid = true;
          }
          that.isCheckingRegistrationInfo = false;
        });
      } 
    }); 
  }

  public skipToNext(){
    $(document).on('keyup',"input[maxlength]",function () {
      if (this.value.length == this.maxLength) {
        var isNext = false;
        var that = this;
        $('input[maxlength]').each(function(index,elem){
          if(that == elem){
            isNext = true;
          }else if(isNext){
            $(elem).focus();
            isNext = false;
          }
        })
      }
  }); 
  }

  public validatePhoneNumber() {
    var that = this;
    this.isSubmiting = true;
    this.auth.verifyPhonenNumber(this.registrationCode,this.phone, (res) => {
      that.isChecked = true;
      that.isSubmiting = false;
      if (res.status) {
        that.isWatingForVerification = true;
        that.skipToNext();
      }else{
        that.hasError = true;
      }
      console.log(res);
    })
  }

  public validatePhoneCode() {
    var that = this;
    var code = this.digit1 +''+this.digit2+''+this.digit3+''+this.digit4;
    this.isSubmiting = true;
    this.auth.verifyPhoneCode(this.registrationCode,this.phone,code, (res) => {
      that.isChecked = true;
      that.isSubmiting = false;
      if (res.access_token) {
         that.isVerified = true;
         that.isWatingForVerification = false;
      }else{
        that.hasError = true;
      }
      console.log(res);
    })
  }


}
