import { Component,Input,Output,EventEmitter,OnInit } from '@angular/core';
import { InfoService } from '@resources/core/services/info.service';
import { AuthService } from '@resources/core/services/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'email_verification',
  templateUrl:'template.html'
})

export class EmailVerificationComponent implements OnInit {

  @Input() token;
  public isChecked = false; 
  public isValid = false; 
  public redirectToPhoneVerificaiton = true;

  constructor(
      public info : InfoService,
      protected auth: AuthService,
    private activeRoute: ActivatedRoute,
    private router: Router
){
  }
 
  public ngOnInit(): any {
    var that = this;
    console.log('email verification init');
    this.activeRoute.params.subscribe(params => {
      if (params['token']) {
        that.token = params['token']; 
        that.auth.verifyEmailToken(that.token,(res) => {
            that.isChecked = true;
                that.isValid = res.status;
                var registration = res.registration ? res.registration : null;
                if(that.isValid && that.redirectToPhoneVerificaiton && registration && (!res.is_phone_verified || 0 == res.is_phone_verified)){
                    that.redirectTiPhoneVerificationPage(registration);
                }else if((res.is_phone_verified && 0 != res.is_phone_verified)){
                  that.router.navigate(['/']);
                }
            console.log(res);
        })
        console.log(that.token);
      } 
    }); 
  }

  public redirectTiPhoneVerificationPage(registration){
    this.router.navigate(['/phone_verification/'+registration]);
  }
  
 
 }
      