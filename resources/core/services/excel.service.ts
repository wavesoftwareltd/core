import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelService {

    constructor() { }

    /**
     * Export Excel file
     * @param json 
     * @param excelFileName 
     */
    public exportAsExcelFile(json: any[], excelFileName: string,isRtl = false ): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
         /* add to workbook */
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'binary' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    /**
     * generate a download
     * @param s 
     */
    public s2ab(s) {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }

    /**
     * Read Excel
     * @param e - event
     * @param successFunction - success callback function 
     */
    public readExcel(e, successFunction) {
        var fileName = e.target.files[0];
        console.log(fileName);
        if (!fileName) {
            return;
        }
        var reader = new FileReader();
        reader.onload = (file: any) => {
            var contents: any = file.target.result;
            var workbook = XLSX.read(contents, { type: 'binary' });

            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });

            successFunction(result);
        };
        reader.readAsBinaryString(fileName);
    }

    /**
     * Save as excel file
     * @param buffer 
     * @param fileName 
     */
    private saveAsExcelFile(buffer: any, fileName: string): void {
        FileSaver.saveAs(new Blob(
            [this.s2ab(buffer)],{
                type:"application/octet-stream"
            })
        , fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
    );

      /*
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
        */
    }

}