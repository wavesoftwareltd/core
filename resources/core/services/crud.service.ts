import { Injectable } from '@angular/core';
import { WebService } from '@resources/core/services/web.service';

declare var TRANSLATE_DATA: any;

@Injectable()
export abstract class CrudService {
 
    protected data;
    protected id;

    constructor(private webService: WebService) { 
    }

    /**
     * Get request path
     */
    abstract getRequestPath() : string;

    /**
     * Load model by id
     * @param id 
     */
    load(id,successCallback = (that) => {},errorCallback = (that) => {}) {
        var that = this;
        this.id = id;
        return this.webService.query(this.getRequestPath(), { 
            id : this.id
         }, response => {
            that.setData(response.result);
            successCallback(that);
        },response => {
            successCallback(that);
        });
    }

    setData(data){
        this.data = data;
    }

    getData(){
        return this.data;
    }
    
  
 
    

}