import { Injectable } from '@angular/core';
import { WebService } from './web.service';

declare var TRANSLATE_DATA: any;

@Injectable()
export class TranslateService {

    private data = [];
    private translateFromServerList = {};
    private translateCache = {};

    constructor(private webService: WebService) {

    }

    getData() {
        if (typeof TRANSLATE_DATA !== 'undefined') {
            return TRANSLATE_DATA;
        } else {
            return {};
        }
    }

    static translate(string: string, domain: string = 'default') {
        var data = {};
        if (typeof TRANSLATE_DATA !== 'undefined') {
            data = TRANSLATE_DATA;
        }

        if (!string) {
            string = '';
        } else {
            string = string.toLowerCase();
        }
        if (typeof data[string] !== 'undefined') {
            if (typeof data[string][domain] !== 'undefined') {
                return data[string][domain];
            } else {
                return data[string][Object.keys(data[string])[0]];
            }
        } else if (typeof data[string] !== 'undefined') {
            return data[string];
        }
        return string;
    }

    /**
     * Translate funciton
     * @param string 
     * @param domain 
     */
    __(string: string, domain: string = 'default') {
        var data = this.getData();
        if (!string) {
            string = '';
        } else {
            string = string.toLowerCase();
        }
        if (typeof data[string] !== 'undefined') {
            if (typeof data[string][domain] !== 'undefined') {
                return data[string][domain];
            } else {
                return data[string][Object.keys(data[string])[0]];
            }
        } else if (typeof this.data[string] !== 'undefined') {
            return this.data[string];
        } else {
            if (this.data.indexOf(string) < 0 && !this.isInTranslatedData(string, domain)) {
                //console.log('translate from server - '+string);
                //console.log(this.data);
                //console.log(TRANSLATE_DATA);
                this.translateFromServer(string);
            }
            return string;
        }
    }

    /**
     * Is in translated data
     * @param string 
     * @param domain 
     */
    isInTranslatedData(string: string, domain: string = 'default'): boolean {
        if(typeof this.translateCache[string + '_' + domain] !== 'undefined'){
            return this.translateCache[string + '_' + domain];
        }
        var data = this.getData();
        if (!string) {
            string = '';
        } else {
            string = string.toLowerCase();
        }
        for (var i in data) {
            let index = i.toLowerCase();
            if (index == string) {
                this.translateCache[string + '_' + domain] =  true;
                return this.translateCache[string + '_' + domain];
            } else {
                for (var x in data[i]) {

                    if (('' + data[i][x]).toLowerCase() == string) {
                        this.translateCache[string + '_' + domain] =  true;
                        return this.translateCache[string + '_' + domain];
                    }
                }
            }
        }
        this.translateCache[string + '_' + domain] =  false;
        return this.translateCache[string + '_' + domain];
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }


    translateFromServer(value) {
        if (typeof this.translateFromServerList[value] === 'undefined') {
            this.translateFromServerList[value] = true;
            return this.webService.query('translate', { text: value }, response => {
                return this.data[value] = response.result;
            });
        }
    }



}