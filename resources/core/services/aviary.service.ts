import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver'; 

declare var jQuery: any;
declare var $: any;
declare var Aviary: any;
declare var C_SDK_KEY: any;

@Injectable()
export class AviaryService {

    csdkImageEditor = null;
    successFunction = (imageID, newURL) => {};
    closeFunction = (isDirty) => {};
    constructor() { 
        var that = this;
        this.csdkImageEditor = new Aviary.Feather({
            apiKey: 'ef560aca390c41d988222a177259414b',
            language:'he',
            fileFormat : 'jpg',
            onSave: function(imageID, newURL){
                //$()currentImage.src = newURL;
                that.csdkImageEditor.close();
                that.successFunction(imageID, newURL);
                console.log(newURL);
            },
            onClose : function(isDirty){
               // that.closeFunction(isDirty);
            },
            onError: function(errorObj) {
                console.log(errorObj.code);
                console.log(errorObj.message);
                console.log(errorObj.args);
                that.closeFunction(true);
            }
        });

    }
 

    /**
     * Read Image
     * @param e - event
     * @param successFunction - success callback function 
     */
    public editImage(imageId,imageSrc, successFunction,closeFunction = this.closeFunction) {
        this.successFunction = successFunction;
        this.closeFunction = closeFunction;
        this.csdkImageEditor.launch({
            image: imageId,
            url: imageSrc
        });
    }


 

}