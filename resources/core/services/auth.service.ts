import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { WebService } from './web.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthService {

    static BASE_URL = 'auth';
    static NAME_KEY = 'name';
    static TOKEN_KEY = 'access_token';
    static COOKIE_KEY = 'user_info';

    protected loginCallback = () => {};

    constructor(private http: WebService, private router: Router,private cookieService: CookieService) {
        this.initFromCookie();
     }
 
     get name() {
        return localStorage.getItem(AuthService.NAME_KEY);
    }
 
     isAuthenticated():boolean {
        return !!localStorage.getItem(AuthService.TOKEN_KEY);
    }

    setEmail(email){
        localStorage.setItem('email',email);
    }
    
    getEmail(){
        localStorage.getItem('email');
    }

    initFromCookie(){
        if(this.cookieService.check(AuthService.COOKIE_KEY)){
            let userData = JSON.parse(this.cookieService.get(AuthService.COOKIE_KEY));
            this.setUserInfo(userData.user_info);
            this.setEmail(userData.user_info.email);
            this.setToken(userData.access_token);
            this.cookieService.delete(AuthService.COOKIE_KEY);
            console.log('init from guid');
        }
    }

    setToken(token){
        localStorage.setItem(AuthService.TOKEN_KEY,token);
    }
    
    setUserInfo(data){
         localStorage.setItem('user_info',JSON.stringify(data));
    }
    
    getUserInfo(){
        JSON.parse(localStorage.getItem('user_info'));
    }

     static getToken():string {
        return localStorage.getItem(AuthService.TOKEN_KEY);
    }
 
    get tokenHeader() {
        var header = new Headers({'Authorization': 'Bearer ' + localStorage.getItem(AuthService.TOKEN_KEY)});
        return new RequestOptions({ headers: header});
    }

    login(email : string,password : string,successCallback = null,errorCallback = null) {
        var that = this;
        this.http.post(AuthService.BASE_URL + '/login', {email : email.trim(),password:password.trim(),grant_type:'client_credentials'},
        res => {
            this.authenticate(res);
            if(successCallback){
                that.setEmail(email);
                that.setUserInfo(res.user_info);
                successCallback(res);
            }
        },errorCallback);
    }
    
    forgotPassword(email : string,successCallback = null,errorCallback = null) {
        this.http.post(AuthService.BASE_URL + '/forgot_password', {email : email},
        res => {
            this.authenticate(res);
            if(successCallback){
                successCallback(res);
            }
        },errorCallback);
    }
    
    updateProfile(params,successCallback = null,errorCallback = null) {
        this.http.post(AuthService.BASE_URL + '/update_profile', params,
        res => {
            this.authenticate(res);
            if(successCallback){
                successCallback(res);
            }
        },errorCallback);
    }

    changePassword(token : string,pass : string,successCallback = null,errorCallback = null) {
        this.http.post(AuthService.BASE_URL + '/change_password', {token:token,password : pass},
        res => {
            this.authenticate(res);
            if(successCallback){
                successCallback(res);
            }
        },errorCallback);
    }

    userChangePassword(currentPassword : string,pass : string,successCallback = null,errorCallback = null) {
        this.http.post(AuthService.BASE_URL + '/change_password', {current_password:currentPassword,password : pass},
        res => {
            this.authenticate(res);
            if(successCallback){
                successCallback(res);
            }
        },errorCallback);
    }

    verifyEmailToken(token : string,successCallback = null,errorCallback = null) {
        this.http.post(AuthService.BASE_URL + '/token', {token : token},
        res => {
            if(successCallback){
                successCallback(res);
            }
        },errorCallback);
    }

    verifyPasswordToken(token : string,successCallback = null,errorCallback = null) {
        this.http.post(AuthService.BASE_URL + '/verify_password_token', {token : token},
        res => {
            if(successCallback){
                successCallback(res);
            }
        },errorCallback);
    }

    verifyLogin(successCallback = null,errorCallback = null) {
        var that = this;
        this.http.post(AuthService.BASE_URL + '/verify_login', {},
        res => {
            if(successCallback){
                successCallback(res);
                that.setUserInfo(res.user_info);
            }
        },errorCallback);
    }
    
    verifyPhonenNumber(registrationCode : string,phoneNumber : string,successCallback = null,errorCallback = null) {
        this.http.post(AuthService.BASE_URL + '/verify_phone_number', {phone : phoneNumber,registration : registrationCode},
        res => {
            if(successCallback){
                successCallback(res);
            }
        },errorCallback);
    }

    getRegistationInfo(registrationCode : string,successCallback = null,errorCallback = null) {
        this.http.post(AuthService.BASE_URL + '/registration_info', {registration : registrationCode},
        res => {
            if(successCallback){
                successCallback(res);
            }
        },errorCallback);
    }

    verifyPhoneCode(registrationCode : string,phoneNumber : string,phoneCode : string,successCallback = null,errorCallback = null) {
        this.http.post(AuthService.BASE_URL + '/verify_phone_code', {
            phone : phoneNumber,
            code : phoneCode,
            grant_type:'client_credentials',
        registration : registrationCode},
        res => {
            this.authenticate(res,false);
            if(successCallback){
                successCallback(res);
            }
        },errorCallback);
    }

    register(user,successCallback = null,errorCallback = null) {
        delete user.confirmPassword;
        this.http.post(AuthService.BASE_URL + '/register', user,res => {
            this.authenticate(res);
            if(successCallback){
                successCallback(res);
            }
        },errorCallback);
    } 

    setLoginCallback(loginCallback){
        this.loginCallback = loginCallback;
    }
 
    logout(loginCallback = null,isReload = true) {
        if(loginCallback){
            this.loginCallback = loginCallback;
        }
        localStorage.clear();
        //localStorage.removeItem(AuthService.NAME_KEY);
        //localStorage.removeItem(AuthService.TOKEN_KEY);
         this.router.navigate(['/login']);
       // document.location = '/#/login';
       if(isReload){
        location.reload();
       }
    }

    authenticate(res,navToHomeAfter = true) {
        var authResponse = res;

        if (!authResponse.access_token){
            return;
        }
        localStorage.setItem(AuthService.TOKEN_KEY, authResponse.access_token);
        localStorage.setItem(AuthService.NAME_KEY, authResponse.firstName);
        this.loginCallback();
        if(navToHomeAfter){
            this.router.navigate(['/']);
        }
    }

}