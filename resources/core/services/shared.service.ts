import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class SharedService {
    // Observable string sources
    private emitChangeSource = new Subject<any>();
    // Observable string streams
    changeEmitted$ = this.emitChangeSource.asObservable();
    // Service message commands
    emitChange(change: any) {
        this.emitChangeSource.next(change);
    }

    /**
     * Listen to shared service event
     * @param namespace 
     * @param callback 
     */
    public listen(namespace : String,callback){
        this.changeEmitted$.subscribe((data) => {
            if(data.namespace == namespace){
                callback(data.result);
            }
        });
    }

    /**
     * Trriger shared service event
     * @param trrigerNamespace 
     * @param data 
     */
    public trigger(trrigerNamespace : String,data){
        this.emitChange({
            namespace : trrigerNamespace, 
            result : data
        });
    }
}