import { Injectable } from '@angular/core';
import { WebService } from './web.service';
import { AuthService } from './auth.service';

@Injectable()
export class UserService {
 
    private data; 
    static USER_INFO_KEY = 'user_info';

    constructor(private webService : WebService) { 
     }

     get name() {
        return localStorage.getItem(AuthService.NAME_KEY);
    }
 
     isAuthenticated():boolean {
        return !!localStorage.getItem(AuthService.TOKEN_KEY);
    }

    setEmail(email){
        localStorage.setItem('email',email);
    }
    
    getEmail(){
       return localStorage.getItem('email');
    }
    
    setUserInfo(data){
         localStorage.setItem(UserService.USER_INFO_KEY,JSON.stringify(data));
    }
    
    getUserInfo(){
       return JSON.parse(localStorage.getItem(UserService.USER_INFO_KEY));
    }

    getUserPermission(){
        return this.getUserInfo().permission;
    }

    isAdministrator(){
        return this.getUserPermission()=='administrator';
    }

}