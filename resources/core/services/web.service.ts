import { Http, Response, Headers, RequestOptions, URLSearchParams,ResponseContentType,RequestMethod } from '@angular/http';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Rx'
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WebService {
    BASE_URL = '/api';

    public showErrorMessages = false;
    public redirectIf401 = true;
    public closeButton = 'close';

    private result = [];
    TOKEN_KEY = 'access_token';

    private messageSubjet = new Subject();


    messages = this.messageSubjet.asObservable();

    constructor(private router: Router, private http: Http, private sb: MatSnackBar) {
    }

    async get(path, successCallback = null, errorCallback = null) {

        var that = this;
        let headers = this.getHeaders();
        let options = new RequestOptions({ headers: headers });
        path = (path) ? '/' + path : '';
        try {
            return this.http.get(this.BASE_URL + path, options).map(result => {
                return result.json();
            }).catch((error) => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
                return Observable.of(false);
            }).subscribe(data => {
                if (successCallback && data) {
                    successCallback(data);
                }
            }, error => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
            });
        } catch (error) {
            if (!errorCallback) {
                this.handleNonHandleErros(error);
            } else {
                errorCallback(error);
                that.handleError(error);
            }
        }
    }

    async query(path, params, successCallback = null, errorCallback = null) {

        var that = this;
        let headers = this.getHeaders();
        let options = new RequestOptions({ headers: headers });
        let urlParams = this.objectToQueryString(params);
        path = (path) ? '/' + path : '';
        try {
            return this.http.get(this.BASE_URL + path + '?' + urlParams, options).map(result => {
                return result.json();
            }).catch((error) => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
                return Observable.of(false);
            }).subscribe(data => {
                if (successCallback && data) {
                    successCallback(data);
                }
            }, error => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
            });
        } catch (error) {
            if (!errorCallback) {
                this.handleNonHandleErros(error);
            } else {
                errorCallback(error);
                that.handleError(error);
            }
        }
    }

    getHeaders() {
        let headersConfig = { 'Content-Type': 'application/x-www-form-urlencoded' };

        if (this.isAuthenticated()) {
            headersConfig['Authorization'] = 'Bearer ' + this.getToken();
        }

        let headers = new Headers(headersConfig);
        return headers;
    }

    getUploadHeaders() {
        let headersConfig = {
        };

        if (this.isAuthenticated()) {
            headersConfig['Authorization'] = 'Bearer ' + this.getToken();
        }

        let headers = new Headers(headersConfig);
        return headers;
    }

    postFormData(path, params, successCallback = null, errorCallback = null) {
        let headers = this.getUploadHeaders();
        let options = new RequestOptions({ headers: headers });
        var that = this;
        path = (path) ? '/' + path : '';
        try {
            return this.http.post(this.BASE_URL + path, params, options).map(result => {
                return result.json();
            }).catch((error) => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
                return Observable.of(false);
            }).subscribe(data => {
                if (successCallback && data) {
                    successCallback(data);
                }
            }, error => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
            });
        } catch (error) {
            if (!errorCallback) {
                this.handleNonHandleErros(error);
            } else {
                errorCallback(error);
                that.handleError(error);
            }
        }

    }

    downloadFile(path,type: string, params,successCallback = () => {},errorCallback = null) {

        var that = this;
        let headers = this.getHeaders();
        let options = new RequestOptions({ headers: headers,responseType: ResponseContentType.Blob });
        let urlParams = this.objectToQueryString(params);
        path = (path) ? '/' + path : '';
        try {
            return this.http.get(this.BASE_URL + path + '?' + urlParams, options).map(res => res.blob()).catch((error) => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
                return Observable.of(false);
            }).subscribe(data => {
                if (data) {
                    successCallback();
                    var blob = new Blob([data], { type: type });
                    var url= window.URL.createObjectURL(blob);
                    window.open(url);
                }
            }, error => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
            });
        } catch (error) {
            if (!errorCallback) {
                this.handleNonHandleErros(error);
            } else {
                errorCallback(error);
                that.handleError(error);
            }
        }
}

    post(path, params, successCallback = null, errorCallback = null) {
        let headers = this.getHeaders();
        let options = new RequestOptions({ headers: headers });
        let urlParams = this.objectToQueryString(params);
        var that = this;
        path = (path) ? '/' + path : '';
        try {
            return this.http.post(this.BASE_URL + path, urlParams, options).map(result => {
                return result.json();
            }).catch((error) => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
                return Observable.of(false);
            }).subscribe(data => {
                if (successCallback && data) {
                    successCallback(data);
                }
            }, error => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
            });
        } catch (error) {
            if (!errorCallback) {
                this.handleNonHandleErros(error);
            } else {
                errorCallback(error);
                that.handleError(error);
            }
        }

    }


    put(path, params, successCallback = null, errorCallback = null) {
        let headers = this.getHeaders();
        let options = new RequestOptions({ headers: headers });
        let urlParams = this.objectToQueryString(params);
        var that = this;

        path = (path) ? '/' + path : '';
        try {
            return this.http.put(this.BASE_URL + path, urlParams, options).map(result => {
                return result.json();
            }).subscribe(data => {
                if (successCallback && data) {
                    successCallback(data);
                }

            }, error => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
            });
        } catch (error) {
            if (!errorCallback) {
                this.handleNonHandleErros(error);
            } else {
                errorCallback(error);
                that.handleError(error);
            }
        }
    }

    delete(path, params, successCallback = null, errorCallback = null) {
        let headers = this.getHeaders();
        let options = new RequestOptions({ headers: headers });
        path = (path) ? '/' + path : '';
        params = { id: params };
        let urlParams = this.objectToQueryString(params);

        var that = this;
        try {
            return this.http.delete(this.BASE_URL + path + '?' + urlParams, options).map(result => {
                return result.json();
            }).subscribe(data => {
                if (successCallback && data) {
                    successCallback(data);
                }

            }, error => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
            });
        } catch (error) {
            if (!errorCallback) {
                this.handleNonHandleErros(error);
            } else {
                errorCallback(error);
                that.handleError(error);
            }
        }
    }

    deletePost(path, params, successCallback = null, errorCallback = null) {
        let headers = this.getHeaders();
        headers.set("Content-Type", 'application/json');
        let options = new RequestOptions({ 
            headers: headers,
            method : 'delete',
            responseType : ResponseContentType.Json
        });
        path = (path) ? '/' + path : '';
        params = { id: params };
        let urlParams = this.objectToQueryString(params);

        var requestoptions = new RequestOptions({
            method: RequestMethod.Delete,
            url: this.BASE_URL + path,
            headers: headers,
            body: JSON.stringify(params)
        })

        var that = this;
        try {
            return this.http.request(this.BASE_URL + path ,requestoptions).map(result => {
                return result.json();
            }).subscribe(data => {
                if (successCallback && data) {
                    successCallback(data);
                }

            }, error => {
                if (!errorCallback) {
                    this.handleNonHandleErros(error);
                } else {
                    errorCallback(error);
                    that.handleError(error);
                }
            });
        } catch (error) {
            if (!errorCallback) {
                this.handleNonHandleErros(error);
            } else {
                errorCallback(error);
                that.handleError(error);
            }
        }
    }

    async postMessage(message) {
        try {
            var response = await this.http.post(this.BASE_URL + '/messages', message).toPromise();
        } catch (error) {
            this.handleError("Unable to post message");
        }

    }

    getUser() {
        //return this.http.get(this.BASE_URL + '/users/me', this.auth.tokenHeader).map(res => res.json());
    }

    saveUser(userData) {
        // return this.http.post(this.BASE_URL + '/users/me', userData,this.auth.tokenHeader).map(res => res.json());
    }

    /**
     * Convert object to query string
     * @param obj 
     * @param keyArray 
     * @return string
     */
    objectToQueryString(obj, keyArray: string = null) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                if (typeof obj[p] === 'object') {
                    let name;
                    if (keyArray) {
                        name = keyArray + '[' + encodeURIComponent(p) + ']';
                    } else {
                        name = encodeURIComponent(p);
                    }

                     var childResult = this.objectToQueryString(obj[p],name);
                    if(childResult){
                        str.push(childResult);
                    }
                } else {
                    let name;
                    let value = encodeURIComponent(obj[p]);
                    let finaleValue;
                    if (keyArray) {
                        name = keyArray + '[' + encodeURIComponent(p) + ']';
                    } else {
                        name = encodeURIComponent(p);
                    }
                    if (typeof (value) === "boolean") {
                        finaleValue = value ? 1 : 0;
                    } else {
                        finaleValue = value;
                    }
                    str.push(name + "=" + finaleValue);
                }
            }
        return str.join("&");
    }

    public handleError(error) {
        try {
            var errorJson = error.json();
        } catch (error) {

        }

        var errorMessage = 'Error with this request';
        if (typeof errorJson !== 'undefined' && typeof errorJson.error !== 'undefined') {
            errorMessage = errorJson.error;
            if (this.showErrorMessages) {
                this.sb.open(errorMessage, this.closeButton, { duration: 3000 });
            }
        }
        // this.sb.open(errorMessage, 'close', { duration: 3000 });

        if (error.status == 401 && this.redirectIf401) {
            localStorage.clear();
            this.router.navigate(['/login']);
        }
    }

    public handleNonHandleErros(error) {
        console.log('handleError              ->');
        console.log(error);
        try {
            var errorJson = error.json();
            console.log('errorJson');
            console.log(errorJson);

        } catch (error) {

        }
    }

    isAuthenticated(): boolean {
        return !!localStorage.getItem(this.TOKEN_KEY);
    }

    getToken(): string {
        return localStorage.getItem(this.TOKEN_KEY);
    }
} 