import { Injectable } from '@angular/core';
import { WebService } from './web.service';

@Injectable()
export class InfoService {
 
    private data;
    protected errorCallbackFunction = () => {
        this.initData();
    };

    constructor(private webService : WebService) {
     this.initData();
     }
 
     initData(){
         var that = this;
        this.webService.get('info',result => {
            that.data = result;
        },result => {
           // that.runErrorCallbackFunction();
            that.webService.handleError(result);
        });
     }

     /**
      * Run error callback function
      */
     runErrorCallbackFunction(){
         this.errorCallbackFunction();
     }

     /**
      * Set error callback function
      * @param function func 
      */
     setErrorCallbackFunction(func){
         this.errorCallbackFunction = func;
     }

     /**
      * Get app name
      * @return string
      */
     getName() {
        return this.data ? this.data.name : '';
    }
 
    /**
     * Get App icon
     * @return string
     */
     getIcon() {
        return this.data ? this.data.icon : '';
    } 
 
    /**
     * Get description
     * @return string
     */
    getDesc() {
        return this.data ? this.data.desc : '';
    }

    /**
     * Get Wide logo path
     * @return string
     */
    getWideLogo() {
       return this.data ? this.data.wide_logo : '';
   }

}