import { Injectable } from '@angular/core';
import { WebService } from './web.service';

declare var jQuery: any;

@Injectable()
export class UploadService {
  
    constructor(private webService : WebService) { 
        console.log('UploadService con ---------------------------')
     }
 
     async uploadFileInput(fileTarget : string,type : string = 'file'){
         var that = this;
         let formData = new FormData();
         console.log(jQuery(fileTarget)[0]);
         formData.append('file', jQuery(fileTarget)[0].files[0]);
         return new Promise(function(resolve, reject) {
            that.webService.postFormData('upload/'+type,formData,result => {
                //console.log(result);
                resolve(result);
             },result => {
                // that.runErrorCallbackFunction();
                 reject(result);
                 that.webService.handleError(result);
             });
          });
       
     }
 

}