import { Injectable } from '@angular/core';
import { WebService } from './web.service';

@Injectable()
export class ContactService {
 
    private data;
    public subject = '';
    public message = '';
    public name = '';
    public email = '';
    public info = {};
    public isSubmiting = false;
 

    constructor(private webService : WebService) {
     
     }
 
     submit(successCallback = (result) => {},errorCallback = (result) => {}){
         var that = this;
         this.isSubmiting = true;
        this.webService.post('contact',{
            name : this.name,
            email : this.email,
            subject : this.subject,
            message : this.message,
            info : this.info
        },result => {
            successCallback(result);
            this.isSubmiting = false;
        },result => {
           errorCallback(result);
           this.isSubmiting = false;
        });
     }
     
     reset(){
        this.subject = ''; 
        this.message = ''; 
        this.name = ''; 
        this.email = ''; 
     }

}