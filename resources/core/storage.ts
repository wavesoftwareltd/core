

declare var numeral:any;
declare var CURRENT_CURRENCY:any;
export class Storage {

    /**
     * Set item on storage
     * @param key 
     * @param val 
     */
    static setItem(key : string,val) {
        let value;
        if(typeof val === 'object'){
            value = JSON.stringify(val);
        }
        localStorage.setItem(key,value);
    }

    /**
     * 
     * @param key Get item from storage
     */
    static getItem(key : string) {
        let value = localStorage.getItem(key);
        return value && JSON.parse(value);
    }

    /**
     * 
     * @param key Get item from storage
     */
    static removeItem(key : string) {
       localStorage.removeItem(key);
    }
 
}