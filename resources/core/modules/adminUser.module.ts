import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import { FormsModule } from '@angular/forms';

import {ValidationModule} from "@resources/core/modules/validation.module";
import {LoginComponent} from "@resources/core/components/login/login.component";
import {ForgotPasswordComponent} from "@resources/core/components/forgot/forgot.component";
import {EmailVerificationComponent} from "@resources/core/components/emailVerification/component";
import {ContactComponent} from "@resources/core/components/contact/contact.component";
import {PhoneVerificationComponent} from "@resources/core/components/phoneVerification/component";
import {RegisterComponent} from "@resources/core/components/register/register.component";
import {ChangePasswordComponent} from "@resources/core/components/changePassword/changePassword.component";
import { TranslateModule } from '@resources/core/modules/translate.module';  


@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    EmailVerificationComponent,
    ContactComponent,
    PhoneVerificationComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent
  ],
  imports: [
    FormsModule,
    ValidationModule, 
    TranslateModule,
    BrowserModule
  ],
  exports: [
    LoginComponent,
    FormsModule, 
    RegisterComponent,
    EmailVerificationComponent,
    ContactComponent,
    PhoneVerificationComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent
  ],
})
export class AdminUserModule {
}
 