import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import { FormsModule } from '@angular/forms';

import {EmailValidatorDirective} from "@resources/core/directives/form/validation/email.directive";
import {PhoneValidatorDirective} from "@resources/core/directives/form/validation/phone.directive";
import {ConfirmFieldValidatorDirective} from "@resources/core/directives/form/validation/confirmField.directive";
import {MinValidatorDirective} from "@resources/core/directives/form/validation/min.directive";


@NgModule({
  declarations: [
    EmailValidatorDirective,
    ConfirmFieldValidatorDirective,
    PhoneValidatorDirective,
    MinValidatorDirective
  ],
  imports: [
    FormsModule, 
    BrowserModule
  ],
  exports: [
    EmailValidatorDirective,
    ConfirmFieldValidatorDirective,
    PhoneValidatorDirective,
    MinValidatorDirective
  ],
})
export class ValidationModule {
}
 