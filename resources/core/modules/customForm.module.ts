import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import { FormsModule } from '@angular/forms';

import {DatetimepickerDirective} from "@resources/core/directives/form/datetimepicker.directive";
import {TickerDirective} from "@resources/core/directives/ticker.directive";


@NgModule({
  declarations: [
    DatetimepickerDirective,
    TickerDirective,
  ],
  imports: [
    FormsModule, 
    BrowserModule
  ],
  exports: [
    TickerDirective,
    DatetimepickerDirective
  ],
})
export class CustomFormModule {
}
 