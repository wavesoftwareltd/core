import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import { FormsModule } from '@angular/forms';

import {TranslatePipe} from "@resources/core/pipes/translate.pipe";
import {TranslateService} from "@resources/core/services/translate.service";


@NgModule({
  declarations: [
    TranslatePipe
  ],
  imports: [
    FormsModule, 
    BrowserModule
  ],
  exports: [
    TranslatePipe
  ],
  providers: [
    TranslateService
  ],
})
export class TranslateModule {
}
 