<?php

namespace Arbel\Listener;

use Zend\Mvc\MvcEvent;
use Zend\Stratigility\Delegate\CallableDelegateDecorator;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;
use Zend\Psr7Bridge\Psr7Response;
use Zend\Psr7Bridge\Psr7ServerRequest;
use Zend\Diactoros\ServerRequest;

class MiddlewareListener
{

    public static function onDispatch(MvcEvent $event)
    {

        
        $application = $event->getApplication();
        $response    = $application->getResponse();
        $sm  = $application->getServiceManager();
            
        if (null !== $event->getResult()) {
            return;
        }

        $routeMatch  = $event->getRouteMatch();
        $middlewares = $routeMatch->getParam('middlewares', false);
        $request     = $event->getRequest();
        $application = $event->getApplication();
        $response    = $application->getResponse();
        $sm  = $application->getServiceManager();
        
               $strategy = $sm->get('ViewJsonStrategy');
            $view     = $sm->get('ViewManager')->getView();
            $strategy->attach($view->getEventManager());
        if(get_class($response) == 'Zend\Console\Response'){
            return;
        }

        $psr7ResponsePrototype = Psr7Response::fromZend($response);
        $psr7RequestPrototype  = Psr7ServerRequest::fromZend($request);


        if (false === $middlewares) {

            $event->getTarget()->getServiceManager()->get('Di')->instanceManager()
                ->setSharedInstance(PsrResponseInterface::class,
                    $psr7ResponsePrototype)
                ->setSharedInstance(ServerRequest::class, $psr7RequestPrototype);

            return;
        }


        foreach ($middlewares as $middleware) {
            $controller            = $event->getTarget()->getServiceManager()->get('Di')->get($middleware);
            if(method_exists($controller, 'modifyViewStrategy')){
                $controller->modifyViewStrategy($sm);
            }
            $middlewareResultArray = $controller->process($psr7RequestPrototype,
                new CallableDelegateDecorator(function($request, $response) {
                return [$request, $response];
            }, $psr7ResponsePrototype));
            $request  = $middlewareResultArray[0];
            $response = $middlewareResultArray[1];
          //  exit('<pre> ----> '.print_r(get_class($response),true).'</pre>');
        }

        $event->getTarget()->getServiceManager()->get('Di')->instanceManager()
            ->setSharedInstance(PsrResponseInterface::class, $response)
            ->setSharedInstance(ServerRequest::class, $request);


        $event->setResponse(Psr7Response::toZend($response));
        $event->setRequest(Psr7ServerRequest::toZend($request));
    }
    
    
     public static function onRender(MvcEvent $event)
    {

        
        $application = $event->getApplication();
        $response    = $application->getResponse();
        $sm  = $application->getServiceManager();
            
//        if (null !== $event->getResult()) {
//            return;
//        }
        
        $routeMatch  = $event->getRouteMatch();
        $middlewares = $routeMatch->getParam('middlewares', false);
        $request     = $event->getRequest();
        $application = $event->getApplication();
        $response    = $application->getResponse();
        $sm  = $application->getServiceManager();
        
               $strategy = $sm->get('ViewJsonStrategy');
            $view     = $sm->get('ViewManager')->getView();
            $strategy->attach($view->getEventManager());
        if(get_class($response) == 'Zend\Console\Response'){
            return;
        }

        $psr7ResponsePrototype = Psr7Response::fromZend($response);
        $psr7RequestPrototype  = Psr7ServerRequest::fromZend($request);


        if (false === $middlewares) {

            $event->getTarget()->getServiceManager()->get('Di')->instanceManager()
                ->setSharedInstance(PsrResponseInterface::class,
                    $psr7ResponsePrototype)
                ->setSharedInstance(ServerRequest::class, $psr7RequestPrototype);

            return;
        }


        foreach ($middlewares as $middleware) {
            $controller            = $event->getTarget()->getServiceManager()->get('Di')->get($middleware);
            if(method_exists($controller, 'render')){
            $middlewareResultArray = $controller->render($psr7RequestPrototype,
                new CallableDelegateDecorator(function($request, $response) {
                return [$request, $response];
            }, $psr7ResponsePrototype),$psr7ResponsePrototype);
            $request  = $middlewareResultArray[0];
            $response = $middlewareResultArray[1];
          //  exit('<pre> ----> '.print_r(get_class($response),true).'</pre>');
            }
        }

        $event->getTarget()->getServiceManager()->get('Di')->instanceManager()
            ->setSharedInstance(PsrResponseInterface::class, $response)
            ->setSharedInstance(ServerRequest::class, $request);


        $event->setResponse(Psr7Response::toZend($response));
        $event->setRequest(Psr7ServerRequest::toZend($request));
    }
    
     
    /**
     * Listen to the "dispatch" event
     *
     * @param  MvcEvent $event
     * @return mixed
     */
    public static function onFinish(MvcEvent $event)
    {

        $routeMatch  = $event->getRouteMatch();
        if(!isset($routeMatch)){
            return;
        }
        $middlewares = $routeMatch->getParam('middlewares', false);
        if (false === $middlewares) {
            return;
        }

        $request     = $event->getRequest();
        $application = $event->getApplication();
        $response    = $application->getResponse();

        $psr7ResponsePrototype = Psr7Response::fromZend($response);
        $psr7RequestPrototype  = Psr7ServerRequest::fromZend($request);

        foreach ($middlewares as $middleware) {
            if (method_exists($middleware, 'after')) {
                $controller            = $event->getTarget()->getServiceManager()->get('Di')->get($middleware);
                $middlewareResultArray = $controller->after($psr7RequestPrototype,
                    new CallableDelegateDecorator(function($request, $response) {
                    return [$request, $response];
                }, $psr7ResponsePrototype), $psr7ResponsePrototype);
                $request  = $middlewareResultArray[0];
                $response = $middlewareResultArray[1];
            }
        }

        $event->getTarget()->getServiceManager()->get('Di')->instanceManager()
            ->setSharedInstance(PsrResponseInterface::class, $response)
            ->setSharedInstance(ServerRequest::class, $request);

        if ($response instanceof PsrResponseInterface) {
            $event->setResponse(Psr7Response::toZend($response));
            $event->setRequest(Psr7ServerRequest::toZend($request));
        }
    }
}