<?php

namespace Arbel\Listener;

use Zend\Mvc\MvcEvent;
use Zend\Stratigility\Delegate\CallableDelegateDecorator;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;
use Zend\Psr7Bridge\Psr7Response;
use Zend\Psr7Bridge\Psr7ServerRequest;
use Zend\Diactoros\ServerRequest;
use Arbel\Acl;

class PermissionListener {

    public static function onDispatch(MvcEvent $event) {


        if (null !== $event->getResult()) {
            return;
        }

        $routeMatch = $event->getRouteMatch();
        $route = $routeMatch->getMatchedRouteName();
        $params = $routeMatch->getParams();
        $controller = $params['controller'] ?? null;
        $action = $params['action'] ?? null;
        $ruleVal = '';
        if(isset($params['controller'])){
            unset($params['controller']);
        }
        if(isset($params['is_use_acl'])){
            unset($params['is_use_acl']);
        } 
        foreach($params as $key => $val){
            if(!empty($ruleVal)){
                $ruleVal .= '/';
            }
            if(!is_array($val)){
                $ruleVal .= $key.'/'.$val;
            }
        }
        
        $module = explode('\\', $controller)[0];
        $userPermission = $event->getTarget()->getServiceManager()->get('User')->getPermission();
        $userIsAdmin = $event->getTarget()->getServiceManager()->get('User')->isAdministrator();
        $userIsExist = $event->getTarget()->getServiceManager()->get('User')->isExist();
        $acl = $event->getTarget()->getServiceManager()->get('Di')->get(Acl::class);
        $isUseAcl = $routeMatch->getParam('is_use_acl', false);
        if (class_exists($controller) && $isUseAcl) {
            if (!$userIsExist && !$acl->isAllowed($userPermission, $controller, $ruleVal)) {
                header('HTTP/1.0 401 Unauthorized');
                exit("You don't have permission to view this page!");
            }
        }
    }

}
