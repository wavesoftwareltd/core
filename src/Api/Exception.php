<?php

namespace Arbel\Api;

class Exception 
{
    protected $message;
    private $string;
    protected $code;
    protected $file;
    protected $line;
    private $trace;
    private $previous;

    /**
     * (PHP 5 &gt;= 5.1.0, PHP 7)<br/>
     * Clone the exception
     * @link http://php.net/manual/en/exception.clone.php
     * @return void No value is returned.
     */
    final private function __clone()
    {

    }

    /**
     * (PHP 5 &gt;= 5.1.0, PHP 7)<br/>
     * Construct the exception
     * @link http://php.net/manual/en/exception.construct.php
     * @param string $message [optional] <p>
     * The Exception message to throw.
     * </p>
     * @param int $code [optional] <p>
     * The Exception code.
     * </p>
     * @param Throwable $previous [optional] <p>
     * The previous exception used for the exception chaining.
     * </p>
     */
    public function __construct(string $message = "")
    {
        $this->message = $message;
    }


    /**
     * (PHP 5 &gt;= 5.1.0, PHP 7)<br/>
     * String representation of the exception
     * @link http://php.net/manual/en/exception.tostring.php
     * @return string the string representation of the exception.
     */
    public function __toString(): string
    {
        return json_encode([$this->message]);
    }
}