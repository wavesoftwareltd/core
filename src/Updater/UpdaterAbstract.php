<?php

namespace Arbel\Updater;

use Arbel\Base\Element;
use Arbel\Log;
use Arbel\Database\DatabaseInterface;
use Zend\Cache\Storage\StorageInterface;
use Arbel\Cache\CacheManager;
use Arbel\ObjectManager;
use Zend\ServiceManager\ServiceManager;

/**
 * Database updater class
 */
abstract class UpdaterAbstract extends Element
{
    const PATH_ID         = 'updater';
    const DEFAULT_VERSION = '0.0.0';
    const FOLDER_PATH     = 'updater';
    const TABLE_NAME     = 'core_updater';

    /**
     * Log object
     * @var Log
     */
    protected $log;

    /**
     * Database object
     * @var Database
     */
    protected $database;

    /**
     * Cache object
     * @var StorageInterface
     */
    protected $cache;

    /**
     * ObjectManager object
     * @var ObjectManager
     */
    protected $objectManager;

    public function __construct(Log $log, DatabaseInterface $database,
                                CacheManager $cache,
                                ObjectManager $objectManager, ServiceManager $sm)
    {
        parent::__construct();
        $this->log           = $log;
        $this->database      = $database;
        $this->cache         = $cache;
        $this->objectManager = $objectManager;
        $this->serviceManager = $sm;
    }

    
    /**
     * Get object manager
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }
    
    /**
     * Run the updater
     */
    function run()
    {

        $db = $this->getDatabase();
        $updter = &$this;
        if ($this->isNewerUpdaterExist()) {
            $list = $this->getFullVersionsList();
            foreach ($list as $version => $versionData) {
                $dbVersion = $this->getCurrentDbUpdaterVersion();
                if (version_compare($dbVersion, $version) < 0 &&
                    version_compare($version,
                        $this->getCurrentSettingUpdaterVersion()) < 1) {

                    $filePath = $this->getVersionFolderPath().'/'.$versionData['file_name'];
                    if (is_readable($filePath)) {

                        $this->log->info(" ---- Attempting to update to  $version ");
                        try {
                            require $filePath;
                        } catch (Exception $e) {
                            $this->log->err(" ---- Updater $version failed to run successfully ");
                            throw($e);
                        }


                        $this->setCurrentDbUpdaterVersion($version);
                        $this->log->info(" ---- Successfully updated to  $version ");
                    } else {
                        $this->log->err(" ---- Updater file for $version  is not readable, scoping updater");
                        throw new Exception("Unable to read updater file for $version");
                    }
                    $this->cache->get('database')->delete();
                    $this->getServiceManager()->get('Cache')->flush();
                }
            }
        }
    }

    /**
     * Return the database object
     * @return \Database object
     */
    abstract function getDatabase();

    function setCurrentDbUpdaterVersion($version)
    {
        $result = $this->getDatabase()->exec("select count(*) as counter from ".$this::TABLE_NAME." where package_name = :package_name",
            array(
            'package_name' => $this->getPackageName()
        ));

        if (!empty($result) && isset($result[key($result)]['counter']) && $result[key($result)]['counter']
            > 0) {
            $result = $this->getDatabase()->exec(" UPDATE ".$this::TABLE_NAME." SET version = :version where package_name = :package_name",
                array(
                'version' => $version,
                'package_name' => $this->getPackageName()
                )
            );
        } else {
            $result = $this->getDatabase()->exec("INSERT INTO ".$this::TABLE_NAME." (version,package_name) values ( :version,:package_name )  ",
                array(
                'version' => $version,
                'package_name' => $this->getPackageName()
                )
            );
        }
    }

    /**
     * Get package name
     * @return string
     */
    abstract function getPackageName(): string;

    function isNewerUpdaterExist()
    {
        $currentSetting = $this->getCurrentSettingUpdaterVersion();
        $currentDb      = $this->getCurrentDbUpdaterVersion();
        if (version_compare($currentDb, $currentSetting) < 0) {
            return true;
        } else {
            return false;
        }
    }

    function isUpdaterTableExist()
    {
        if (!$this->get('is_updater_table_exist')) {
            $testResult = $this->getDatabase()->exec("select *
                    FROM information_schema.tables
                    WHERE table_schema = :db_name
                    AND table_name = '".$this::TABLE_NAME."' ",
                array(
                'db_name' => $this->getDatabase()->getDatabaseName()
            ),1);

            if (!empty($testResult)) {
                $this->set('is_updater_table_exist', true);
            }
        }
        return $this->get('is_updater_table_exist');
    }

    function getCurrentDbUpdaterVersion()
    {

        if (!$this->isUpdaterTableExist()) {
            return $this::DEFAULT_VERSION;
        }

        $result = $this->getDatabase()->exec("SELECT * FROM ".$this::TABLE_NAME." where package_name = :package_name ",
            array(
            'package_name' => $this->getPackageName()
        ),1);
        if (empty($result)) {
            return $this::DEFAULT_VERSION;
        }

        return $result[key($result)]['version'];
    }

    /**
     * Get the current updater version
     */
    abstract function getCurrentSettingUpdaterVersion();

    function getFullVersionsList()
    {
        $folderPath = $this->getVersionFolderPath();
        $result     = array();
        if (file_exists($folderPath)) {
            $folderContent = scandir($folderPath);
            foreach ($folderContent as $fileName) {
                if ($fileName == '.' || $fileName == '..') {
                    continue;
                }
                $versionFileName = str_replace('.php', '', $fileName);
                if (strpos($versionFileName, '-') !== false) {
                    $versionFileNameArray = explode('-', $versionFileName);
                    $version              = $versionFileNameArray[1];
                } else {
                    $version = $versionFileName;
                }

                $result[$version] = array(
                    'version' => $version,
                    'file_name' => $fileName
                );
            }
        }

        return $result;
    }

    /**
     * Return the version folder path
     * @return string full path
     */
    abstract function getVersionFolderPath();


    /**
     * Get object manager
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * Alias name for get object manager
     * @param string $modelName
     * @return type
     */
    public function getModel(string $modelName)
    {
        return $this->getObjectManager()->get($modelName);
    }
}