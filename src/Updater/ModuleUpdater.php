<?php

namespace Arbel\Updater;

/**
 * Database updater class
 * 
 */
class ModuleUpdater extends UpdaterAbstract
{
    /**
     * Updater version function name
     * @var string
     */
    const UPDATER_VERSION_FUNCTION = 'getUpdaterVersion';

    /**
     * Updater version function name
     * @var string
     */
    const UPDATER_FOLDER_FUNCTION = 'getUpdaterFolder';

    /**
     * Updater package function name
     * @var string
     */
    const UPDATER_PACKAGE_FUNCTION = 'getPackageName';

    /**
     * Current module to update
     * @var Module
     */
    protected $module;

    /**
     * Load module
     * @param Module $module
     */
    public function loadModule($module)
    {
        $this->module = $module;
    }

    /**
     * Is updater exist
     * @return bool
     */
    public function isUpdaterExist()
    {
        return $this->getVersionFromModule() != null;
    }

    /**
     * Load module
     * @param Module $module
     */
    public function getVersionFromModule()
    {
        $config  = $this->module->getConfig();
        $version = $config['updater'] ?? null;
        if (method_exists($this->module, $this::UPDATER_VERSION_FUNCTION)) {
            $version = $this->module->getUpdaterVersion();
        }
        if (isset($version) && $this->validVersion($version)) {
            return $version;
        } else {
            return null;
        }
    }

    public function validVersion(string $version)
    {
        if (!(version_compare($version, '0.0.1', '>=') >= 0)) {
            throw new \Exception('The version '.$version.' is not valid');
        }
        return true;
    }

    public function isNeedToUpdate()
    {
        return $this->isNewerUpdaterExist();
    }

    /**
     * Get data base object
     * @return Database
     */
    function getDatabase()
    {
        return $this->database;
    }

    /**
     * Get version folder path
     * @return string
     */
    public function getVersionFolderPath()
    {
        if (method_exists($this->module, $this::UPDATER_FOLDER_FUNCTION)) {
            $path = $this->module->getUpdaterFolder();
        } else {
            $reflector = new \ReflectionClass($this->module);
            $path      = dirname(dirname($reflector->getFileName())).DIRECTORY_SEPARATOR.'updater';
        }

        return $path;
    }

    /**
     * Get current code version
     * @return string
     */
    public function getCurrentSettingUpdaterVersion()
    {
        return $this->getVersionFromModule();
    }

    /**
     * Get package name
     * @return string
     */
    public function getPackageName(): string
    {
        if (method_exists($this->module, $this::UPDATER_PACKAGE_FUNCTION)) {
            $packageName = $this->module->getPackageName();
        } else {
            $class     = get_class($this->module);
            $namespace = substr($class, 0, strrpos($class, '\\'));
            $pieces    = preg_split('/(?=[A-Z])/', $namespace);
            if (empty($pieces[0])) {
                unset($pieces[0]);
            }
            $packageName = str_replace("\\", '_',
                strtolower(implode("_", $pieces)));
        }
        return $packageName;
    }

    /**
     * Get table name with prefix
     * @param type $name
     * @return type
     */
    public function getTableName($name)
    {
        $prefix = $this->module->getTablePrefix();
        return $prefix.'_'.$name;
    }
}