<?php

namespace Arbel\Base;

/**
 * Base class that handle json string as advanced object
 * @version 0.0.1
 */
class JsonObject extends Element
{
    protected $json_object_parent      = null;
    protected $json_object_parent_name = null;
    protected $object_db_parent = null;

    public function __construct($data = null,JsonObject &$parent = null, $name = null,ModelAbstract &$objectDbParent = null)
    {
        $data = $this->recursiveJsonDecode($data);

        if ($parent) {
            $this->setJsonObjectParent($parent);
        }
        if ($name) {
            $this->setJsonObjectName($name);
        }
        if ($objectDbParent) {
            $this->setObjectDbParent($objectDbParent);
        }
        
        if(is_array($data)){
        parent::__construct($data);
        }
    }

    /**
     * Get data by key
     * @param string $key
     * @param mixed $default
     * @return \Arbel\Base\JsonObject || string - result of the data
     */
    public function get(string $key, $default = null)
    {
        $parentResult = parent::get($key, $default);
        if (is_string($parentResult) && !is_numeric($parentResult) && !is_null(json_decode($parentResult))) {
            $parentResult = json_decode($parentResult, true);
        }
        if (is_array($parentResult)) {
            $childObject = new JsonObject($parentResult, $this, $key);
            return $childObject;
        } else {
            return $parentResult;
        }
    }

    /**
     * Recursive json decode when element is array with json values
     * @param mixed $element
     * @return mixed
     */
    public function recursiveJsonDecode($element)
    {
        if (is_string($element) && !is_numeric($element) && !is_null(json_decode($element))) {
            return json_decode($element, true);
        } elseif (is_array($element) && !empty($element)) {
            foreach ($element as $key => $val) {
                $element[$key] = $this->recursiveJsonDecode($val);
            }
        }
        return $element;
    }

    /**
     * Set data by key and value
     * @param string $key - key
     * @param mixed $val - value
     */
    public function set(string $key, $val)
    {
        if (is_string($val) && !is_numeric($val) && !is_null(json_decode($val))) {
            $val = json_decode($val, true);
        }
        parent::set($key, $val);
        if (isset($this->json_object_parent)) {
            $this->json_object_parent->setArrayData($this->json_object_parent_name,
                $key, $val);
        }
        if($this->hasObjectDbParent()){
           $this->getObjectDbParent()->set($this->json_object_parent_name,$this);
        }
    }

    /**
     * Save object db parent
     */
    public function save()
    {
        if($this->hasObjectDbParent()){
           $this->getObjectDbParent()->set($this->json_object_parent_name,$this);
           $this->getObjectDbParent()->save();
        }
    }

    /**
     * Set the object parent (for update)
     * @param JsonObject $parent
     */
    public function setObjectDbParent(ModelAbstract &$parent)
    {
        $this->object_db_parent = $parent;
    }

    /**
     * Set the object parent (for update)
     * @param JsonObject $parent
     */
    public function setJsonObjectParent(JsonObject &$parent)
    {
        $this->json_object_parent = $parent;
    }

    /**
     * Set the json as
     * @param string $name
     */
    public function setJsonObjectName(string &$name)
    {
        $this->json_object_parent_name = $name;
    }

    /**
     * Convert to json
     * @return string - json
     */
    public function toJson()
    {

        return json_encode($this->getData(),
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    /**
     * Convert to String
     * @return string
     */
    public function __toString()
    {
        return $this->toJson();
    }
}