<?php

namespace Arbel\Base\Sql;

class Select extends \Zend\Db\Sql\Select{
    use RunQuery;

    public function orWhere($predicate)
    {
       return parent::where($predicate, \Zend\Db\Sql\Predicate\PredicateSet::OP_OR);
    }

}
