<?php

namespace Arbel\Base\Sql;

use Arbel\Base\ModelAbstract;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\AbstractPreparableSql;

/**
 * Can run Zend Queries ( Query builder )
 */
trait RunQuery
{

    protected $sqlHandler;
    protected $sql;
    protected $className;

    /**
     * Execute the query that was built
     * @return Zend Sql object
     */
    public function run()
    {
        $statement = $this->sqlHandler->prepareStatementForSqlObject($this);
        return $statement->execute();
    }

    /**
     * @param Sql $sqlHandler
     * @see Zend Query Builder documentation
     */
    public function setHandler(Sql $sqlHandler)
    {
        $this->sqlHandler = $sqlHandler;
    }

    /**
     * @return string get the raw query that was built
     */
    public function __toString(): string
    {
        return $this->sqlHandler->buildSqlString($this);
    }

    /**
     * @return \Arbel\Base\ModelAbstract the first
     */
    public function first()
    {
        $queryResult = $this->run()->current();
        $className   = $this->className;
        return new $className($queryResult);
    }

    /**
     * @return array of objects \Arbel\Base\ModelAbstract
     */
    public function all()
    {
        $queryResult = $this->run();
        $result      = array();
        $className   = $this->className;
        foreach ($queryResult as $row) {
            $result[] = new $className($row);
        }
        return $result;
    }

    /**
     * convert Zend Sql object to array
     * @return array of data colname => value
     */
    public function fetchAll()
    {
        $result      = array();
        $queryResult = $this->run();
        foreach ($queryResult as $key => $value) {
            $result[$key] = $value;
        }
        return $result;
    }

    /**
     * @param string $className  the class of type \Arbel\Base\ObjectDB
     *                           to be used for queries.
     * @return $this
     */
    public function setClassName($className)
    {
        $this->className = $className;
        return $this;
    }
}