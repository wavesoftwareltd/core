<?php

namespace Arbel\Base\Sql;

use Zend\Db\Sql\Sql;

/**
 * Override Zend Sql and add run function
 */
class SqlHandler extends Sql
{

    /**
     *
     * @param type $table
     * @return \Arbel\Base\Sql\Select
     * @throws Exception\InvalidArgumentException
     */
    public function select($table = null)
    {
        if ($this->table !== null && $table !== null) {
            throw new Exception\InvalidArgumentException(sprintf(
                'This Sql object is intended to work with only the table "%s" provided at construction time.',
                $this->table
            ));
        }
        $select = new Select(($table) ?: $this->table);
        $select->setHandler($this);
        return $select;
    }

    /**
     *
     * @param type $table
     * @return \Arbel\Base\Sql\Insert
     * @throws Exception\InvalidArgumentException
     */
    public function insert($table = null)
    {
        if ($this->table !== null && $table !== null) {
            throw new Exception\InvalidArgumentException(sprintf(
                'This Sql object is intended to work with only the table "%s" provided at construction time.',
                $this->table
            ));
        }
        $insert = new Insert(($table) ?: $this->table);
        $insert->setHandler($this);
        return $insert;
    }

    /**
     *
     * @param type $table
     * @return \Arbel\Base\Sql\Update
     * @throws Exception\InvalidArgumentException
     */
    public function update($table = null)
    {
        if ($this->table !== null && $table !== null) {
            throw new Exception\InvalidArgumentException(sprintf(
                'This Sql object is intended to work with only the table "%s" provided at construction time.',
                $this->table
            ));
        }
        $update = new Update(($table) ?: $this->table);
        $update->setHandler($this);
        return $update;
    }

    /**
     *
     * @param type $table
     * @return \Arbel\Base\Sql\Delete
     * @throws Exception\InvalidArgumentException
     */
    public function delete($table = null)
    {
        if ($this->table !== null && $table !== null) {
            throw new Exception\InvalidArgumentException(sprintf(
                'This Sql object is intended to work with only the table "%s" provided at construction time.',
                $this->table
            ));
        }
        $delete = new Delete(($table) ?: $this->table);
        $delete->setHandler($this);
        return $delete;
    }
}