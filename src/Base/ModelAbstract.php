<?php

namespace Arbel\Base;

use Arbel\Database\DatabaseInterface;
use Arbel\ObjectManager;
use Zend\ServiceManager\ServiceManager;
use Arbel\Database\Where;
use Arbel\Model\Translator;
use Arbel\Config;
use Arbel\Log;
use Arbel\Model\Log as DbLog;
use Arbel\Model\Audit;

/**
 * General Object for db store data use
 * 
 */
abstract class ModelAbstract extends Element
{
    /**
     * Json to object fields
     * @var array
     */
    protected $jsonToObjectFields = array();

    /**
     * Json to object fields
     * @var array
     */
    protected $oneObjectReleationFields = array();

    /**
     * Check if object was inserted
     * @var boolean
     */
    protected $isNew = false;

    /**
     * Option column name
     * @var string|null
     */
    protected $optionColumn = null;

    /**
     * Init relation lazy loading list
     * @var array
     */
    protected $lazyLoadinRelationList = array();

    /**
     * Existing columns from table
     * @var array
     */
    protected $existingColumns = array();

    /**
     * Existing columns from table
     * @var array
     */
    protected $blacklistColumns = array('created_at', 'updated_at');

    /**
     * Origin data from db
     * @var array|null
     */
    protected $originData = null;

    /**
     * Unique table column
     * @var type
     */
    protected $uniqueTableColumn = array();

    /**
     * Private columns
     * @var array
     */
    protected $privateColumns = array('password', 'pass', 'objectManager','guid');

    /**
     * Public columns
     * @var array
     */
    protected $publicColumns = array();

    /**
     * Flag if to allow update
     * @var bool
     */
    protected $allowUpdate = true;

    /**
     * Is Auto Audit Mode
     * @var bool
     */
    protected $isAutoAuditMode = false;

    /**
     * Hold the request
     * @var array
     */
    protected $getAllWithRelationsData = array(
        'where_count' => array(),
        'where' => array(),
        'relations' => array()
    );

    /**
     * boolean fields
     * @var array
     */
    protected $booleanFields = [];

    /**
     * Database object
     * @var Database
     */
    protected $database;

    /**
     * ServiceManager object
     * @var ServiceManager
     */
    protected $serviceManager;
    protected $addRelationsOnSave = array();

    /**
     * Init the object
     * @param array/int $data
     * @param string $session - session name
     */
    public function __construct(DatabaseInterface $database,
                                ObjectManager $objectManager, ServiceManager $sm)
    {
        $this->database       = $database;
        $this->objectManager  = $objectManager;
        $this->serviceManager = $sm;
        $this->initConfig();
    }

    /**
     * Translate helper function
     * @param string $key
     * @param string $lang
     * @return type
     */
    public function __(string $key, string $lang = null)
    {
        return $this->getModel(Translator::class)->__($key, $lang);
    }

    /**
     * Config relation function
     */
    protected function initConfig()
    {

    }

    /**
     * Config helper
     */
    protected function getConfig()
    {
        return $this->getModel(Config::class);
    }

    /**
     * Get log helper function
     * @return Log
     */
    protected function log(): Log
    {
        return $this->getModel(Log::class);
    }

    /**
     * Get log helper function
     * @return Log
     */
    protected function dbLog(): DbLog
    {
        return $this->getModel(DbLog::class);
    }

    /**
     * Select fields to save on serialize
     * @return type
     */
    public function __sleep()
    {
        return array('data', 'originData', 'existingColumns', 'lazyLoadinRelationList',
            'oneObjectReleationFields', 'jsonToObjectFields',
        );
    }

    public function __wakeup()
    {
        // $this->database = $this->objectManager->get('Arbel\Database');
    }

    /**
     * Return lazy loading relation array
     * @param $key - wanted relation to retrieve
     * @return array|null
     */
    public function getLazyLoadinRelationList($key = null)
    {
        if (isset($key)) {
            return $this->lazyLoadinRelationList[$key] ?? null;
        }
        return $this->lazyLoadinRelationList;
    }

    /**
     * Get public data
     * @return array
     */
    public function getPublicData(): array
    {
        $data   = $this->getData();
        $result = array();
        foreach ($data as $key => $val) {
            if (!empty($this->publicColumns) && in_array($key,
                    $this->publicColumns)) {
                $result[$key] = $val;
            } elseif (empty($this->publicColumns) && !in_array($key,
                    $this->privateColumns)) {
                $result[$key] = $val;
            }
            if (isset($result[$key]) && is_object($result[$key])) {
                if (is_a($result[$key], ModelAbstract::class)) {
                    $result[$key] = $result[$key]->getPublicData();
                }elseif (is_a($result[$key], JsonObject::class)) {
                    $result[$key] = $result[$key]->getData();
                } else {
                    unset($result[$key]);
                }
            }
        }
        return $result;
    }

    /**
     * Get public data with relations ids
     * @return array
     */
    public function getPublicDataWithRelationsIds()
    {
        $data = $this->getPublicData();
        foreach ($this->getAllRelationsList() as $key => $params) {
            $data[$key]    = [];
            $relationsData = $this->get($key);
            foreach ($relationsData as $relationObject) {
                if (is_object($relationObject) && is_a($relationObject, ModelAbstract::class)) {
                    $data[$key][] = $relationObject->getId();
                }
            }
        }
        return $data;
    }

    /**
     * Check if object is exist
     * @return bool
     */
    public function isExist(): bool
    {
        return (bool) $this->getId();
    }

    /**
     * Return all relations list
     * @return array|null
     */
    public function getAllRelationsList()
    {
        return array_merge($this->oneObjectReleationFields,
            $this->lazyLoadinRelationList);
    }

    /**
     * Return one object relation fields
     * @param $key - wanted relation to retrieve
     * @return array|null
     */
    public function getOneObjectReleationFields($key = null)
    {
        if (isset($key)) {
            return $this->oneObjectReleationFields[$key] ?? null;
        }
        return $this->oneObjectReleationFields;
    }

    /**
     * Add json to object field flag
     * @param string $fieldName
     */
    public function addJsonField(string $fieldName)
    {
        $this->jsonToObjectFields[] = $fieldName;
    }

    /**
     * Generate code value from other field
     * @param type $sourceField
     * @param type $targetField
     */
    public function generateCode($sourceField = 'name', $targetField = 'code')
    {
        $key = strtolower(str_replace(" ", "_", $this->get($sourceField)));
        $this->set($targetField, $key);
    }

    /**
     * Generate name value from other field
     * @param type $sourceField
     * @param type $targetField
     */
    public function generateName($sourceField = 'code', $targetField = 'name')
    {
        $key = ucfirst(str_replace("_", " ", $this->get($sourceField)));
        $this->set($targetField, $key);
    }

    /**
     * Add relation to other tables
     * @param string $key - the key on the data array
     * @param string | array $columnName - the column name
     * @param string $className - the full class name
     * @param string $currentColumnName - (optinal) the current object column name
     * @param array $whereParams - (optinal) more where params for targeted orm
     */
    public function hasOne(string $key, $columnName, string $className,
                           string $currentColumnName = null,
                           array $whereParams = array())
    {
        if (is_array($columnName)) {
            $currentColumnName = key($columnName);
            $columnName        = current($columnName);
        }
        $this->oneObjectReleationFields[$key] = array(
            'column_name' => $columnName,
            'class_name' => $className,
            'current_column_name' => $currentColumnName,
            'where_params' => $whereParams,
        );
    }

    /**
     * Get db object
     * @return Database 
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * Load Object From Database By Id
     * @param int/string $id
     */
    public function load($id)
    {
        return $this->loadByKey($this->getPrimaryKey(), $id);
    }

    /**
     * Load Object From Database By Id
     * @param int/string $id
     */
    public function loadByCodeId($id)
    {
         $id = \Arbel\Utils\Shorten::toNumber($id);
        return $this->loadByKey($this->getPrimaryKey(), $id);
    }

    public function convertJsonToObjectFields($data)
    {
        foreach ($this->jsonToObjectFields as $fieldName) {
            if (array_key_exists($fieldName, $data)) {
                $data[$fieldName] = new JsonObject($data[$fieldName]);
            }
        }
        return $data;
    }

    public function convertParamToObject($key, &$value)
    {
        if (in_array($key, $this->jsonToObjectFields) && !is_object($value)) {
            $value = new JsonObject($value);
        }
    }

    public function convertJsonToObject(string $json)
    {
        return new JsonObject($json);
    }

    /**
     * Duplicate object and remove primary key
     * @param array $newData
     * @return $object
     */
    public function duplicate(array $newData = array())
    {
        $data = $this->getFilterdData();
        if (isset($data[$this->getPrimaryKey()])) {
            unset($data[$this->getPrimaryKey()]);
        }
        $object = clone $this;
        $object->setData($data);
        $object->updateData($newData);
        return $object;
    }

    /**
     * Get value by key
     * @param string $key - key
     * @param mixed $default - default value that return when not exist
     * @return \Arbel\Base\JsonObject or array
     */
    public function get(string $key, $default = null)
    {
        $this->runInitRelationListByKey($key);
        $this->runInitHasOneByKey($key);
        $result = parent::get($key, $default);
        if (in_array($key, $this->jsonToObjectFields) && !is_object($result)) {
            $result = new JsonObject($result);
            $this->set($key, $result);
        }
        return $result;
    }

    /**
     * Convert json object to json fields
     * @param array $data
     * @return array
     */
    public function convertObjectToJsonFields(array &$data): array
    {
        foreach ($this->jsonToObjectFields as $fieldName) {
            if (isset($data[$fieldName]) && is_object($data[$fieldName])) {
                $data[$fieldName] = (string) $data[$fieldName];
            }
        }
        return $data;
    }

    /**
     * Get date/time format of existing date/time value
     * @param string $key - targeted key of data
     * @param string $targetFormat - the target format - if null return timestamp
     * @return type
     */
    public function format(string $key, string $targetFormat = null)
    {
        $val = $this->get($key);
        if (is_numeric($val)) {
            $dateTime = new DateTime();
            $dateTime->setTimestamp($val);
        } elseif (!empty($val)) {
            if (strpos($val, ' ') !== false && strpos($val, '-') !== false && strpos($val, '.') !== false) {
                $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s.u', $val);
            } elseif (strpos($val, ' ') !== false) {
                $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $val);
            } elseif (strpos($val, '-') !== false) {
                $dateTime = \DateTime::createFromFormat('Y-m-d', $val);
            } else {
                $dateTime = \DateTime::createFromFormat('H:i:s', $val);
            }
        } else {
            return null;
        }
        if ($targetFormat) {
            return $dateTime->format($targetFormat);
        } else {
            return $dateTime->getTimestamp();
        }
    }

    /**
     * Get date/time object of existing date/time value
     * @param string $key - targeted key of data
     * @param string $targetFormat - the target format - if null return timestamp
     * @return type
     */
    public function datetime(string $key)
    {
        $val = $this->get($key);
        if (is_numeric($val)) {
            $dateTime = new \DateTime();
            $dateTime->setTimestamp($val);
        } elseif (!empty($val)) {
            if (strpos($val, ' ') !== false) {
                $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $val);
            } elseif (strpos($val, '-') !== false) {
                $dateTime = \DateTime::createFromFormat('Y-m-d', $val);
            } else {
                $dateTime = \DateTime::createFromFormat('H:i:s', $val);
            }
        } else {
            return null;
        }
        return $dateTime;
    }

    /**
     * Return diff datetime from value
     * @param string $key
     * @param \DateTime $target
     * @return type
     */
    public function diff(string $key, \DateTime $target = null)
    {
        if (!isset($target)) {
            $target = new \DateTime();
        }
        $current = $this->datetime($key);
        return $current ? $current->diff($target) : null;
    }

    /**
     * Load Object From Database By Id
     * @param string $key
     * @param string $val
     */
    public function loadByKey(string $key, string $val)
    {
        $db   = $this->getDatabase();
        $data = $db->load($this->getTableName(), $key, $val);
        if (!empty($data)) {
            $data = $data[key($data)];
        }
        $this->getColumnsFromTable($this->getTableName());
        $this->initExistingColumns();
        $this->convertJsonToObjectFields($data);
        $this->originData = $data;
        $this->setData($data);
        if ($this->needToSaveInSession()) {
            $this->updateSession();
        }
        $this->init();

        return $this;
    }

    /**
     * Load Object From Database By Id
     * @param array $data
     */
    public function loadBy(array $data)
    {
        $db               = $this->getDatabase();
        $data             = $db->load($this->getTableName(), $data);
//        if (!empty($data) && is_numeric($data[key($data)])) {
//            $data = $data[key($data)];
//        }
        $this->getColumnsFromTable($this->getTableName());
        $this->initExistingColumns();
        $this->convertJsonToObjectFields($data);
        $this->originData = $data;
        $this->setData($data);
        if ($this->needToSaveInSession()) {
            $this->updateSession();
        }
        $this->init();

        return $this;
    }

    /**
     * Init object by Key
     * @param string $key
     * @param string $val
     * @return \Arbel\Base\className
     */
    public function initByKey(string $key, string $val)
    {
        $className = get_class($this);
        $object    = $this->getModel($className);
        $object->loadByKey($key, $val);
        return $object;
    }

    /**
     * Init object by Params
     * @param array $params
     * @return \Arbel\Base\className
     */
    public function initBy(array $params)
    {
        $className = get_class($this);
        $object    = $this->getModel($className);
        $object->loadBy($params);
        return $object;
    }

    /**
     * Init object by Id
     * @param string $id
     * @return ModelAbstract
     */
    public function initById(string $id)
    {
        $className = get_class($this);
        return $this->getModel($className)->initByKey($this->getModel($className)->getPrimaryKey(),
                $id);
    }

    /**
     * Init object by code Id
     * @param string $id
     * @return ModelAbstract
     */
    public function initByCodeId(string $id)
    {
        $id = \Arbel\Utils\Shorten::toNumber($id);
        return $this->initById($id);
    }

    /**
     * Check if the data was change from the last loading
     * @return boolean
     */
    public function hasChanged()
    {

        $originData = $this->originData;
        if (isset($originData)) {
            $this->removeBlacklistColumns($originData);
        }
        $currentData = $this->getFilterdData();
        return ($originData != $currentData);
    }

    /**
     * Init existing columns 
     * @param array $data - data that load from db
     */
    public function initExistingColumns(array $data = null)
    {
        if (!empty($this->existingColumns)) {
            return;
        }
        if (isset($data)) {
            $this->existingColumns = array_keys($data);
        } else {
            $this->existingColumns = $this->getColumnsFromTable($this->getTableName());
        }
    }
    
    
    public function getColumnsFromTable($tableName){
        $key = $tableName.'_table_columns';
            $cache = $this->getServiceManager()->get('Cache');
            $result = $cache->getItem($key, $success);

            if($success){
                $result = json_decode($result,true);
            }else{
             $result = $this->getDatabase()
                ->getColumnsFromTable($tableName);
             $cache->setItem($key,json_encode($result));
            }
            
            return $result;
    }

    /**
     * Get existing columns
     * @return array - get existing columns from db
     */
    public function getExistingColumns(): array
    {
        if (empty($this->existingColumns)) {
            $this->initExistingColumns();
        }
        return $this->existingColumns;
    }

    /**
     * Check if column name exist in the db
     * @param string $key
     * @return bool
     */
    public function isExistingColumn(string $key): bool
    {
        return array_key_exists($key, $this->existingColumns);
    }

    /**
     * Set data and run init function
     * @param mixed $data
     * @param mixed $value
     * @return $this
     */
    public function setData($data, $value = null)
    {
        $filterdData = array();
        foreach ($data as $key => $val) {
            if (!(isset($this->lazyLoadinRelationList[$key]) || isset($this->oneObjectReleationFields[$key]))) {
                $filterdData[$key] = $val;
                unset($data[$key]);
            }
        }
        if (is_array($filterdData)) {
            foreach ($filterdData as $k => $v) {
                parent::setData($k, $v);
            }
        }
        $this->init();

        $this->addRelationsOnSave = $data;


        return $this;
    }

    /**
     * Run relations on save
     */
    public function runRelationsOnSave()
    {
        foreach ($this->addRelationsOnSave as $key => $val) {
            if (is_array($val)) {
                foreach ($val as $id) {
                    $this->addRelation($key, $id);
                }
            } else {
                $this->addRelation($key, $val);
            }
            unset($this->addRelationsOnSave[$key]);
        }
    }

    /**
     * Empty function for override option that will called when the object is loaded
     */
    public function init()
    {
        
    }

    /**
     * Get collection of objects
     * @param array | Where $params - filter conditions :
     * example 1 : array('key' => val)
     * example 2 : array('key' => array('>','now()'))
     *
     * @param string $orderBy - order by string
     * @param array $joinParams - example - array (array('join','table as t', 't.a = main_s.b'))
     * @param array $byKey - the value key to get the objects
     * @param array $limit - limit number of rows
     * @return ModelAbstract[] - collections of objects
     */
    public function getAll($params = array(), string $orderBy = null, array $joinParams = null,
                           $byKey = null, int $limit = null, int $offset = null, $countSelect = [])
    {
        $result      = array();
        $className   = get_class($this);
        $db          = $this->getModel($className)->getDatabase();
        $queryResult = $db->getAll($this->getModel($className)->getTableName(), $params, $orderBy,
            $joinParams, $limit, false, $countSelect, null, $offset);
        if (!empty($countSelect)) {
            return current(current($queryResult));
        }
        foreach ($queryResult as $row) {
            if ($byKey && isset($row[$byKey])) {
                $result[$row[$byKey]] = $this->getModel($className)->factory($row);
            } else {
                $result[] = $this->getModel($className)->factory($row);
            }
        }
        return $result;
    }

    /**
     * Get collection of objects
     * @param array | Where $params - filter conditions :
     * example 1 : array('key' => val)
     * example 2 : array('key' => array('>','now()'))
     *
     * @param string $orderBy - order by string
     * @param array $joinParams - example - array (array('join','table as t', 't.a = main_s.b'))
     * @param array $byKey - the value key to get the objects
     * @param array $limit - limit number of rows
     * @return ModelAbstract[] - collections of objects
     */
    public function count($params = array(), array $joinParams = null, $byKey = null)
    {
        $res = $this->getAll($params, null, $joinParams, $byKey);
        return count($res);
    }

    /**
     * Get collection of objects by search 
     * @param array $params - filter conditions :
     * example 1 : array('key' => val)
     * example 2 : array('key' => array('>','now()'))
     *
     * @param string $orderBy - order by string
     * @param array $joinParams - example - array (array('join','table as t', 't.a = main_s.b'))
     * @param array $byKey - the value key to get the objects
     * @param array $limit - limit number of rows
     * @return ModelAbstract[] - collections of objects
     */
    public function searchAll(string $searchString, $columnsToSearch = array(),
                              array $params = array(), string $orderBy = null,
                              array $joinParams = null, $byKey = null, int $limit = null,
                              int $offset = null)
    {
        $whereParams = new Where($params);
        $columns     = $this->getColumnsFromTable($this->getTableName());
        foreach ($columns as $name => $columnInfo) {
            if (($columnInfo['type'] == 'varchar' || $columnInfo['type'] == 'text') &&
                ((!empty($columnsToSearch) && in_array($name, $columnsToSearch)) || empty($columnsToSearch))
                && ((strpos($name, 'pass') === false) && !empty($searchString))
            ) {
                $whereParams->add($name, array('like', '%'.$searchString.'%'));
            }
        }
        return $this->getAll($whereParams, $orderBy, $joinParams, $byKey, $limit, $offset);
    }

    /**
     * Delete all by params
     * @param type $params
     * @return int number of deleted
     */
    public function deleteAll(array $params)
    {
        $className = get_class($this);

        /** @var Database $db */
        $db = $this->getModel($className)->getDatabase();
        $db->delete($this->getModel($className)->getTableName(), $params);
        return $db->lastRowCount;
    }

    /**
     * Get one object
     * @param array $params - filter conditions
     * @param string $orderBy - order by string
     * @param array $joinParams - example - array (array('join','table as t', 't.a = main_s.b'))
     * @return Object/null
     */
    public function getOne(array $params = array(), string $orderBy = null,
                           array $joinParams = null, $byKey = null)
    {
        $result = $this->getAll($params, $orderBy, $joinParams, $byKey, 1);
        if (!empty($result)) {
            return current($result);
        } else {
            return null;
        }
    }

    /**
     * Get Primary key value
     * @return string
     */
    public function getId()
    {
        return $this->get($this->getPrimaryKey());
    }

    public function getCodeId()
    {
        $id = $this->getId();
        return is_numeric($id) ? \Arbel\Utils\Shorten::toKey($this->getId()) : $id;
    }

    /**
     * 
     * @param string $val
     * @return $this
     */
    public function setId($val)
    {
        return $this->set($this->getPrimaryKey(), $val);
    }

    /**
     * Remove the objects from the wanted data
     * @param string $key
     * @param type $default
     * @return type
     */
    public function getNoneRelationData(string $key = null, $default = null)
    {
        $data = parent::getData($key, $default);
        if (!isset($key) && !isset($default) && is_array($data)) {
            foreach ($data as $key => $val) {
                if (isset($this->lazyLoadinRelationList[$key]) || isset($this->oneObjectReleationFields[$key])) {
                    unset($data[$key]);
                }
            }
        }
        return $data;
    }

    /**
     * Get zend select object for current table
     * @return \Arbel\Base\Sql\Select
     */
    public function select(): \Arbel\Base\Sql\Select
    {
        $className = get_class($this);
        return $this->getModel($className)->getDatabase()->select()
                ->from($this->getModel($className)->getTableName())->setClassName($className);
    }

    /**
     * Get all result by sql 
     * @return  []
     */
    public function sql(string $sql, array $params = [])
    {
        $className = get_class($this);
        $dbResult  = $this->getDatabase()->exec($sql, $params);
        $result    = [];
        foreach ($dbResult as $row) {
            $clone    = clone ($this);
            $result[] = $clone->setData($row);
        }
        return $result;
    }

    /**
     * get select with where
     * @param type $whereParam
     * @return \Arbel\Base\Sql\Select
     */
    public function where($whereParam, $combination = \Zend\Db\Sql\Predicate\PredicateSet::OP_AND): \Arbel\Base\Sql\Select
    {
        $className = get_class($this);
        return $this->getModel($className)->select()->where($whereParam, $combination);
    }

    /**
     * Get Primary Key
     * @return string - key
     */
    abstract static function getPrimaryKey(): string;

    /**
     * Remove object from database
     */
    public function delete()
    {
        $this->getDatabase()
            ->delete($this->getTableName(), $this->getPrimaryKey(), $this->getId()
        );
    }

    /**
     * Get options as array or result
     * @param string $key
     * @param string $default
     * @return array
     */
    public function getOptions($default = array()): array
    {
        if (!$this->optionColumn) {
            return $this->get('options', $default);
        }

        $result  = array();
        $options = $this->getData($this->optionColumn);

        if (!empty($options)) {
            $result = explode(',', $options);
        }
        return $result;
    }

    /**
     * Check if has option
     * @param string $option
     * @return boolean
     */
    public function hasOption(string $option): bool
    {
        $options = $this->getOptions();
        return in_array($option, $options);
    }

    /**
     * Init many to many relation
     * @param string $key - the current object key for this relation
     * @param string $tableName - the targeted table
     * @param string $targetId - the target object id column
     * @param string $targetClass - the target object class name
     * @param string $primaryKey - the current object primary key for the targeted table
     * @param array $params - more params (key,val) for the where query
     */
    public function initRelation($key, $tableName, $targetId, $targetClass, $primaryKeyParam = null,
                                 $params = array())
    {
        $objects = array();

        if (is_array($targetId)) {
            $objectTargetId = $targetId[key($targetId)];
            $targetId       = key($targetId);
        } else {
            $objectTargetId = $targetId;
            $targetId       = $targetId;
        }

        if (is_array($primaryKeyParam)) {
            $objectPrimaryKey = $primaryKeyParam[key($primaryKeyParam)];
            $primaryKey       = key($primaryKeyParam);
        } elseif (!isset($primaryKeyParam)) {
            $primaryKey       = $this->getPrimaryKey();
            $objectPrimaryKey = $primaryKey;
        } else {
            $primaryKey       = $primaryKeyParam;
            $objectPrimaryKey = $primaryKey;
        }

        $params[$primaryKey] = $this->get($objectPrimaryKey);
        $model               = $this->getModel($targetClass);
        if (isset($params[$primaryKey]) && !isset($this->data[$key])) {
            if ($model->getTableName() == $tableName &&
                $model->getDatabase() == $this->getDatabase()) {
                $rows = $this->getDatabase()->getAll($tableName, $params);
            } elseif ($model->getTableName() == $tableName) {
                $rows = $model->getDatabase()->getAll($model->etTableName(), $params);
            } else {
                $rows = $model->getDatabase()->getAll($model->getTableName(), $params, null,
                    array(
                    array('join', $tableName, $tableName.'.'.$targetId.' = '.$model->getTableName().'.'.$objectTargetId)
                ));
            }

            foreach ($rows as $row) {
                $object                    = $model->factory($row);
                $objects[$object->getId()] = clone $object;
            }

            $this->data[$key] = $objects;
        }
    }

    /**
     * Init one to one relation
     * @param string $key - the current object key for this relation
     * @param string $columnName - the target object key column
     * @param string $className - the target object class name
     * @param string|null $className - current column name
     * @param array $whereParams - (optinal) more where condition
     */
    public function initHasOneRelation(string $key, string $columnName, string $className,
                                       $currentColumnName, array $whereParams = array())
    {
        $columnValue = $currentColumnName ? $this->get($currentColumnName) : $this->get($columnName);
        if (isset($columnValue) && !isset($this->data[$key])) {
            $whereParams[$columnName] = $columnValue;
            $object                   = $this->getModel($className)->getOne($whereParams);
            $this->data[$key]         = $object;
        }
    }

    /**
     * Add many to many relation as lazy loading
     * @param string $key - the current object key for this relation
     * @param string $tableName - the targeted table
     * @param string | array $targetId - the target object id column
     * @param string $targetClass - the target object class name
     * @param string | array $primaryKey - the current object primary key for the targeted table
     * @param array $params - more params (key,val) for the where query
     */
    public function hasMany(string $key, string $tableName, $targetId, string $targetClass,
                            $primaryKey = null, array $params = array())
    {
        $this->lazyLoadinRelationList[$key] = array(
            'table_name' => $tableName,
            'target_id' => $targetId,
            'class_name' => $targetClass,
            'primary_key' => $primaryKey,
            'params' => $params
        );
    }

    /**
     * Remove many to many relation
     * @param string $key - the object key on the current data
     * @param mixed $params - the array params for the relations or the object for the relation or the id
     */
    public function removeRelation(string $key, $params = array())
    {
        if (isset($this->lazyLoadinRelationList[$key])) {
            $whereParams     = array();
            $targetClass     = $this->lazyLoadinRelationList[$key]['class_name'];
            $tableName       = $this->lazyLoadinRelationList[$key]['table_name'];
            $columnNameParam = $this->lazyLoadinRelationList[$key]['target_id'];
            $primaryKeyParam = $this->lazyLoadinRelationList[$key]['primary_key'];

            if (is_array($columnNameParam)) {
                $objectFieldName = $columnNameParam[key($columnNameParam)];
                $columnName      = key($columnNameParam);
            } else {
                $objectFieldName = $columnNameParam;
                $columnName      = $columnNameParam;
            }

            if (is_array($primaryKeyParam)) {
                $objectPrimaryKey = $primaryKeyParam[key($primaryKeyParam)];
                $primaryKey       = key($primaryKeyParam);
            } elseif (!isset($primaryKeyParam)) {
                $primaryKey       = $this->getPrimaryKey();
                $objectPrimaryKey = $primaryKey;
            } else {
                $primaryKey       = $primaryKeyParam;
                $objectPrimaryKey = $primaryKey;
            }

            if (is_object($params)) {
                $whereParams[$columnName] = $params->get($objectFieldName);
            } elseif (!is_array($params)) {
                $whereParams[$columnName] = $params;
            } else {
                $whereParams = $params;
            }

            $whereParams[$primaryKey] = $this->get($objectPrimaryKey);
            $targetClass::getDatabase()->delete($tableName, $whereParams);
        }
    }

    /**
     * Remove all many to many relations
     * @param string $key - the object key on the current data
     * @param mixed $params - the array params for the relations or the object for the relation or the id
     */
    public function resetRelations(string $key)
    {
        if (isset($this->lazyLoadinRelationList[$key])) {
            $whereParams     = array();
            $targetClass     = $this->lazyLoadinRelationList[$key]['class_name'];
            $tableName       = $this->lazyLoadinRelationList[$key]['table_name'];
            $primaryKeyParam = $this->lazyLoadinRelationList[$key]['primary_key'];

            if (is_array($primaryKeyParam)) {
                $objectPrimaryKey = $primaryKeyParam[key($primaryKeyParam)];
                $primaryKey       = key($primaryKeyParam);
            } elseif (!isset($primaryKeyParam)) {
                $primaryKey       = $this->getPrimaryKey();
                $objectPrimaryKey = $primaryKey;
            } else {
                $primaryKey       = $primaryKeyParam;
                $objectPrimaryKey = $primaryKey;
            }

            $whereParams[$primaryKey] = $this->get($objectPrimaryKey);
            $targetClass::getDatabase()->delete($tableName, $whereParams);
        }
    }

    /**
     * Add many to many relation ignore if exist
     * @param string $key - the object key on the current data
     * @param mixed $params - the array params for the relations or the object for the relation or the id
     */
    public function addRelation(string $key, $params = array())
    {
        if (isset($this->lazyLoadinRelationList[$key])) {
            $whereParams     = array();
            $targetClass     = $this->lazyLoadinRelationList[$key]['class_name'];
            $tableName       = $this->lazyLoadinRelationList[$key]['table_name'];
            $columnNameParam = $this->lazyLoadinRelationList[$key]['target_id'];
            $primaryKeyParam = $this->lazyLoadinRelationList[$key]['primary_key'];
            $targetModel     = $this->getModel($targetClass);

            if (is_array($columnNameParam)) {
                $objectFieldName = $columnNameParam[key($columnNameParam)];
                $columnName      = key($columnNameParam);
            } else {
                $objectFieldName = $columnNameParam;
                $columnName      = $columnNameParam;
            }

            if (is_array($primaryKeyParam)) {
                $objectPrimaryKey = $primaryKeyParam[key($primaryKeyParam)];
                $primaryKey       = key($primaryKeyParam);
            } elseif ($targetModel->getTableName() == $tableName && !isset($primaryKeyParam)) {
                $primaryKey       = $columnName;
                $objectPrimaryKey = $this->getPrimaryKey();
            } elseif (!isset($primaryKeyParam)) {
                $primaryKey       = $this->getPrimaryKey();
                $objectPrimaryKey = $primaryKey;
            } else {
                $primaryKey       = $primaryKeyParam;
                $objectPrimaryKey = $primaryKey;
            }

            //If the reciving params is the object the get the id
            if (is_object($params)) {
                $whereParams[$columnName] = $params->get($objectFieldName);

                //If target id is is the reciving params
            } elseif (!is_array($params)) {
                $whereParams[$columnName] = $params;
            } else {
                $whereParams = $params;
            }

            $whereParams[$primaryKey] = $this->get($objectPrimaryKey);
            if ($targetModel->getTableName() == $tableName) {
                $targetModel->setData($whereParams)
                    ->save(true);
            } else {
                $this->getModel($targetClass)->getDatabase()->insert($tableName, $whereParams,
                    array(), true);
            }
        }
        return $this;
    }

    /**
     * Set data and run init
     * If set relation object the update the relation id
     * @param string $key
     * @param mixed $val
     * @return $this
     */
    public function set(string $key, $val)
    {
        $this->runInitRelationListByKey($key, $val);
        $this->runInitHasOneByKey($key, $val);
        $this->convertParamToObject($key, $val);

        return parent::set($key, $val);
    }

    /**
     * Remove data by key
     * If this relation one to one object then remove the object id as well
     * @param string $key
     */
    public function remove($key)
    {
        $val = $this->get($key);
        if (isset($this->oneObjectReleationFields[$key]) &&
            is_object($val)) {
            parent::set($this->oneObjectReleationFields[$key]['column_name'], null);
        }
        parent::remove($key);
    }

    /**
     * Check if have the key and run init
     * @param string $key
     * @return bool
     */
    public function have(string $key): bool
    {
        $this->runInitRelationListByKey($key);
        return parent::have($key);
    }

    /**
     * Run init relation list
     * @param string $key
     */
    public function runInitRelationListByKey(string $key, $val = null)
    {

        if (isset($this->lazyLoadinRelationList[$key]) && !is_object($val)) {
            $data = $this->lazyLoadinRelationList[$key];
            $this->initRelation($key, $data['table_name'], $data['target_id'], $data['class_name'],
                $data['primary_key'], $data['params']);
        } elseif (isset($this->lazyLoadinRelationList[$key]) && is_object($val)) {
            $data = $this->lazyLoadinRelationList[$key];
            $this->set($data['primary_key'], $val->get($data['target_id']));
        }
    }

    /**
     * Run init one relation list
     * @param string $key
     * @param mixed $key
     */
    public function runInitHasOneByKey(string $key, $val = null)
    {
        if (isset($this->oneObjectReleationFields[$key]) && !is_object($val)) {
            $data = $this->oneObjectReleationFields[$key];
            $this->initHasOneRelation($key, $data['column_name'], $data['class_name'],
                $data['current_column_name'], $data['where_params']);
        } elseif (isset($this->oneObjectReleationFields[$key]) && is_object($val)) {
            $data = $this->oneObjectReleationFields[$key];
            $this->set($data['current_column_name'], $val->get($data['column_name']));
        }
    }

    /**
     * Remove relation data
     * @param array $data
     */
    public function removeNonExistingColumns(array &$data)
    {
        $exitingColumns = $this->getExistingColumns();
        foreach ($data as $key => $rowData) {
            if (!array_key_exists($key, $exitingColumns)) {
                unset($data[$key]);
            }
        }
    }

    /**
     * Remove relation data
     * @param array $data
     */
    public function removeBlacklistColumns(array &$data)
    {
        $blacklistColumns = $this->blacklistColumns;
        foreach ($data as $key => $rowData) {
            if (in_array($key, $blacklistColumns)) {
                unset($data[$key]);
            }
        }
    }

    /**
     * Get filtered data for saving db
     * @return mixed
     */
    public function getFilterdData()
    {
        $data = $this->getNoneRelationData();
        $this->convertObjectToJsonFields($data);
        $this->convertBooleanFields($data);
        $this->removeNonExistingColumns($data);
        $this->removeBlacklistColumns($data);

        if (isset($data[$this->getPrimaryKey()])) {
            unset($data[$this->getPrimaryKey()]);
        }
        return $data;
    }

    /**
     * Convert string fields to boolean if required
     * @param array $data
     */
    public function convertBooleanFields(array &$data)
    {
        foreach ($this->booleanFields as $fieldName) {
            if (isset($data[$fieldName]) && !is_int($data[$fieldName])) {
                if (is_string($data[$fieldName])) {
                    $data[$fieldName] = \Arbel\Helper::stringToBool($data[$fieldName]);
                }
            }
        }
        return $data;
    }

    /**
     * Check if is new object
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->isNew;
    }

    public function runAutoAudit()
    {
        if ($this->getId()) {
            $newData = $this->getChangedData(true);
            foreach ($newData as $k => $v) {
                $this->getModel(Audit::class)->addEvent($this->getAuditKey().'_'.$k.'_update', $v,
                    ['id' => $this->getId()], 'default',($this->getOriginData($k)), $this->getId());
            }
        } else {
            $this->getModel(Audit::class)->addEvent($this->getAuditKey().'_new',
                $this->getFilterdData());
        }
    }

    public function getOriginData(string $key = null,$default = null)
    {
        if(is_null($key)){
            return $this->originData;
        }else{
            return $this->originData[$key] ?? $default;
        }
    }

    public function isAutoAuditMode()
    {
        return $this->isAutoAuditMode && $this->getAuditKey();
    }

    /**
     * Get Object Table Name
     */
    abstract static public function getTableName(): string;

    /**
     * Save the object after check if object already exist
     * @param bool $isInsertOrUpdate- is to update when key exist
     * @param bool $isInsertIfNoExist- is ignore
     * @return ModelAbstract
     */
    public function save(bool $isInsertOrUpdate = false, bool $isInsertIfNoExist = false)
    {

        if ($this->isAutoAuditMode()) {
            $this->runAutoAudit();
        }

        if (!$this->getId() && $this->hasUniqueFields()) {
            $dataFromDb = $this->getDataByUniqueFields();
            if (!empty($dataFromDb)) {
                $this->addData($dataFromDb);
            }
        }

        if (!$this->getId() && $isInsertIfNoExist) {
            $this->insertIfNoExist();
        } elseif (!$this->getId() && $isInsertOrUpdate && $this->allowUpdate) {
            $this->insertOrUpdate();
        } elseif ($this->getId() && $this->allowUpdate) {
            $this->update();
        } elseif (!$this->getId()) {
            $this->insert();
        }
        if (!$this->get('created_at')) {
            $this->set('created_at', date("Y-m-d H:i:s"));
            $this->set('updated_at', date("Y-m-d H:i:s"));
        }

        $this->runRelationsOnSave();

        return $this;
    }

    /**
     * Create new of current object
     * @return ModelAbstract
     */
    public function create()
    {
        $name = get_class($this);
        return $this->getModel($name);
    }

    /**
     * Insert row on the database
     * @return $this
     */
    public function insert()
    {
        $data = $this->getFilterdData();
        $db   = $this->getDatabase();
        $id   = $db->insert(
            $this->getTableName()
            , $data);
        $this->setId($id);
        if ($id) {
            $this->isNew = true;
        }
        return $this;
    }

    /**
     * Insert row on the database
     * @return $this
     */
    public function insertOrUpdate()
    {
        $data        = $this->getFilterdData();
        $changedData = $this->getChangedData();
        $db          = $this->getDatabase();
        $id          = $db->insert(
            $this->getTableName()
            , $data, $changedData);
        $this->setId($id);
        if ($id) {
            $this->isNew = true;
        }
        return $this;
    }

    /**
     * Insert ignore row on the database
     * @return $this
     */
    public function insertIfNoExist()
    {
        $data = $this->getFilterdData();
        $db   = $this->getDatabase();
        $id   = $db->insert(
            $this->getTableName()
            , $data, array(), true);
        $this->setId($id);
        if ($id) {
            $this->isNew = true;
        }
        return $this;
    }

    /**
     * Get changed data only
     * @return array
     */
    public function getChangedData($isLoadFromDb = false): array
    {
        $result = array();
        $data   = $this->getFilterdData();
        if (!$this->originData && $this->getId() && $isLoadFromDb) {
             $this->originData = $this->initById($this->getId())->getData();
        }
        foreach ($data as $key => $val) {
            if (($this->originData && isset($this->originData[$key]) &&
                $this->originData[$key] != $val) || (!$this->originData || !isset($this->originData[$key]))) {
                $result[$key] = $val;
            }
        }
        return $result;
    }

    /**
     * Get audit flag
     * @return string
     */
    public function getAuditKey(): string
    {
        return get_class($this);
    }

    /**
     * Update row on the database
     * if is no change then skip the update
     */
    public function update()
    {
        $data = $this->getChangedData();
        $db   = $this->getDatabase();
        unset($data[$this->getPrimaryKey()]);
        if (!empty($data)) {
            $db->update(
                $this->getTableName(), array($this->getPrimaryKey() => $this->getId()), $data);
        }
        return $this;
    }

    /**
     * Update all rows
     * @param array $whereParams
     * @param array $newData
     */
    public function updateAll(array $whereParams, array $newData)
    {
        $className = get_class($this);
        $db        = $this->getModel($className)->getDatabase();
        $db->update(
            $this->getModel($className)->getTableName(), $whereParams, $newData);
    }

    /**
     * Get data by unique fields
     * @return array - sql result
     */
    public function getDataByUniqueFields()
    {
        $uniqueFields = $this->getUniqueFields();
        $db           = $this->getDatabase();
        $dataFromDb   = $db->load($this->getTableName(), $uniqueFields);
        return $dataFromDb;
    }

    /**
     * Check if has unique fields
     * @return bool
     */
    public function hasUniqueFields(): bool
    {
        $uniqueFields = $this->getUniqueFields();
        return !empty($uniqueFields);
    }

    /**
     * Get unique fields data
     * @return array - result
     */
    public function getUniqueFields(): array
    {
        $data   = $this->getFilterdData();
        $result = $this->getValuesByKeys($this->uniqueTableColumn, $data);
        return $result;
    }

    /**
     * Set has relation condition and create new object
     * @example
     *
     *   Retrieve all posts that have at least one comment...
     *   $books = Book::has('comments')->all();
     *
     *   Retrieve all posts that have at least one comment and one store...
     *   $books = Book::has(array('comments','stores'))->all();
     *
     *   Retrieve all posts that have three or more comments...
     *   $books = Book::has('comments', '>=', 6)->all();
     *
     *   Retrieve all posts that have at least one comment with votes...
     *   $books = Book::has('comments.votes')->all();
     *
     *   Retrieve all posts that have at least one comment with user Dan...
     *   $books = Book::has('comment',array('user' => 'Dan'))->all();
     * 
     * @param array|string $rel
     * @param mixed $where - where params
     * @param mixed $count - count value
     * @return new ModelAbstract object
     */
    public function has($rel, $where = null, $count = null)
    {
        $currentClassName = get_class($this);
        $object           = $this->getModel($currentClassName);

        return $object->hasRelation($rel, $where, $count);
    }

    /**
     * Retrieve more relations with single query and create new object
     * @example
     *
     *   Retrieve all posts with there comments
     *   $books = Book::with('comments')->all();
     *
     *   Retrieve all posts with there comments
     *   $books = Book::with(array('comments','stores'))->all();
     *
     *   Retrieve all posts with there comment only if there user name is Dan...
     *   $books = Book::has('comment',array('user' => 'Dan'))->all();
     *
     * @param array|string $rel
     * @param mixed $where
     * @param mixed $count
     * @return new ModelAbstract object
     */
    public function with($rel, $where = null)
    {
        $currentClassName = get_class($this);
        $object           = $this->getModel($currentClassName);

        return $object->withRelation($rel, $where);
    }

    /**
     * Set has relation condition
     * @example
     *
     *   Retrieve all posts that have at least one comment...
     *   $books = $book->hasRelation('comments')->all();
     *
     *   Retrieve all posts that have at least one comment and one store...
     *   $books = $book->hasRelation(array('comments','stores'))->all();
     *
     *   Retrieve all posts that have three or more comments...
     *   $books = $book->hasRelation('comments', '>=', 6)->all();
     *
     *   Retrieve all posts that have at least one comment with votes...
     *   $books = $book->hasRelation('comments.votes')->all();
     *
     *   Retrieve all posts that have at least one comment with user Dan...
     *   $books = $book->hasRelation('comment',array('user' => 'Dan'))->all();
     *
     *   Retrieve all posts that have at least one comment with user Dan or Yosi...
     *   $books = $book->hasRelation('comment',array('user' => array('Dan','Yosi')))->all();
     *
     *   Retrieve all posts that have at least one comment with user Dan and user Yosi...
     *   $books = $book->hasRelation('comment',[ ['user' => ['Dan']] , ['user' => ['Yosi']] ])->all();
     *
     * @param array|string $rel
     * @param mixed $where - where params
     * @param mixed $count - count value
     * @return new ModelAbstract object
     */
    public function hasRelation($rel, $where = null, $count = null)
    {

        if (is_array($rel)) {
            $newWhere = array();
            $this->addTableDot($newWhere, $where, $this::getTableName());
            foreach ($rel as $r) {
                $this->hasRelation($r, $newWhere);
            }
            return $this;
        }
        $joinCounter = 1;

        if (strpos($rel, '.') !== false) {
            $array = explode('.', $rel);
            $rel   = $array[0];
            if (!is_array($where)) {
                $where = array();
            }
            $where[$array[1]] = array('is not null');
        }

        $relData   = $this->getOneObjectReleationFields($rel) ?? $this->getLazyLoadinRelationList($rel);
        $className = $relData['class_name'];
        $tableName = $this->getModel($className)->getTableName();


        if (!empty($where) && is_numeric(key($where)) && is_array($where[key($where)])) {
            $joinCounter = count($where);
        }

        $this->getAllWithRelationsData['relations'][$rel] = [
            'type' => 'join',
            'join_counter' => $joinCounter
        ];


        if (is_array($where)) {
            $this->addTableDot($this->getAllWithRelationsData['where'], $where,
                $this->getModel($className)->getTableName());
        } elseif (is_string($where) && is_numeric($count)) {

            $columns = $this->getModel($className)->getColumnsFromTable($this->getModel($className)->getTableName());

            $this->addTableDot($this->getAllWithRelationsData['where_count'],
                array(
                key($columns) => array(" {$where} {$count}"),
                ), $this->getModel($className)->getTableName());
        }
        return $this;
    }

    /**
     * Retrieve more relations with single query
     * @example
     *
     *   Retrieve all posts with there comments
     *   $books = $book->with('comments')->all();
     *
     *   Retrieve all posts with there comments
     *   $books = $book->with(array('comments','stores'))->all();
     *
     *   Retrieve all posts with there comment only if there user name is Dan...
     *   $books = $book->has('comment',array('user' => 'Dan'))->all();
     *
     * @param array|string $rel
     * @param mixed $where
     * @param mixed $count
     * @return new ModelAbstract object
     */
    public function withRelation($rel, array $where = array())
    {

        if (is_array($rel)) {
            $newWhere = array();
            $this->addTableDot($newWhere, $where, $this::getTableName());
            foreach ($rel as $r) {
                $this->withRelation($r, $newWhere);
            }
            return $this;
        }
        $joinCounter = 1;

        if (strpos($rel, '.') !== false) {
            $array = explode('.', $rel);
            $rel   = $array[0];
            if (!is_array($where)) {
                $where = array();
            }
            $where[$array[1]] = array('is not null');
        }

        $relData   = $this->getOneObjectReleationFields($rel) ?? $this->getLazyLoadinRelationList($rel);
        $className = $relData['class_name'];
        $tableName = $this->getModel($className)->getTableName();


        if (isset($this->getAllWithRelationsData['relations'][$rel]) && $this->getAllWithRelationsData['relations'][$rel]['type']
            == 'join') {
            $isRelExist = true;
        } else {
            $isRelExist = false;
        }

        if (!empty($where) && is_numeric(key($where)) && is_array($where[key($where)])) {
            $joinCounter = count($where);
        }

        $this->getAllWithRelationsData['relations'][$rel] = [
            'type' => 'left join',
            'join_counter' => $joinCounter
        ];

        if ($isRelExist) {
            $columns              = $this->getModel($className)->getColumnsFromTable($this->getModel($className)->getTableName());
            $where[key($columns)] = array('is not null');
        }
        $this->addTableDot($this->getAllWithRelationsData['where'], $where,
            $this->getModel($className)->getTableName());

        return $this;
    }

    /**
     * Fetch the result of has/with funcitons and return all results
     * @return array - result
     */
    public function all()
    {
        return $this->getAllWithRelations($this->getAllWithRelationsData['relations'],
                $this->getAllWithRelationsData['where'], true, 'left join',
                $this->getAllWithRelationsData['where_count']);
    }

    /**
     * Fetch the result of has/with and return the first result
     * @return ModelAbstract | false
     */
    public function first()
    {
        $result = $this->getAllWithRelations($this->getAllWithRelationsData['relations'],
            $this->getAllWithRelationsData['where'], true, 'left join',
            $this->getAllWithRelationsData['where_count'], null, 1);
        return current($result);
    }

    /**
     * Fetch the result of has/with and return the first result
     * @return ModelAbstract | false
     */
    public function last()
    {
        $result = $this->getAllWithRelations($this->getAllWithRelationsData['relations'],
            $this->getAllWithRelationsData['where'], true, 'left join',
            $this->getAllWithRelationsData['where_count'], $this->getPrimaryKey().' desc', 1);
        return current($result);
    }

    /**
     * Add where params
     * @param array $array
     */
    public function addWhere(array $params)
    {
        $this->addTableDot($this->getAllWithRelationsData['where'], $params, $this->getTableName());
        return $this;
    }

    /**
     * Get all objects with relations
     * @param array $relations - relation array - relName => join type
     * @param array $where -  array with where params
     * @param bool $isSelectAllRelationsData - if get data from all other tables
     * @param string $defaultJoinType - default join type - if the rel don't have one
     * @param array $whereCount - array of count condition
     * @param string $orderBy - order by
     * @param int $limit - limit result
     * @return \Arbel\Base\currentClassName
     */
    public function getAllWithRelations(array $relations, array $where = array(),
                                        bool $isSelectAllRelationsData = true,
                                        string $defaultJoinType = 'left join',
                                        array $whereCount = array(), $orderBy = null,
                                        int $limit = null)
    {
        $result           = array();
        $currentClassName = get_class($this);
        $object           = $this->getModel($currentClassName);
        $joinsArray       = array();
        $whereArray       = array();
        self::addTableDot($whereArray, $where, $currentClassName::getTableName());

        $joinCounter = isset($relations[key($relations)]) ? $relations[key($relations)]['join_counter']
                : 1;

        $object->initLazyRelationsWhereAndJoin($whereArray, $joinsArray, $relations,
            $defaultJoinType);

        $object->initOneRelationsWhereAndJoin($whereArray, $joinsArray, $relations, $defaultJoinType);

        if (empty($joinsArray) || $joinCounter > 1) {
            $isSelectAllRelationsData = false;
        }


        $rows = $object::getDatabase()->getAll($object::getTableName(), $whereArray, $orderBy,
            $joinsArray, $limit, $isSelectAllRelationsData, $whereCount);

        if ($isSelectAllRelationsData) {
            $result = self::splitSqlResultToObjects($rows, $relations);
        } else {
            foreach ($rows as $objectData) {
                $result[] = $this->getModel($currentClassName)->setData($objectData);
            }
        }

        return $result;
    }

    /**
     * Init join array by many to many relations
     * @param array $whereArray - the where array to edit
     * @param array $joinsArray - the join array to edit
     * @param array $relations - the wanted relations
     * @param string $defaultJoinType - the default join type
     */
    public function initLazyRelationsWhereAndJoin(array &$whereArray, array &$joinsArray,
                                                  array $relations, string $defaultJoinType)
    {
        foreach ($this->getLazyLoadinRelationList() as $key => $params) {
            $className          = $params['class_name'];
            $model              = $this->getModel($className);
            $tableName          = $params['table_name'];
            $fullTableName      = $model->getDatabase()->getName().'.'.$params['table_name'];
            $columnName         = $params['target_id'];
            $primaryKey         = $params['primary_key'] ?? $this->getPrimaryKey();
            $whereParams        = $params['params'] ?? array();
            $classFullTableName = $model->getDatabase()->getName().'.'.$model->getTableName();
            $classTableName     = $model->getTableName();
            $isPivot            = $fullTableName != $classFullTableName;


            if ((!in_array($key, $relations) && !isset($relations[$key]) ) && !in_array($model->getTableName(),
                    $relations)) {
                continue;
            }

            $joinType    = isset($relations[$key]) ? $relations[$key]['type'] : $defaultJoinType;
            $joinCounter = isset($relations[$key]) ? $relations[$key]['join_counter'] : 1;

            if ($joinCounter > 1) {
                if ($isPivot) {
                    $joinsArray[] = array($joinType, array($tableName => $fullTableName),
                        $tableName.'.'.$primaryKey.'='.$this::getTableName().'.'.$primaryKey,
                        $model->getDatabase()->getName());
                }
                for ($i = 0; $i < $joinCounter; $i++) {
                    for ($i = 0; $i < $joinCounter; $i++) {
                        $aliasName = $classTableName.'_'.$i;
                        if ($isPivot) {
                            $joinWhere = $aliasName.'.'.$columnName.'='.$tableName.'.'.$columnName;
                        } else {
                            $joinWhere = $aliasName.'.'.$columnName.'='.$this::getTableName().'.'.$primaryKey;
                        }
                        $originWhereArray = $whereArray;
                        foreach ($originWhereArray as $whereKey => $whereVal) {
                            if (strpos($whereKey, $aliasName) !== false) {
                                $whereKeyBinding                                    = ':'.str_replace(".",
                                        "_", $whereKey);
                                $joinWhere                                          .= ' and '.$whereKey.'='.$whereKeyBinding;
                                unset($whereArray[$whereKey]);
                                $whereArray[$whereKeyBinding]                       = $whereVal;
                                $whereArray[$aliasName.'.'.$model->getPrimaryKey()] = array('is not null');
                            }
                        }
                        if ($isPivot) {
                            $joinsArray[] = array($joinType, array($model->getTableName() => $aliasName),
                                $joinWhere,
                                $model->getDatabase()->getName());
                        } else {
                            $joinsArray[] = array($joinType, array($model->getTableName() => $aliasName),
                                $joinWhere);
                        }
                    }
                }
            } else {
                if ($isPivot) {
                    $joinsArray[] = array($joinType, array($tableName => $fullTableName),
                        $tableName.'.'.$primaryKey.'='.$this::getTableName().'.'.$primaryKey,
                        $model->getDatabase()->getName());
                    $joinsArray[] = array($joinType, array($model->getTableName() => $classFullTableName),
                        $model->getTableName().'.'.$columnName.'='.$tableName.'.'.$columnName,
                        $model->getDatabase()->getName());
                } else {
                    if (is_array($primaryKey)) {
                        $joinsArray[] = array($joinType, array($model->getTableName() => $classFullTableName),
                            $model->getTableName().'.'.key($primaryKey).'='.$this::getTableName().'.'.current($primaryKey));
                    } else {
                        $joinsArray[] = array($joinType, array($model->getTableName() => $classFullTableName),
                            $model->getTableName().'.'.$columnName.'='.$this::getTableName().'.'.$primaryKey);
                    }
                }
            }


            self::addTableDot($whereArray, $whereParams, $model->getTableName());
        }
    }

    /**
     * Init join array by one to one relations
     * @param array $whereArray - the where array to edit
     * @param array $joinsArray - the join array to edit
     * @param array $relations - the wanted relations
     * @param string $defaultJoinType - the default join type
     */
    public function initOneRelationsWhereAndJoin(array &$whereArray, array &$joinsArray,
                                                 array $relations, string $defaultJoinType)
    {

        foreach ($this->getOneObjectReleationFields() as $key => $params) {
            $className          = $params['class_name'];
            $model              = $this->getModel($className);
            $columnName         = $params['column_name'];
            $currentColumnName  = $params['current_column_name'] ?? $columnName;
            $whereParams        = $params['where_params'] ?? array();
            $classFullTableName = $model->getDatabase()->getName().'.'.$model->getTableName();

            if ((!in_array($key, $relations) && !isset($relations[$key]) ) && !in_array($model->getTableName(),
                    $relations)) {
                continue;
            }

            $joinType    = isset($relations[$key]) ? $relations[$key]['type'] : $defaultJoinType;
            $joinCounter = isset($relations[$key]) ? $relations[$key]['join_counter'] : 1;

            if ($joinCounter > 1) {
                for ($i = 0; $i < $joinCounter; $i++) {
                    $aliasName = $classFullTableName.'_'.$i;
                    $joinWhere = $aliasName.'.'.$columnName.'='.$this::getTableName().'.'.$currentColumnName;
                    foreach ($whereArray as $whereKey => $whereVal) {
                        $whereKeyBinding = ':'.str_replace(".", "_", $whereKey);
                        if (strpos($whereKey, $aliasName) !== false) {
                            $joinWhere .= ' and '.$whereKey.'='.$whereKeyBinding;
                        }
                    }
                    $joinsArray[] = array($joinType, array($model->getTableName() => $classFullTableName.'_'.$i),
                        $joinWhere);
                }
            } else {
                $joinsArray[] = array($joinType, array($model->getTableName() => $classFullTableName),
                    $model->getTableName().'.'.$columnName.'='.$this::getTableName().'.'.$currentColumnName);
            }
            self::addTableDot($whereArray, $whereParams, $model->getTableName());
        }
    }

    /**
     * Split query result to objects
     * @param array $rows - query result
     * @param array $relations - the relations for object selection
     * @return \Arbel\Base\currentClassName
     */
    public function splitSqlResultToObjects(array $rows, array $relations)
    {
        $currentClassName = get_class($this);
        $result           = array();
        foreach ($rows as $objectsData) {
            $object = $this->getModel($currentClassName)->setData($objectsData[$currentClassName::getTableName()]);
            foreach ($object->getAllRelationsList() as $key => $params) {
                if ((!in_array($key, $relations) && !isset($relations[$key]))) {
                    continue;
                }
                $className      = $params['class_name'];
                $tableName      = $this->getModel($className)->getTableName();
                $relationObject = $this->getModel($className)->setData($objectsData[$tableName]);
                if ($relationObject->getId()) {
                    if ($object->getLazyLoadinRelationList($key)) {
                        $object->setArrayData($key, $relationObject->getId(), $relationObject);
                    } else {
                        $object->set($key, $relationObject);
                    }
                }
            }
            $result[$object->getId()] = $object;
        }
        return $result;
    }

    /**
     * Add table name to the where params columns
     * @param type $whereArray
     * @param array $params
     * @param type $tableName
     */
    public static function addTableDot(&$whereArray, array $params, $tableName)
    {
        foreach ($params as $keyWhere => $valWhere) {
            if (strpos($keyWhere, '.') === false) {
                if (is_numeric($keyWhere) && is_array($valWhere)) {
                    foreach ($valWhere as $k => $v) {
                        $keyWhereAlias              = $tableName.'_'.$keyWhere.'.'.$k;
                        $whereArray[$keyWhereAlias] = $v;
                    }
                    continue;
                }if (is_numeric($keyWhere)) {
                    $whereArray[] = $valWhere;
                } else {
                    $keyWhere = $tableName.'.'.$keyWhere;
                }
            }
            $whereArray[$keyWhere] = $valWhere;
        }
    }

    /**
     * General function that create instances
     * @param array $data
     * @return type
     */
    public function factory(array $data = array())
    {
        return $this->setData($data);
    }

    /**
     * Get object manager
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        if (!$this->objectManager && $this->serviceManager) {
            $this->objectManager = $this->serviceManager->get('Di');
        }
        return $this->objectManager;
    }

    /**
     * Get object manager
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * Alias name for get object manager
     * @param string $modelName
     * @return ModelAbstract
     */
    public function getModel(string $modelName)
    {

        return $this->getObjectManager()->get($modelName);
    }
}