<?php

namespace Arbel\Base;

use Illuminate\Support\Arr;
use Arbel\Base\Session;

/**
 * Base class that have every basic functionality
 * that need to be in class
 * @version 0.0.6
 */
class Element
{
    /**
     * Data array
     * @var array
     */
    protected $data = array();

    /**
     * Save in session flag
     * @var bool
     */
    protected $saveInSession = false;

    /**
     * Session container name
     * @var string
     */
    protected $sessionName = '';

    /**
     * Session object
     * @var Session
     */
    protected $session;
    static protected $instance        = null;
    static protected $currentInstance = array();
    static protected $instanceGroups  = array();

    /**
     * Init the object
     * Have 2 options:
     * @param array or string $data - set data array
     * @param string $session - set session name and activate the session stored
     */
    function __construct($data = null, string $sessionName = null)
    {
        if (!is_null($data)) {
            $this->initData($data);
        }
        if (!is_null($sessionName) || $this->saveInSession) {
            $this->initSession($sessionName);
        }
    }

    /**
     * Init session
     * @param string $sessionName
     */
    public function initSession(string $sessionName = null)
    {
        if (is_null($sessionName)) {
            $sessionName = empty($this->sessionName) ? get_class($this) : $this->sessionName;
        } else {
            $this->sessionName = $sessionName;
        }
        $this->saveInSession = true;
        $this->session       = new Session($sessionName);
        $this->initDataFromSession();
    }

    /**
     * reset object data
     */
    function reset()
    {
        $this->setData(array());
    }

    /**
     * 
     * @param mixed - array/string $params
     * @return \Arbel\Base\className
     */
    static function getNewObjegt($params = null)
    {
        $className = get_called_class();
        return new $className($params);
    }

    /**
     * Set the session name and activate the session stored
     * @param string $sessionName
     */
    function setSessionName(string $sessionName)
    {
        $this->sessionName = $sessionName;
    }

    /**
     * Check if we have session in server
     * @return boolean
     */
    function haveSession()
    {
        return isset($this->session);
    }

    /**
     * Check if need to save data in the session
     * @return bool
     */
    function needToSaveInSession(): bool
    {
        return $this->saveInSession;
    }

    /**
     * Init the class data
     * @param array $array
     * @return $this;
     */
    function initData(array $array)
    {
        $this->data = $array;
        if ($this->needToSaveInSession()) {
            $this->updateSession();
        }
        return $this;
    }

    /**
     * Init data data from session
     */
    function initDataFromSession()
    {
        $this->data = $this->session->getData();
    }

    /**
     * Update the session by current data
     */
    function updateSession()
    {
        $this->session->setData($this->data);
    }

    /**
     * Get value from data by key
     * @param string $key
     * @param $default value
     * @return value or null if not exist
     */
    public function get(string $key, $default = null)
    {
        $this->checkKeyValidation($key);

        if ($this->isProperty($key) && $this->isPublic($key)) {
            return $this->$key;
        }

        return Arr::get($this->data, $key, $default);
    }

    /**
     * Check if property is exist
     * @param string $varibaleName
     * @return bool
     */
    public function isProperty(string $varibaleName): bool
    {
        return property_exists($this, $varibaleName);
    }

    /**
     * Check if property is public
     * @param string $varibaleName
     * @return bool
     */
    public function isPublic(string $varibaleName): bool
    {
        $result = false;
        try {
            $reflection = new \ReflectionProperty($this, $varibaleName);
            $result     = $reflection->isPublic();
        } catch (Exception $exc) {
            \Log::info($exc->getTraceAsString());
        }
        return $result;
    }

    /**
     * Validate access to property
     * @param string $varibaleName
     * @return boolean
     * @throws Exception
     */
    public function validateAccess(string $varibaleName): bool
    {
        if (!$this->isPublic($varibaleName)) {
            throw new \Exception(" Can't Acess {$varibaleName} because is not public ");
        }
        return true;
    }

    /**
     * Check key validation
     * @param string $key
     * @throws \Exception
     */
    public function checkKeyValidation(string $key)
    {
        if (!preg_match("/^[a-zA-Z_0-9.]+$/", $key)) {
            throw new \Exception('Error - not valid key - '.print_r($key));
        }
    }

    /**
     * Check if have value by key
     * @param string $key
     * @return boolean
     */
    public function have(string $key): bool
    {
        return is_array($this->data) && array_key_exists($key, $this->data);
    }

    /**
     * Set value by key
     * @param string $key
     * @param mixed $val
     * @return $this
     */
    public function set(string $key, $val)
    {
        $this->checkKeyValidation($key);

        if ($this->isProperty($key) && $this->isPublic($key)) {
            $this->$key = $val;
        } else {
            Arr::set($this->data, $key, $val);
            if ($this->needToSaveInSession()) {
                $this->updateSession();
            }
        }
        return $this;
    }

    /**
     * Remove value by key
     * @param string $key
     */
    public function remove($key)
    {
        if (isset($this->data[$key])) {
            unset($this->data[$key]);
        }
    }

    /**
     * Add new data only
     * @param array $newData
     */
    public function updateData(array $newData)
    {
        return $this->initData(array_merge($this->data, $newData));
    }

    /**
     * Get the data or by value or all data
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function getData(string $key = null, $default = null)
    {

        if (!is_null($key)) {
            return $this->get($key, $default);
        }
        return $this->data;
    }

    /**
     * Remove the objects from the wanted data
     * @param string $key
     * @param type $default
     * @return type
     */
    public function getNoneObjectsData(string $key = null, $default = null,
                                      $showAll = false)
    {
        $data = $this->getData($key, $default);
        if (!isset($key) && !isset($default) && is_array($data) && !$showAll) {
            foreach ($data as $key => $val) {
                if (is_object($val)) {
                    unset($data[$key]);
                }
            }
        }
        return $data;
    }

    /**
     * Set the data or by value or all data
     * @param mixed $key or array
     * @return $this
     */
    public function setData($data, $value = null)
    {

        if (is_array($data)) {
            return $this->initData($data);
        } else{
            return $this->set($data, $value);
        }
    }

    /**
     * Add new data only
     * @param mixed $key string or array
     * @param mixed $value string or null
     * @return mixed
     */
    public function addData($data, $value = null)
    {
        if (is_array($data)) {
            return $this->initData(array_merge($data, $this->data));
        } elseif (!is_null($value)) {
            return $this->set($data, $value);
        }
    }

    /**
     * Magic method of setting data
     * @param string $key
     * @param mixed $val
     * @return $this
     */
    public function __set(string $key, $val)
    {
        return $this->set($key, $val);
    }

    /**
     * Magic method of getting data
     * @param string $key
     * @return mixed
     */
    public function __get(string $key)
    {
        return $this->get($key);
    }

    /**
     * Check if have data
     * @param string key
     * @return boolean
     */
    public function isEmpty(string $key = null)
    {
        if (isset($key)) {
            $value = $this->get($key);
            return empty($value);
        } else {
            return empty($this->data);
        }
    }

    /**
     * Get values by keys
     * @param array $values
     * @param array $data - optional array to search from
     */
    public function getValuesByKeys(array $keys, array $data = array()): array
    {
        $result = array();
        if (empty($data)) {
            $data = $this->data;
        }
        foreach ($keys as $key) {
            if (array_key_exists($key, $data)) {
                $result[$key] = $data[$key];
            } else {
                $result[$key] = null;
            }
        }
        return $result;
    }

    /**
     * Duplicate this object
     * @param array $newData
     * @return \Arbel\Base\Element
     */
    public function duplicate(array $newData = array())
    {
        $object = clone $this;
        $object->updateData($newData);
        return $object;
    }

    /**
     * Get the non empty value or the default
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function getNoneEmpty(string $key, $default)
    {
        $value = $this->get($key);
        return empty($value) ? $default : $value;
    }

    /**
     * Get instance of current object
     * @param string/null $groupName
     * @return singelton of current class
     */
    static function instance($groupName = null)
    {
        if (empty($groupName)) {
            $className = get_called_class();
            if (empty(self::$currentInstance) || !isset(self::$currentInstance[$className])) {
                self::$currentInstance[$className] = new $className;
            }
            return self::$currentInstance[$className];
        } else {
            if (isset(self::$instanceGroups[$groupName])) {
                return self::$instanceGroups[$groupName];
            } else {
                $className                        = get_called_class();
                self::$instanceGroups[$groupName] = new $className;
                return self::$instanceGroups[$groupName];
            }
        }
    }

    /**
     * Set array data
     * @param string $groupKey
     * @param string $key
     * @param string $val
     */
    public function setArrayData($groupKey, $key, $val)
    {
        if (!isset($this->data[$groupKey]) || !is_array($this->data[$groupKey])) {
            $this->data[$groupKey] = array();
        }
        $groupData             = $this->data[$groupKey];
        $groupData[$key]       = $val;
        $this->data[$groupKey] = $groupData;
    }

    /**
     * Add more values to sub array data
     * @param string $key
     * @param string $val
     */
    public function add($key, $val)
    {
        if (isset($this->data[$key]) && is_array($this->data[$key])) {
            $this->data[$key][] = $val;
        }
    }

    /**
     * Get array data
     * @param string $groupKey
     * @param string/null $key
     * @return array / null
     */
    public function getArrayData($groupKey, $key = null)
    {
        $groupData = $this->get($groupKey, array());
        if (!is_null($key) && isset($groupData[$key])) {
            return $groupData[$key];
        } elseif (is_null($key)) {
            return $groupData;
        } else {
            return null;
        }
    }

    /**
     * Dynamic method for set / get / remove functions
     * @param string $methodName
     * @param array/null $params
     * @return boolean
     * @throws Exception
     */
    public function __call($methodName, $params = null)
    {
        $pieces = preg_split('/(?=[A-Z])/', $methodName);
        if (isset($pieces[0])) {
            $methodPrefix = substr($pieces[0], 0, 3);
            unset($pieces[0]);
        } else {
            return false;
        }
        $key = strtolower(implode("_", $pieces));


        if ($methodPrefix == 'set') {

            if (is_array($params) && empty($params)) {
                $params = null;
            }

            if (is_array($params) && (isset($params[0]) || (count($params) > 0 && key($params)
                == 0))) {
                $params = $params[0];
            }


            return $this->set($key, $params);
        } elseif ($methodPrefix == 'get') {
            if (isset($params[0]) || (count($params) > 0 && key($params) == 0)) {
                $params = $params[0];
            }

            if (is_array($params) && empty($params)) {
                $params = null;
            }
            return $this->get($key, $params);
        } elseif ($methodPrefix == 'add') {
            if (isset($params[0]) || (count($params) > 0 && key($params) == 0)) {
                $params = $params[0];
            }

            if (is_array($params) && empty($params)) {
                $params = null;
            }
            return $this->add($key, $params);
        } elseif ($methodPrefix == 'remove') {
            if (isset($params[0]) || (count($params) > 0 && key($params) == 0)) {
                $params = $params[0];
            }
            if (is_array($params) && empty($params)) {
                $params = null;
            }
            return $this->remove($key);
        } elseif ($methodPrefix == 'has') {
            if (isset($params[0]) || (count($params) > 0 && key($params) == 0)) {
                $params = $params[0];
            }
            if (is_array($params) && empty($params)) {
                $params = null;
            }
            return $this->have($key);
        } elseif ($methodPrefix == 'increment') {
            if (isset($params[0]) || (count($params) > 0 && key($params) == 0)) {
                $params = $params[0];
            }
            if (is_array($params) && empty($params)) {
                $params = null;
            }
            return $this->increment($key);
        } else {
            throw new \Exception('Opps! The method '.get_class($this).'::'.$methodName.' was not defined!');
        }
    }

    /**
     * Increment value
     * @param type $key
     * @return type
     */
    public function increment($key)
    {
        return $this->set($key,$this->get($key) + 1);
    }
}