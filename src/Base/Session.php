<?php

namespace Arbel\Base;

use Zend\Session\Container;
use Zend\Stdlib\ArrayObject;


/**
 * Session class
 * 
 */
class Session
{
    /**
     * Data of array
     * @var array
     */
    protected $data = array();

    /**
     * Session Container
     * @var Container
     */
    protected $container;

    function __construct($sessionName = 'defualt')
    {
        $this->container = new Container($sessionName);
    }

    /**
     * Magic method that create dynamic method
     * @param string $methodName
     * @param mixed $params
     * @return boolean
     * @throws Exception
     */
    public function __call($methodName, $params = null)
    {
        $pieces = preg_split('/(?=[A-Z])/', $methodName);
        if (isset($pieces[0])) {
            $methodPrefix = substr($pieces[0], 0, 3);
            unset($pieces[0]);
        } else {
            return false;
        }
        $key = strtolower(implode("_", $pieces));


        if ($methodPrefix == 'set') {

            if (isset($params[0]) || (count($params) > 0 && key($params) == 0)) {
                $params = $params[0];
            }

            if (is_array($params) && empty($params)) {
                $params = null;
            }
            return $this->set($key, $params);
        } elseif ($methodPrefix == 'get') {
            if (isset($params[0]) || (count($params) > 0 && key($params) == 0)) {
                $params = $params[0];
            }
            if (is_array($params) && empty($params)) {
                $params = null;
            }
            return $this->get($key, $params);
        } elseif ($methodPrefix == 'remove') {
            if (isset($params[0]) || (count($params) > 0 && key($params) == 0)) {
                $params = $params[0];
            }
            if (is_array($params) && empty($params)) {
                $params = null;
            }
            return $this->remove($key);
        } else {
            throw new Exception('Opps! The method is not defined!');
        }
    }

    /**
     * Get value from data by key
     * @param string $key
     * @param $default value
     * @return boolean
     */
    public function get(string $key, $default = null)
    {

        if (isset($this->container->$key)) {
            return $this->container->$key;
        } else if (!is_null($default)) {
            return $default;
        } else {
            return false;
        }
    }

    /**
     * Set value by key
     * @param string $key
     * @param mixed $val
     * @return $this
     */
    public function set(string $key, $val)
    {
        $this->container->$key = $val;
        return $this;
    }

    /**
     * Megic method
     * @param string $key
     * @param mixed $val
     * @return $this
     */
    public function __set(string $key, $val)
    {
        return $this->set($key, $val);
    }

    /**
     * Megic method
     * @param string $key
     * @return mixed
     */
    public function __get(string $key)
    {
        return $this->get($key);
    }

    /**
     * Get array copy of session
     * @return array
     */
    public function getData()
    {
        return $this->container->getArrayCopy();
    }

    /**
     * Get array copy of session
     * @return array
     */
    public function setData(array $data)
    {
        return $this->container->exchangeArray($data);
    }
}