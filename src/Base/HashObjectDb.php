<?php

namespace Arbel\Base;

/**
 * General Object for db store data use and hash key handling
 * 
 */
abstract class HashObjectDb extends ModelAbstract
{
    protected $hashFunctionName         = 'hashArray';
    protected $generateHashFunctionName = 'generateHash';
    protected $columnsForHashing        = array();

    /**
     * Get hash column name
     * @return string - column name
     */
    abstract public static function getHashColumn(): string;

    /**
     * Override parent save function and add generate hash key before
     * the action
     * @param bool - is to update when key exist
     * @return this
     */
    public function save(bool $isInsertOrUpdate = false)
    {

        if (!$this->get($this->getHashColumn())) {
            $this->set($this->getHashColumn(), $this->generateHash());
        } elseif (!$this->getId()) {
            $this->initIdByHash();
        }
        return parent::save($isInsertOrUpdate);
    }

    /**
     * Init id by hash
     */
    public function initIdByHash()
    {
        $data = $this->getDatabase()
            ->load($this->getTableName(), $this->getHashColumn(),
            $this->getHash());
        if(!empty($data)){
            $row = current($data);
            $id = $row[$this->getPrimaryKey()];
            $this->setId($id);
        }
    }

    /**
     * Generate hashing by array
     * If no params then generate new random hash
     * @param array $hashingArray - optional - array of values
     * @return string
     */
    public function generateHash(array $hashingArray = array()): string
    {
        $hashFunction = $this->hashFunctionName;
        if (empty($hashingArray)) {
            if (empty(!$this->columnsForHashing)) {
                $hashingArray = $this->columnsForHashing;
            } elseif (!empty($this->uniqueTableColumn)) {
                $hashingArray = $this->uniqueTableColumn;
            } else {
                $hashFunction = $this->generateHashFunctionName;
            }
        }

        $valuesHashingArray = $this->getValuesByKeys($hashingArray);
        $paramForFunction   = empty($valuesHashingArray) ? array() : array($valuesHashingArray);
        return call_user_func_array($hashFunction, $paramForFunction);
    }

    /**
     * Load widget by hash
     * @param string $hash - hash
     * @return \Asset
     */
    public function loadByHash($hash)
    {
        return $this->loadByKey($this->getHashColumn(), $hash);
    }

    /**
     * Init object by hash
     * @param string $hash
     * @return $object
     */
    public static function initByHash(string $hash)
    {
        $className = get_called_class();
        return $className::initByKey($className::getHashColumn(), $hash);
    }

    /**
     * Duplicate object and remove hash key
     * @param array $newData
     * @return $object
     */
    public function duplicate(array $newData = array())
    {
        $object = parent::duplicate($newData);
        $object->remove($this->getHashColumn());
        return $object;
    }

    /**
     * Get hash
     * @return string - hash
     */
    public function getHash()
    {
        return $this->get($this->getHashColumn());
    }

    /**
     * Get hash
     * @return $this
     */
    public function setHash(string $hash)
    {
        return $this->set($this->getHashColumn(), $hash);
    }

    /**
     * Add hash column to unique columns if not exist
     */
    public function addHashToUnique()
    {
        if (!in_array($this->getHashColumn(), $this->uniqueTableColumn)) {
            $this->uniqueTableColumn[] = $this->getHashColumn();
        }
    }

    /**
     * Get changed data only
     * And remove hash column
     * @return array
     */
    public function getChangedData(): array
    {
        $result = parent::getChangedData();
        if(isset($result[$this->getHashColumn()])){
            unset($result[$this->getHashColumn()]);
        }
        return $result;
    }
}