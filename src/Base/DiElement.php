<?php
/**
 * Statistics class
 */
namespace Arbel\Base;

use Arbel\Database\DatabaseInterface;
use Arbel\ObjectManager;
use Zend\ServiceManager\ServiceManager;
use Arbel\Database\Where;
use Arbel\Model\Translator;
use Arbel\Config;
use Arbel\Log;


class DiElement extends Element {

    /**
     * Database object
     * @var Database
     */
    protected $database;

    /**
     * ServiceManager object
     * @var ServiceManager
     */
    protected $serviceManager;
    protected $addRelationsOnSave = array();

    /**
     * Init the object
     * @param array/int $data
     * @param string $session - session name
     */
    public function __construct(DatabaseInterface $database, ObjectManager $objectManager, ServiceManager $sm) {
        $this->database = $database;
        $this->objectManager = $objectManager;
        $this->serviceManager = $sm;
        $this->initialize();
    }
        /**
     * Config helper
     */
    protected function getConfig() {
        return $this->getModel(Config::class);
    }

    /**
     * Get log helper function
     * @return Log
     */
    protected function log() : Log{
        return $this->getModel(Log::class);
    }


    /**
     * Get object manager
     * @return ObjectManager
     */
    public function getObjectManager() {
        if (!$this->objectManager && $this->serviceManager) {
            $this->objectManager = $this->serviceManager->get('Di');
        }
        return $this->objectManager;
    }

    /**
     * Get object manager
     * @return ServiceManager
     */
    public function getServiceManager() {
        return $this->serviceManager;
    }

    /**
     * Alias name for get object manager
     * @param string $modelName
     * @return ModelAbstract
     */
    public function getModel(string $modelName) {

        return $this->getObjectManager()->get($modelName);
    }

    public function initialize()
    {
        
    }

}

