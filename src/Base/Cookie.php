<?php

namespace Arbel\Base;

use Zend\Session\Container;
use Zend\Stdlib\ArrayObject;


/**
 * Session class
 *
 */
class Cookie
{
    /**
     * Data of array
     * @var array
     */
    protected $data = array();

    /**
     * Session Container
     * @var Container
     */
    protected $container;

    function __construct($sessionName = 'defualt')
    {
        $this->container = new Container($sessionName);
    }

    /**
     * Magic method that create dynamic method
     * @param string $methodName
     * @param mixed $params
     * @return boolean
     * @throws Exception
     */
    public function __call($methodName, $params = null)
    {
        $pieces = preg_split('/(?=[A-Z])/', $methodName);
        if (isset($pieces[0])) {
            $methodPrefix = substr($pieces[0], 0, 3);
            unset($pieces[0]);
        } else {
            return false;
        }
        $key = strtolower(implode("_", $pieces));


        if ($methodPrefix == 'set') {

            if (isset($params[0]) || (count($params) > 0 && key($params) == 0)) {
                $params = $params[0];
            }

            if (is_array($params) && empty($params)) {
                $params = null;
            }
            return $this->set($key, $params);
        } elseif ($methodPrefix == 'get') {
            if (isset($params[0]) || (count($params) > 0 && key($params) == 0)) {
                $params = $params[0];
            }
            if (is_array($params) && empty($params)) {
                $params = null;
            }
            return $this->get($key, $params);
        } elseif ($methodPrefix == 'remove') {
            if (isset($params[0]) || (count($params) > 0 && key($params) == 0)) {
                $params = $params[0];
            }
            if (is_array($params) && empty($params)) {
                $params = null;
            }
            return $this->remove($key);
        } else {
            throw new Exception('Opps! The method is not defined!');
        }
    }

    /**
     * Get value from data by key
     * @param string $key
     * @param $default value
     * @return boolean
     */
    public function get(string $key, $default = null)
    {

        if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        } else if (!is_null($default)) {
            return $default;
        } else {
            return null;
        }
    }

    public function remove($key)
    {
        if(isset($_COOKIE[$key])){
            setcookie($key, '', time() - 3600);
            return true;
        }
        return false;
    }

    /**
     * Set value by key
     * @param string $key
     * @param mixed $val
     * @return $this
     */
    public function set(string $key, $val,$time=null)
    {
         if(!$time){
            $time = time() + 365 * 60 * 60 * 24;
        }
        setcookie($key, $val, $time,'/'); // now + 1 year

        return $this;
    }

    /**
     * Megic method
     * @param string $key
     * @param mixed $val
     * @return $this
     */
    public function __set(string $key, $val)
    {
        return $this->set($key, $val);
    }

    /**
     * Megic method
     * @param string $key
     * @return mixed
     */
    public function __get(string $key)
    {
        return $this->get($key);
    }

    /**
     * Get array copy of session
     * @return array
     */
    public function getData()
    {
        return $_COOKIE;
    }

    /**
     * Get array copy of session
     * @return array
     */
    public function setData(array $data,$time = null)
    {
        if(!$time){
            $time = time() + 365 * 60 * 60 * 24;
        }
        foreach($data as $key => $val){
          $cookie = setcookie($key, $val, $time,'/'); // now + 1 year
        }

        return $this;
    }
}