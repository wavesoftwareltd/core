<?php

namespace Arbel\OAuth2;

class Pdo extends \OAuth2\Storage\Pdo {
    
    public function __construct($connection, $config = array()) {
        parent::__construct($connection, $config);
    }
}