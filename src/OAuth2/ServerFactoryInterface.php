<?php

namespace Arbel\OAuth2;

use Arbel\Service\Interfaces\CallbackInterface;

interface ServerFactoryInterface extends CallbackInterface {
    
}