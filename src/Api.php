<?php

namespace Arbel;

use Arbel\Base\DiElement;
use Arbel\Helper;

/**
 * General Object for db store data use
 *
 */
abstract class Api extends DiElement
{
    const GET_TYPE = 'GET';
    const POST_TYPE = 'POST';
    const DELETE_TYPE = 'DELETE';
    const PUT_TYPE = 'PUT';
    
    static $callsHistory = [];

    protected $key;
    protected $url;

    public function initConfig(string $url, string $key)
    {
        $this->url = $url;
        $this->key = $key;
    }
    
    public function callOnce(string $path, array $params = [], string $method = self::GET_TYPE) {
        if(!isset(self::$callsHistory[$this->getCallKey($path,$params,$method)])){
           self::$callsHistory[$this->getCallKey($path,$params,$method)] = $this->call($path,$params,$method);
        }else{
           $this->getModel(\Arbel\Model\Log::class)->info('api_history_request/'.time(), [
            'url' => $this->getFinalUrl($path),
            'path' => $path,
            'params' => $params
        ]);
        }
            return self::$callsHistory[$this->getCallKey($path,$params,$method)];
    }
    
    public function getCallKey(string $path, array $params = [], string $method = self::GET_TYPE){
        return $path.'_'.json_encode($params).'_'.$method;
    }

    public function call(string $path, array $params = [], string $method = self::GET_TYPE)
    {
        $time = time();
        $this->getModel(\Arbel\Model\Log::class)->info('api_request/'.$time, [
            'url' => $this->getFinalUrl($path),
            'path' => $path,
            'params' => $params
        ]);
        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization:Bearer '.$this->key
        ));
        curl_setopt($ch, CURLOPT_URL, $this->getFinalUrl($path));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_REFERER, $this->getFinalUrl($path));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        $this->getModel(\Arbel\Model\Log::class)->info('api_response/'.$time, [
            'result' => $result,
            'info' => $info
        ]);
        curl_close($ch);
        if(Helper::isJson($result)){
            $result = json_decode($result,true);
            $this->setIsValid(true);
        }else{
            $this->setIsValid(false);
        }
        return $result;
    }

    public function getFinalUrl(string $path)
    {
        return $this->url.'/'.$path;
    }
}