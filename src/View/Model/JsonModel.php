<?php


namespace Arbel\View\Model;

use Zend\Json\Json;

class JsonModel extends \Zend\View\Model\JsonModel {

    /**
     * Is this a standalone, or terminal, model?
     *
     * @var bool
     */
    protected $terminate = true;

     /**
     * Serialize to JSON
     *
     * @return string
     */
    public function serialize()
    {
        $variables = $this->getVariables();
        if ($variables instanceof Traversable) {
            $variables = ArrayUtils::iteratorToArray($variables);
        }
 
        $encodeOptions = JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP;

        if ($this->getOption('prettyPrint')) {
            $encodeOptions |= JSON_PRETTY_PRINT;
        }
        
        $this->toNumbers($variables);

        if (null !== $this->jsonpCallback) {
            return $this->jsonpCallback.'('.json_encode($variables, $encodeOptions).');';
        }
        return json_encode($variables, $encodeOptions);
    }

    public function toNumbers(&$variables)
    {
        foreach($variables as &$val){
            if(is_array($val)){
                $this->toNumbers($val);
            }elseif(is_numeric($val) && !preg_match("~^0\d+$~", $val) && !is_null($val)){
                $val = $val*1;
            }
        }
    }

    public function getFinalOptions() : array
    {
        return [
            'prettyPrint' => $this->getOption('prettyPrint'),
        ];
    }

}

