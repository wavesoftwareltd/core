<?php

namespace Arbel;

use Arbel\Base\Element;


/**
 * Log class
 * Warp Monolog methods
 * @version 1.0.0
 */
class Test extends Element
{
    protected $seesionName = 'new_test3445';
    
    public function __construct($data = null)
    {
        parent::__construct($data, $this->seesionName);
    }
}