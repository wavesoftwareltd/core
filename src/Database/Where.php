<?php

namespace Arbel\Database;

use Arbel\Base\Element;

class Where extends Element{
    
    const AND_FLAG = 'and';
    const OR_FLAG = 'or';
    
    protected $type = self::AND_FLAG;

    public function __construct($data = null, $type = self::AND_FLAG)
    {
        parent::__construct($data);
        $this->setType($type);
    }

    /**
     * Add or
     * @return Where
     */
    public function &addOr() : Where {
        $where = new Where();
        $where->setType($this::OR_FLAG);
        $index = count($this->data);
        $this->data[$index] = $where;
        return $this->data[$index];
    }
    
    /**
     * Get type
     * @return type
     */
    public function getType() {
        return $this->type;
    }
    
    /**
     * Set type
     * @param string $type
     */
    public function setType(string $type) {
        $this->type = $type;
    }
    
}