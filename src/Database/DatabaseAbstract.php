<?php

namespace Arbel\Database;

use Zend\Db\Sql\Sql;
use Arbel\Base\Element;
use Arbel\Base\Sql\SqlHandler;
use Zend\Db\Adapter\Adapter;
use Arbel\Log;
use PDO;
use Zend\Db\Adapter\Driver;
use Illuminate\Database\Capsule\Manager as Capsule;
use Arbel\Database\Schema\Blueprint;
use Arbel\Database\Schema\Eav;
use Arbel\Database\Schema\MySqlGrammar;
use Zend\Cache\Storage\StorageInterface;
use Arbel\Cache\CacheManager;
use Zend\ServiceManager\ServiceManager;

abstract class DatabaseAbstract extends Element implements DatabaseInterface {
    
    const JSON_TYPE                     = 'json';
    const ENUM_TYPE                     = 'enum';
    const SET_TYPE                     = 'set';
    const PREFIX_TABLE_SEPERATE         = '_table_';
    const PREFIX_TABLE_SEPERATE_COUNTER = '_counter_';
    const CACHE_CODE = 'database';
    
    static $columnsFromTables = [];

    /**
     * Allowed operations for where condition
     * @var type
     */
    protected $allowedOp                = array(
        'in', 'is', 'is not', 'not in', 'is null', '=', '<', '>', '<>', '!=', 'is not null'
    );
    private $blacklist_columns        = array(
        'updated_at',
        'created_at'
    );
    static protected $instance        = null;
    static protected $currentInstance = array();
    static protected $instanceGroups  = array();

    /** @var $PDO PDO */
    private $PDO;

    /**
     * Zend adapter
     * @var Adapter
     */
    protected $adapter;

    /**
     * Illuminate Database Manager
     * @var Capsule
     */
    protected $capsule;

    /**
     * Log object
     * @var Log
     */
    protected $log;
    
    /**
     * Log object
     * @var Log
     */
    protected $cache;

    /**
     * sql history
     * @var array
     */
    protected $sqlHistory = array();
    
    /**
     * ServiceManager object
     * @var ServiceManager
     */
    protected $serviceManager;

     public function __construct(Adapter $adapter, Log $log, Capsule $capsule,StorageInterface $cache, ServiceManager $sm)
    {
        $this->adapter = $adapter;
        $this->cache = $cache;
        $this->log     = $log;
        $this->capsule = $capsule;
        $this->serviceManager = $sm;
    }
    
    
    /**
     * Get object manager
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    public function &getCache() {
        return $this->sqlHistory;
    }


    abstract function getGrammar();
    

    abstract function isAllowOnUpdate();
    /**
     * Get Schema
     * @return Schema
     */
    public function getSchema($connection = null)
    {
        $connection = $this->capsule->getConnection($connection);
        $connection->setSchemaGrammar($this->getGrammar());
        $builder    = $connection->getSchemaBuilder();
        $builder->blueprintResolver(function($table, $callback) use ($connection) {
            $blueprint = new Blueprint($table, $callback);
            $blueprint->setAllowOnUpdate($this->isAllowOnUpdate());
            $blueprint->setConnection($connection);
            return $blueprint;
        });
        return $builder;
    }

    /**
     * Inject objects
     * @param StorageInterface $cache
     */
    public function setInjections(CacheManager $cacheManager)
    {
        $this->cacheManager = $cacheManager;
    }

    public function getEav()
    {
        return new Eav($this);
    }

    /**
     * Execute an db query
     *
     * This lastStatement will contain the statement
     * This lastRowCount will have the rowCount
     *
     * @param string $sql raw query to run
     * @param array $params binding params key value param name to value of param
     * @throws mysqli_sql_exception  on failure to execute provided SQL
     * @return array[] - query results of fetchall if doing a SELECT
     */
    function exec($sql, $params = array(),$limit = null)
    {
        if($limit){
            $this->addLimitToQuery($sql, $limit);
        }
        $result = array();
        $this->paramsModify($params);
        $stmt   = $this->adapter()->createStatement($sql, $params);

        try {
            $queryResult = $stmt->execute();
        } catch (Exception $exc) {

            $this->LogError($sql, $exc->getTraceAsString());
            throw new mysqli_sql_exception(print_r($exc->errorInfo(), true));
        }

        if ($queryResult->isQueryResult()) {
            $result = $this->fetchAll($queryResult);
        }

        $this->setLastStatement($stmt);
        $this->setLastRowCount(count($result));
        $this->modifyQueryResult($result);

        return $result;
    }

    public function modifyQueryResult(&$result)
    {
   
    }

    /**
     * convert Zend Sql object to array
     * @param \Zend\Db\Adapter\Driver\Pdo\Result $queryResult
     * @return array of data colname => value
     */
    public function fetchAll(\Zend\Db\Adapter\Driver\Pdo\Result $queryResult)
    {
        $result = array();
        foreach ($queryResult as $key => $value) {
            $result[$key] = $value;
        }
        return $result;
    }

    public function checkKeyValidation(string $key)
    {
        if (empty($key) && !preg_match("/^[a-zA-Z_0-9]+$/", $key)) {
            throw new\ Exception('Error - not valid key - '.print_r($key));
        }
    }


    /**
     * Load row from table
     * @param string $tableName - table name
     * @param string/array $key - column or params on where to select
     * @param type $val - column value on where to select
     * @return array - query result
     */
    abstract function load($tableName, $key, $val = null);


    /**
     * Delete row from db
     * @param string $tableName - table name
     * @param string|array $key - column on where to delete
     * @see Database::paramsToWhereBindingString() for array format to use
     * @param string $val - column value on where to delete
     *
     */
    function delete(string $tableName, $key, string $val = null)
    {
        if (is_array($key)) {
            $params = $key;
            $sql    = "delete from `{$tableName}` where ";
            $sql    .= $this->paramsToWhereBindingString($params, $tableName);
        } else {
            $params = array(
                'val' => $val
            );
            $this->checkKeyValidation($tableName);
            $this->checkKeyValidation($key);
            $sql    = "delete from `{$tableName}` where {$key} = :val";
        }
        $this->exec($sql, $params);
    }

    /**
     * Update row on the db
     * @param string $tableName - table name
     * @param string $whereParams - column on where to update
     * @see Database::paramsToWhereBindingString()
     * @param array $params - params to update
     * @see Database::paramsToBindingString()
     */
    function update($tableName, array $whereParams, array $params)
    {

        $this->filterBlacklistParams($params);
        $sql    = "update `{$tableName}` set";
        $sql    .= $this->paramsToBindingString($params);
        $sql    .= " where ";
        $sql    .= $this->paramsToWhereBindingString($whereParams, $tableName);
        $params = array_merge($params, $whereParams);
        $this->paramsModify($params,$tableName,true);
        $this->exec($sql, $params);
    }


    /**
     * Get all rows form table
     * @param type $tableName - table name
     * @param array | Where $params - conditions params
     * @param array $joinParams - example - array (array('join','table as t', 't.a = main_s.b'))
     * @param int $limit - limit result
     * @param bool $isSelectAllFromJoinTable - if to retrieve all join tables data
     * @param array $whereCount - where counter array
     * @param string $groupBy - group by string
     * @return array query result
     */
    abstract function getAll($tableName, $params = array(), $orderBy = null,
                    array $joinParams = null, int $limit = null,
                    bool $isSelectAllFromJoinTable = false,
                    array $whereCount = array(), string $groupBy = null,
                    int $offset = null);

    abstract function addLimitToQuery(&$query,$limit);

    /**
     * Convert params to where count string
     * @param array $whereCount
     * @return string - where string
     */
    public function paramsToWhereCountString(array $whereCount)
    {
        $newWhere = array();
        $prefix   = $this::PREFIX_TABLE_SEPERATE_COUNTER;
        foreach ($whereCount as $key => $val) {
            $newKey            = str_replace('.', $prefix, $key);
            $newWhere[$newKey] = $val;
        }
        return $this->paramsToWhereBindingString($newWhere);
    }

    /**
     * Convert params to select count string
     * @param array $whereCount
     * @return type
     */
    public function paramsToSelectCountString(array $whereCount)
    {
        $result = '';
        $prefix = $this::PREFIX_TABLE_SEPERATE_COUNTER;
        foreach ($whereCount as $key => $val) {
            $newKey = str_replace('.', $prefix, $key);
            if (!empty($result)) {
                $result .= ' , ';
            }
            $result = " count({$key}) as {$newKey} ";
        }
        return $result;
    }
    
    /**
     * Get column from table
     * @param type $tableName
     * @return type
     */
    abstract public function fetchColumnsFromTable($tableName);

    
    
    public function getColumnsFromTable($tableName){
        $key = $tableName.'_table_columns';
        if(isset(self::$columnsFromTables[$key])){
            return self::$columnsFromTables[$key]; 
        }  
            $cache = $this->getServiceManager()->get('Cache');
            $result = $cache->getItem($key, $success);

            if($success){
                $result = json_decode($result,true);
           }else{
             $result = $this->fetchColumnsFromTable($tableName);
             $cache->setItem($key,json_encode($result));
             DatabaseAbstract::$columnsFromTables[$key] = $result;
            }
            
            return $result;
    }
    
     /**
     * Get column from table with select
     * @param type $tableName
     * @return type
     */
    abstract public function getColumnsFromTableWithSelect($tableName);


    /**
     * Get column from table
     * @param type $tableName
     * @return type
     */
    function isJsonColumn($tableName, $columnName)
    {
        $columns = $this->getColumnsFromTable($tableName);
        return (isset($columns[$columnName]) && $columns[$columnName]['type'] == $this::JSON_TYPE);
    }

    /**
     * Get column from table
     * @param type $tableName
     * @return type
     */
    function isEnumColumn($tableName, $columnName)
    {
        $columns = $this->getColumnsFromTable($tableName);
        return (isset($columns[$columnName]) && $columns[$columnName]['type'] == $this::ENUM_TYPE);
    }

    /**
     * Get column from table
     * @param type $tableName
     * @return type
     */
    function isSetColumn($tableName, $columnName)
    {
        $columns = $this->getColumnsFromTable($tableName);
        return (isset($columns[$columnName]) && $columns[$columnName]['type'] == $this::SET_TYPE);
    }

    function sql()
    {
        if (is_null($this->sql)) {
            $this->sql = new SqlHandler($this->adapter());
        }
        return $this->sql;
    }

    /**
     * Get select zend object
     * @return type
     */
    function select()
    {
        return $this->sql()->select();
    }

    /**
     * Get auto increment field name
     * @param type $tableName
     * @return type
     */
    public function getAutoIncrementFieldName($tableName)
    {
        $columns = $this->getColumnsFromTable($tableName);
        foreach ($columns as $columnName => $column) {
            if (isset($column['extra']) && $column['extra'] == 'auto_increment') {
                return $columnName;
            }
        }
        return null;
    }

    /**
     * Update row on the db
     * @param string $tableName - table name
     * @param string $whereKey - column on where to update
     * @param string $whereVal - column value on where to update
     * @param array $params - params to update
     * @return - new insert row id
     */
    abstract public function insert($tableName, array $params,
                    array $updateIfExistData = array(), $isIgnore = false);

    
    abstract public function adapter();


    /**
     * Modify params for sql
     * convert bool to int
     * @param array $params
     */
    function paramsModify(array &$params,string $tableName = null,$modifyIfExist = false)
    {
        if($tableName){
                 $columns = $this->getColumnsFromTable($tableName);
        }
        foreach ($params as $key => $val) {
            if($tableName){
                $searchKey = $key;
                if($modifyIfExist && strpos($key, 'if_exist_') === 0){
                    $searchKey = str_replace('if_exist_','',$key);
                }
                if((isset($columns[$searchKey]) && $columns[$searchKey]['type'] == $this::ENUM_TYPE)){
                    if(is_bool($val) || $val == '1' || $val == '0'){
                        $params[$key] = (string) (int) $val;
                    }
                }elseif((isset($columns[$searchKey]) && $columns[$searchKey]['type'] == $this::SET_TYPE)){
                    if(is_array($val)){
                        $params[$key] = implode(',',$val);
                    }
                }
            }elseif (is_bool($val)) {
                $params[$key] = (string) (int) $val;
            }
        }
    }

    /**
     * Filter Blacklist params
     * @param array $params - params to filter
     * @return type
     */
    function filterBlacklistParams(array $params)
    {
        foreach ($this->blacklist_columns as $columnName) {
            if (isset($params[$columnName])) {
                unset($params[$columnName]);
            }
        }
        return $params;
    }

    /**
     * Convert params to binding columns string
     * @param array $params - example - array (array('join','table as t', 't.a = main_s.b'))
     * @return string - binding columns string
     */
    function joinParamsToString(array $params, $escapeOn = true)
    {
        $result = ' ';
        foreach ($params as $joinRowArray) {

            $joinType = $joinRowArray[0];

            if (is_array($joinRowArray[1])) {
                $joinTable = $this->escapeTableName(key($joinRowArray[1])).'  as  '.$this->escapeTableName(current($joinRowArray[1]));
            } else {
                $joinTable = $this->escapeTableName($joinRowArray[1]);
            }
            $joinOn = $joinRowArray[2];
            $result .= ' '.$joinType.' '.$joinTable.' on '.($escapeOn ? $this->escapeTableName($joinOn)
                    : $joinOn);
        }
        return $result;
    }

    /**
     * Convert params to binding columns string
     * @param array $params - example - array (array('join','table as t', 't.a = main_s.b'))
     * @return string - binding columns string
     */
    function getSelectFromTables(array $tables)
    {
        $result = ' ';
        $index  = 0;
        $prefix = $this::PREFIX_TABLE_SEPERATE;
        foreach ($tables as $table => $dbName) {
            if (!is_numeric($dbName)) {
                $fullTablePath = $this->escapeTableName($dbName.'.'.$table);
            } else {
                $fullTablePath = $this->escapeTableName($table);
            }
            $columns = $this->getColumnsFromTable($fullTablePath);
            foreach ($columns as $columnName => $column) {
                if ($index > 0) {
                    $result .= ",";
                }
                $result .= " `$table`.`$columnName` as `{$table}{$prefix}{$columnName}`";
                $index++;
            }
        }

        return $result;
    }

    /**
     * Convert params to binding columns string
     * @param array $params - example - array (array('join','table as t', 't.a = main_s.b'))
     * @return string - binding columns string
     */
    function splitResultByTable(array $tables, array $queryResult)
    {
        $result = array();
        foreach ($queryResult as $rowKey => $row) {
            $rowResult = array();
            $table     = key($tables);
            foreach ($row as $key => $val) {
                foreach ($tables as $table => $dbName) {
                    $tablePrefix = $table."_table_";
                    if (substr($key, 0, strlen($tablePrefix)) === $tablePrefix) {
                        $originKey = str_replace($tablePrefix, '', $key);
                        break;
                    }
                }

                if (!isset($result[$rowKey][$table])) {
                    $result[$rowKey][$table] = array();
                }
                $result[$rowKey][$table][$originKey] = $val;
            }
        };
        return $result;
    }

    /**
     * Convert params to binding columns string
     * @param array $params - example - array (array('join','table as t', 't.a = main_s.b'))
     * @return string - binding columns string
     */
    function getTablesFromJoinParams(array $params)
    {
        $result = array();

        foreach ($params as $joinRowArray) {
            $joinType = $joinRowArray[0];
            if (is_array($joinRowArray[1])) {
                $joinTable = key($joinRowArray[1]);
            } else {
                $joinTable = $joinRowArray[1];
            }
            if (isset($joinRowArray[3])) {
                $dbName             = $joinRowArray[3];
                $result[$joinTable] = $dbName;
            } else {
                $result[$joinTable] = $this->getName();
            }
        }

        return $result;
    }

    /**
     * Escape table name and add ` table name `
     * @param string $name
     * @return string
     */
    static function escapeTableName($name)
    {
        if (strpos(trim($name), ' ') !== false) {
            $spaceArray = explode(' ', trim($name));
            foreach ($spaceArray as &$spaceElement) {
                if (strpos($spaceElement, '=') !== false) {
                    $spaceElementEqArr = explode('=', trim($spaceElement));
                    if (strpos($spaceElementEqArr[0], '.') !== false) {
                        $spaceElementEqArr[0] = self::escapeTableName($spaceElementEqArr[0]);
                    }
                    if (strpos($spaceElementEqArr[1], '.') !== false) {
                        $spaceElementEqArr[1] = self::escapeTableName($spaceElementEqArr[1]);
                    }
                    $spaceElement = implode('=', $spaceElementEqArr);
                } else {
                    if (strpos($spaceElement, '.') !== false) {
                        $spaceElement = self::escapeTableName($spaceElement);
                    }
                }
            }
            return implode(' ', $spaceArray);
        }
        if (strpos($name, '.') !== false) {
            $array = explode('.', $name);
            return '`'.implode('`.`', $array).'`';
        } elseif ((strpos($name, '=') !== false || strpos($name, '>') !== false || strpos($name,
                '<') !== false)) {
            return $name;
        } else {
            return '`'.$name.'`';
        }
    }

    /**
     * Convert params to binding columns string
     * @param array $params
     * @return string - binding columns string
     */
    function paramsToColumnsString(array $params)
    {
        $result = ' ( ';
        $index  = 1;
        foreach ($params as $k => $v) {
            $this->checkKeyValidation($k);
            $result .= " `{$k}` ";
            if ($index < count($params)) {
                $result .= ",";
            }
            $index++;
        }
        $result .= ' ) ';
        return $result;
    }
    
    function isUnicode($string){
        return strlen($string) != strlen(utf8_decode($string));
    }

    /**
     * Convert params to binding values string
     * @param array $params
     * @return string - binding values string
     */
    function paramsToBindingValuesString(array $params)
    {
        $result  = ' VALUES ( ';
        $index   = 1;
        $counter = count($params);
        foreach ($params as $k => $v) {
            $this->checkKeyValidation($k);
            $result .= " :{$k} ";
            if ($index < $counter) {
                $result .= ",";
            }
            $index++;
        }
        $result .= ' ) ';
        return $result;
    }

    /**
     * Convert params to binding if exist update string
     * @param array $params
     * @return string - binding values string
     */
    abstract public function paramsToIfExistBindingString(array &$params, $updateParams,
                                          $incrementField = null);


    /**
     * Convert params to binding string
     * @param array $params
     *    If the array is without keys, then the RAW sql is added to the where
     *    If key value pair with <columnname> => <condition>
     *        `condition`  null : will test for null column (is null )
     *        `condition` scalar : will test if column is equal to <condition>
     *        `condition` array  : <condtion>[0] is the operation ( `=`,'<','>' ...)
     *                             <condtion>[1] is the unquoted value to test ( it will be quoted )
     * @see Database::$allowedOp
     * @return string - binding string
     * @throws Exception
     */
    abstract public function paramsToWhereBindingString(array &$params, $tableName = null,
                                        $whereOp = "and");


    /**
     * Convert params to binding string
     * @param array $params
     *    If the array is without keys, then the RAW sql is added to the where
     *    If key value pair with <columnname> => <condition>
     *        `condition`  null : will test for null column (is null )
     *        `condition` scalar : will test if column is equal to <condition>
     *        `condition` array  : <condtion>[0] is the operation ( `=`,'<','>' ...)
     *                             <condtion>[1] is the unquoted value to test ( it will be quoted )
     * @see Database::$allowedOp
     * @return string - binding string
     * @throws Exception
     */
    abstract public function paramsToWhereBindingStringObject(\Arbel\Database\Where &$params,
                                              $tableName = null);


    function arrayToBindingSql($array, &$params)
    {
        $sql           = ' ( ';
        $index         = 1;
        $paramsCounter = count($array);
        foreach ($array as $val) {
            $uniqueKey          = 'in_array_'.uniqid();
            $params[$uniqueKey] = $val;
            $sql                .= ':'.$uniqueKey;
            if ($index < $paramsCounter) {
                $sql .= " , ";
            }
            $index++;
        }
        $sql .= ' ) ';
        return $sql;
    }

    /**
     * Log the SQL error to file.
     *
     * @param $query
     * @param $sqlerror
     *
     * @return bool
     */
    public function LogError($query, $sqlerror)
    {
        // Skip error logging if using the @ operator
        if (!error_reporting()) return true;

        list ($file, $line) = $this->GetReferringScriptInfo();


        // Construct log entry
        $log = "SQL Error in $file (line $line). /n";
        $log .= "Date: ".strftime("%d/%m/%Y %H:%M:%S%z").", /n";
        $log .= "Query: $query";
        $log .= "Error: $sqlerror";
        $log .= "/n";

        $this->log::err($log);
    }

    /**
     * Get database name
     * @return string
     */
    public function getDatabaseName(): string
    {
        if (!$this->have('database_name')) {
            $this->set('database_name',
                $this->adapter()->getDriver()->getConnection()->getCurrentSchema());
        }
        return $this->get('database_name');
    }

    /**
     * Alias function for getDatabaseName()
     * @return string
     */
    public function getName(): string
    {
        return $this->getDatabaseName();
    }
}