<?php

namespace Arbel\Database\Adapter;

use Zend\Db\Sql\Sql;
use Arbel\Base\Element;
use Arbel\Base\Sql\SqlHandler;
use Zend\Db\Adapter\Adapter;
use Arbel\Log;
use PDO;
use Zend\Db\Adapter\Driver;
use Illuminate\Database\Capsule\Manager as Capsule;
use Arbel\Database\Schema\Blueprint;
use Arbel\Database\Schema\Eav;
use Arbel\Database\Schema\MySqlGrammar;
use Arbel\Database\DatabaseAbstract;

class Mssql extends DatabaseAbstract
{

    /**
     * Get column from table
     * @param type $tableName
     * @return type
     */
    function fetchColumnsFromTable($tableName)
    {

        if (!isset($this->sqlHistory['column_from_table_'.$tableName])) {

            $result = array();
            if (strpos($tableName, '`') !== false) {
                $sql = "exec sp_columns ".$tableName;
            } else {
                $sql = "exec sp_columns ".$tableName."";
            }
            $sqlResult = $this->exec($sql);
            foreach ($sqlResult as $row) {
                if (strpos($row['TYPE_NAME'], '(')) {
                    $typeArray = explode('(', $row['TYPE_NAME']);
                } else {
                    $typeArray = explode(' ', $row['TYPE_NAME']);
                }
                $type                  = current($typeArray);
                $result[strtolower($row['COLUMN_NAME'])] = array(
                    'type' => $type,
                    'full_type' => $row['TYPE_NAME'],
                    'allow_null' => ($row['NULLABLE'] == 1),
                    'key' => '',
                    'default' => '',
                    'extra' => '',
                );
            }
            $this->sqlHistory['column_from_table_'.$tableName] = $result;
        } else {

            $result = $this->sqlHistory['column_from_table_'.$tableName];
        }

        return $result;
    }
    
    
    /**
     * Get column from table with select
     * @param type $tableName
     * @return type
     */
    function getColumnsFromTableWithSelect($tableName)
    {
        if (!isset($this->sqlHistory['column_from_table_'.$tableName])) {
            $result    = array();
            $sql       = "SELECT COLUMN_NAME,DATA_TYPE "
                ."FROM INFORMATION_SCHEMA.COLUMNS "
                ."WHERE TABLE_SCHEMA = :database_name AND TABLE_NAME = :table_name;";
            $sqlResult = $this->exec($sql,
                array('database_name' => $this->getDatabaseName(),
                'table_name' => $tableName));
            foreach ($sqlResult as $row) {
                $result[$row['COLUMN_NAME']] = $row['DATA_TYPE'];
            }
            $this->sqlHistory['column_from_table_'.$tableName] = $result;
        } else {
            $result = $this->sqlHistory['column_from_table_'.$tableName];
        }

        return $result;
    }

    /**
     * Update row on the db
     * @param string $tableName - table name
     * @param string $whereKey - column on where to update
     * @param string $whereVal - column value on where to update
     * @param array $params - params to update
     * @return - new insert row id
     */
    function insert($tableName, array $params,
                    array $updateIfExistData = array(), $isIgnore = false)
    {

        $this->checkKeyValidation($tableName);

        $this->filterBlacklistParams($params);
        $sql = "insert ";
        if ($isIgnore) {
           // $sql .= " ignore ";
        }
        $sql .= "into {$tableName} ";
        $sql .= $this->paramsToColumnsString($params);
        $sql .= $this->paramsToBindingValuesString($params);

        if (!empty($updateIfExistData) && false) {
            $incrementField = $this->getAutoIncrementFieldName($tableName);
            $sql            .= $this->paramsToIfExistBindingString($params,
                $updateIfExistData, $incrementField);
        }
        $this->paramsModify($params,$tableName,true);
        $this->exec($sql, $params);

        $result = $this->adapter()->getDriver()->getConnection()->getLastGeneratedValue();

        return $result;
    }


    /**
     * Get all rows form table
     * @param type $tableName - table name
     * @param array | Where $params - conditions params
     * @param array $joinParams - example - array (array('join','table as t', 't.a = main_s.b'))
     * @param int $limit - limit result
     * @param bool $isSelectAllFromJoinTable - if to retrieve all join tables data
     * @param array $whereCount - where counter array
     * @param string $groupBy - group by string
     * @return array query result
     */
    function getAll($tableName, $params = array(), $orderBy = null,
                    array $joinParams = null, int $limit = null,
                    bool $isSelectAllFromJoinTable = false,
                    array $whereCount = array(), string $groupBy = null,
                    int $offset = null)
    {
        $this->checkKeyValidation($tableName);
        if (!$isSelectAllFromJoinTable || !isset($joinParams)) {
            $sql = "select {$tableName}.* from {$tableName} ";
        } else {
            $sql                = "select ";
            $tables             = $this->getTablesFromJoinParams($joinParams);
            $tables[$tableName] = $this->getName();
            $sql                .= $this->getSelectFromTables($tables, true);
            $sql                .= " from {$tableName} ";
        }
        if (isset($joinParams) && !empty($joinParams)) {
            if (!is_array($joinParams[key($joinParams)])) {
                $joinParams = array($joinParams);
            }
            $sql .= $this->joinParamsToString($joinParams, true);
        }

        if (!empty($params)) {
            if (is_object($params)) {
                $whereString = $this->paramsToWhereBindingStringObject($params,
                    $tableName);
            } else {
                $whereString = $this->paramsToWhereBindingString($params,
                    $tableName);
            }

            if (!empty($whereString)) {
                $sql .= " where ".$whereString;
            }
        }

        if ($orderBy) {
            if (strpos($orderBy, '.') === false) {
                $orderBy = ''.$tableName.'.'.$orderBy;
            }
            $sql .= ' order by '.trim($orderBy);
        }

        if ($groupBy) {
            $sql .= ' group by '.$groupBy;
        }

        if ($limit) {
            $this->addLimitToQuery($sql,$limit);
        }

        if ($offset) {
            $sql .= ' offset '.$offset;
        }

        if (!empty($whereCount)) {
            $group       = '';
            $prefix      = $this::PREFIX_TABLE_SEPERATE;
            $selectCount = $this->paramsToSelectCountString($whereCount);
            $sql         = str_replace('select ', 'select '.$selectCount.' , ',
                $sql);

            if (!$groupBy) {
                $columns = $this->getColumnsFromTable($tableName);
                $sql     .= " group by {$tableName}{$prefix}".key($columns)."";
            }
            $sql = "select main_t.* from ( {$sql} ) as main_t where ";
            $sql .= $this->paramsToWhereCountString($whereCount);
        }

        $result = $this->exec($sql, $params);
        if ($isSelectAllFromJoinTable && isset($joinParams)) {
            $result = $this->splitResultByTable($tables, $result);
        }
        return $result;
    }
 
    public function modifyQueryResult(&$result)
    {
        foreach($result as &$row){
            foreach($row as $key=>$val){
               if(preg_match('/[a-z]/', $key)){
                   unset($row[$key]);
                   $row[strtolower($key)] = $val;
                }
            }
        }
    }


    /**
     * Convert params to binding columns string
     * @param array $params
     * @return string - binding columns string
     */
    function paramsToColumnsString(array $params)
    {
        $result = ' ( ';
        $index  = 1;
        foreach ($params as $k => $v) {
            $this->checkKeyValidation($k);
            $result .= " {$k} ";
            if ($index < count($params)) {
                $result .= ",";
            }
            $index++;
        }
        $result .= ' ) ';
        return $result;
    }


    /**
     * Load row from table
     * @param string $tableName - table name
     * @param string/array $key - column or params on where to select
     * @param type $val - column value on where to select
     * @return array - query result
     */
    function load($tableName, $key, $val = null)
    {
        if (is_array($key)) {
            $results = self::getAll($tableName, $key);
            if (!empty($results)) {
                $result = current($results);
            } else {
                $result = array();
            }
        } else {
            $this->checkKeyValidation($tableName);
            $this->checkKeyValidation($key);
            $sql    = "select top 1 * from {$tableName} where {$key} = :val ;";
            $result = $this->exec($sql,
                array(
                'val' => $val
            ));
        }
        return $result;
    }

     function addLimitToQuery(&$query,$limit){
        $query = str_replace('select ', 'select top '.$limit.' ', $query);
     }

     function getGrammar(){
         return new \Arbel\Database\Schema\SqlServerGrammar();
     }

     function isAllowOnUpdate(){
         return false;
     }


    function adapter()
    {
        if (is_null($this->adapter)) {
            $this->adapter = new \Zend\Db\Adapter\Adapter(array(
                'driver' => 'Pdo_Mssql',
                'database' => $this->connection_info['database'],
                'username' => $this->connection_info['username'],
                'password' => $this->connection_info['password'],
                'port' => $this->connection_info['port'],
                'Server' => $this->connection_info['server'],
            ));
        }
        return $this->adapter;
    }

    /**
     * Convert params to binding if exist update string
     * @param array $params
     * @return string - binding values string
     */
    function paramsToIfExistBindingString(array &$params, $updateParams,
                                          $incrementField = null)
    {
        $result  = ' ON DUPLICATE KEY UPDATE ';
        $pre     = 'if_exist_';
        $index   = 1;
        $counter = count($updateParams);
        if ($incrementField) {
            $result .= ' '.$incrementField.'=LAST_INSERT_ID('.$incrementField.') ';
            if ($counter > 0) {
                $result .= ", ";
            }
        }
        foreach ($updateParams as $k => $v) {
            $newKey          = $pre.$k;
            $params[$newKey] = $v;
            $this->checkKeyValidation($newKey);
            $result          .= " $k = :{$newKey} ";
            if ($index < $counter) {
                $result .= ", ";
            }
            $index++;
        }
        return $result;
    }

    

    /**
     * Convert params to binding values string
     * @param array $params
     * @return string - binding values string
     */
    function paramsToBindingValuesString(array &$params)
    {
        $result  = ' VALUES ( ';
        $index   = 1;
        $counter = count($params);
        foreach ($params as $k => $v) {
            $this->checkKeyValidation($k);
            if($this->isUnicode($v)){
                $result .= " N'".htmlspecialchars($v)."' ";
                unset($params[$k]);
            }else{
                $result .= " :{$k} ";
            }
            if ($index < $counter) {
                $result .= ",";
            }
            $index++;
        }
        $result .= ' ) ';
        return $result;
    }

    function isUnicode($string){
        return strlen($string) != strlen(utf8_decode($string));
    }
    
    /**
     * Convert params to binding string
     * @param array $params
     * @return string - binding string
     */
    function paramsToBindingString(array &$params)
    {
        $index         = 1;
        $result        = "";
        $paramsCounter = count($params);
        foreach ($params as $k => $v) {
            $this->checkKeyValidation($k);
            if($this->isUnicode($v)){
                $result .= " {$k} = N'".htmlspecialchars ($v)."' ";
                unset($params[$k]);
            }else{
                $result .= " {$k} = :{$k} ";
            }
            if ($index < $paramsCounter) {
                $result .= ",";
            }
            $index++;
        }
        return $result;
    }


    /**
     * Escape table name and add ` table name `
     * @param string $name
     * @return string
     */
    static function escapeTableName($name)
    {
        if (strpos(trim($name), ' ') !== false) {
            $spaceArray = explode(' ', trim($name));
            foreach ($spaceArray as &$spaceElement) {
                if (strpos($spaceElement, '=') !== false) {
                    $spaceElementEqArr = explode('=', trim($spaceElement));
                    if (strpos($spaceElementEqArr[0], '.') !== false) {
                        $spaceElementEqArr[0] = self::escapeTableName($spaceElementEqArr[0]);
                    }
                    if (strpos($spaceElementEqArr[1], '.') !== false) {
                        $spaceElementEqArr[1] = self::escapeTableName($spaceElementEqArr[1]);
                    }
                    $spaceElement = implode('=', $spaceElementEqArr);
                } else {
                    if (strpos($spaceElement, '.') !== false) {
                        $spaceElement = self::escapeTableName($spaceElement);
                    }
                }
            }
            return implode(' ', $spaceArray);
        }
        if (strpos($name, '.') !== false) {
            $array = explode('.', $name);
            return ''.implode('.', $array).'';
        } elseif ((strpos($name, '=') !== false || strpos($name, '>') !== false || strpos($name,
                '<') !== false)) {
            return $name;
        } else {
            return ''.$name.'';
        }
    }


    /**
     * Convert params to binding string
     * @param array $params
     *    If the array is without keys, then the RAW sql is added to the where
     *    If key value pair with <columnname> => <condition>
     *        `condition`  null : will test for null column (is null )
     *        `condition` scalar : will test if column is equal to <condition>
     *        `condition` array  : <condtion>[0] is the operation ( `=`,'<','>' ...)
     *                             <condtion>[1] is the unquoted value to test ( it will be quoted )
     * @see Database::$allowedOp
     * @return string - binding string
     * @throws Exception
     */
    function paramsToWhereBindingString(array &$params, $tableName = null,
                                        $whereOp = "and")
    {
        $index         = 1;
        $result        = "";
        $originParams  = $params;
        $paramsCounter = count($params);
        foreach ($originParams as $k => $v) {
            if (strpos($k, ':') !== false) {
                $index++;
                continue;
            }
            if (is_object($v)) {
                $subObjectParams = $v->getData();
                $subStringResult = $this->paramsToWhereBindingStringObject($subObjectParams,
                    $tableName);
                if (empty($result)) {
                    $result = $subStringResult;
                } else {
                    $result = $params->getType().'('.$subStringResult.')';
                }
                unset($params[$k]);
                $params = array_merge($params, $subObjectParams);
            } elseif (is_int($k)) {
                if (!is_string($v)) {
                    throw new \Exception("Condition without specified column must be a string");
                }
                if (!empty($result)) {
                    $result .= " {$whereOp} ";
                }
                $result .= PHP_EOL.$v.PHP_EOL;
                unset($params[$k]);
            } else {
                $this->checkKeyValidation($k);
                if (!empty($result)) {
                    $result .= " {$whereOp} ";
                }
                if (is_null($v)) {
                    $result .= " {$k} is null ";
                    unset($params[$k]);
                } elseif (is_array($v)) {
                    if (count($v) == 2 && in_array($v[0], $this->allowedOp)) {
                        $op  = $v[0];
                        $val = $v[1];
                        if (is_array($val)) {
                            $val    = $this->arrayToBindingSql($val, $params);
                            $result .= " {$k} {$v[0]} {$val} ";
                        } else {
                            if (is_null($val) && $v[0] == '!=') {
                                $result .= " {$k} is not null ";
                            } elseif (is_null($val) && $v[0] == '=') {
                                $result .= " {$k} is null ";
                            } else {
                                $uniqueKey          = $k.'_'.uniqid();
                                $params[$uniqueKey] = $val;
                                $result             .= " {$k} {$v[0]} :{$uniqueKey} ";
                            }
                        }
                        unset($params[$k]);
                    } elseif (count($v) == 2 && strtolower($v[0]) == 'like' && !empty($v[1])) {
                        $splitedParam = explode('%', $v[1]);
                        foreach ($splitedParam as $subK => $subY) {
                            if (empty($subY)) {
                                $splitedParam[$subK] = "'%'";
                            } else {
                                $uniqueKey           = $k.'_'.uniqid();
                                $params[$uniqueKey]  = $subY;
                                $splitedParam[$subK] = ":{$uniqueKey}";
                            }
                        }
                        $result .= $k." LIKE CONCAT( ".implode(',',
                                $splitedParam)." ) ";
                        unset($params[$k]);
                    } elseif (count($v) == 1 && !is_numeric($v[0])) {
                        $val    = $v[0];
                        $result .= " {$k} {$val} ";
                        unset($params[$k]);
                    } else {
                        $val    = $this->arrayToBindingSql($v, $params);
                        $result .= " {$k} in {$val} ";
                        unset($params[$k]);
                    }
                } else {
                    $kDot = str_replace('.', '_', $k);
                    if ($this->isJsonColumn($tableName, $k)) {
                        $result .= " {$k} = CAST(:where_{$kDot} AS JSON) ";
                    } else {
                        $result .= " {$k} = :where_{$kDot} ";
                    }
                    unset($params[$k]);

                    $params['where_'.$kDot] = $v;
                }
            }

            $index++;
        }
        return $result;
    }


    /**
     * Delete row from db
     * @param string $tableName - table name
     * @param string|array $key - column on where to delete
     * @see Database::paramsToWhereBindingString() for array format to use
     * @param string $val - column value on where to delete
     *
     */
    function delete(string $tableName, $key, string $val = null)
    {
        if (is_array($key)) {
            $params = $key;
            $sql    = "delete from {$tableName} where ";
            $sql    .= $this->paramsToWhereBindingString($params, $tableName);
        } else {
            $params = array(
                'val' => $val
            );
            $this->checkKeyValidation($tableName);
            $this->checkKeyValidation($key);
            $sql    = "delete from {$tableName} where {$key} = :val";
        }
        $this->exec($sql, $params);
    }


    /**
     * Update row on the db
     * @param string $tableName - table name
     * @param string $whereParams - column on where to update
     * @see Database::paramsToWhereBindingString()
     * @param array $params - params to update
     * @see Database::paramsToBindingString()
     */
    function update($tableName, array $whereParams, array $params)
    {

        $this->filterBlacklistParams($params);
        $sql    = "update {$tableName} set";
        $sql    .= $this->paramsToBindingString($params);
        $sql    .= " where ";
        $sql    .= $this->paramsToWhereBindingString($whereParams, $tableName);
        $params = array_merge($params, $whereParams);
        $this->paramsModify($params,$tableName,true);
        $this->exec($sql, $params);
    }

    /**
     * Convert params to binding string
     * @param array $params
     *    If the array is without keys, then the RAW sql is added to the where
     *    If key value pair with <columnname> => <condition>
     *        `condition`  null : will test for null column (is null )
     *        `condition` scalar : will test if column is equal to <condition>
     *        `condition` array  : <condtion>[0] is the operation ( `=`,'<','>' ...)
     *                             <condtion>[1] is the unquoted value to test ( it will be quoted )
     * @see Database::$allowedOp
     * @return string - binding string
     * @throws Exception
     */
    function paramsToWhereBindingStringObject(\Arbel\Database\Where &$params,
                                              $tableName = null)
    {
        $index        = 1;
        $result       = "";
        $arrayParams  = $params->getData();
        $resultParams = array();
        foreach ($params->getData() as $k => $v) {
            if (strpos($k, ':') !== false) {
                $index++;
                continue;
            }
            if (is_object($v)) {
                $subObjectParams = $v->getData();
                $subStringResult = $this->paramsToWhereBindingString($subObjectParams,
                    $tableName, $v->getType());
                if (empty($subStringResult)) {
                    $result = $subStringResult;
                } else {
                    $result = $params->getType().'('.$subStringResult.')';
                }
                unset($arrayParams[$k]);
                $resultParams = array_merge($resultParams, $subObjectParams);
            } elseif (is_int($k)) {
                if (!is_string($v)) {
                    throw new Exception("Condition without specified column must be a string");
                }
                $result .= PHP_EOL.$v.PHP_EOL;
                unset($arrayParams[$k]);
            } else {
                $this->checkKeyValidation($k);
                if (!empty($result)) {
                    $result .= " ".$params->getType()." ";
                }
                if (is_null($v)) {
                    $result .= " {$k} is null ";
                    unset($arrayParams[$k]);
                } elseif (is_array($v)) {
                    if (count($v) == 2 && in_array($v[0], $this->allowedOp)) {
                        $op  = $v[0];
                        $val = $v[1];
                        if (is_array($val)) {
                            $val    = $this->arrayToBindingSql($val,
                                $arrayParams);
                            $result .= " {$k} {$v[0]} {$val} ";
                        } else {
                            if (is_null($val) && $v[0] == '!=') {
                                $result .= " {$k} is not null ";
                            } elseif (is_null($val) && $v[0] == '=') {
                                $result .= " {$k} is null ";
                            } else {
                                $uniqueKey               = $k.'_'.uniqid();
                                $arrayParams[$uniqueKey] = $val;
                                $result                  .= " {$k} {$v[0]} :{$uniqueKey} ";
                            }
                        }
                        unset($arrayParams[$k]);
                    } elseif (count($v) == 1) {
                        $val    = $v[0];
                        $result .= " {$k} {$val} ";
                        unset($arrayParams[$k]);
                    } else {
                        $val    = $this->arrayToBindingSql($v, $arrayParams);
                        $result .= " {$k} in {$val} ";
                        unset($arrayParams[$k]);
                    }
                } else {
                    $kDot = str_replace('.', '_', $k);
                    if ($this->isJsonColumn($tableName, $k)) {
                        $result .= " {$k} = CAST(:where_{$kDot} AS JSON) ";
                    }elseif ($this->isEnumColumn($tableName, $k)) {
                        $result .= " {$k} = :where_{$kDot} ";
                    } else {
                        $result .= " {$k} = :where_{$kDot} ";
                    }
                    unset($arrayParams[$k]);

                    $arrayParams['where_'.$kDot] = $v;
                }
            }

            $index++;
        }
        $params = $arrayParams;
        return $result;
    }

}