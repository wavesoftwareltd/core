<?php

namespace Arbel\Database\Schema;

use Illuminate\Support\Facades\DB;

class Blueprint extends \Illuminate\Database\Schema\Blueprint
{

    /**
     * The storage engine that should be used for the table.
     *
     * @var string
     */
    public $engine = 'InnoDB';

    /**
     * \Illuminate\Database\Connection
     * @var Connection
     */
    protected $connection;

    protected $allowOnUpdate;

    /**
     * Add automatic creation and update datetime to the table.
     *
     * @return void
     */
    public function datetimes()
    {
        $this->dateTime('created_at')->default($this->raw('CURRENT_TIMESTAMP'));
        if($this->allowOnUpdate){
         $this->dateTime('updated_at')->default($this->raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        }else{
         $this->dateTime('updated_at')->default($this->raw('CURRENT_TIMESTAMP'));
        }
        
    }

    /**
     * Set connection
     * @param type $connection
     */
    public function setConnection(\Illuminate\Database\Connection $connection)
    {
        $this->connection = $connection;
    }

    public function setAllowOnUpdate($status){
        $this->allowOnUpdate = $status;
    }

    /**
     * Get connection
     * @return type
     */
    public function getConnection() :\Illuminate\Database\Connection
    {
        return $this->connection;
    }

    /**
     * Short alias to connection raw function
     * @param string $string
     * @return type
     */
    public function raw(string $string)
    {
        return $this->getConnection()->raw($string);
    }

    /**
     * Create a new integer (4-byte) column on the table.
     *
     * @param  string  $column
     * @param  bool  $autoIncrement
     * @param  bool  $unsigned
     * @return \Illuminate\Support\Fluent
     */
    public function integer($column, $autoIncrement = false, $unsigned = true)
    {
        return $this->addColumn('integer', $column, compact('autoIncrement', 'unsigned'));
    }

       /**
     * Create a new 'set' column on the table.
     *
     * @param  string  $column
     * @param  array   $allowed
     * @return \Illuminate\Support\Fluent
     */
    public function set($column, array $allowed)
    {
        return $this->addColumn('set', $column, compact('allowed'));
    }
}