<?php

namespace Arbel\Database\Schema;

use Arbel\Database\DatabaseInterface;

class Eav
{
    protected $db;

    public function __construct(DatabaseInterface $db)
    {
        $this->db = $db;
    }

    public function getDatabase()
    {
        return $this->db;
    }

    public function create(string $name, $callback = null)
    {
        $this->createEntity($name, $callback);
        $this->createEntityType($name);
        $this->createAttribute($name);
        $this->createEntityDatetime($name);
        $this->createEntity($name, $callback);
    }

    public function createEntity(string $name, $callback = null)
    {
        $this->getDatabase()->getSchema()->create($name.'_eav_entity',
            function ($table) use ($callback) {
            $table->increments('entity_id');
            $table->string('scope')->default('default');
            $table->datetimes();
            if ($callback && is_callable($callback)) {
                $callback($table);
            }
        });
    }

    public function createEntityType(string $name, $callback = null)
    {
        $this->getDatabase()->getSchema()->create($name.'_eav_entity_type',
            function ($table) use ($callback) {
            $table->increments('entity_type_id');
            $table->string('entity_type_code', 255)->unique();
            $table->string('entity_table', 255);
            $table->string('additional_attribute_table', 255);
            $table->string('scope')->default('default');
            $table->datetimes();
            if ($callback && is_callable($callback)) {
                $callback($table);
            }
        });
    }

    public function createAttribute(string $name, $callback = null)
    {
        $this->getDatabase()->getSchema()->create($name.'_eav_entity',
            function ($table) use ($callback, $name) {
            $table->increments('attribute_id');
            $table->integer('entity_type_id', false, true);
            $table->string('attribute_code', 255)->unique();
            $table->string('backend_type', 255)->default('static');
            $table->string('frontend_input', 255);
            $table->string('frontend_label', 255);
            $table->enum('is_required', array(0, 1))->default(0);
            $table->enum('is_user_defined', array(0, 1))->default(1);
            $table->enum('is_unique', array(0, 1))->default(1);
            $table->text('default_value')->nullable();
            $table->text('note')->nullable();
            $table->datetimes();
            $table->foreign('entity_type_id')->references('entity_type_id')->on($name.'_eav_entity_type');
            if ($callback && is_callable($callback)) {
                $callback($table);
            }
        });
    }

    public function createEntityDatetime(string $name, $callback = null)
    {
        $this->getDatabase()->getSchema()->create($name.'_entity_datetime',
            function ($table) use ($callback, $name) {
            $table->increments('value_id');
            $table->integer('attribute_id', false, true);
            $table->integer('entity_id', false, true);
            $table->datetime('value')->nullable();
            $table->datetimes();
            $table->foreign('attribute_id')->references('id')->on($name.'_eav_entity');
            if ($callback && is_callable($callback)) {
                $callback($table);
            }
        });
    }
}