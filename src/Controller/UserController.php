<?php

namespace Arbel\Controller;

use Arbel\Model\User;
use Zend\InputFilter\InputFilter;
use Arbel\Base\ModelAbstract;

/**
 * User controller
 */
class UserController extends AbstractCrudController
{
    public function getEntity(): ModelAbstract
    {
        return $this->getModel(User::class);
    }

    public function getInputFilter(): InputFilter
    {
        return new InputFilter();
    }
}