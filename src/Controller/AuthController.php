<?php

namespace Arbel\Controller;

use Carbon\Carbon;
use Arbel\Model\Communication\Platform;
use Arbel\Model\Communication\Conversation;
use Arbel\Model\Communication\Interlocutor;
use Arbel\Model\Communication\Interlocutor\Test;
use Arbel\Model\Translator;
use Arbel\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\View\Model\JsonModel;
use OAuth2\Server;
use Arbel\Helper;
use Arbel\Utils\Shorten;
use Arbel\Model\Auth\Registration;
use Arbel\Model\Auth\ChangePassword;
use Arbel\Model\Oauth\AccessToken;

/**
 * Auth controller
 */
class AuthController extends \Arbel\Controller\OAuth2\AuthController
{
    protected $acl;

    /**
     * Inject objects
     * @param StorageInterface $cache
     */
    public function setInjections(Acl $acl)
    {
        $this->acl = $acl;
    }

    public function loginAction()
    {

        $email    = trim(strtolower($this->getRequest()->getPost('email')));
        $password = $this->getRequest()->getPost('password');
        $user     = $this->getModel('Arbel\Model\User')->loadByKey('email',
            $email);

        if ($user->isExist() && $user->isVerified() && $user->isPasswordVerified($password)) {
            $httpResponse         = $this->loginDefaultAppToken($user->getId());
            $content              = json_decode($httpResponse->getContent(),
                true);
            $content['user_info'] = $user->getPublicData();
            $httpResponse->setContent(json_encode($content));
            return $httpResponse;
        } else {
            $httpResponse = $this->getResponse();
            $httpResponse->setStatusCode(401);
            $httpResponse->getHeaders()->addHeaders(['Content-type' => 'application/json']);
            $httpResponse->setContent(
                json_encode(array(
                'result' => false,
                'error' => $this->getModel(Translator::class)->__('user or password are wrong').'!'
                ))
            );
            return $httpResponse;
        }
    }

    public function guidAction()
    {
        $guid = $this->params()->fromRoute('guid');
        $redirectUrl = $this->getRequest()->getQuery('redirect_url','/');
        $isPassInfo = $this->getRequest()->getQuery('is_pass_info',false);
        $user     = $this->getModel('Arbel\Model\User')->loadByKey('guid',
            $guid);
        if ($user->isExist() && $user->isVerified() && $this->getConfig()->get('auth.allow_guid_login')) {
            $httpResponse         = $this->loginDefaultAppToken($user->getId());
            $content              = json_decode($httpResponse->getContent(),
                true);
             $token = $this->getModel(AccessToken::class)->getOne([
                'user_id' => $user->getId(),
                'access_token' => $content['access_token']
            ]);

            $token->extend($this->getConfig()->get('auth.guid_liferime_limit',60 * 60 * 24 * 90));
            $content['user_info'] = $user->getPublicData();
            $this->getCookie()->set('user_info',json_encode($content));
            if($isPassInfo){
                if(strpos($redirectUrl, '?') === false){
                    $redirectUrl = $redirectUrl . '?';
                }else{
                    $redirectUrl = $redirectUrl . '&';
                }
                    $redirectUrl = $redirectUrl . 'user_info='.urlencode(base64_encode(json_encode($content)));
            }
            return $this->redirect()->toUrl($redirectUrl);
        } else {
            $httpResponse = $this->getResponse();
            $httpResponse->setStatusCode(404);
            $httpResponse->getHeaders()->addHeaders(['Content-type' => 'application/json']);
            $httpResponse->setContent(
                json_encode(array(
                'result' => false,
                'error' => $this->getModel(Translator::class)->__('page was not found').'!'
                ))
            );
            return $httpResponse;
        }
    }

    public function forgotPasswordAction()
    {

        $email = trim(strtolower($this->getRequest()->getPost('email')));
        $user  = $this->getModel('Arbel\Model\User')->loadByKey('email', $email);

        if ($user->isExist() && $user->isVerified()) {
            $changeObject = $this->getModel(ChangePassword::class)->setUserId($user->getId())->save();
            $changeObject->sendEmail();
            return new JsonModel(array(
                'result' => true,
                'message' => $this->getModel(Translator::class)->__('Reset email was sent').'!'
            ));
        } else {
            $httpResponse = $this->getResponse();
            $httpResponse->setStatusCode(500);
            $httpResponse->getHeaders()->addHeaders(['Content-type' => 'application/json']);
            $httpResponse->setContent(
                json_encode(array(
                'result' => false,
                'error' => $this->getModel(Translator::class)->__('user email not exist!').'!'
                ))
            );
            return $httpResponse;
        }
    }

    public function verifyLoginAction()
    {
        $userId = $this->getServiceManager()->get('User')->getId();
        if (!$userId) {
            $httpResponse = $this->getResponse();
            $httpResponse->setStatusCode(401);
            $httpResponse->getHeaders()->addHeaders(['Content-type' => 'application/json']);
            $httpResponse->setContent(
                json_encode(array(
                'result' => false,
                'error' => $this->getModel(Translator::class)->__('User is not login').'!'
                ))
            );
            return $httpResponse;
        } else {
            return new JsonModel([
                'result' => true,
                'user_info' => $this->getServiceManager()->get('User')->getPublicData()]);
        }
    }

    public function changePasswordAction()
    {
        $token           = trim($this->getRequest()->getPost('token'));
        $password        = trim($this->getRequest()->getPost('password'));
        $user            = $this->getServiceManager()->get('User');
        $currentPassword = trim($this->getRequest()->getPost('current_password',
                ''));
        $result          = [
            'status' => false
        ];
        if ($user && $user->isExist() && $user->isVerified() && $user->isPasswordVerified($currentPassword)
            && $user->getId() && !empty($currentPassword) && !empty($password)) {
            $user->setPassword(Helper::bcryptHashing($password))
                ->save();
            $result['status'] = true;
        } elseif (!empty($token) && !empty($password)) {
            $changePasswordObject = $this->getModel(ChangePassword::class)->loadByKey('email_token',
                $token);
            if (!$changePasswordObject->isExpired()) {
                $changePasswordObject->getUser()->setSecurePassword($password)->save();
                $changePasswordObject->setIsUsed(1)->save();
                $result['status'] = true;
            }
        } else {
            $httpResponse = $this->getResponse();
            $httpResponse->setStatusCode(500);
            $httpResponse->getHeaders()->addHeaders(['Content-type' => 'application/json']);
            $httpResponse->setContent(
                json_encode(array(
                'result' => false,
                'error' => $this->getModel(Translator::class)->__('Error').'!'
                ))
            );
            return $httpResponse;
        }
        return new JsonModel($result);
    }

    public function verifyPasswordTokenAction()
    {
        $token  = trim($this->getRequest()->getPost('token'));
        $result = [
            'status' => false
        ];
        if (!empty($token)) {
            $changePasswordObject = $this->getModel(ChangePassword::class)->loadByKey('email_token',
                $token);

            $result['status'] = $changePasswordObject->isExist() && !$changePasswordObject->getIsUsed()
                && !$changePasswordObject->isExpired();
        }
        return new JsonModel($result);
    }

    public function registerAction()
    {

        $firstName = trim($this->getRequest()->getPost('first_name', ''));
        $lastName  = trim($this->getRequest()->getPost('last_name', ''));
        $response  = [];
        $name      = trim($this->getRequest()->getPost('name', ''));
        if (!empty($name)) {
            $firstName = $name;
        }
        $email    = trim(strtolower($this->getRequest()->getPost('email')));
        $password = trim($this->getRequest()->getPost('password'));
        $phone    = trim($this->getRequest()->getPost('phone'));

        $user = $this->getModel('Arbel\Model\User')->loadByKey('email', $email);
        if (!$user || !$user->isExist()) {
            $user = $this->getModel('Arbel\Model\User')->loadByKey('phone',
                $phone);
            if (!$user->isExist()) {
                //create non existing user
                $user = $this->getModel('Arbel\Model\User')->setData(
                        [
                            'first_name' => $firstName,
                            'last_name' => $lastName,
                            'email' => $email,
                            'password' => Helper::bcryptHashing($password),
                            'phone' => $phone,
                        ]
                    )->save();
            } else {
                $response = [
                    'action' => 'error',
                    'message' => $this->__('The Email\Phone already exists in the system,')
                ];
            }
        } else {
            $response = [
                'action' => 'error',
                'message' => $this->__('The Email\Phone already exists in the system,')
            ];
        }

        if (!$user->isVerified()) {
            if (!$user->getRegistration()) {
                $user->initRegistration();
            }
            if (!$user->getRegistration()->getIsEmailVerified()) {
                $actionStatus = $user->getRegistration()->sendEmailVerification();
                $response     = [
                    'action' => 'email_verification',
                    'action_status' => $actionStatus
                ];
            } elseif (!$user->getRegistration()->getIsPhoneVerified()) {
                $actionStatus = $user->getRegistration()->sendPhoneVerification();
            }
        }
        return new JsonModel($response);
    }

    public function updateProfileAction()
    {

        $email          = trim($this->getRequest()->getPost('email', ''));
        $phone          = trim($this->getRequest()->getPost('phone', ''));
        $name           = trim($this->getRequest()->getPost('name'));
        $currentUser    = $this->getServiceManager()->get('User');
        $isEmailChanged = $currentUser->getEmail() != $email;
        $isPhoneChanged = $currentUser->getPhone() != $phone;

        $user = $this->getModel('Arbel\Model\User')->loadByKey('email', $email);
        if (!$user || !$user->isExist() || $currentUser->getId() == $user->getId()) {
            $user = $this->getModel('Arbel\Model\User')->loadByKey('phone',
                $phone);
            if (!$user->isExist() || $currentUser->getId() == $user->getId()) {
                //upate existing user
                $currentUser
                    ->setPhone($phone)
                    ->setEmail($email)
                    ->setName($name)
                    ->setIsVerified(!$isEmailChanged && !$isPhoneChanged)
                    ->save();
                $user = $currentUser;
            } else {
                $response = [
                    'action' => 'error',
                    'message' => $this->__('The Email\Phone already exists in the system,')
                ];
            }
        } else {
            $response = [
                'action' => 'error',
                'message' => $this->__('The Email\Phone already exists in the system,')
            ];
        }

        if (!$user->isVerified()) {
            $tokens = $this->getModel(AccessToken::class)->getAll([
                'user_id' => $user->getId(),
                'expires > NOW()'
            ]);
            foreach($tokens as $token){
                $token->setExpires(date("Y-m-d H:i:s"))
                    ->save();
            }
            if (!$user->getRegistration()) {
                $user->initRegistration();
                $user->getRegistration()
                    ->setIsEmailVerified(!$isEmailChanged)
                    ->setIsPhoneVerified(!$isPhoneChanged)
                    ->save();
            }
            if (!$user->getRegistration()->getIsEmailVerified()) {
                $actionStatus = $user->getRegistration()->sendEmailVerification();
                $response     = [
                    'action' => 'email_verification',
                    'action_status' => $actionStatus,
                    'verification_type' => 'email',
                    'message' => 'We sent you verification email. please verify your email'
                ];
            } elseif (!$user->getRegistration()->getIsPhoneVerified()) {
                $actionStatus = $user->getRegistration()->sendPhoneVerification();
                $response     = [
                    'action' => 'phone_verification',
                    'action_status' => $actionStatus,
                    'verification_type' => 'sms',
                    'registration_code' => Shorten::toKey($user->getRegistration()->getId()),
                    'message' => 'We sent you sms verification . please verify your phone'
                ];
            }
        }
        return new JsonModel($response);
    }

    public function tokenAction()
    {
        $token  = trim($this->getRequest()->getPost('token', ''));
        $result = [
            'status' => false
        ];
        if (!empty($token)) {
            $registration = $this->getModel(\Arbel\Model\Auth\Registration::class)->loadByKey('email_token',
                $token);
            if (!$registration->isExpired()) {
                $registration->setIsEmailVerified(true)->save();
                if ($registration->getIsPhoneVerified()) {
                    $registration->getUser()->setIsVerified(true)
                        ->save();
                }else{
                    $registration->sendPhoneVerification();
                }
                $result['status']            = true;
                $result['is_phone_verified'] = $registration->getIsPhoneVerified();
                $result['registration']      = Shorten::toKey($registration->getId());
            }
        }
        return new JsonModel($result);
    }

    public function verifyPhoneNumberAction()
    {
        $phone            = trim($this->getRequest()->getPost('phone'));
        $registrationCode = trim($this->getRequest()->getPost('registration'));
        $result           = [
            'status' => false
        ];
        if (!empty($registrationCode) && !empty($phone)) {
            $registrationId = Shorten::toNumber($registrationCode);
            /**
             * @var Registration;
             */
            $registration   = $this->getModel(\Arbel\Model\Auth\Registration::class)->load($registrationId);
            if (
                $registration->isExist() &&
                !$registration->isExpired() &&
                !$registration->getUser()->isVerified() &&
                $registration->getUser()->getPhone() == $phone
            ) {
                $registration->initPhoneCode()->save();
                //$registration->getUser()->setPhone($phone)->save();
                $result['status'] = $registration->sendPhoneVerification();
            }
        }
        return new JsonModel($result);
    }

    public function registrationInfoAction()
    {
        $registrationCode = trim($this->getRequest()->getPost('registration'));
        $result           = [
            'status' => false,
            'is_expired' => false,
            'is_exist' => false
        ];
        if (!empty($registrationCode)) {
            $registrationId = Shorten::toNumber($registrationCode);
            /**
             * @var Registration;
             */
            $registration   = $this->getModel(\Arbel\Model\Auth\Registration::class)->load($registrationId);
            if ($registration->isExist()) {
                $result = [
                    'status' => $registration->getUser()->isVerified(),
                    'is_expired' => $registration->isExpired(),
                    'is_exist' => true
                ];
            }
        }
        return new JsonModel($result);
    }

    public function verifyPhoneCodeAction()
    {
        $phone            = trim($this->getRequest()->getPost('phone'));
        $registrationCode = trim($this->getRequest()->getPost('registration'));
        $code             = trim($this->getRequest()->getPost('code'));
        $result           = [
            'status' => false
        ];
        if (!empty($code) && !empty($phone)) {
            $registrationId = Shorten::toNumber($registrationCode);
            /**
             * @var Registration;
             */
            $registration   = $this->getModel(\Arbel\Model\Auth\Registration::class)->load($registrationId);
            if (
                $registration->isExist() &&
                !$registration->isExpired() &&
                !$registration->getUser()->isVerified() &&
                $registration->getPhoneCode() == $code
            ) {
              
                $registration->setIsPhoneVerified(true)->save();
                if ($registration->getIsEmailVerified()) {
                    $registration->getUser()->setIsVerified(true)->save();
                    $result['status'] = true;
                    return $this->loginDefaultAppToken($registration->getUser()->getId());
                }
            }
        }
        return new JsonModel($result);
    }
}