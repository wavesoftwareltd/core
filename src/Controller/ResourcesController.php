<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Arbel\Controller;

use Zend\View\Model\ViewModel;
use Arbel\Cache\CacheManager;
use Zend\Console\Adapter\AdapterInterface as Console;
use Zend\Console\Exception\RuntimeException;
use Zend\ServiceManager\ServiceManager;

/**
 * Cache controller
 */
class ResourcesController extends AbstractActionController
{
    /**
     * CacheManager
     * @var CacheManager
     */
    protected $cacheManager;

    /**
     * Inject objects
     * @param StorageInterface $cache
     */
    public function setInjections(CacheManager $cacheManager)
    {
        $this->cacheManager = $cacheManager;
    }

    public function copyAction()
    {
        $manager       = $this->getServiceManager()->get('ModuleManager');
        $modules       = $manager->getLoadedModules();
        $loadedModules = array_keys($modules);

        foreach ($loadedModules as $loadedModule) {
            $console      = $this->getServiceManager()->get('console');
            $console->writeLine('Load module '.$loadedModule);
            $moduleClass  = '\\'.$loadedModule.'\Module';
            $reflector    = new \ReflectionClass($moduleClass);
            $moduleDir    = dirname(dirname($reflector->getFileName()));
            $resourcesDir = $moduleDir.DIRECTORY_SEPARATOR.'resources';
            $targetFolder = ROOT_DIR.DIRECTORY_SEPARATOR.'resources';
            if (file_exists($resourcesDir)) {
                $console->writeLine($loadedModule.' $resourcesDir exist = '.$resourcesDir);
                \Arbel\Helper::copyFolder($resourcesDir, $targetFolder);
            }
        }
    }

    public function symlinkAction()
    {
        $manager       = $this->getServiceManager()->get('ModuleManager');
        $modules       = $manager->getLoadedModules();
        $loadedModules = array_keys($modules);

        foreach ($loadedModules as $loadedModule) {
            $console      = $this->getServiceManager()->get('console');
            $moduleClass  = '\\'.$loadedModule.'\Module';
            $reflector    = new \ReflectionClass($moduleClass);
            $moduleDir    = dirname(dirname($reflector->getFileName()));
            $resourcesDir = $moduleDir.DIRECTORY_SEPARATOR.'resources';
            $targetFolder = ROOT_DIR.'resources';
            if (file_exists($resourcesDir)) {
                $console->writeLine($loadedModule.' sync folder = '.$resourcesDir);
                \Arbel\Helper::symlinkFiles($resourcesDir, $targetFolder);
            }
        }
    }

    public function symlinkFoldersAction()
    {
        $manager       = $this->getServiceManager()->get('ModuleManager');
        $modules       = $manager->getLoadedModules();
        $loadedModules = array_keys($modules);

        foreach ($loadedModules as $loadedModule) {
            $console      = $this->getServiceManager()->get('console');
            $moduleClass  = '\\'.$loadedModule.'\Module';
            $reflector    = new \ReflectionClass($moduleClass);
            $moduleDir    = dirname(dirname($reflector->getFileName()));
            $resourcesDir = $moduleDir.DIRECTORY_SEPARATOR.'resources';
            $targetFolder = ROOT_DIR.'resources';
            if (file_exists($resourcesDir)) {
                $console->writeLine($loadedModule.' sync folder = '.$resourcesDir);
                \Arbel\Helper::symlinkFolders($resourcesDir, $targetFolder);
            }
        }
    }

    public function watchAction()
    {
        $this->getModel('Arbel\Manage\Manager')->watch();
    }

    public function initAction()
    {
        $this->getModel('Arbel\Manage\Manager')->init();
    }
    
    public function buildAction()
    {
        $code = $this->getRequest()->getParam('code');
        $this->getModel('Arbel\Manage\Manager')->getAngularCli()->buildTransaltedApps($code);
    }

}