<?php

namespace Arbel\Controller;

use Arbel\Base\ModelAbstract;
use Zend\InputFilter\InputFilter;
use Zend\View\Model\JsonModel;
use Arbel\Model\Audit;

/**
 * Abstract Crud Controller
 */
abstract class AbstractCrudController extends AbstractRestfulController
{
    /**
     * Where join permission params
     * @var array
     */
    protected $whereJoinPermissionParams = array();

    /**
     * Where permission params
     * @var array
     */
    protected $wherePermissionParams = array();

    /**
     * Last entity that created
     * @var ModelAbstract
     */
    protected $lastEntity;

    /**
     * Auto audit mode
     * @var is auto audit mode
     */
    protected $isAutoAuditMode;

    /**
     * Current crud entity
     */
    abstract function getEntity(): ModelAbstract;

    /**
     * Input filter for create
     */
    abstract function getInputFilter(): InputFilter;

    /**
     * Get error messages from input filter
     * @return array
     */
    public function getErrorMessages(): array
    {
        $result = array();
        foreach ($this->getInputFilter()->getInvalidInput() as $error) {
            $result[] = $error->getMessages();
            return $result;
        }
    }

    /**
     * Return error to client
     * @param string $message
     * @return JsonModel
     */
    public function throwError(string $message)
    {
        $this->getResponse()->setStatusCode(500);
        return new JsonModel([
            'message' => $message,
            'error' => true
        ]);
    }

    /**
     * Create by data
     * @param array $data
     */
    public function create($data)
    {
        $this->getInputFilter()->setData($data);

        //If data was valid
        if ($this->getInputFilter()->setData($data)->isValid()) {
            $entity = $this->getEntity();
            //is update
            if (isset($data[$entity->getPrimaryKey()])) {
                $entity->loadBy($this->getWherePermissionParams([
                        $entity->getPrimaryKey() => $data[$entity->getPrimaryKey()]
                ]));
            } else {
                $data = array_merge($data, $this->getWherePermissionParams());
            }
            $entity->setData($data);
            if ($this->isAutoAuditMode) {
                if (isset($data[$entity->getPrimaryKey()])) {
                    $newData = $entity->getChangedData();
                    foreach ($newData as $k => $v) {
                        $this->getModel(Audit::class)->addEvent($entity->getAuditKey().'_'.$k.'_update',
                            $v,['id' => $entity->getId()],$entity->getOriginData($k),$data[$entity->getPrimaryKey()]);
                    }
                } else {
                    $this->getModel(Audit::class)->addEvent($entity->getAuditKey().'_new', $data);
                }
            }

            $entity->save(true);
            $this->lastEntity = $entity;
            $result           = $entity->getPublicData();
        } else {
            $this->getResponse()->setStatusCode(400);
            $result = $this->getErrorMessages();
        }

        return new JsonModel($result);
    }

    /**
     * Get last entity that created
     * return ModelAbstract
     */
    public function getLastEntity()
    {
        return $this->lastEntity;
    }

    /**
     * Get List of all users
     * @return JsonModel
     */
    public function getList()
    {
        $entities = $this->getEntity()->getAll($this->getWherePermissionParams(), null,
            $this->getWhereJoinPermissionParams());
        $result   = array();
        foreach ($entities as $entity) {
            $result[] = $entity->getPublicData();
        }

        return new JsonModel($result);
    }

    /**
     * Get user by id
     * @param int $id
     * @return JsonModel
     */
    public function get($id)
    {
        $result = [];
        // report error, redirect, etc.
        $entity = $this->getEntity()->loadBy($this->getWherePermissionParams([$this->getEntity()->getPrimaryKey() => $id]));
        if ($entity->getId()) {
            $result = $entity->getPublicDataWithRelationsIds();
        }

        return new JsonModel($result);
    }

    /**
     * Delete an existing resource
     *
     * @param  mixed $id
     * @return mixed
     */
    public function delete($id)
    {
        $result['status'] = false;
        // report error, redirect, etc.
        $entites          = $this->getEntity()->getAll($this->getWherePermissionParams([$this->getEntity()->getPrimaryKey() => $id]));
        foreach ($entites as $entity) {
            $entity->delete();
            $result['status'] = true;
            if ($this->isAutoAuditMode) {
                 $this->getModel(Audit::class)->addEvent($entity->getAuditKey().'_delete', $entity->getId());
            }
        }

        return new JsonModel($result);
    }


    public function deleteList($data){
         $result['status'] = false;
         if(isset($data['id'])){
             $data = $data['id'];
         }
        // report error, redirect, etc.
        $entites          = $this->getEntity()->getAll($this->getWherePermissionParams([$this->getEntity()->getPrimaryKey() => $data]));
        foreach ($entites as $entity) {
            $entity->delete();
            $result['status'] = true;
            if ($this->isAutoAuditMode) {
                 $this->getModel(Audit::class)->addEvent($entity->getAuditKey().'_delete', $entity->getId());
            }
        }

        return new JsonModel($result);
    }

    /**
     * Get user by id
     * @param int $id
     * @return JsonModel
     */
    public function relationsAction()
    {
        $relations = $this->getRequest()->getQuery('relations');
        $result    = [];
        // report error, redirect, etc.
        $entity    = $this->getEntity()->loadBy($this->getWherePermissionParams([
                $this->getEntity()->getPrimaryKey() => $id]));
        if ($entity->getId()) {
            $result = $entity->getPublicData();
        }

        return new JsonModel($result);
    }

    /**
     * Grid action filter support
     */
    public function gridAction()
    {
        $page            = $this->getRequest()->getQuery('draw');
        $start           = $this->getRequest()->getQuery('start');
        $length          = $this->getRequest()->getQuery('length');
        $search          = $this->getRequest()->getQuery('search');
        $searchValue     = $search['value'] ?? '';
        $orderBy         = null;
        $entities        = $this->getEntity()->searchAll($searchValue, array(),
            $this->getWherePermissionParams(), $orderBy, $this->getWhereJoinPermissionParams(),
            null, $length, $start);
        $filterdEntities = $this->getEntity()->searchAll($searchValue, array(),
            $this->getWherePermissionParams(), null, $this->getWhereJoinPermissionParams());
        $allEntities     = $this->getEntity()->getAll($this->getWherePermissionParams(), null,
            $this->getWhereJoinPermissionParams());
        $result          = [
            'data' => $this->getGridData($entities),
            'recordsFiltered' => count($filterdEntities),
            'recordsTotal' => count($allEntities)
        ];

        return new JsonModel($result);
    }

    /**
     * Get grid data
     * @param array $entities
     * @return array data 
     */
    public function getGridData(array $entities): array
    {
        $result = array();
        foreach ($entities as $entity) {
            $result[] = $entity->getPublicData();
        }
        return $result;
    }

    /**
     * Get where permission params
     * @return array
     */
    public function getWherePermissionParams(array $moreParams = array()): array
    {
        return array_merge($moreParams, $this->wherePermissionParams);
    }

    /**
     * Set where permission params
     * @param array $params
     */
    public function setWherePermissionParams(array $params)
    {
        $this->wherePermissionParams = $params;
    }

    /**
     * Get where permission params
     * @return array
     */
    public function getWhereJoinPermissionParams(array $moreParams = array()): array
    {
        return array_merge($moreParams, $this->whereJoinPermissionParams);
    }

    /**
     * Set where permission params
     * @param array $params
     */
    public function setWhereJoinPermissionParams(array $params)
    {
        $this->whereJoinPermissionParams = $params;
    }

    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        parent::onDispatch($e);
    }
}