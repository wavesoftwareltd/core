<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Arbel\Controller;

use Zend\View\Model\ViewModel;
use Arbel\Cache\CacheManager;
use Zend\Console\Adapter\AdapterInterface as Console;
use Zend\Console\Exception\RuntimeException;

/**
 * Cache controller
 */
class CacheController extends AbstractActionController
{
    /**
     * CacheManager
     * @var CacheManager
     */
    protected $cacheManager;

    /**
     * Inject objects
     * @param StorageInterface $cache
     */
    public function setInjections(CacheManager $cacheManager)
    {
        $this->cacheManager = $cacheManager;
    }

    public function listAction()
    {
        $cacheList = $this->getModel('Arbel\Model\Cache')->getAll();
        $console   = $this->getServiceManager()->get('console');
        if (!$console instanceof Console) {
            throw new \RuntimeException('Cannot obtain console adapter. Are we running in a
console?');
        }
        foreach ($cacheList as $cache) {
            $console->writeLine($cache->getName().' ('.$cache->getCode().') - '.($cache->getStatus() ? 'enable'
                        : 'disabled'));
        }
    }


    public function enableAction()
    {
        $cacheObject = $this->getModel('Arbel\Model\Cache')->loadByKey('code',$this->getRequest()->getParam('code'));

        $console   = $this->getServiceManager()->get('console');
        if (!$console instanceof Console) {
            throw new \RuntimeException('Cannot obtain console adapter. Are we running in a
console?');
        }
        if($cacheObject){
             $cacheObject->setStatus('1');
             $console->writeLine($this->getRequest()->getParam('code').' cache was updated!');

        }else{
             $console->writeLine('Cache with this name was not found!');
        }

    }



    public function disableAction()
    {
        $cacheObject = $this->getModel('Arbel\Model\Cache')->loadByKey('code',$this->getRequest()->getParam('code'));

        $console   = $this->getServiceManager()->get('console');
        if (!$console instanceof Console) {
            throw new \RuntimeException('Cannot obtain console adapter. Are we running in a
console?');
        }
        if($cacheObject){
             $cacheObject->setStatus('0')->save();
             $console->writeLine($this->getRequest()->getParam('code').' cache was updated!');

        }else{
             $console->writeLine('Cache with this name was not found!');
        }

    }


    public function cleanAction()
    {
        $code = $this->getRequest()->getParam('code','all');
        $cacheObject = $this->cacheManager->get($code);

        $console   = $this->getServiceManager()->get('console');
        if (!$console instanceof Console) {
            throw new \RuntimeException('Cannot obtain console adapter. Are we running in a
console?');
        }

        if($code == 'all'){
        $cacheObject->deleteAll();
        }else{
        $cacheObject->delete();
        }
             $console->writeLine($this->getRequest()->getParam('code').' cache was cleand!');

    }
}