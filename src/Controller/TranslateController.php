<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Arbel\Controller;

use Carbon\Carbon;
use Arbel\Model\Communication\Platform;
use Arbel\Model\Communication\Conversation;
use Arbel\Model\Communication\Interlocutor;
use Arbel\Model\Communication\Interlocutor\Test;
use Arbel\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\View\Model\JsonModel;

/**
 * Info controller
 */
class TranslateController extends AbstractActionController {

    public function indexAction() {
        $value = $this->queryParam('text');
        return new JsonModel(
                array(
            'result' => $this->getTranslator()->__(strip_tags($value))
                )
        );
    }

}
