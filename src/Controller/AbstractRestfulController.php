<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Arbel\Controller;

use Zend\ServiceManager\ServiceManager;
use Arbel\Log;
use Arbel\ObjectManager;
use Arbel\Model\Translator;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;
use Zend\Diactoros\ServerRequest;
use Zend\Stdlib\RequestInterface as Request;
use Arbel\Base\ModelAbstract;
use Arbel\Model\Log as DbLog;
/**
 * Abstract controller
 */
class AbstractRestfulController extends \Zend\Mvc\Controller\AbstractRestfulController
{
    /**
     * ServiceManager object
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * Log object
     * @var Log
     */
    protected $log;

    /**
     * ObjectManager object
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * Translator object
     * @var Translator
     */
    protected $translator;

    /**
     * Translator object
     * @var Translator
     */
    protected $psrResponse;

    /**
     * Translator object
     * @var Translator
     */
    protected $psrRequest;

    public function __construct(ServiceManager $sm, Log $log,
                                ObjectManager $objectManager,
                                Translator $translate,
                                PsrResponseInterface $psrResponse = null,
                                ServerRequest $psrRequest = null)
    {
        $this->serviceManager = $sm;
        $this->log            = $log;
        $this->objectManager  = $objectManager;
        $this->translator     = $translate;
        $this->psrResponse    = $psrResponse;
        $this->psrRequest     = $psrRequest;
        $this->init();
        
    }

    public function init()
    {

    }

    /**
     * Get service manager
     * @return ServiceManager
     */
    public function getServiceManager(): ServiceManager
    {
        return $this->serviceManager;
    }

    /**
     * Get service manager
     * @return ServiceManager
     */
    public function getPsrResponse()
    {
        return $this->psrResponse;
    }

    /**
     * Get service manager
     * @return ServiceManager
     */
    public function getPsrRequest()
    {
        return $this->psrRequest;
    }

    /**
     * Get log
     * @return Log
     */
    public function getLog(): Log
    {
        return $this->log;
    }

    /**
     * Get object manager
     * @return ObjectManager
     */
    public function getObjectManager(): ObjectManager
    {
        return $this->objectManager;
    }

    /**
     * Alias name for get object manager
     * @param string $modelName
     * @return ModelAbstract
     */
    public function getModel(string $modelName)
    {
        return $this->getObjectManager()->get($modelName);
    }

    /**
     * Get Translator object
     * @return Translator
     */
    public function getTranslator(): Translator
    {
        return $this->translator;
    }

    /**
     * Get db log
     * @return DbLog
     */
    protected function log(): DbLog
    {
        return $this->getModel(DbLog::class);
    }

    /**
     * Alias name for translate
     * @param string $key
     * @param string $lang
     * @param string $domain
     * @return string
     */
    public function __(string $key, string $lang = null,
                       string $domain = 'default'): string
    {
        return $this->getTranslator()->translate($key, $lang, $domain);
    }

    public function preDispatch(\Zend\Mvc\MvcEvent $e)
    {
        $request = $e->getRequest();
    }

    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        $this->preDispatch($e);
        parent::onDispatch($e);
    }

    /**
     * Valid params
     * @param mixed $target
     * @param array $params
     * @throws Exception
     */
    public function validParams($target,array $params)
    {
        if(!is_array($target)){
            $target = [$target];
        }
        foreach($target as $key){
            if(!isset($params[$key])){
                throw new \Exception('The param '.$key.' is not exist!');
            }
        }
    }

 
}