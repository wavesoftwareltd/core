<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Arbel\Controller;

use Carbon\Carbon;
use Arbel\Model\Communication\Platform;
use Arbel\Model\Communication\Conversation;
use Arbel\Model\Communication\Interlocutor;
use Arbel\Model\Communication\Interlocutor\Test;
use Arbel\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Arbel\Model\ShortenUrl;
use Arbel\View\Model\JsonModel;
/**
 * Info controller
 */
class ShortenController extends AbstractActionController
{
    
    
    public function indexAction()
    {

        $code = $this->params()->fromRoute('code');
        $shorten     = $this->getModel(ShortenUrl::class)->loadByKey('code',
            $code);
        $asJson = $this->getRequest()->getQuery('as_json',false);
        if($shorten->isExist()){
            if($asJson){
                return new JsonModel([
                    'url' => $shorten->getValue()
                ]);
            }
            return $this->redirect()->toUrl($shorten->getValue());
        }else{
            $httpResponse = $this->getResponse();
            $httpResponse->setStatusCode(404);
            return $httpResponse;
        }
    }

}