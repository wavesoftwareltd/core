<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Arbel\Controller;

use Zend\Mvc\Controller\AbstractActionController as ZendAbstractActionController;
use Zend\ServiceManager\ServiceManager;
use Arbel\Log;
use Arbel\ObjectManager;
use Arbel\Model\Translator;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;
use Zend\Psr7Bridge\Psr7Response;
use Zend\Psr7Bridge\Psr7ServerRequest;
use Zend\Diactoros\ServerRequest;
use Zend\Mvc\MvcEvent;
use Arbel\Base\ModelAbstract;
use Zend\Http\PhpEnvironment\Response as HttpResponse;
use Arbel\Upload\UploadInterface;
use Arbel\Model\Log as DbLog;
use Arbel\Config;
use Zend\Http\Request;
use Arbel\Helper;


/**
 * Abstract controller
 */
class AbstractActionController extends ZendAbstractActionController
{
    /**
     * ServiceManager object
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * Log object
     * @var Log
     */
    protected $log;

    /**
     * ObjectManager object
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * Translator object
     * @var Translator
     */
    protected $translator;

    /**
     * Translator object
     * @var Translator
     */
    protected $psrResponse;

    /**
     * Translator object
     * @var Translator
     */
    protected $psrRequest;

    /**
     * UploadInterface object
     * @var UploadInterface
     */
    protected $uploadManager;

    public function __construct(ServiceManager $sm, Log $log,
                                ObjectManager $objectManager,
                                Translator $translate,
                                UploadInterface $upload,
                                PsrResponseInterface $psrResponse = null,
                                ServerRequest $psrRequest = null )
    {
        $this->serviceManager = $sm;
        $this->log            = $log;
        $this->objectManager  = $objectManager;
        $this->translator     = $translate;
        $this->psrResponse    = $psrResponse;
        $this->psrRequest     = $psrRequest;
        $this->uploadManager  = $upload;
        
    }
    

    /**
     * Get service manager
     * @return ServiceManager
     */
    public function getServiceManager(): ServiceManager
    {
        return $this->serviceManager;
    }

    /**
     * Get service manager
     * @return ServiceManager
     */
    public function getPsrResponse()
    {
        return $this->psrResponse;
    }

    /**
     * Get service manager
     * @return UploadInterface
     */
    public function getUploadManager()
    {
        return $this->uploadManager;
    }

    /**
     * Get service manager
     * @return Psr7ServerRequest
     */
    public function getPsrRequest()
    {
        return $this->psrRequest;
    }

    /**
     * Get log
     * @return Log
     */
    public function getLog(): Log
    {
        return $this->log;
    }


    /**
     * Get db log
     * @return DbLog
     */
    protected function log(): DbLog
    {
        return $this->getModel(DbLog::class);
    }

    /**
     * Get db log
     * @return DbLog
     */
    protected function getConfig()
    {
        return $this->getModel(Config::class);
    }

    /**
     * Get object manager
     * @return ObjectManager
     */
    public function getObjectManager(): ObjectManager
    {
        return $this->objectManager;
    }

    /**
     * Alias name for get object manager
     * @param string $modelName
     * @return ModelAbstract
     */
    public function getModel(string $modelName)
    {
        return $this->getObjectManager()->get($modelName);
    }

    /**
     * Get Translator object
     * @return Translator
     */
    public function getTranslator(): Translator
    {
        return $this->translator;
    }

    /**
     * Alias name for translate
     * @param string $key
     * @param string $lang
     * @param string $domain
     * @return string
     */
    public function __(string $key, string $lang = null,
                       string $domain = 'default'): string
    {
        return $this->getTranslator()->translate($key, $lang, $domain);
    }

    /**
     * We override the parent class' onDispatch() method to
     * set an alternative layout for all actions in this controller.
     */
    public function onDispatch(MvcEvent $e)
    {
        // Call the base class' onDispatch() first and grab the response
        $response        = parent::onDispatch($e);
        $class           = get_class($this);
        $ns              = substr($class, 0, strrpos($class, '\\'));
        $moduleNameSapce = strtolower(str_replace('\\', '/',
                str_replace('\Controller', '', $ns)));
        // Set alternative layout
        $this->layout()->setTemplate($moduleNameSapce.$this->getCurrentLayout());

        // Return the response
        return $response;
    }

    public function getCurrentLayout()
    {
        return '/layout/layout';
    }

    /**
     * Get request object
     *
     * @return pointer Request
     */
//    public function &getRequest()
//    {
//        if (!$this->request) {
//            $this->request = new Request();
//        }
//
//        return $this->request;
//    }

    /**
     * Get response object
     *
     * @return pointer Response
     */
    public function &getResponse()
    {
        if (!$this->response) {
            $this->response = new HttpResponse();
        }

        return $this->response;
    }


    /**
     * Valid params
     * @param mixed $target
     * @param array $params
     * @throws Exception
     */
    public function validParams($target,array $params)
    {
        if(!is_array($target)){
            $target = [$target];
        }
        foreach($target as $key){
            if(!isset($params[$target])){
                exit('The param '.$key.' is not exist!');
            }
        }
    }


    /**
     * Valid params
     * @param mixed $target
     * @param array $params
     * @throws Exception
     */
    public function getValidQueryParam($target)
    {
        $res = $this->getRequest()->getQuery($target,null);
        if(!is_null($res)){
            return $res;
        }else{
            exit('The param '.$target.' is not exist!');
        }
    }

    /**
     * Valid params
     * @param mixed $target
     * @param array $params
     * @throws Exception
     */
    public function getValidPostParam($target)
    {
        $res = $this->getRequest()->getPost($target,null);
        if(!is_null($res)){
            return $res;
        }else{
            exit('The param '.$target.' is not exist!');
        }
    }

    
    /**
     * Valid params
     * @param mixed $target
     * @param array $params
     * @throws Exception
     */
    public function getValidArrParam($target,$arr)
    {
        if(array_key_exists($target,$arr)){
            return $arr[$target];
        }else{
            exit('The param '.$target.' is not exist!');
        }
    }

    public function getCookie(){
        return new \Arbel\Base\Cookie();
    }
}