<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Arbel\Controller;

use Carbon\Carbon;
use Arbel\Model\Communication\Platform;
use Arbel\Model\Communication\Conversation;
use Arbel\Model\Communication\Interlocutor;
use Arbel\Model\Communication\Interlocutor\Test;
use Arbel\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Arbel\Model\Communication\Notification;
use Arbel\View\Model\JsonModel;

/**
 * Communication controller
 */
class CommunicationController extends AbstractActionController
{
    protected $acl;

    /**
     * Inject objects
     * @param StorageInterface $cache
     */
    public function setInjections(Acl $acl)
    {
        $this->acl = $acl;
    }

    public function contactAction()
    {
        $email   = $this->getRequest()->getPost('email');
        $name    = $this->getRequest()->getPost('name');
        $subject = $this->getRequest()->getPost('subject');
        $message = $this->getRequest()->getPost('message');
        $res = $this->getModel(Notification::class)->sendEmailToAddress($this->getConfig()->get('system.email'),
            'Contact us',
            'Email : '.$email.'<br>'
            .'Name : '.$name.'<br>'
            .'Subject : '.$subject.'<br>'
            .'Message : '.$message.'<br>'
        );
        return new JsonModel([
            'status' => $res
        ]);
    }

    public function createAction()
    {


        $user = $this->getModel('Arbel\Model\User');
        $user->setFirstName('Yariv')
            ->setLastName('Luts')
            ->setEmail('yariv@picapp.co.il')
            ->setLanguageCode('he-il')
            ->setPhone('0545742747')
            ->setCountryCode('IL')
            ->save(true);

        $user2 = $this->getModel('Arbel\Model\User');
        $user2->setFirstName('System')
            ->setLastName('System')
            ->setEmail('service@w.wave-software.net')
            ->setLanguageCode('he-il')
            ->setPhone('0526264227')
            ->setCountryCode('IL')
            ->save(true);


        $channel = $this->getModel('Arbel\Model\Channel');
        $channel->setName('Test channel')
            ->setFinishAt(Carbon::now()->addDay()->toDateTimeString())
            ->save(true);

        $conversation = $this->getModel(Conversation::class);
        $conversation->setChannel($channel)
            ->save(true);

        $interlocutor = $this->getModel(Interlocutor::class);
        $interlocutor->setConversation($conversation)
            ->setUser($user)
            ->setPlatformCode(Platform::SMS)
            ->setSource($user->getFinalPhone())
            ->save();

        $interlocutor2 = $this->getModel(Test::class);
        $interlocutor2->setConversation($conversation)
            ->save();

        $interlocutor2->send('מה המצב?');
        exit('***exit***');
    }

    public function smsReciverAction()
    {
        $callBack = $_GET['callback'] ?? '';
        $data     = (array) $this->getRequest()->getQuery();

        $from    = '+'.$this->getRequest()->getQuery('From');
        $to      = '+'.$this->getRequest()->getQuery('To');
        $message = $this->getRequest()->getQuery('Text');

        $conversation = $this->getModel(Conversation::class)->getOpenBy(array($from,
            $to));

        if ($conversation) {
            $sender   = $this->getModel(Interlocutor::class)->getOne(
                array(
                    'conversation_id' => $conversation->getId(),
                    'source' => $from
            ));
            $recviers = $this->getModel(Interlocutor::class)->getAll(
                array(
                    'conversation_id' => $conversation->getId(),
                    'source' => array('!=', $from)
            ));

            foreach ($recviers as $reciver) {
                $reciver->receive($sender, $message);
            }
        }


        exit($_GET['callback'].'('.json_encode(array('result' => array())).')');
    }

    public function testAction()
    {
        return ['m' => 4];
        exit('<pre>----->'.print_r($this->getRequest(), true).'</pre>');
        exit('***exit***');
    }
}