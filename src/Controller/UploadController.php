<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Arbel\Controller;

use Arbel\View\Model\JsonModel;

/**
 * Info controller
 */
class UploadController extends AbstractActionController
{

    public function imageAction()
    {

        $files     = $this->getRequest()->getFiles()->toArray();
        $result    = array();
        $filesize  = new \Zend\Validator\File\Size(array('min' => 10000, 'max' => 10000
            * 5000)); //1KB
        $extension = new \Zend\Validator\File\Extension(array('extension' => array(
                'jpg', 'png', 'jpeg', 'gif')));
        foreach ($files as $fileKey => $file) {
            $fileName = $file['name'];
            $filePath = $file['tmp_name'];
            $ext      = pathinfo($fileName, PATHINFO_EXTENSION);

            if ($extension->isValid($file)) {
                $url    = $this->getUploadManager()->upload($filePath, $ext,
                    'upload');
                $result = array(
                    'uploaded' => 1,
                    'fileName' => $fileName,
                    'url' => $url
                );
            } else {
                $result = array(
                    'uploaded' => 0,
                    'error' => [
                        'message' => 'the file is not match!'
                    ]
                );
            }
        }

        return new JsonModel($result);
    }

    public function fileAction()
    {
       
        $files     = $this->getRequest()->getFiles()->toArray();
        $result    = array();
        $filesize  = new \Zend\Validator\File\Size(array('min' => 10000, 'max' => 10000
            * 5000)); //1KB
        foreach ($files as $fileKey => $file) {
            $fileName = $file['name'];
            $filePath = $file['tmp_name'];
            $ext      = pathinfo($fileName, PATHINFO_EXTENSION);

//            if ($filesize->isValid($file)) {
                $url    = $this->getUploadManager()->upload($filePath, $ext,
                    'upload');
                $result = array(
                    'uploaded' => 1,
                    'fileName' => $fileName,
                    'url' => $url
                );
//            } else {
//                $result = array(
//                    'uploaded' => 0,
//                    'error' => [
//                        'message' => 'the file is not match!'
//                    ]
//                );
//            }
        }

        return new JsonModel($result);
    }

    public function imageCkeditorAction()
    {

        $files     = $this->getRequest()->getFiles()->toArray();
        $functionNumber     = $this->getRequest()->getQuery('CKEditorFuncNum');
        $result    = array();
        $filesize  = new \Zend\Validator\File\Size(array('min' => 1000, 'max' => 1000
            * 5000)); //1KB
        $extension = new \Zend\Validator\File\Extension(array('extension' => array(
                'jpg', 'png', 'jpeg', 'gif')));
        foreach ($files as $fileKey => $file) {
            $fileName = $file['name'];
            $filePath = $file['tmp_name'];
            $ext      = pathinfo($fileName, PATHINFO_EXTENSION);

            if ($extension->isValid($file) && $filesize->isValid($file)) {
                $url = $this->getUploadManager()->upload($filePath, $ext,
                    'upload');
                exit('<script type="text/javascript">
    window.parent.CKEDITOR.tools.callFunction("'.$functionNumber.'", "'.$url.'", "");
</script>');
            }
        }

        exit;
    }
}