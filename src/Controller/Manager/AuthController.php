<?php

namespace Arbel\Controller\Manager;

use Carbon\Carbon;
use Arbel\Model\Communication\Platform;
use Arbel\Model\Communication\Conversation;
use Arbel\Model\Communication\Interlocutor;
use Arbel\Model\Communication\Interlocutor\Test;
use Arbel\Model\Translator;
use Arbel\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\View\Model\JsonModel;
use OAuth2\Server;
use Arbel\Helper;
use Arbel\Utils\Shorten;
use Arbel\Model\Auth\Registration;
use Arbel\Model\Auth\ChangePassword;
use Arbel\Model\Communication\Notification;
use Arbel\Model\Communication\Email\Template;

/**
 * Auth controller
 */
class AuthController extends \Arbel\Controller\OAuth2\AuthController
{
    protected $acl;

    /**
     * Inject objects
     * @param StorageInterface $cache
     */
    public function setInjections(Acl $acl)
    {
        $this->acl = $acl;
        if(!$this->getServiceManager()->get('User')->isAdministrator()){
            exit("you don't administrator and can't access this actions");
        }
    }

    public function registerAction()
    {

        $firstName = trim($this->getRequest()->getPost('first_name', ''));
        $resetUserPassword = trim($this->getRequest()->getPost('reset_user_password', false));
        $lastName  = trim($this->getRequest()->getPost('last_name', ''));
        $response  = [];
        $name      = trim($this->getRequest()->getPost('name', ''));
        if (!empty($name)) {
            $firstName = $name;
        }
        $email    = trim(strtolower($this->getRequest()->getPost('email')));
        $inputPassword = trim($this->getRequest()->getPost('password'));
        $info = $this->getRequest()->getPost('info',[]) ;
        $password = empty($inputPassword) ? rand(111111,999999) : $inputPassword;
        $phone    = trim($this->getRequest()->getPost('phone'));
        $id    = trim($this->getRequest()->getPost('id'));
        if(isset($id) && is_numeric($id)){
            $user = $this->getModel('Arbel\Model\User')->load($id);
        }else{
            $user = $this->getModel('Arbel\Model\User')->loadByKey('email', $email);
        }

        if (!$user ||!$user->isExist()) {
            if (!$user->isExist()) {
                //create non existing user
                $user = $this->getModel('Arbel\Model\User')->setData(
                        [
                            'first_name' => $firstName,
                            'last_name' => $lastName,
                            'email' => $email,
                            'password' => Helper::bcryptHashing($password),
                            'phone' => $phone,
                            'is_verified' => true,
                            'info' => $info
                        ]
                    )->save();
            }
            $response  = [
                    'status' => 'create_new_user',
                    'user' => $user->getPublicData()
                ];
            $emailTemplate = $this->getModel(Template::class)->loadByKey('code',
                'manager_user_registration');
            if($emailTemplate->isExist()){
                $emailTemplate->setParams([
                            'first_name' => $firstName,
                            'last_name' => $lastName,
                            'name' => $firstName.' '.$lastName,
                            'email' => $email,
                            'password' => $password,
                            'phone' => $phone,
                            'info' => $info
                ]);
                if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                  $this->getModel(Notification::class)
                    ->sendEmailToUser($user,$emailTemplate->generateSubject(),$emailTemplate->generateContent());  
                }
                
            }
        }else{

             $emailTemplate = $this->getModel(Template::class)->loadByKey('code',
                'manager_user_registration');

            if($resetUserPassword && $emailTemplate->isExist()){
                $user->setPassword(Helper::bcryptHashing($password));
                $emailTemplate->setParams([
                            'first_name' => $firstName,
                            'last_name' => $lastName,
                            'name' => $firstName.' '.$lastName,
                            'email' => $email,
                            'password' => $password,
                            'phone' => $phone,
                            'info' => $info
                ]);
                 if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                $this->getModel(Notification::class)
                    ->sendEmailToUser($user,$emailTemplate->generateSubject(),$emailTemplate->generateContent());
                 }
            }
              
            $user->setPhone($phone)
                ->setFirstName($firstName)
                ->setEmail($email)
                ->setLastName($lastName)
                ->setInfo($info)
                ->save();

            $response     = [
                    'status' => 'user_exist',
                    'user' => $user->getPublicData()
                ];
        } 
        return new JsonModel($response);
    }


}