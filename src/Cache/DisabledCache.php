<?php

namespace Arbel\Cache;

use Arbel\Cache\CacheInterface;
use Zend\Cache\Storage\StorageInterface;

/**
 * Disabled cache
 */
class DisabledCache implements CacheInterface{

    /**
     * Cache object
     * @var StorageInterface
     */
    protected $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    public function get(string $key, $default = null)
    {
        return null;
    }

    public function has(string $key): bool
    {
        return false;
    }

    public function set(string $key, $val, int $expiredTime = null)
    {

    }

    public function setPrefix(string $prefix)
    {

    }

    public function delete()
    {
        
    }

    /**
     * Clean all cache
     * @param string $prefix
     */
    public function deleteAll()
    {
        return $this->storage->flush();
    }
}