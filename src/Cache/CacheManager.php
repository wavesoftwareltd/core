<?php

namespace Arbel\Cache;

use Arbel\Model\Cache as CacheModel;
use Arbel\Base\Element;


class CacheManager extends Element
{
    /**
     * Cache
     * @var Cache
     */
    protected $cache;

    /**
     * CacheModel
     * @var CacheModel
     */
    protected $cacheModel;

    /**
     * DisabledCache
     * @var DisabledCache
     */
    protected $disabledCache;

    /**
     * Allowed caches
     * @var array
     */
    protected $allowedCache = array();

    public function __construct(CacheInterface $cache,DisabledCache $disabledCache, CacheModel $cacheModel)
    {
        $this->cacheModel = $cacheModel;
        $this->cache      = $cache;
        $this->disabledCache      = $disabledCache;
    }

    /**
     * Cache model
     * @return CacheModel
     */
    public function getModel()
    {
        return $this->cacheModel;
    }

    /**
     * CacheInterface
     * @return CacheInterface
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * CacheInterface
     * @return CacheInterface
     */
    public function getDisabledCache()
    {
        return $this->disabledCache;
    }

    /**
     *
     * @param string $cacheKey
     * @return CacheInterface
     */
    public function get(string $cacheKey, $default = NULL): CacheInterface
    {
        if(in_array($cacheKey, $this->allowedCache)){
            $this->getCache()->setPrefix($cacheKey);
            return $this->getCache();
        }
        $model = $this->getCacheData($cacheKey);
        if ($model->getStatus()) {
            $this->allowedCache[] = $cacheKey;
            $this->getCache()->setPrefix($cacheKey);
            return $this->getCache();
        } else {
            return $this->getDisabledCache();
        }
    }

    public function getCacheData(string $code)
    {
        if(parent::get($code)){
            return parent::get($code);
        }else{
            $model = $this->getModel()->loadByKey('code', $code);
            parent::set($code,$model);
            return $model;
        }
    }
}