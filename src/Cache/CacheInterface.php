<?php

namespace Arbel\Cache;

interface CacheInterface {

    /**
     * Get cache value
     * @param string $key
     * @param mixed $default
     */
     public function get(string $key,$default = null);

     /**
      * Set cache value
      * @param string $key
      * @param mixed $val
      * @param int $expiredTime - time in seconds for cache expiration
      */
     public function set(string $key,$val,int $expiredTime = null);

     /**
      * Check if cache exist
      * @param string $key
      */
     public function has(string $key) :bool;

     /**
      * Set prefix for caching key
      * @param string $prefix
      */
     public function setPrefix(string $prefix);


     /**
      * Clean all cache by prefix
      * @param string $prefix
      */
     public function delete();


     /**
      * Clean all cache
      * @param string $prefix
      */
     public function deleteAll();

}