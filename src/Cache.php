<?php

namespace Arbel;

use Zend\Cache\Storage\StorageInterface;

class Cache implements Cache\CacheInterface
{
    /**
     * Cache prefix
     * @var string
     */
    protected $prefix = '';

    /**
     * Cache object
     * @var StorageInterface
     */
    protected $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * StorageInterface
     * @return StorageInterface
     */
    public function getStorage(): StorageInterface
    {
        return $this->storage;
    }

    /**
     * Get cache value
     * @param string $key
     * @param mixed $default
     */
    public function get(string $key, $default = null)
    {
        $result = $this->getStorage()->getItem($this->prefix.$key);
        if (isset($result)) {
            $data = @unserialize($result);
            if ($data !== false) {
                return $data;
            } else {
                return $result;
            }
        } else {
            return $default;
        }
    }

    /**
     * Set cache value
     * @param string $key
     * @param mixed $val
     * @param int $expiredTime - time in seconds for cache expiration
     */
    public function set(string $key, $val, int $expiredTime = null)
    {
        if (is_array($val) || is_object($val)) {
            $val = serialize($val);
        }
        $this->getStorage()->setItem($this->prefix.$key, $val);
    }

    /**
     * Check if cache exist
     * @param string $key
     */
    public function has(string $key): bool
    {
        return $this->getStorage()->hasItem($key);
    }

    /**
     * Set prefix for caching key
     * @param string $prefix
     */
    public function setPrefix(string $prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * Clean all cache by prefix
     * @param string $prefix
     */
    public function delete()
    {
        if (empty($this->prefix)) {
            return $this->deleteAll();
        }
        return $this->getStorage()->clearByPrefix($this->prefix);
    }

    /**
     * Clean all cache
     * @param string $prefix
     */
    public function deleteAll()
    {
        return $this->getStorage()->flush();
    }
}