<?php
/**
 * Helper class
 *
 */

namespace Arbel;

use Zend\Dom\Query;

class Helper
{
    //Round
    const ROUND              = 10;
    const DEFAULT_SOLT       = 'sdcm3Kd3df09123kj3ldcfs3';
    const MODIFIED_FILE_FLAG = 'modified';
    const DELETED_FILE_FLAG  = 'deleted';

    static protected $oldModifiedFiles = array();
    static $filesModifiedList          = array();
    private static $xml                = null;
    private static $encoding           = 'UTF-8';

    /**
     * Json encode from array with utf-8 and numeric mode
     * @param array $array
     * @return string
     */
    static function jsonEncode(array $array): string
    {
        return json_encode($array,
            JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES);
    }

    static function fixXML($xml)
    {

        return \tidy_repair_string($xml,
            array(
            'output-xml' => true,
            'input-xml' => true
            ), 'utf8');
    }

    /**
     * Strip dimension from array
     * @param array $array
     * @return array
     */
    static function stripDimension(array $array): array
    {
        return array_reduce($array, 'array_merge', array());
    }

    /**
     * Generate bcrypt hashing
     * @param string $value
     * @return type
     * @throws RuntimeException
     */
    public static function bcryptHashing(string $value)
    {

        $hash = password_hash($value, PASSWORD_BCRYPT, [
            'cost' => self::ROUND
        ]);

        if ($hash === false) {
            throw new RuntimeException('Bcrypt hashing not supported.');
        }

        return $hash;
    }

    /**
     * Check if string is json
     * @param type $string
     * @return bool
     */
    public static function isJson($string): bool
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * Generate md5 hashing
     * @param string $value
     * @return type
     */
    public function md5Hashing(string $value)
    {
        return hash_hmac("md5", $value, SITE_SALT);
    }

    /**
     * Generate a more truly "random" alpha-numeric string.
     *
     * @param  int  $length
     * @return string
     */
    public static function random($length = 16)
    {
        $string = '';

        while (($len = static::length($string)) < $length) {
            $size = $length - $len;

            $bytes = random_bytes($size);

            $string .= static::substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0,
                    $size);
        }

        return $string;
    }

    /**
     * Returns the portion of string specified by the start and length parameters.
     *
     * @param  string  $string
     * @param  int  $start
     * @param  int|null  $length
     * @return string
     */
    public static function substr($string, $start, $length = null)
    {
        return mb_substr($string, $start, $length, 'UTF-8');
    }

    /**
     * convert array to xml
     * @param array $array
     * @param string $parent
     * @return type
     */
    public static function array2Xml($data, $result = '<?xml version="1.0" encoding="UTF-8"?>')
    {
        if (is_string($data)) {
            return '<![CDATA['.$data.']]>';
        } elseif (is_int($data)) {
            return $data;
        } else {
            foreach ($data as $key => $val) {
                if (!empty($val) && is_array($val) && is_int(key($val))) {
                    foreach ($val as $k => $v) {
                        $result .= '<'.$key.'>'.self::array2Xml($v, '').'</'.$key.'>';
                    }
                } else {
                    $result .= '<'.$key.'>'.self::array2Xml($val, '').'</'.$key.'>';
                }
            }
        }
        return $result;
    }

    public static function xmlToArray($input_xml)
    {

        $input_xml  = self::fixXML($input_xml);
        $array_data = json_decode(json_encode(simplexml_load_string($input_xml),
                JSON_UNESCAPED_UNICODE), true);
        return $array_data;
    }

    /**
     * Return the length of the given string.
     *
     * @param  string  $value
     * @return int
     */
    public static function length($value)
    {
        return mb_strlen($value);
    }

    /**
     * Error handler function
     * Add error log
     * @param type $errno
     * @param type $errstr
     * @param type $errfile
     * @param type $errline
     * @return boolean
     */
    public static function customErrorHandler($errno, $errstr, $errfile, $errline)
    {
        $message = self::generateCustomHandlerMessage($errno, $errstr, $errfile, $errline);

        switch ($errno) {

            case E_USER_WARNING || E_WARNING:
                \Log::warning($message);
                break;

            case E_USER_NOTICE:
                \Log::notice($message);
                break;

            case E_DEPRECATED:
            case E_USER_ERROR:
            default:
                \Log::error($message);
                break;
        }

        /* execute PHP internal error handler */
        return false;
    }

    /**
     * generate custom handler message
     * @param type $errno
     * @param type $errstr
     * @param type $errfile
     * @param type $errline
     * @return string - message
     */
    public static function generateCustomHandlerMessage($errno, $errstr, $errfile, $errline)
    {
        $message = '';

        switch ($errno) {
            case E_USER_ERROR:
                $message .= "* ERROR - [$errno] $errstr \n";
                $message .= "  Fatal error on line $errline in file $errfile";
                $message .= ", PHP ".PHP_VERSION." (".PHP_OS.") \n";
                $message .= "...\n";
                break;

            case E_USER_WARNING || E_WARNING:
                $message .= "* WARNING - [$errno] $errstr \n";
                $message .= "  on line $errline in file $errfile\n";
                break;

            case E_USER_NOTICE:
                $message .= "* NOTICE - [$errno] $errstr \n";
                $message .= "  on line $errline in file $errfile\n";
                break;

            case E_DEPRECATED:
                $message .= "* DEPRECATED - [$errno] $errstr \n";
                $message .= "  on line $errline in file $errfile\n";
                break;


            default:
                $message .= "Unknown error type: [$errno] $errstr \n";
                $message .= "  on line $errline in file $errfile\n";
                break;
        }

        return $message;
    }

    /**
     * Check if value is empty
     * @param mixed $value
     * @return bool
     */
    public static function isEmpty($value): bool
    {
        return empty($value);
    }

    /**
     * Convert a decimal representation of a color to a hex-html one.
     *
     * @param $dec
     * @return string
     */
    public static function decToHexColor($dec)
    {
        return '#'.str_pad(dechex((int) $dec), 6, '0', STR_PAD_LEFT);
    }

    /**
     * Remove multiple spaces and convert tabs to spaces (and newlines if necessary)
     *
     * @param $str
     * @param bool $isRemoveNewlines
     * @return mixed
     */
    public static function normalizeString($str, $isRemoveNewlines = true)
    {
        if ($isRemoveNewlines) {
            return preg_replace('~\s+~s', ' ', $str);
        }
        return preg_replace('~\s+~', ' ', $str);
    }

    /**
     * Generate hashing by array
     * If no params then generate new random hash
     * @param array $hashingArray - optional - array of values
     * @return string
     */
    public static function generateHash(array $hashingArray = array()): string
    {
        $hashFunction = self::$hashFunctionName;
        if (empty($hashingArray)) {
            if (empty(!self::$columnsForHashing)) {
                $hashingArray = self::$columnsForHashing;
            } elseif (!empty(self::$uniqueTableColumn)) {
                $hashingArray = self::$uniqueTableColumn;
            } else {
                $hashFunction = self::$generateHashFunctionName;
            }
        }

        $valuesHashingArray = self::$getValuesByKeys($hashingArray);
        $paramForFunction   = empty($valuesHashingArray) ? array() : array($valuesHashingArray);
        return call_user_func_array($hashFunction, $paramForFunction);
    }

    /**
     * @param $array array
     * @param $salt mixed NULL for using SITE_SALT
     *              FALSE for not using any salt
     *              other for custom salt value
     *
     * @return string
     */
    public static function hashArray(array $array, $salt = null)
    {
        // Set default salt
        if (!isset($salt)) $salt = self::SITE_SALT;

        return hash_hmac("md5", serialize(json_encode($array)), $salt);
    }

    /**
     * @param int $binary_len length in binary bytes
     * @param bool $binary_output
     *
     * Performance note: Benchmarks showed that PHP7's random_bytes() is slower than openssl_random_pseudo_bytes() on Linux.
     * Also, running bin2hex() directly on the result with no auxiliary variable is significantly faster.
     *
     * @return string
     */
    public static function generateRandomHash(int $binary_len = 16, bool $binary_output = false)
    {
        return $binary_output ? openssl_random_pseudo_bytes($binary_len) : bin2hex(openssl_random_pseudo_bytes($binary_len));
    }

    /**
     * Get current base url
     * @return string
     */
    public static function getCurrentBaseUrl(): string
    {
        return (isset($_SERVER['HTTPS']) ? "https" : "http")."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    /**
     * Get current base host
     * @return string
     */
    public static function getCurrentBaseHost(): string
    {
        return (isset($_SERVER['HTTPS']) ? "https" : "http")."://$_SERVER[HTTP_HOST]";
    }

    /**
     * Copy folder to target
     * @param type $source
     * @param type $target
     */
    public static function copyFolder($source, $target, $callback = null, &$existingFiles = array())
    {
        if (!file_exists($target)) {
            mkdir($target, 0755);
        }
        foreach (
        $iterator = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS),
        \RecursiveIteratorIterator::SELF_FIRST) as $item
        ) {
            $targetPath                 = $target.DIRECTORY_SEPARATOR.$iterator->getSubPathName();
            $itemPath                   = (string) $item;
            $existingFiles[$targetPath] = $itemPath;
            if (!file_exists($itemPath) && isset(self::$filesModifiedList[$itemPath])) {
                unset(self::$filesModifiedList[$itemPath]);
            }
            if ($item->isDir() && !file_exists($targetPath)) {
                mkdir($targetPath);
            } else {
                if ((isset(self::$filesModifiedList[$itemPath]) && self::$filesModifiedList[$itemPath]
                    < $item->getMTime()) || !isset(self::$filesModifiedList[$itemPath])) {
                    if (file_exists($targetPath) && is_link($targetPath)) {
                        unlink($targetPath);
                    }
                    if (!is_dir($itemPath)) {
                        copy($itemPath, $targetPath);
                        self::$filesModifiedList[$itemPath] = time();
                        if (is_callable($callback)) {
                            $callback($item);
                        }
                    }
                }
            }
        }
    }

    /**
     * Copy folder to target
     * @param type $source
     * @param type $target
     */
    public static function syncFiles(string $sourceFolder, string $targetFolder, array $filesList)
    {
        if (!file_exists($targetFolder)) {
            mkdir($targetFolder, 0755);
        }
        foreach ($filesList as $filePath) {
            $targetPath = str_replace($sourceFolder, $targetFolder, $filePath);
            if (!file_exists($filePath) && file_exists($targetPath)) {
                unlink($targetPath);
            } elseif (file_exists($filePath)) {
                if (file_exists($targetPath)) {
                    unlink($targetPath);
                }
                copy($filePath, $targetPath);
            }
        }
    }

    /**
     * Copy folder to target
     * @param type $source
     * @param type $target
     */
    public static function symlinkFiles($source, $target, $overwrite = true, $callback = null)
    {
        if (!file_exists($target)) {
            mkdir($target, 0755);
        }
        foreach (
        $iterator = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS),
        \RecursiveIteratorIterator::SELF_FIRST) as $item
        ) {
            $targetFile = $target.DIRECTORY_SEPARATOR.$iterator->getSubPathName();
            if ($item->isDir()) {
                if (file_exists($targetFile)) {
                    if (is_link($targetFile) && $overwrite) {
                        rmdir($targetFile);
                        mkdir($targetFile);
                    }
                } else {
                    mkdir($targetFile);
                }
            } else {
                if (file_exists($targetFile) && $overwrite) {
                    if (is_link($targetFile)) {
                        if (is_dir($item)) {
                            rmdir($targetFile);
                        } else {
                            unlink($targetFile);
                        }
                    }
                }
                if ((file_exists($targetFile) && $overwrite) || !file_exists($targetFile)) {
                    if (is_callable($callback)) {
                        $callback($item);
                    }
                    symlink($item, $targetFile);
                }
            }
        }
    }

    /**
     * Copy folder to target
     * @param type $source
     * @param type $target
     */
    public static function symlinkFolders($source, $target)
    {
        if (!file_exists($target)) {
            mkdir($target, 0755);
        }
        $list = scandir($source);
        foreach ($list as $file) {
            if ($file != '.' && $file != '..') {
                $item       = $source.DIRECTORY_SEPARATOR.$file;
                $targetFile = $target.DIRECTORY_SEPARATOR.$file;
                if (is_dir($item)) {
                    if (file_exists($targetFile)) {
                        if (is_link($targetFile)) {
                            rmdir($targetFile);
                        }
                    }
                    symlink($item, $targetFile);
                }
            }
        }
    }

    /**
     * Remove folder content
     * @param string $path
     * @param array $ignoredFiles
     */
    public static function removeFolderContent(string $path, array $ignoredFiles = array(),
                                               $removeEmptyFolders = false, $callback = null,
                                               array $blackList = array())
    {
        $files = glob($path.'/*'); // get all file names
        foreach ($files as $file) { // iterate files
            $isSkip = false;
            foreach ($blackList as $blacklistRule) {
                if (self::like_match($blacklistRule, $file)) {
                    $isSkip = true;
                    break;
                }
            }
            if ($isSkip) {
                continue;
            }
            if (is_file($file) && !in_array(realpath($file), $ignoredFiles)) {
                unlink($file); // delete file
                if (is_callable($callback)) {
                    $callback($file);
                }
            } else if (is_dir($file)) {
                self::removeFolderContent($file, $ignoredFiles, true, $callback, $blackList);
                if ($removeEmptyFolders) {
//                echo ' delete dir - '.$file;
                    rmdir($file); // delete file
                }
            }
        }
    }

    /**
     * SQL Like operator in PHP.
     * Returns TRUE if match else FALSE.
     * @param string $pattern
     * @param string $subject
     * @return bool
     */
    public static function like_match($pattern, $subject)
    {
        $pattern = str_replace('%', '*', preg_quote($pattern, '/'));
        return fnmatch($pattern, $subject);
    }

    public static function findReplaceBullk(array $searchReplace, string $subject)
    {
        foreach ($searchReplace as $search => $replace) {
            $subject = str_replace($search, $replace, $subject);
        }
        return $subject;
    }

    /**
     * Watch folder/file
     * @param type $source
     * @param type $target
     */
    public static function checkNewModifyFiles(string $filePath, int $time, $callback)
    {

        $oldFiles     = self::$oldModifiedFiles[$filePath] ?? array();
        $currentFiles = array();
        foreach (
        $iterator     = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($filePath, \RecursiveDirectoryIterator::SKIP_DOTS),
        \RecursiveIteratorIterator::SELF_FIRST) as $item
        ) {
            if (!isset(self::$oldModifiedFiles[$filePath])) {
                
            }
            $itemPath     = (string) $item;
            $modifiedTime = filemtime($itemPath);
            if (!is_dir($itemPath)) {
                $currentFiles[$itemPath] = $modifiedTime;
                if (isset($oldFiles[$itemPath])) {
                    unset($oldFiles[$itemPath]);
                }
                if ($modifiedTime > $time) {
                    $callback($itemPath, self::MODIFIED_FILE_FLAG);
                }
            }
        }

        foreach ($oldFiles as $itemPath => $lastModified) {
            $callback($itemPath, self::DELETED_FILE_FLAG);
        }
        self::$oldModifiedFiles[$filePath] = $currentFiles;
    }

    /**
     * Check if dir is empty
     * @param string $dir
     * @return boolean
     */
    public static function isDirEmpty(string $dir)
    {
        if (!is_dir($dir)) {
            $dir = dirname($dir);
        }
        if (!is_readable($dir)) return null;
        $handle = opendir($dir);
        while (false !== ($entry  = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                return false;
            }
        }
        return true;
    }

    public static function removeEmptyParentsFolders(string $dir)
    {
        if (!is_dir($dir)) {
            $dir = dirname($dir);
        }
        if (file_exists($dir) && self::isDirEmpty($dir)) {
            rmdir($dir);
            self::removeEmptyParentsFolders(dirname($dir));
            return true;
        }
        return false;
    }

    /**
     * Get file content
     * @param string $url - url/path
     */
    static function getContentByUrl(string $url)
    {
        if (self::isUrl($url)) {
            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_REFERER, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        } elseif (file_exists($url)) {
            return file_get_contents($url);
        } else {
            return '';
        }
    }

    static function xmlCall(string $url, string $xml = '')
    {
        //setting the curl parameters.
        $ch         = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
// Following line is compulsary to add as it is:
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $data       = curl_exec($ch);
        curl_close($ch);
        //convert the XML result into array
        $array_data = self::xmlToArray($data);
        return $array_data;
    }

    static function utf8Strrev($str)
    {
        preg_match_all('/./us', $str, $ar);
        return implode(array_reverse($ar[0]));
    }

    static function downloadFile(string $filePath, $fileName = null)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 300); 
        ini_set('memory_limit','2048M');
        if ($fileName) {
            $fileName = $fileName.'.'.pathinfo($filePath, PATHINFO_EXTENSION);
        } else {
            $fileName = basename($filePath);
        }
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"".$fileName."\"");
        ob_clean();
        flush();
        readfile($filePath); // do the double-download-dance (dirty but worky)
    }

    static function getFilePathByUrl(string $url)
    {

        if (self::isUrl($url)) {
            $path   = sys_get_temp_dir().DIRECTORY_SEPARATOR.'copy-'.md5($url);
            $fp     = fopen($path, 'w') or die('Cannot open file:  '.$path);
            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_REFERER, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds
            $result = curl_exec($ch);
            fwrite($fp, $result);
            curl_close($ch);
            return $path;
        } elseif (file_exists($url)) {
            return $url;
        } else {
            return null;
        }
    }
    
    
    static function sendPost(string $url,array $params)
    {

        if (self::isUrl($url)) { 
            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_HEADER, false); 
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);  
            curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
            curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($params));
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds
            $result = curl_exec($ch);
            return [$url,$result];
        } else {
            return null;
        }
    }

    /**
     * Check if the string is url
     * @return type
     */
    static function isUrl($url)
    {
        return (strpos($url, 'http://') !== false || strpos($url, 'https://') !== false);
    }

    /**
     * Make boolean to string true/false
     * @param type $boolean
     * @return string
     */
    static function stringToBool($boolean)
    {
        if (strtolower($boolean) == 'true') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Make boolean to string true/false
     * @param type $boolean
     * @return string
     */
    static function boolToString($boolean)
    {
        if ($boolean) {
            return 'true';
        } else {
            return 'false';
        }
    }

    static function guid()
    {

        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535),
            mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535),
            mt_rand(0, 65535), mt_rand(0, 65535));
    }
}