<?php
 
namespace Arbel;

class ObjectManager extends \Zend\Di\Di{



    /**
     * Retrieve a new instance of a class
     *
     * Forces retrieval of a discrete instance of the given class, using the
     * constructor parameters provided.
     *
     * @param  mixed                            $name     Class name or service alias
     * @param  array                            $params   Parameters to pass to the constructor
     * @param  bool                             $isShared
     * @return object|null
     * @throws Exception\ClassNotFoundException
     * @throws Exception\RuntimeException
     */
    public function newInstance($name, array $params = [], $isShared = true)
    {
        // localize dependencies
        $definitions     = $this->definitions;
        $instanceManager = $this->instanceManager();

        if ($instanceManager->hasAlias($name)) {
            $class = $instanceManager->getClassFromAlias($name);
            $alias = $name;
        } else {
            $class = $name;
            $alias = null;
        }

        array_push($this->instanceContext, ['NEW', $class, $alias]);

        if (!$definitions->hasClass($class)) {
            $aliasMsg = ($alias) ? '(specified by alias ' . $alias . ') ' : '';
            throw new \Exception(
                'Class ' . $aliasMsg . $class . ' could not be located in provided definitions.'
            );
        }

        $instantiator     = $definitions->getInstantiator($class);
        $injectionMethods = [];
        $injectionMethods[$class] = $definitions->getMethods($class);

        foreach ($definitions->getClassSupertypes($class) as $supertype) {
            $injectionMethods[$supertype] = $definitions->getMethods($supertype);
        }

        if ($instantiator === '__construct') {
            $instance = $this->createInstanceViaConstructor($class, $params, $alias);
            if (array_key_exists('__construct', $injectionMethods)) {
                unset($injectionMethods['__construct']);
            }
        } elseif (is_callable($instantiator, false)) {
            $instance = $this->createInstanceViaCallback($instantiator, $params, $alias);
        } else {
            if (is_array($instantiator)) {
                $msg = sprintf(
                    'Invalid instantiator: %s::%s() is not callable.',
                    isset($instantiator[0]) ? $instantiator[0] : 'NoClassGiven',
                    isset($instantiator[1]) ? $instantiator[1] : 'NoMethodGiven'
                );
            } else {
                $msg = sprintf(
                    'Invalid instantiator of type "%s" for "%s".',
                    gettype($instantiator),
                    $name
                );
            }
            throw new \Zend\Di\Exception\RuntimeException($msg);
        }

        if ($isShared) {
            if ($callParameters = $this->getCallParameters($name, $params)) {
                $this->instanceManager->addSharedInstanceWithParameters($instance, $name, $callParameters);
            } else {
                $this->instanceManager->addSharedInstance($instance, $name);
            }
        }
        $instanceManager->filterMethods($injectionMethods,$instantiator);
        
        $this->handleInjectDependencies($instance, $injectionMethods, $params, $class, $alias, $name);

        array_pop($this->instanceContext);

        return $instance;
    }

        /**
     *
     * @return InstanceManager
     */
    public function instanceManager()
    {
        return $this->instanceManager->setObjectManager($this);
    }


}