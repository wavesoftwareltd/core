<?php

namespace Arbel\Sms;

use Arbel\Sms\SmsInterface;
use Arbel\Base\Element;
use Arbel\Config;
use Arbel\Helper;

class Sms019 extends Element implements SmsInterface
{

    const SUCCESS_CODE = 0;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function addTo(string $number)
    {
        $filterdNumber = filter_var($number, FILTER_SANITIZE_NUMBER_INT);
        $filterdNumber = str_replace('+','',$filterdNumber);
        parent::setArrayData('to', $filterdNumber, $filterdNumber);
        return $this;
    }
    
    /**
     * Send sms
     * @return boolean - sending status
     */
    public function send()
    {
        $params = [
            'sms' => [
                'user' => [
                    'username' => $this->config->get('api.sms019.user.username'),
                    'password' => $this->config->get('api.sms019.user.password')
                ],
                'source' => str_replace('+','',str_replace('972','0',$this->getFrom())),//$this->getFrom(),
                'destinations' => [
                    'phone' => array_keys($this->getTo())
                ],
                'message' => $this->getMessage(),
                'response' => (int) $this->getResponse(false)
            ]
        ];
       $res = Helper::xmlCall($this->config->get('api.sms019.url'),Helper::array2Xml($params));

       $trackingKey = $res['shipment_id'] ?? '';
       $message = $res['message'] ?? '';
       $status = $res['status'] ?? '';
       if($trackingKey == '' && $message == ''){
          $message = '019 error'; 
       }
       $this->setTrackingKey($trackingKey);
       $this->setInfo($message.' - '.$status);
       return $status == $this::SUCCESS_CODE && $trackingKey != '';
    }
 
    public function reset()
    {
        $this->setTo([]);
        parent::setFrom(null);
        return $this;
    }

    public function setMessage(string $message)
    {
        parent::setMessage($message);
        return $this;
    }

    public function setFrom(string $number)
    {
        //$filterdNumber = filter_var($number, FILTER_SANITIZE_NUMBER_INT);
        parent::setFrom($number);
        return $this;
    }
    
    public function getParamsUrl()
    {
        $params = $this->getParams();
        $pre = '';
        if($params && !empty($params) && is_array($params)){
            $pre = '?'.http_build_query($params);
        }
        return $pre;
    }
}