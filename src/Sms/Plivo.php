<?php

namespace Arbel\Sms;

use Arbel\Sms\SmsInterface;
use Plivo\RestClient;
use Plivo\RestAPI;
use Arbel\Base\Element;
use Plivo\Exceptions\PlivoRestException;
use Arbel\Config;

class Plivo extends Element implements SmsInterface
{
    /**
     * RestClient
     * @var RestClient
     */
    protected $plivo;
    /**
     * RestAPI
     * @var RestAPI
     */
    protected $apiPlivo;

    public function __construct(RestClient $plivo,RestAPI $apiPlivo,Config $config)
    {
        $this->plivo = $plivo;
        $this->apiPlivo = $apiPlivo;
        $this->config = $config;
    }

    public function addTo(string $number)
    {
        $filterdNumber = filter_var($number, FILTER_SANITIZE_NUMBER_INT);
        parent::setArrayData('to', $filterdNumber, $filterdNumber);
        return $this;
    }
    
    /**
     * Send sms
     * @return boolean - sending status
     */
    public function send()
    {
        $result = $this->apiPlivo->send_message(array(
            'src' => $this->getFrom(),
            'dst' => implode('<', $this->getTo()),
            'url' => $this->config->get('api.plivo.sms.report_url').$this->getParamsUrl(),
            'text' => $this->getMessage()
        )); 
        $status = $result['status'] ?? 600;
        if(isset($result['response']['message_uuid'])){
            $res = $result['response']['message_uuid'];
            if(is_array($res)){
                $res = current($res);
            }
            $this->setTrackingKey($res);
        }

        if (isset($result['response']['message']) && $status < 400) {
            $this->setInfo($status.' - '.$result['response']['message']);
            return true;
        } elseif (isset($result['response']['error'])) {
            $this->setInfo($status.' - '.$result['response']['error']);
            return false;
        } else {
            $this->setInfo(print_r($result, true));
            return false;
        }
    }

    /**
     * Send sms
     * @return boolean - sending status
     */
    public function newApiSend()
    {
        $message = '';
        $errorMessage = '';
        $status = 200;
        try {
            $result  = $this->plivo->messages->create(
                $this->getFrom(), //src
                $this->getTo(), //target array
                $this->getMessage()//message
            );
            $message = $result->getMessage();
        } catch (PlivoRestException $exc) {
            $errorMessage = $exc->getMessage();
            $status = 500;
        }

        if (!empty($message) && $status < 400) {
            $this->setInfo($status.' - '.$message);
            return true;
        } elseif (!empty($errorMessage)) {
            $this->setInfo($status.' - '.$errorMessage);
            return false;
        } else {
            $this->setInfo(print_r($message, true));
            return false;
        }
    }

    public function reset()
    {
        $this->setTo([]);
        parent::setFrom(null);
        return $this;
    }

    public function setMessage(string $message)
    {
        parent::setMessage($message);
        return $this;
    }

    public function setFrom(string $number)
    {
        //$filterdNumber = filter_var($number, FILTER_SANITIZE_NUMBER_INT);
        parent::setFrom($number);
        return $this;
    }
    
    public function getParamsUrl()
    {
        $params = $this->getParams();
        $pre = '';
        if($params && !empty($params) && is_array($params)){
            $pre = '?'.http_build_query($params);
        }
        return $pre;
    }
}