<?php

namespace Arbel\Google;

use Arbel\Base\Element;

/**
 * Google Translate Api
 * @version 0.0.1
 */
class Translate extends Element {

    const SUPPORTED_URL = 'https://www.googleapis.com/language/translate/v2/languages';
    const DETECT_LANGUAGE_BY_STRING_URL = 'https://www.googleapis.com/language/translate/v2/detect';
    const TRANSLATE_URL = 'https://www.googleapis.com/language/translate/v2';
 
     function sendRequest($url,$params = array()){

        $params['key'] = $this->getKey();
        $stringParams = http_build_query($params);
         $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url.'?'.$stringParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
      //  curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $stringParams);
        $resultJson = curl_exec($ch);
        $result = json_decode($resultJson,true);
        if (!is_null($result)) {
            return $result;
        } else {
            return null;
        }

    }

    /**
     * Get supported languages list
     * @return array
     */
     function getSupportedList() {
        $result = $this->sendRequest($this::SUPPORTED_URL);
        if (isset($result['data']['languages'])) {
            return $result['data']['languages'];
        } else {
            return array();
        }
    }

    /**
     * Detect language by string
     * @param type $string
     * @return array
     */
     function detectedLanguageByString($string) {
        $urlString = $string;
        $result = $this->sendRequest(
                        $this::DETECT_LANGUAGE_BY_STRING_URL, array('q' => $urlString));
        if (isset($result['data']['detections'])) {
            return $result['data']['detections'];
        } else {
            return array();
        }
    }

    /**
     * Translate string to another language
     * @param type $string
     * @param type $targetLanguageCode
     * @param type $isReturndSourceLang
     * @return type
     */
     function __($string, $targetLanguageCode, $isReturndSourceLang = false) {
        
        $urlString =  $string;
        $result = $this->sendRequest(
                        $this::TRANSLATE_URL, array(
                    'q' => $urlString,
                    'target' => $targetLanguageCode
        ));
        if (isset($result['data']['translations'][0]['translatedText'])) {
            if ($isReturndSourceLang) {
                $transalte =  $result['data']['translations'][0];
            } else {
                $transalte =  $result['data']['translations'][0]['translatedText'];
            }
        } else {
            if ($isReturndSourceLang) {
                $transalte =  array();
            } else {
                $transalte =  '';
            }
        }
        return $transalte;
    }

}
