<?php

namespace Arbel\Google;

use Arbel\Base\Element;

/**
 * Google Translate Api
 * @version 0.0.1
 */
class Maps extends Element
{

    static public function getCoordinates($address,$key)
    {

        $address = urlencode($address); // replace all the white space with "+" sign to match with google search pattern
        $url = "https://maps.google.com/maps/api/geocode/json?key=".$key."&sensor=false&address=$address";
        $response = self::getContentByUrl($url);
        $json = json_decode($response, TRUE); //generate array object from the response from the web
        if(!isset($json['results'][0]['geometry']['location']['lat']) || $json['status'] != 'OK'){
            return null;
        }
        return $json['results'][0]['geometry']['location'];
    }

    static function getContentByUrl($url)
    {
        if (self::isUrl($url)) {
            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_REFERER, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        } elseif (file_exists($url)) {
            return file_get_contents($url);
        } else {
            return '';
        }
    }
    
    /**
     * Check if the string is url
     * @return type
     */
    static function isUrl($url)
    {
        return (strpos($url, 'http://') !== false || strpos($url, 'https://') !== false);
    }
}