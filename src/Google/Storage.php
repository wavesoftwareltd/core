<?php

namespace Arbel\Google;

use Arbel\Upload\UploadInterface;
use Zend\ServiceManager\ServiceManager;
use Arbel\Config;

/**
 * Remote File Helper
 * @version 1.0.0
 */
class Storage implements UploadInterface
{
    const PROJECT_ID          = 'symmetric-fin-137523';
    const BUCKET_NAME         = 'symmetric-fin-137523.appspot.com';
    const FILES_BY_URL_FOLDER = 'files_by_url';

    protected $projectId;
    protected $bucketName;
    protected $prefix;
    protected $sm;
    protected $credentials;
    protected $targetDomain;

    public function __construct(ServiceManager $sm)
    {
        $this->sm          = $sm;
        $config            = $sm->get(Config::class)->get('api.google.storage');
        $this->projectId   = $config['project_id'];
        $this->bucketName  = $config['bucket_name'];
        $this->prefix      = $config['default_prefix'];
        $this->credentials = $config['login_info'];
        $this->targetDomain = $config['target_domain'] ?? null;
    }

    public function upload(string $filePath, string $ext = null, string $prefix = '',string $newName = null): string
    {

// Authenticate your API Client
        $client = new \Google_Client();
        $client->setAuthConfig($this->credentials);
        $client->addScope(\Google_Service_Storage::DEVSTORAGE_FULL_CONTROL);

        $storage = new \Google_Service_Storage($client);
        if (!$ext) {
            $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        }
        if (!empty($prefix)) {
            $prefix = '/'.$prefix;
        }
        if(!$newName){
            $fileName  = str_replace('=', 'xxxxxx', base64_encode($filePath));
            $fileName = hash_file("md5",$filePath);
        }else{
            $fileName = $newName;
        }
        $file_name =  $this->prefix.$prefix."/". $fileName.".".$ext;
        $obj       = new \Google_Service_Storage_StorageObject();
        $obj->setName($file_name);
        $response  = $storage->objects->insert(
            $this->bucketName, // Bucket Name
            $obj,
            [
            'name' => $file_name,
            'data' => $this->getContentByUrl($filePath),
            'uploadType' => 'media',
            'predefinedAcl' => 'publicRead'
            ]
        );
        return $this->getStorageLinkByUrl($filePath,$ext, $prefix,$newName);
    }

    public function getStorageLinkByUrl($url, $ext = null, $folder = '',string $newName = null)
    {
        if (!$ext) {
            $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        }
        if(!$newName){
        $fileName = hash_file("md5",$url);
        }else{
        $fileName = $newName;
        } 
//        $fileName        = str_replace('=', 'xxxxxx', base64_encode($url));
        $file_name       = $this->prefix.$folder."/".$fileName.".".$ext;
        if($this->targetDomain){
          $storageLocation = $this->targetDomain.'/'.$file_name;  
        }else{
         $storageLocation = 'https://storage.googleapis.com/'.$this->bucketName.'/'.$file_name;   
        }


        return $storageLocation;
    }

    static function getContentByUrl($url)
    {
        if (self::isUrl($url)) {
            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_REFERER, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        } elseif (file_exists($url)) {
            return file_get_contents($url);
        } else {
            return '';
        }
    }

    /**
     * Check if the string is url
     * @return type
     */
    static function isUrl($url)
    {
        return (strpos($url, 'http://') !== false || strpos($url, 'https://') !== false);
    }
}