<?php

namespace Arbel\Service\Interfaces;

interface CallbackInterface {
    
    public function __construct($function);
    
    /**
     * Set callback function
     * @param callable $function
     */
    public function setCallback($function);
    
    
    /**
     * Get callback function
     * @return callback function
     */
    public function getCallback();
    
    /**
     * Call the callback function
     */
    public function call();
}