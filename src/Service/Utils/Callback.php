<?php

namespace Arbel\Service\Utils;

/* 
 * General callback class
 */

use Arbel\Service\Interfaces\CallbackInterface;

class Callback implements CallbackInterface{
    
    protected $callback;
    
    public function __construct($function) {
        $this->setCallback($function);
    }
    
    public function call() {
         return $this->callback();
    }

    public function getCallback() {
        return $this->callback;
    }

    public function setCallback($function) {
        $this->callback = $function;
    }


}
