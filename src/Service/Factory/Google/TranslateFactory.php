<?php

namespace Arbel\Service\Factory\Google;

use Arbel\Google\Translate;

class TranslateFactory
{

    public function __invoke($serviceLocator)
    {

        $config = $serviceLocator->get('Di')->get('Arbel\Config');
        $translate = new Translate();
        $translate->setIsActive($config->get('translate.use_google_translate'));
        $translate->setKey($config->get('api.google.translate.key'));
        
        return $translate;
    }
}