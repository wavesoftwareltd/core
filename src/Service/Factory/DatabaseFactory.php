<?php

namespace Arbel\Service\Factory;

use Arbel\Database;
use Zend\Db\Adapter\Adapter;
use Arbel\Database\Adapter\Mssql;
use Arbel\Database\Adapter\Mysql;

class DatabaseFactory 
{

    public function __invoke($serviceLocator)
    {

        $config   = $serviceLocator->get('Di')->get('Arbel\Config');
        if($config->get('db_connection.adapter') == 'mssql'){
        $database = $serviceLocator->get('Di')->get(Mssql::class);
        }else{
        $database = $serviceLocator->get('Di')->get(Mysql::class);
        }
        
        return $database;
    }
}