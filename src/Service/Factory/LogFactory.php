<?php

namespace Arbel\Service\Factory;

use Arbel\Log;
use Arbel\Service\Log\Writer\File;

class LogFactory
{

    public function __invoke($serviceLocator)
    {

        $config = $serviceLocator->get('Di')->get('Arbel\Config'); 
        $logger = new Log();

        //File log
        $fileWriter = new File($config->get('log.dir'),$config->get('log.permission'));
        $logger->addWriter($fileWriter);

        //Zend log
        if($config->get('log.enable_zend_server_log')){
        $zendWriter = new \Zend\Log\Writer\ZendMonitor();
        $logger->addWriter($zendWriter);
        }

        Log::registerErrorHandler($logger);

        return $logger;
    }
}