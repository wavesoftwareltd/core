<?php

namespace Arbel\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\Api\UserController;

/**
 * This is the factory for all classes that used di
 */
class DiServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container,
                     $requestedName, array $options = null)
    {
        $di = $container->get('Di');
        return $di->get($requestedName);
    }
}