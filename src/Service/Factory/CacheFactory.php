<?php

namespace Arbel\Service\Factory;
 
use Zend\Cache\StorageFactory;

class CacheFactory
{

    public function __invoke($serviceLocator)
    {
        $config = $serviceLocator->get('Di')->get('Arbel\Config');
        $cacheFolder = $config->get('cache.adapter.options.cache_dir');
        
        if($cacheFolder && !file_exists(ROOT_DIR.$cacheFolder)){
            mkdir(ROOT_DIR.$cacheFolder,0700,true);
        }

        $cache = StorageFactory::factory($config->get('cache'));
        return $cache;
    }
}