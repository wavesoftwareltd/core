<?php

namespace Arbel\Service\Factory;
 
use OAuth2\Request as OAuth2Request;
use Zend\Console\Request;

class UserFactory
{

    public function __invoke($serviceLocator)
    {
        $config = $serviceLocator->get('Di')->get('Arbel\Config');
        $user = $serviceLocator->get('Di')->get('Arbel\Model\User');
        $lang = $serviceLocator->get('Di')->get('Arbel\Model\Language');
        $request = $serviceLocator->get('Request');
         
        //$user->initSession();
        if(get_class($request) != Request::class && $request->getHeaders('Authorization')){
        $user->initToken($request->getHeaders('Authorization')->getFieldValue());
        }elseif(get_class($request) != Request::class && $request->getQuery('token')){
        $user->initToken($request->getQuery('token'));
        }
        
        //Set default language
        if (!$user->getLanguage()) {
            $lang->loadByKey('code',$config->get('language.default.local'));
            $user->setLanguage($lang);
        }
    
        return $user;
    }
}