<?php

namespace Arbel\Service\Factory\Google;

use Arbel\OAuth2\Pdo;

class AccessTokenFactory
{

    public function __invoke($serviceLocator)
    {

        $config = $serviceLocator->get('Di')->get('Arbel\Config');
        $pdo = new Pdo($config->get('zf-oauth2.db'));
        $translate->setIsActive($config->get('translate.use_google_translate'));
        $translate->setKey($config->get('api.google.translate.key'));
        
        return $translate;
    }
}