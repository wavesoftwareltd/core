<?php

namespace Arbel\Service\Factory;

use Zend\Log\Logger;
use Arbel\Base\Element;
use Arbel\Service\Log\Writer\File;
class ConfigFactory
{

    public function __invoke($serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        $object = $serviceLocator->get('Di')->get(Element::class)->setData($config);
        return $object;
    }
}