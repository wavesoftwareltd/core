<?php

namespace Arbel\Service\Factory;

use Interop\Container\ContainerInterface;

class ContainerFactory implements FactoryInterface
{
    /**
     * Create and return abstract factory seeded by dependency injector
     *
     */
    public function __invoke(ContainerInterface $container, $name, array $options = null)
    {

        return $container;
    }
 
}
