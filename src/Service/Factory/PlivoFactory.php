<?php

namespace Arbel\Service\Factory;

use Plivo\RestClient;

class PlivoFactory
{

    public function __invoke($serviceLocator)
    {

        $config = $serviceLocator->get('Di')->get('Arbel\Config');
        $client   = new RestClient($config->get('api.plivo.auth_id'),
            $config->get('api.plivo.auth_token'));

        return $rest;
    }
}