<?php

namespace Arbel\Service\Factory;

use Illuminate\Database\Capsule\Manager as Capsule;

class CapsuleFactory
{

    public function __invoke($container)
    {
        $capsule = new Capsule;
        $config  = $container->get('Arbel\Config');
        $capsule->addConnection(array_merge($config->get('db_connection'),
                $config->get('illuminate_db_config')));

        // Make this Capsule instance available globally via static methods... (optional)
        $capsule->setAsGlobal();

        return $capsule;
    }
}