<?php

namespace Arbel\Service\Log\Writer;

use Zend\Log\Writer\Stream;

/**
 * Overwrite stream writer to write logs to current day file
 */
class File extends Stream
{
    /**
     * Logs Root folder name
     * @var string
     */
    protected $folderName = 'logs';

    /**
     * Logs folder permission
     * @var int 
     */
    protected $folderPermission = 0755;

    public function __construct($folderName = null,$permission = null)
    {
        if(isset($folderName)){
            $this->folderName = $folderName;
        }
//        if(isset($permission)){
//            $this->folderPermission = $permission;
//        }
        $this->verifyFolderExist();
        parent::__construct($this->getLogFilePath());
    }

    /**
     * Get log file path
     * @return string
     */
    public function getLogFilePath(): string
    {
        return ROOT_DIR.$this->folderName.'/'.date("Y").'/'.date("m").'/'.date("d").'.log';
    }

    /**
     * Verify if folder exist
     */
    public function verifyFolderExist()
    {
        if (!file_exists(dirname($this->getLogFilePath()))) {
            mkdir(dirname($this->getLogFilePath()),
                $this->folderPermission, true);
        }
    }
}