<?php

namespace Arbel;

use Arbel\Model\Acl\Role;
use Arbel\Model\Acl\Resource;
use Arbel\Model\Acl\Privilege;
use Arbel\Model\Acl\Permission;
use Arbel\ObjectManager;
use Zend\ServiceManager\ServiceManager;
use Zend\Permissions\Acl\Role\Registry;

class Acl extends \Zend\Permissions\Acl\Acl
{
    /**
     * ServiceManager object
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * ServiceManager object
     * @var ServiceManager
     */
    protected $objectManager;

    /**
     * data array
     * @var array
     */
    protected $data = array();

    /**
     * Init the object
     * @param array/int $data
     * @param string $session - session name
     */
    public function __construct(ObjectManager $objectManager, ServiceManager $sm)
    {
        $this->objectManager  = $objectManager;
        $this->serviceManager = $sm;
        $this->initFromDb();
    }

    /**
     * Get object manager
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        if (!$this->objectManager && $this->serviceManager) {
            $this->objectManager = $this->serviceManager->get('Di');
        }
        return $this->objectManager;
    }

    /**
     * Get object manager
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * Alias name for get object manager
     * @param string $modelName
     * @return type
     */
    public function getModel(string $modelName)
    {

        return $this->getObjectManager()->get($modelName);
    }

    /**
     * Returns the Role registry for this ACL
     *
     * If no Role registry has been created yet, a new default Role registry
     * is created and returned.
     *
     * @return Role\Registry
     */
    public function getRoleRegistry()
    {
        if (null === $this->roleRegistry) {
            $this->roleRegistry = new Registry();
        }
        return $this->roleRegistry;
    }

    public function getAllResources()
    {
        return $this->resources;
    }

    public function getAllRules()
    {
        return $this->rules;
    }

    public function save()
    {
        
    }

    public function addRole( $role, $parents = null)
    {
            $roleObject = $this->getSingelModel(Role::class, $role);
            $roleObject->setCode($role)->save();
            if (is_array($parents)) {
                foreach ($parents as $parent) {
                    $roleObject->addParent($parent);
                }
            } elseif (isset($parents)) {
                $roleObject->addParent($parents);
            }
        parent::addRole($role, $parents);
    }

    public function addResource($resource, $parent = null)
    {
        $resourceObject = $this->getSingelModel(Resource::class, $resource);
        $resourceObject->setCode($resource)->save();
        if (isset($parent)) {
            $resourceObject->addParent($parent);
        }
        parent::addResource($resource, $parent);
    }

    public function initFromDb()
    {
        $this->initRolesFromDb();
        $this->initResourcesFromDb();
        $this->initPermissionFromDb();
    }

    public function initPermissionFromDb()
    {
        $permissions = $this->getModel(Permission::class)->getAll();
        foreach ($permissions as $permission) {
             $type = $permission->getType();
             parent::$type($permission->getRoleCode(),$permission->getResourceCode(),$permission->getPrivilegeCode());
        }
    }


    public function initResourcesFromDb()
    {
        $resources = $this->getModel(Resource::class)->getAll();
        foreach ($resources as $resource) {
            $this->addResourceByObject($resource);
        }
    }

    public function addResourceByObject($resource)
    {
        $parent = $resource->getParent(null);
        if (isset($parent) && !$this->hasResource($parent)) {
            $parentObject = $this->getSingelModel(Resource::class, $parent);
            $this->addResourceByObject($parentObject);
        }
        if (!$this->hasResource($resource)) {
            parent::addResource($resource->getCode(), $parent);
        }
    }

    public function addRolesByObject($role)
    {
        $parents     = $role->getParents(array());
        $parentsCode = null;

        foreach ($parents as $parent) {
            if (!$this->hasRole($parent)) {
                $parentObject = $this->getSingelModel(Role::class, $parent);
                $this->addRolesByObject($parentObject);
            }
            $parentsCode[] = $parent->getCode();
        }
        if (!$this->hasRole($role)) {
            parent::addRole($role->getCode(), $parentsCode);
        }
    }

    public function initRolesFromDb()
    {
        $roles = $this->getModel(Role::class)->getAll();
        foreach ($roles as $role) {
            $this->addRolesByObject($role);
        }
    }

    public function addPrivilege($privilege, $parent = null)
    {
        $privilegeObject = $this->getSingelModel(Privilege::class, $privilege);
        $privilegeObject->setCode($privilege)->save();
        if (isset($parent)) {
            $privilegeObject->addParent($parent);
        }
    }

    public function allow($roles = null, $resources = null, $privileges = null,
                          \Zend\Permissions\Acl\Assertion\AssertionInterface $assert
    = null)
    {
        parent::allow($roles, $resources, $privileges, $assert);
        $roles      = isset($roles) ? (!is_array($roles) ? [$roles] : $roles) : array();
        $resources  = isset($resources) ? (!is_array($resources) ? [$resources] : $resources)
                : array();
        $privileges = isset($privileges) ? (!is_array($privileges) ? [$privileges]
                : $privileges) : array();

        foreach ($privileges as $privilege) {
            $this->addPrivilege($privilege);
        }
        $this->addMultiPermissions($roles, $resources, $privileges, true);
    }

    public function deny($roles = null, $resources = null, $privileges = null,
                         \Zend\Permissions\Acl\Assertion\AssertionInterface $assert
    = null)
    {
        parent::deny($roles, $resources, $privileges, $assert);
        $roles      = isset($roles) ? (!is_array($roles) ? [$roles] : $roles) : array();
        $resources  = isset($resources) ? (!is_array($resources) ? [$resources] : $resources)
                : array();
        $privileges = isset($privileges) ? (!is_array($privileges) ? [$privileges]
                : $privileges) : array();

        foreach ($privileges as $privilege) {
            $this->addPrivilege($privilege);
        }
        $this->addMultiPermissions($roles, $resources, $privileges, false);
    }

    public function addMultiPermissions(array $roles, array $resources,
                                        array $privileges, $isAllow = true)
    {
        $type = $isAllow ? 'allow' : 'deny';

        if (empty($roles)) {
            if (empty($resources)) {
                if (empty($privileges)) {
                    $this->addPermission(null, null, null, $type);
                } else {
                    foreach ($privileges as $privilege) {
                        $this->addPermission(null, null, $privilege, $type);
                    }
                }
            } else {
                foreach ($resources as $resource) {
                    if (empty($privileges)) {
                        $this->addPermission(null, $resource, null, $type);
                    } else {
                        foreach ($privileges as $privilege) {
                            $this->addPermission(null, $resource, $privilege,
                                $type);
                        }
                    }
                }
            }
        } else {
            foreach ($roles as $role) {
                if (empty($resources)) {
                    if (empty($privileges)) {
                        $this->addPermission($role, null, null, $type);
                    } else {
                        foreach ($privileges as $privilege) {
                            $this->addPermission($role, null, $privilege, $type);
                        }
                    }
                } else {
                    foreach ($resources as $resource) {
                        if (empty($privileges)) {
                            $this->addPermission($role, $resource, null, $type);
                        } else {
                            foreach ($privileges as $privilege) {
                                $this->addPermission($role, $resource,
                                    $privilege, $type);
                            }
                        }
                    }
                }
            }
        }
    }

    public function addPermission($role = null, $resource = null,
                                  $privilege = null, string $type = 'allow')
    {
        $this->getModel(Permission::class)
            ->setRoleCode($role)
            ->setResourceCode($resource)
            ->setPrivilegeCode($privilege)
            ->setType($type)
            ->save();
    }

    public function saveRoles()
    {
        $roles = $this->getRoleRegistry()->getRoles();
        foreach ($roles as $role => $roleData) {
            $newRole = $this->getSingelModel(Role::class, $role);
            $newRole->save();
            if (isset($roleData['children']) && !empty($roleData['children'])) {
                if (is_array($roleData['children'])) {
                    foreach ($roleData['children'] as $childCode => $childData) {
                        $subRole = $this->getSingelModel(Role::class, $childCode);
                        $subRole->setParent($newRole);
                        $subRole->save();
                    }
                } else {
                    $childCode = $roleData['children'][key($roleData['children'])];

                    $subRole = $this->getSingelModel(Role::class, $childCode);
                    $subRole->setParent($newRole);
                    $subRole->save();
                }
            }
        }
    }

    public function getSingelModel($modelName, string $code)
    {
        if (!isset($this->data[$modelName][$code])) {
            $model = $this->getModel($modelName)->loadByKey('code', $code);

            $this->data[$modelName][$code] = $model;
        }
        return $this->data[$modelName][$code];
    }

    public function saveResources()
    {
        $resources = $this->getAllResources();
        foreach ($resources as $role => $roleData) {
            $newRole = $this->getSingelModel(Role::class, $role);
            $newRole->save();
            if (isset($roleData['children']) && !empty($roleData['children'])) {
                if (is_array($roleData['children'])) {
                    foreach ($roleData['children'] as $childCode => $childData) {
                        $subRole = $this->getSingelModel(Resource::class,
                            $childCode);
                        $subRole->setParent($newRole);
                        $subRole->save();
                    }
                } else {
                    $childCode = $roleData['children'][key($roleData['children'])];

                    $subRole = $this->getSingelModel(Resource::class, $childCode);
                    $subRole->setParent($newRole);
                    $subRole->save();
                }
            }
        }
    }

    public function saveRelations()
    {
        $rules           = $this->getAllRules();
        $rulesByResource = $rules['byResourceId'] ?? array();
        foreach ($rulesByResource as $ruleByResource) {
            
        }
    }

    /**
     * Returns true if and only if the Role has access to the Resource
     *
     * The $role and $resource parameters may be references to, or the string identifiers for,
     * an existing Resource and Role combination.
     *
     * If either $role or $resource is null, then the query applies to all Roles or all Resources,
     * respectively. Both may be null to query whether the ACL has a "blacklist" rule
     * (allow everything to all). By default, Zend\Permissions\Acl creates a "whitelist" rule (deny
     * everything to all), and this method would return false unless this default has
     * been overridden (i.e., by executing $acl->allow()).
     *
     * If a $privilege is not provided, then this method returns false if and only if the
     * Role is denied access to at least one privilege upon the Resource. In other words, this
     * method returns true if and only if the Role is allowed all privileges on the Resource.
     *
     * This method checks Role inheritance using a depth-first traversal of the Role registry.
     * The highest priority parent (i.e., the parent most recently added) is checked first,
     * and its respective parents are checked similarly before the lower-priority parents of
     * the Role are checked.
     *
     * @param  Role\RoleInterface|string            $role
     * @param  Resource\ResourceInterface|string    $resource
     * @param  string                               $privilege
     * @return bool
     */
    public function isAllowed($role = null, $resource = null, $privilege = null)
    {
        return ($this->hasResource($resource) && $this->hasRole($role) && parent::isAllowed($role, $resource, $privilege));
    }


}