<?php

namespace Arbel\MobileNotification;

use Arbel\MobileNotification\MobileNotificationInterface;
use Arbel\Base\Element;
use Arbel\Config;
use Arbel\Helper;
use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device;
use paragraph1\phpFCM\Notification;

class Fcm extends Element implements MobileNotificationInterface {

    const SUCCESS_CODE = 0;

    public function __construct(Config $config) {
        $this->config = $config;
    }

    public function addTo(string $deviceToken) {
        parent::setArrayData('to', $deviceToken, $deviceToken);
        return $this;
    }

    /**
     * Send sms
     * @return boolean - sending status
     */
    public function send() {


        $apiKey = $this->config->get('api.fcm.server_key');
        $client = new Client();
        $client->setApiKey($apiKey);
        $client->injectHttpClient(new \GuzzleHttp\Client());
        $note = new Notification($this->getSubject(), $this->getContent());
        $note->setIcon('notification_icon_resource_name')
                ->setColor('#ffffff')
                ->setBadge(1);

        foreach ($this->getTo() as $deviceToken) {
            
        }
        $message = new Message();
        $message->addRecipient(new Device($deviceToken));
        $message->setNotification($note)
                ->setData($this->get('message_data',[]));

        $response = $client->send($message);
        $this->setTrackingKey($response->getStatusCode());
        $this->setInfo($response->getBody()->getContents());
      
        return $response->getStatusCode() == 200;//$this::SUCCESS_CODE;
    }

    public function reset() {
        $this->setTo([]);
        parent::setFrom(null);
        return $this;
    }

    public function setMessage(string $message) {
        parent::setMessage($message);
        return $this;
    }

    public function setFrom(string $deviceToken) {
        parent::setFrom($deviceToken);
        return $this;
    }

    public function getParamsUrl() {
        $params = $this->getParams();
        $pre = '';
        if ($params && !empty($params) && is_array($params)) {
            $pre = '?' . http_build_query($params);
        }
        return $pre;
    }

}
