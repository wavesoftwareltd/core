<?php

namespace Arbel\Manage;

use Arbel\Helper;

/**
 * AngularCli manager
 */
class AngularCli extends Base {

    const FILE_NAME = '.angular-cli.json';
    const BUILD_181N_ANGULAR_TEMPLATE_COMMAND = '"' . ROOT_DIR . '/node_modules/.bin/ng-xi18n" --app {{APP_NAME}} --i18nFormat xlf';
    const BUILD_181N_ANGULAR_BUILD_COMMAND = '"' . ROOT_DIR . '/node_modules/.bin/ng" build {{WATCH_FLAG}} --app {{APP_NAME}} '
            . '--prod --aot --output-hashing none --locale {{LANG}} --i18n-file '
            . '{{I18N_FILE}} --i18n-format xlf --output-path {{TARGET_DIR}}  --deploy-url "{{DEPLOY_URL}}"';
    const ANGULAR_LANG_TEMPLATE_DEFUALT = ROOT_DIR . "messages.xlf";
    const LANG_FILE_NAME = "messages.{{LANG}}.xlf";
    const DEFAULT_LANG = "en-us";

    /**
     * Init angular cli 
     */
    public function init() {
        $loadedModules = $this->getModules();
        $mainFile = ROOT_DIR . DIRECTORY_SEPARATOR . $this::FILE_NAME;

        foreach ($loadedModules as $loadedModule) {
            $console = $this->getConsole();
            $console->writeLine('Load module ' . $loadedModule);
            $moduleClass = '\\' . $loadedModule . '\Module';
            $reflector = new \ReflectionClass($moduleClass);
            $moduleDir = dirname(dirname($reflector->getFileName()));
            $currentFile = $moduleDir . DIRECTORY_SEPARATOR . $this::FILE_NAME;
            if (file_exists($currentFile)) {
                $console->writeLine($loadedModule . ' ' . $this::FILE_NAME . ' exist = ' . $currentFile);
                $this->syncFiles($mainFile, $currentFile);
            }
        }
    }

    /**
     * Sync angular cli files
     * @param type $mainFile
     * @param type $currentFile
     * @return type
     */
    public function syncFiles($mainFile, $currentFile) {
        if (file_exists($mainFile)) {
            $mainContent = file_get_contents($mainFile);
            $mainArray = json_decode($mainContent, true);
        } else {
            $mainArray = array();
        }
        $currentContent = file_get_contents($currentFile);
        $currentArray = json_decode($currentContent, true);
        if (empty($mainArray) && !empty($currentArray)) {
            $mainArray = $currentArray;
        } else {
            if (!$currentArray && !isset($currentArray['apps'])) {
                $this->getConsole()->writeLine('Error - ' . $currentFile . ' are not valid json');
                return;
            }
            $currentApps = $currentArray['apps'];
            $mainApps = $mainArray['apps'] ?? array();
            foreach ($currentApps as $currentIndex => $currentApp) {
                foreach ($mainApps as $mainIndex => $mainApp) {
                    if ($mainApp['name'] == $currentApp['name']) {
                        $mainApps[$mainIndex] = $currentApp;
                        unset($currentApps[$currentIndex]);
                    }
                }
            }
            foreach ($currentApps as $currentApp) {
                $mainApps[] = $currentApp;
            }
            $mainArray['apps'] = $mainApps;
        }
        $mergedJson = json_encode($mainArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        file_put_contents($mainFile, $mergedJson);
    }

    /**
     * Build transalte app
     * @param type $appName
     * @return type
     */
    public function buildTransaltedApps($appName = null) {
        $loadedModules = $this->getModules();
        $mainFile = ROOT_DIR . DIRECTORY_SEPARATOR . $this::FILE_NAME;

        foreach ($loadedModules as $loadedModule) {
            $console = $this->getConsole();
            $moduleClass = '\\' . $loadedModule . '\Module';
            $reflector = new \ReflectionClass($moduleClass);
            $moduleDir = dirname(dirname($reflector->getFileName()));
            $currentFile = $moduleDir . DIRECTORY_SEPARATOR . $this::FILE_NAME;
            if (file_exists($currentFile)) {
                $console->writeLine('Load module ' . $loadedModule);
                $string = file_get_contents($currentFile);
                $currentArray = json_decode($string, true);
                if (!$currentArray || !isset($currentArray['apps'])) {
                    $this->getConsole()->writeLine('Error - ' . $currentFile . ' are not valid json');
                    return;
                }
                $apps = $currentArray['apps'];

                foreach ($apps as $app) {
                    if(($appName && $appName == $app['name']) || !$appName){
                    $this->buildTransaltedByApp($app);
                    }
                }

                $console->writeLine($loadedModule . ' ' . $this::FILE_NAME . ' exist = ' . $currentFile);
                $this->syncFiles($mainFile, $currentFile);
            }
        }
    }

    public function buildTransaltedByApp(array $app) {
        if (isset($app['locale']) && !empty($app['locale'])) {
            $locales = $app['locale'];
            $originDeployUrl = $app['deployUrl'];
            $appName = $app['name'];
            $distDir = $app['outDir'];
            $targetLocaleDir = ROOT_DIR . $app['root'] . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR;
            $command = str_replace('{{APP_NAME}}', $appName, $this::BUILD_181N_ANGULAR_TEMPLATE_COMMAND);


            //Delete old template
            if (file_exists($this::ANGULAR_LANG_TEMPLATE_DEFUALT)) {
                unlink($this::ANGULAR_LANG_TEMPLATE_DEFUALT);
            }
            
            //Run create new template command
            $this->getConsole()->writeLine('Run command - ' . $command);
            $output = shell_exec($command);
            $this->getConsole()->writeLine(print_r($output, true));

            foreach ($locales as $lang) {

                //Create modify translate
                $langDir = $distDir . DIRECTORY_SEPARATOR . $lang;
                $fullPath = $this->translateTemplate($lang, $langDir, $targetLocaleDir);
                $this->getConsole()->writeLine('Create modify translate - ' . $fullPath);
                $deployUrl = $originDeployUrl.$lang.'/';

                $buildCommand = Helper::findReplaceBullk(array(
                            '{{APP_NAME}}' => $appName,
                            '{{TARGET_DIR}}' => $langDir,
                            '{{I18N_FILE}}' => $fullPath,
                            '{{LANG}}' => $lang,
                            '{{WATCH_FLAG}}' => '',
                            '{{DEPLOY_URL}}' => '',
                                ), $this::BUILD_181N_ANGULAR_BUILD_COMMAND);


                //Run create new template command
                $this->getConsole()->writeLine('Run build command - ' . $buildCommand);
                $output = shell_exec($buildCommand);
                $this->getConsole()->writeLine(print_r($output, true));
            }
        }
    }

    public function translateTemplate(string $lang, string $targetDir, string $targetLocaleDir) {
        if (file_exists($this::ANGULAR_LANG_TEMPLATE_DEFUALT)) {
            if (!file_exists($targetLocaleDir)) {
                mkdir($targetLocaleDir, 0777, true);
            }

            $string = file_get_contents($this::ANGULAR_LANG_TEMPLATE_DEFUALT);
            $xml = simplexml_load_string($string);
            $trans = "trans-unit";
            foreach ($xml->file->body->$trans as $index => $translate) {
                $source = $translate->source;
                $domain = (string) $translate->note;

                if ($lang != $this::DEFAULT_LANG) {
                    $translate->target = $this->getTranslator()->__($source, $lang, $domain);
                } else {
                    $translate->target = $source;
                }
            }
            $content = $xml->asXML();


            $fileName = $this->getFileNameByLang($lang);
            $fullPath = $targetLocaleDir . $fileName;
            $myfile = fopen($fullPath, "w") or die("Unable to open file!");
            fwrite($myfile, $content);
            fclose($myfile);
            return $fullPath;
        }
    }

    public function getFileNameByLang(string $lang) {
        return str_replace('{{LANG}}', $lang, $this::LANG_FILE_NAME);
    }

}
