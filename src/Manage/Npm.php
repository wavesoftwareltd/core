<?php

namespace Arbel\Manage;

/**
 * Npm manager
 */
class Npm extends Base
{
    const FILE_NAME = 'package.json';

    /**
     * Init
     */
    public function init()
    {
        $loadedModules = $this->getModules();
        $mainFile      = ROOT_DIR.DIRECTORY_SEPARATOR.$this::FILE_NAME;

        foreach ($loadedModules as $loadedModule) {
            $console     = $this->getConsole();
            $console->writeLine('Load module '.$loadedModule);
            $moduleClass = '\\'.$loadedModule.'\Module';
            $reflector   = new \ReflectionClass($moduleClass);
            $moduleDir   = dirname(dirname($reflector->getFileName()));
            $currentFile = $moduleDir.DIRECTORY_SEPARATOR.$this::FILE_NAME;
            if (file_exists($currentFile)) {
                $console->writeLine($loadedModule.' '.$this::FILE_NAME.' exist = '.$currentFile);
                $this->syncFiles($mainFile, $currentFile);
            }
        }
    }

    /**
     * Sync package files
     * @param type $mainFile
     * @param type $currentFile
     * @return type
     */
    public function syncFiles($mainFile, $currentFile)
    {
        if (file_exists($mainFile)) {
            $mainContent = file_get_contents($mainFile);
            $mainArray   = json_decode($mainContent, true);
        } else {
            $mainArray = array();
        }
        $currentContent = file_get_contents($currentFile);
        $currentArray   = json_decode($currentContent, true);
        if (!$currentArray) {
            $this->getConsole()->writeLine('Error - '.$currentFile.' are not valid json');
            return;
        }
        $mergedArray = array_merge_recursive($mainArray, $currentArray);
        if (isset($mergedArray['name']) && count($mergedArray['name']) > 1) {
            $mergedArray['name'] = current($mergedArray['name']);
        }
        if (isset($mergedArray['version']) && count($mergedArray['version']) > 1) {
            $mergedArray['version'] = current($mergedArray['version']);
        }
        if (isset($mergedArray['private']) && count($mergedArray['private']) > 1) {
            $mergedArray['private'] = current($mergedArray['private']);
        }
        $mergedJson = json_encode($mergedArray,
            JSON_PRETTY_PRINT | JSON_FORCE_OBJECT | JSON_UNESCAPED_SLASHES);
        file_put_contents($mainFile, $mergedJson);
    }
}