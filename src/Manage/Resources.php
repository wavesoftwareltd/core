<?php

namespace Arbel\Manage;

use Arbel\Helper;

/**
 * Resources manager
 */
class Resources extends Base {

    protected $existingFiles = array();
    protected $ignoredFilesToDelete = array(
        '%.xlf'
    );

    /**
     * Copy resources files to root path
     */
    public function copy() {
        $loadedModules = $this->getModules();

        foreach ($loadedModules as $loadedModule) {
            $console = $this->getConsole();
            $console->writeLine('Load module ' . $loadedModule);
            $moduleClass = '\\' . $loadedModule . '\Module';
            $reflector = new \ReflectionClass($moduleClass);
            $moduleDir = dirname(dirname($reflector->getFileName()));
            $resourcesDir = $moduleDir . DIRECTORY_SEPARATOR . 'resources';
            $targetFolder = ROOT_DIR . DIRECTORY_SEPARATOR . 'resources';
            if (file_exists($resourcesDir)) {
                $console->writeLine($loadedModule . ' $resourcesDir exist = ' . $resourcesDir);
                \Arbel\Helper::copyFolder($resourcesDir, $targetFolder, null, $this->existingFiles);
            }
        }
    }

    /**
     * Symlink resources files to root path
     */
    public function symlink() {
        $loadedModules = $this->getModules();

        foreach ($loadedModules as $loadedModule) {
            $console = $this->getConsole();
            $console->writeLine('Load module ' . $loadedModule);
            $moduleClass = '\\' . $loadedModule . '\Module';
            $reflector = new \ReflectionClass($moduleClass);
            $moduleDir = dirname(dirname($reflector->getFileName()));
            $resourcesDir = $moduleDir . DIRECTORY_SEPARATOR . 'resources';
            $targetFolder = ROOT_DIR . DIRECTORY_SEPARATOR . 'resources';
            if (file_exists($resourcesDir)) {
                $console->writeLine($loadedModule . ' $resourcesDir exist = ' . $resourcesDir);
                \Arbel\Helper::symlinkFiles($resourcesDir, $targetFolder);
            }
        }
    }

    /**
     * Symlink folders resources files to root path
     */
    public function symlinkFolders() {
        $loadedModules = $this->getModules();

        foreach ($loadedModules as $loadedModule) {
            $console = $this->getConsole();
            $console->writeLine('Load module ' . $loadedModule);
            $moduleClass = '\\' . $loadedModule . '\Module';
            $reflector = new \ReflectionClass($moduleClass);
            $moduleDir = dirname(dirname($reflector->getFileName()));
            $resourcesDir = $moduleDir . DIRECTORY_SEPARATOR . 'resources';
            $targetFolder = ROOT_DIR . DIRECTORY_SEPARATOR . 'resources';
            if (file_exists($resourcesDir)) {
                $console->writeLine($loadedModule . ' $resourcesDir exist = ' . $resourcesDir);
                \Arbel\Helper::symlinkFolders($resourcesDir, $targetFolder);
            }
        }
    }

    /**
     * Init
     */
    public function init() {

        Helper::removeFolderContent(ROOT_DIR . DIRECTORY_SEPARATOR . 'resources');
        $this->copy();
    }

    /**
     * Watch changes on files
     */
    public function watch() {

        $loadedModules = $this->getModules();
        $console = $this->getConsole();
        $console->writeLine(' start watching... ');
        $lastTimeModified = time();
        $modulesDir = $this->getModulesDirs();
        while (true) {
            foreach ($modulesDir as $moduleDir) {
                $resourcesDir = $moduleDir . DIRECTORY_SEPARATOR . 'resources';
                $targetFolder = ROOT_DIR . 'resources';
                if (file_exists($resourcesDir)) {
                    \Arbel\Helper::checkNewModifyFiles($resourcesDir, $lastTimeModified, function($filePath, $status) use ($console, $modulesDir, $resourcesDir, $targetFolder) {
                      
                        $relativePath = str_replace($resourcesDir, '', $filePath);
                        $targetPath = $targetFolder . DIRECTORY_SEPARATOR . $relativePath;
                         if($status != \Arbel\Helper::DELETED_FILE_FLAG && file_exists($targetPath) && file_exists($targetPath) && (filemtime($filePath) < filemtime($targetPath))){
                             return ;
                         }
                        $console->writeLine(' update modified file ' . $relativePath . ' = '.$status);
                        $needToRemove = true;
                        foreach ($modulesDir as $moduleDir) {
                            $currentResourcesDir = $moduleDir . DIRECTORY_SEPARATOR . 'resources';
                            $currentFileSearch = $currentResourcesDir . DIRECTORY_SEPARATOR . $relativePath;
                            if (file_exists($currentFileSearch)) {
                                if (file_exists($targetPath)) {
                            $console->writeLine(' unlink =  ' . $targetPath);
                                    unlink($targetPath);
                                }
                                if (!file_exists(dirname($targetPath))) {
                                    mkdir(dirname($targetPath), 0777, true);
                                }
                                copy($currentFileSearch, $targetPath);
                                $needToRemove = false;
                            }
                        }
                        
                            $console->writeLine(' $needToRemove =  ' . (int)$needToRemove);
                        if ($needToRemove && file_exists($targetPath)) {
                            $console->writeLine(' remove deleted file ' . $relativePath);
                           unlink($targetPath);
                            if (\Arbel\Helper::isDirEmpty($targetPath)) {
                                $console->writeLine(' remove empty dir ' . dirname($targetPath));
                                \Arbel\Helper::removeEmptyParentsFolders($targetPath);
                            }
                        }
                    });
                }
            }
        }
    }

    public function getModulesDirs() {
        $loadedModules = $this->getModules();
        $result = array();
        foreach ($loadedModules as $loadedModule) {
            $moduleClass = '\\' . $loadedModule . '\Module';
            $reflector = new \ReflectionClass($moduleClass);
            $moduleDir = dirname(dirname($reflector->getFileName()));
            $result[] = $moduleDir;
        }
        return $result;
    }

}
