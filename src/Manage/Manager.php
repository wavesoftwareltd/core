<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Arbel\Manage;

use Arbel\Helper;

/**
 * Description of Manager
 *
 * @author yariv-work
 */
class Manager extends Base
{

    public function __construct(\Zend\ServiceManager\ServiceManager $sm, Resources $resources,
     AngularCli $angularCli, Npm $npm)
    {
        parent::__construct($sm,$sm->get('Di'));
        $this->setAngularCli($angularCli)
            ->setNpm($npm)
            ->setResources($resources);
    }
    public function watch()
    {

       $this->init();
       $this->getResources()->watch();
    }
     
    public function init()
    {
       $this->getNpm()->init();
       $this->getAngularCli()->init();
       $this->getResources()->init();
    }

}