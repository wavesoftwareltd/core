<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Arbel\Manage;

use Arbel\Base\Element;
use Zend\ServiceManager\ServiceManager;
use Arbel\ObjectManager;
use Arbel\Model\Translator;

/**
 * Description of Base
 *
 * @author yariv-work
 */
class Base extends Element
{
    public function __construct(ServiceManager $sm,
                                ObjectManager $objectManager)
    {
        parent::__construct();
        $this->setServiceManager($sm);
        $this->setObjectManager($objectManager);
    }

    public function getModules()
    {
        $manager       = $this->getServiceManager()->get('ModuleManager');
        $modules       = $manager->getLoadedModules();
        $loadedModules = array_keys($modules);
        return $loadedModules;
    }

    public function getConsole()
    {
        return $this->getServiceManager()->get('console');
    }
    
    /**
     * Get Model
     * @param string $class
     * @return mixed
     */
    
    public function getModel(string $class) {
        return  $this->getObjectManager()->get($class);
    }
    
    /**
     * get Translator
     * @return Translator
     */
    public function getTranslator() {
        return $this->getModel(Translator::class);
    }


}