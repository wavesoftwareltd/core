<?php

namespace Arbel\Calls;

interface CallInterface {

    /**
     * Add number
     * @param string $number
     */
     public function addTo(string $number);

     /**
      * Set message text
      * @param string $param
      */
     public function setMessage(string $param);

     /**
      * Set source number
      * @param string $number
      */
     public function setFrom(string $number);

     /**
      * Send sms
      */
     public function send();
}