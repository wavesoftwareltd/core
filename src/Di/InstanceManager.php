<?php

namespace Arbel\Di;

use Zend\Di\InstanceManager as ZendInstanceManager;
use Zend\ServiceManager\ServiceManager;
use Arbel\ObjectManager;
/**
 * Overwrite di instance manager to support set instance
 */
class InstanceManager extends ZendInstanceManager
{

    const DEFAULT_ALLOWED_METHOD = 'setInjections';

    /**
     * ObjectManager
     * @var ObjectManager
     */
    protected $objectManager;

    
    /**
     * Set shared instance
     * @return InstanceManager;
     */
    public function setSharedInstance(string $classOrAlias, $object)
    {
        $this->sharedInstances[$classOrAlias] = $object;
        return $this;
    }

    /**
     * Search on factory when get value string
     * @param string $classOrAlias
     * return @mixed
     */
    public function getSharedInstance($classOrAlias)
    {
        $value = parent::getSharedInstance($classOrAlias);
        if (is_string($value) && isset($this->sharedInstances[ServiceManager::class])) {
            if($this->sharedInstances[ServiceManager::class]->has($value)){
            return $this->sharedInstances[ServiceManager::class]->get($value);
            }else{
            return $this->getObjectManager()->get($value);
            }
        } else {
            return $value;
        }
    }

    /**
     * setParameters() is a convenience method for:
     *    setConfig($type, array('parameters' => array(...)), true);
     *
     * @param  string $aliasOrClass Alias or Class
     * @param  array  $parameters   Multi-dim array of parameters and their values
     * @return $this
     */
    public function setParameters($aliasOrClass, array $parameters,
                                  array $methods = array())
    {
        $this->setConfig($aliasOrClass,
            ['parameters' => $parameters, 'methods' => $methods], true);
        return $this;
    }

    /**
     * @param  string $aliasOrClass
     * @return array
     */
    public function getConfig($aliasOrClass)
    {
        $key = ($this->hasAlias($aliasOrClass)) ? 'alias:'.$this->getBaseAlias($aliasOrClass)
                : $aliasOrClass;
        if (isset($this->configurations[$key])) {
            ;
            return $this->configurations[$key];
        } else {
            foreach ($this->configurations as $key => $config) {
                if (is_subclass_of($aliasOrClass, $key)) {
                    return $config;
                }
            }
        }

        return $this->configurationTemplate;
    }

    /**
     * Set shared
     *
     * @param  string $aliasOrClass
     * @param  bool   $isShared
     * @return $this
     */
    public function setShared($aliasOrClass, $isShared)
    {
        parent::setShared($aliasOrClass, $isShared);
        return $this;
    }

    /**
     * Sets configuration for a single alias/class
     *
     * @param string $aliasOrClass
     * @param array  $configuration
     * @param bool   $append
     */
    public function setConfig($aliasOrClass, array $configuration,
                              $append = false)
    {
        $key = ($this->hasAlias($aliasOrClass)) ? 'alias:'.$this->getBaseAlias($aliasOrClass)
                : $aliasOrClass;
        if (!isset($this->configurations[$key]) || !$append) {
            $this->configurations[$key] = $this->configurationTemplate;
        }
        // Ignore anything but 'parameters' and 'injections'
        $configuration              = [
            'parameters' => isset($configuration['parameters']) ? $configuration['parameters']
                : [],
            'injections' => isset($configuration['injections']) ? $configuration['injections']
                : [],
            'methods' => isset($configuration['methods']) ? $configuration['methods']
                : [],
            'shared' => isset($configuration['shared']) ? $configuration['shared']
                : true
        ];
        $this->configurations[$key] = array_replace_recursive($this->configurations[$key],
            $configuration);
    }

    /**
     * Check if method is allow for di
     * @param string $key
     * @param string $method
     * @return bool
     */
    public function isAllowedMethods($key, $method)
    {
        return $method == $this::DEFAULT_ALLOWED_METHOD ||
            (isset($this->configurations[$key]) && in_array($method, $this->configurations[$key]['methods']));
    }

    /**
     * Check if class is defined
     * @param string $key
     * @return bool
     */
    public function isClassDefined($key)
    {
        return isset($this->configurations[$key]);
    }

    public function filterMethods(array &$classs,string $ignoreFunction = null)
    {
        foreach ($classs as $classNmae => $methods) {
            foreach ($methods as $method => $dCounter) {
                if ($ignoreFunction != $method && !$this->isAllowedMethods($classNmae, $method)) {
                    unset($classs[$classNmae][$method]);
                }elseif($classs[$classNmae][$method] == ObjectManager::METHOD_IS_OPTIONAL){
                    $classs[$classNmae][$method] = ObjectManager::METHOD_IS_REQUIRED;
                }
            }
        }
    }

    /**
     * Set object manager
     * @param ObjectManager $objectManager
     * @return $this
     */
    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
        return $this;
    }

    /**
     * Get object manager
     * @return ObjectManager
     */
    public function getObjectManager() : ObjectManager
    {
        return $this->objectManager;
    }
}