<?php

namespace Arbel\Utils;

class Shorten
{
    const MULTI_BY = 10000;
    protected static $chars = "123456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";
    protected static $key = "Nasn7mnasd@SDMF4$@#SDNgwxpYariArbelReyrlsd342!@##";



    static function convertIntToShortCode($id)
    {
        $id = intval($id);
        if ($id < 1) {
            throw new Exception(
            "The ID is not a valid integer");
        }

        $length = strlen(self::$chars);
        // make sure length of available characters is at
        // least a reasonable minimum - there should be at
        // least 10 characters
        if ($length < 10) {
            throw new Exception("Length of chars is too small");
        }

        $code = "";
        while ($id > $length - 1) {
            // determine the value of the next higher character
            // in the short code should be and prepend
            $code = self::$chars[fmod($id, $length)].
                $code;
            // reset $id to remaining value to be converted
            $id   = floor($id / $length);
        }

        // remaining value of $id is less than the length of
        // self::$chars
        $code = self::$chars[$id].$code;

        return $code;
    }

    static function alphaID($in, $to_num = false, $pad_up = false, $passKey = null)
    {
        if(is_null($passKey)){
            $passKey = self::$key;
        }
        $index = self::$chars;
        if ($passKey !== null) {
            /* Although this function's purpose is to just make the
             * ID short - and not so much secure,
             * with this patch by Simon Franz (http://blog.snaky.org/)
             * you can optionally supply a password to make it harder
             * to calculate the corresponding numeric ID */

            for ($n = 0; $n < strlen($index); $n++) {
                $i[] = substr($index, $n, 1);
            }

            $passhash = hash('sha256', $passKey);

            $passhash = (strlen($passhash) < strlen($index)) ? hash('sha512',
                    $passKey) : $passhash;

            for ($n = 0; $n < strlen($index); $n++) {
                $p[] = substr($passhash, $n, 1);
            }

            array_multisort($p, SORT_DESC, $i);
            $index = implode($i);
        }

        $base = strlen($index);

        if ($to_num) {
            // Digital number  <<--  alphabet letter code
            $in  = strrev($in);
            $out = 0;
            $len = strlen($in) - 1;

            for ($t = 0; $t <= $len; $t++) {
                $bcpow = bcpow($base, $len - $t);
                $out   = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
            }

            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $out -= pow($base, $pad_up);
                }
            }
            $out = sprintf('%F', $out);
            $out = substr($out, 0, strpos($out, '.'));
        } else {
            // Digital number  -->>  alphabet letter code
            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $in += pow($base, $pad_up);
                }
            }

            $out = "";
            for ($t = floor(log($in, $base)); $t >= 0; $t--) {
                $bcp = bcpow($base, $t);
                $a   = floor($in / $bcp) % $base;
                $out = $out.substr($index, $a, 1);
                $in  = $in - ($a * $bcp);
            }
            $out = strrev($out); // reverse
        }
        return $out;
    }

    static function toKey(int $number){
        return self::alphaID($number * self::MULTI_BY);
    }

    static function toNumber(string $key){
        return self::alphaID($key,true) / self::MULTI_BY;
    }
}