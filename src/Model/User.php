<?php

namespace Arbel\Model;

use Arbel\Base\ModelAbstract;
use Arbel\Model\Oauth\AccessToken;
use Arbel\Model\Auth\Registration;
use Arbel\Helper;

/**
 * User model
 */
class User extends ModelAbstract {

    //table name
    const TABLE_NAME = 'core_user';
    //primary key
    const PRIMARY_KEY = 'id';
    //default permission
    const DEFAULT_PERMISSION = 'basic';
    //default permission
    const ADMINISTRATOR_RULE_CODE = 'administrator';
    //default country code
    const DEFAULT_COUNTRY_CODE = 'IL';
    
     protected $jsonToObjectFields = array('info');

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string {
        return self::TABLE_NAME;
    }

    public function initConfig() {
        $this->hasOne('language', 'code', 'Arbel\Model\Language', 'language_code');
        $this->hasOne('country', 'iso', 'Arbel\Model\Country', 'country_code');
        $this->hasOne('registration', 'id', Registration::class, 'user_id');
    }

    /**
     * Set password and hash it
     * @param string $password
     */
    public function setSecurePassword(string $password)
    {
        return $this->setPassword(\Arbel\Helper::bcryptHashing($password));
    }

    /**
     * Check if user is login
     */
    public function isConnected(): bool {
        return (bool) $this->getId();
    }

    /**
     * Check if user is login
     */
    public function getPermission(): string {
        return parent::getPermission($this::DEFAULT_PERMISSION);
    }

    /**
     * Get final phone with prefix
     * @return string
     */
    public function getFinalPhone() {
        $originPhone = $this->getPhone();
        if (substr($originPhone, 0, 1) == '0') {
            $fullPhone = '+' . $this->getCountry()->getPhonePrefix() . ltrim($originPhone, '0');
            return $fullPhone;
        } else {
            return $originPhone;
        }
    }

    /**
     * Is password verified
     * @param string $password
     * @return bool
     */
    public function isPasswordVerified(string $password): bool {
        return password_verify($password, $this->getPassword());
    }

    public function initToken(string $token) {
        if (strpos($token, ' ') !== false) {
            $tokenArray = explode(' ', $token);
            $token = $tokenArray[1];
        }
        /*
         * @var $tokenObject \Arbel\Model\Oauth\AccessToken
         */
        $tokenObject = $this->getModel(AccessToken::class)->loadBy(array(
            AccessToken::getPrimaryKey() => $token,
            "expires > '".date("Y-m-d H:i:s")."'"
            ));
        if($tokenObject->isExist()){
            if($tokenObject->isNeedToExtend()){
                $tokenObject->extend();
            }
            $userId = $tokenObject->getUserId();
            $this->load($userId);
        }
    }
    
    /**
     * Check if user is administrator
     * @return bool
     */
    public function isAdministrator() : bool {
        return $this->getPermission() == $this::ADMINISTRATOR_RULE_CODE;
    }

    /**
     * Check if user is verified
     * @return bool
     */
    public function isVerified() : bool
    {
        return (bool)$this->getIsVerified();
    }

    /**
     * init Registration
     * @return Registration
     */
    public function initRegistration()
    {
        $registration = $this->getModel(Registration::class)
            ->setUserId($this->getId())
            ->save();
        $this->setRegistration($registration);
        return $registration;
    }

    public function getName(){
        return $this->getFirstName().' '.$this->getLastName();
    }

    public function save($isInsertOrUpdate = false, $isInsertIfNoExist = false)
    {
        if(!$this->hasCountryCode()){
            $this->setCountryCode($this::DEFAULT_COUNTRY_CODE);
        }
        if(!$this->hasPermission()){
            $this->setPermission($this::DEFAULT_PERMISSION);
        }
        if(!$this->hasGuid()){
            $this->setGuid(Helper::guid());
        }
       return parent::save($isInsertOrUpdate, $isInsertIfNoExist);
    }

}
