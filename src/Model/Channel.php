<?php

namespace Arbel\Model;

use Arbel\Base\ModelAbstract;

/**
 * Channel model
 */
class Channel extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_channel';
    //primary key
    const PRIMARY_KEY  = 'id';

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function save($isInsertOrUpdate = false, $isInsertIfNoExist = false)
    {
        if(!$this->getCode()){
            $this->generateCode();
        }
        parent::save($isInsertOrUpdate, $isInsertIfNoExist);
    }

}