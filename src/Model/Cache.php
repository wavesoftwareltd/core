<?php

namespace Arbel\Model;

use Arbel\Base\ModelAbstract;

/**
 * Cache model
 */
class Cache extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_cache';
    //primary key
    const PRIMARY_KEY  = 'id';

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

}