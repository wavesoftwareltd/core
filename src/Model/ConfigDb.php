<?php

namespace Arbel\Model;

use Arbel\Base\ModelAbstract;

/**
 * Config model
 */
class ConfigDb extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_config';
    //primary key
    const PRIMARY_KEY  = 'id';

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }
//
//    public function get(string $key,$default = null)
//    {
//       return $this->initByKey('key', $key)->getValue($default);
//    }
//
//    public function set(string $key,string $value)
//    {
//      return $this->initByKey('key', $key)->setValue($value)->save();
//    }

}