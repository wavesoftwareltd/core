<?php

namespace Arbel\Model;

use Arbel\Base\ModelAbstract;
use Zend\Cache\Storage\StorageInterface;
use Arbel\Google\Translate;
use Arbel\Cache\CacheManager;

/**
 * Translator model
 */
class Translator extends ModelAbstract {

    //table name
    const TABLE_NAME = 'core_translate';
    //primary key
    const PRIMARY_KEY = 'id';
    //Default language
    const DEFAULT_LANG = 'en-us';
    //Default domain
    const DEFAULT_DOMAIN = 'default';
    //table name
    const CACHE_CODE = 'translate';
    
    static $translateHistory = [];


    /**
     * Unique table column
     * @var type
     */
    protected $uniqueTableColumn = ['name','domain','language_code'];

    /**
     * Cache object
     * @var StorageInterface
     */
    protected $cache;

    /**
     * Google Translate object
     * @var \Arbel\Google\Translate
     */
    protected $googleTrasnalte;

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string {
        return self::PRIMARY_KEY;
    }

    /**
     * Inject objects
     * @param StorageInterface $cache
     */
    public function setInjections(CacheManager $cacheManager, Translate $googleTrasnalte) {
        $this->cache = $cacheManager->get($this::CACHE_CODE);
        $this->googleTrasnalte = $googleTrasnalte;
    }

    public function getCache() {
        return $this->cache;
    }

    public function getGoogleTranslate() {
        return $this->googleTrasnalte;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string {
        return self::TABLE_NAME;
    }

    /**
     * Get translate
     * @param string $key
     * @param string $lang
     * @param string $domain
     * @return string|null
     */
    public function translateFromDb(string $key, string $lang = null, string $domain = 'default') {
        if (!$lang) {
            $lang = $this->getCurrentLang();
        }
        $result = $this->getOne(array(
            'name' => $key,
            'language_code' => $lang,
            'domain' => $domain
        ));
        if (is_object($result)) {
            $this->getCache()->set(md5(json_encode(array(
                'name' => $key,
                'local' => $lang,
                'domain' => $domain
                    ))), $result->getText());
            return $result->getText();
        } else {
            return null;
        }
    }

    public function getAllForViewByLang(string $lang = null): array {
        $result = array();
        if (!$lang) {
            $lang = $this->getCurrentLang();
        }
        if ($this->getCache()->has(get_class($this) . $lang . '_all_view_lang')) {
            return $this->getCache()->get(get_class($this) . $lang . '_all_view_lang');
        }
        $dbResult = $this->getAll(array(
            'language_code' => $lang
        ));
        foreach ($dbResult as $row) {
            $translate = $row->getText();
            $result[trim(strtolower($row->getName()))][trim(strtolower($row->getDomain()))] = empty($translate) ? $this->__($row->getName(), $lang, $row->getDomain()) : $translate;
        }
         if (!empty($result)) {
            $this->getCache()->set(get_class($this) . $lang . '_all_view_lang', $result);
        }
        return $result;
    }

    /**
     * Get translate
     * @param string $key
     * @param string $lang
     * @param string $domain
     * @return string|null
     */
    public function translateFromCache(string $key, string $lang = null, string $domain = 'default') {
        if (!$lang) {
            $lang = $this->getCurrentLang();
        }
        $result = $this->getCache()->get(md5(json_encode(array(
            'name' => $key,
            'local' => $lang,
            'domain' => $domain
        ))));
        return $result;
    }

    /**
     * Get current lang by global user
     * @return type
     */
    public function getCurrentLang() {
        return $this->getServiceManager()->get('User')->getLanguage()->getCode();
    }

    /**
     * Get current lang by global user
     * @return type
     */
    public function getShortCurrentLang() {
        return $this->getServiceManager()->get('User')->getLanguage()->getLang();
    }

    /**
     * Add translate to db
     * @param string $key
     * @param string $text
     * @param string $lang
     * @param string $domain
     */
    public function addToDb(string $key, string $text, string $lang = null, string $domain = 'default') {
        if (!$lang) {
            $lang = $this->getCurrentLang();
        }

        $this->setName($key)
                ->setText($text)
                ->setLanguageCode($lang)
                ->setDomain($domain)
                ->save(true);
    }

    public function __(string $key, string $lang = null, string $domain = self::DEFAULT_DOMAIN) {
        return $this->translate($key, $lang, $domain);
    }

    /**
     * Get translate from this order :
     * - cache
     * - db with custom domain
     * - db with default domain
     * - i18n files
     * - get from google translate api (optinal)
     * - the same with default language
     * - the key params
     * @param string $key
     * @param string $lang
     * @param string $domain
     * @return string
     */
    public function translate(string $key, string $lang = null, string $domain = self::DEFAULT_DOMAIN) {
        if (!$lang) {
            $lang = $this->getCurrentLang();
        }
        $k = $key . ($lang ? $lang : 'null') . $domain;

        if(isset(self::$translateHistory[$k])){
            return self::$translateHistory[$k];
        }
        //Get translate from cache
        $cacheResult = $this->translateFromCache($key, $lang, $domain);
        if (!empty($cacheResult)) {
            self::$translateHistory[$k] = $cacheResult;
            return $cacheResult;
        }

        //Get translate from db
        $dbResult = $this->translateFromDb($key, $lang, $domain);
        if (!empty($dbResult)) {
            self::$translateHistory[$k] = $dbResult;
            return $dbResult;
        }

        //Get translate from i18n files
        $i18nResult = $this->translateFromI18n($key, $lang, $domain);
        if (!empty($i18nResult)) {
            self::$translateHistory[$k] = $i18nResult;
            return $i18nResult;
        }

        if ($this->getGoogleTranslate()->getIsActive()) {
            //Get translate from db
            $googleResult = $this->translateFromGoogleTranslate($key, $lang, $domain);
            if (!empty($googleResult)) {
            self::$translateHistory[$k] = $googleResult;
                return $googleResult;
            }
        }

        return $key;
    }

    public function isGoogleTranslateActive() {
        return $this->getModel('Arbel\Config')->get('translate.use_google_translate');
    }

    public function translateFromGoogleTranslate(string $key, string $lang, string $domain = self::DEFAULT_DOMAIN) {

        if ($lang == $this->getCurrentLang()) {
            $apiLang = $this->getShortCurrentLang();
        } else {
            if (strpos($lang, '-') !== false) {
                $langArray = explode('-', $lang);
                $apiLang = $langArray[0];
            } else {
                $apiLang = $lang;
            }
        }
        $translate = $this->getGoogleTranslate()->__($key, $apiLang);

        if ($translate) {
            $this->addToDb($key, $translate, $lang, $domain);

            $this->getCache()->set(md5(json_encode(array(
                'name' => $key,
                'local' => $lang,
                'domain' => $domain
                    ))), $translate);

            return $translate;
        }
    }

    /**
     * Translate from i18n
     * @param string $key
     * @param string $lang
     * @return string|null
     */
    public function translateFromI18n(string $key, string $lang, string $domain = self::DEFAULT_DOMAIN) {
        $array = $this->getI18nArray($lang, $domain);
        if (isset($array[$key])) {
            $translate = $array[$key];
            $this->addToDb($key, $translate, $lang, $domain);

            $this->getCache()->set(md5(json_encode(array(
                'name' => $key,
                'local' => $lang,
                'domain' => $domain
                    ))), $translate);

            return $translate;
        }
        return $array[$key] ?? null;
    }

    /**
     * Get 18n array
     * @param type $lang
     * @return type
     */
    public function getI18nArray($lang, $domain) {
        if ($this->getCache()->has(get_class($this) . $lang . '_i18n')) {
            return $this->getCache()->get(get_class($this) . $lang . '_i18n');
        }
        $result = array();
        $manager = $this->getServiceManager()->get('ModuleManager');
        $modules = $manager->getLoadedModules();
        $loadedModules = array_keys($modules);

        foreach ($loadedModules as $loadedModule) {
            $moduleClass = '\\' . $loadedModule . '\Module';
            $moduleObject = new $moduleClass;
            $path = $this->getI18nFolderPath($moduleObject);
            if (!is_null($path)) {
                $domainFilePath = $path . DIRECTORY_SEPARATOR . $domain . DIRECTORY_SEPARATOR . $lang . '.php';
                $filePath = $path . DIRECTORY_SEPARATOR . $lang . '.php';
                if (file_exists($filePath)) {
                    $fileArray = require $filePath;
                    $result = array_merge($result, $fileArray);
                }
                if (file_exists($domainFilePath)) {
                    $fileArray = require $domainFilePath;
                    $result = array_merge($result, $fileArray);
                }
            }
        }

        if (!empty($result)) {
            $this->getCache()->set(get_class($this) . $lang . '_i18n', $result);
        }
        return $result;
    }

    /**
     * Get version folder path
     * @return string
     */
    public function getI18nFolderPath($module) {
        $reflector = new \ReflectionClass($module);
        $path = dirname(dirname($reflector->getFileName())) . DIRECTORY_SEPARATOR . 'i18n';
        if (file_exists($path)) {
            return $path;
        } else {
            return null;
        }
    }

    public function save($isInsertOrUpdate = true, $isInsertIfNoExist = false)
    {
        parent::save($isInsertOrUpdate, $isInsertIfNoExist);
    }

}
