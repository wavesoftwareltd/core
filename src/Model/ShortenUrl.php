<?php

namespace Arbel\Model;

use Arbel\Base\ModelAbstract;

/**
 * Cache model
 */
class ShortenUrl extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_shorten_url';
    //primary key
    const PRIMARY_KEY  = 'id';
    const HASH_LENGTH = 6;

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function generate($url)
    {
        $code = \Arbel\Helper::generateRandomHash($this::HASH_LENGTH);
        $this->setCode($code)
            ->setValue($url)
            ->save();
        return $code;
    }
    
    
    public function generateToUrl($url)
    {
        $code = $this->generate($url);
        return \Arbel\Helper::getCurrentBaseHost().'/s/'.$code;
    }

}