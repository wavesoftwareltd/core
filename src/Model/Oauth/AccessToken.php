<?php

namespace Arbel\Model\Oauth;

use Arbel\Base\ModelAbstract;

/**
 * Cache model
 */
class AccessToken extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'oauth_access_tokens';
    //primary key
    const PRIMARY_KEY  = 'access_token';
    const DEFAULT_EXTEND_TIME = 60 * 60 * 3;

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }
    
    public function extend(int $seconds = self::DEFAULT_EXTEND_TIME) {
        $time = date("Y-m-d H:i:s", time() + $seconds);
        $this->setExpires($time)->save();
    }

    public function isNeedToExtend()
    {
        $expiresDiff = $this->diff('expires');
        return $expiresDiff->days < 1;
    }

}