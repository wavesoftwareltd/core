<?php

namespace Arbel\Model\Communication;

use Arbel\Base\ModelAbstract;
use Arbel\Model\User;
use Arbel\Model\Communication\Interlocutor\Email\System as EmailSystem;
use Arbel\Model\Communication\Interlocutor\Sms\System as SmsSystem;
use Arbel\Model\Communication\Interlocutor\MobileNotification\System as MobileNotificationSystem;
use Arbel\Model\Channel;

/**
 * Notification model
 */
class Notification extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_communication_notification';
    //primary key
    const PRIMARY_KEY  = 'id';
    const CHANNEL_CODE = 'general_notification';


    public function initConfig()
    {
        $this->hasOne('channel', 'id', 'Arbel\Model\Channel','channel_id');
        $this->hasMany('interlocutors', Interlocutor::TABLE_NAME, 'notification_id',
            Interlocutor::class, 'id');
    }
    
    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    /**
     * Init general notification
     * @return type
     */
    public function initGeneralNotification(){
        return $this->getModel(Notification::class)
            ->setChannel($this->initChannel())
            ->save(true);
    }

    /**
     * Send sms to user
     * @param User $user
     * @param string $content
     * @return type
     */
    public function sendSmsToUser(User $user,string $content,$fromName = null){
        $notification = $this->initGeneralNotification();
        $this->getModel(Interlocutor::class)
            ->setNotification($notification)
            ->setUserId($user->getId())
            ->setPlatformCode(Platform::SMS)
            ->setSource($user->getFinalPhone())
            ->save();

         $systemInterlocutor = $this->getModel(SmsSystem::class)
             ->setSource($fromName)
            ->setNotification($notification)
            ->save();

        $this->log()->info('sms/to_user',['user_id' => $user->getId(),'content' => $content]);

        $result = $systemInterlocutor->send(
            $content);
        return $result[0] ?? false;
    }

    /**
     * Send sms to user
     * @param User $user
     * @param string $content
     * @return type
     */
    public function sendSmsToNumber(string $phoneNumber,string $content,$fromName = null){
        $notification = $this->initGeneralNotification();
        $this->getModel(Interlocutor::class)
            ->setNotification($notification)
            ->setPlatformCode(Platform::SMS)
            ->setSource($phoneNumber)
            ->save();

         $systemInterlocutor = $this->getModel(SmsSystem::class)
            ->setNotification($notification)
            ->setSource($fromName)
            ->save();


        $result = $systemInterlocutor->send(
            $content);
        return $result[0] ?? false;
    }
    
    
    /**
     * Send sms to user
     * @param User $user
     * @param string $content
     * @return type
     */
    public function sendMessageToMobile(string $deviceToken,string $content,string $subject = null,$data = []){
   
        $notification = $this->initGeneralNotification();
        $this->getModel(Interlocutor::class)
            ->setNotification($notification)
            ->setPlatformCode(Platform::MOBILE_NOTIFICATION)
            ->setSource($deviceToken)
            ->save();

         $systemInterlocutor = $this->getModel(MobileNotificationSystem::class)
            ->setNotification($notification)
            ->setSource('default_system')
            ->save();


        $result = $systemInterlocutor->send(
            $content,$subject,$data);
        return $result[0] ?? false;
    }

    /**
     * Send email to user
     * @param User $user
     * @param string $header
     * @param string $content
     * @return type
     */
    public function sendEmailToUser(User $user,string $header,string $content){
        $notification = $this->initGeneralNotification();
        $this->getModel(Interlocutor::class)
            ->setNotification($notification)
            ->setUserId($user->getId())
            ->setPlatformCode(Platform::EMAIL)
            ->setSource($user->getEmail())
            ->save();

         $systemInterlocutor = $this->getModel(EmailSystem::class)
            ->setNotification($notification)
            ->save();

        $this->log()->info('email/to_user',['user_id' => $user->getId(),'header' => $header,'content' => $content]);

        $result = $systemInterlocutor->send(
            $content,$header);
        return $result[0] ?? false;
    }
    /**
     * Send email to user
     * @param User $user
     * @param string $header
     * @param string $content
     * @return type
     */
    public function sendEmailToAddress(string $email,string $header,string $content){
        $notification = $this->initGeneralNotification();
        $this->getModel(Interlocutor::class)
            ->setNotification($notification)
            ->setPlatformCode(Platform::EMAIL)
            ->setSource($email)
            ->save();

         $systemInterlocutor = $this->getModel(EmailSystem::class)
            ->setNotification($notification)
            ->save();

        $this->log()->info('email/to_address',['email' => $email,'header' => $header,'content' => $content]);

        $result = $systemInterlocutor->send(
            $content,$header);
        return $result[0] ?? false;
    }

    /**
     * Init random channel
     * @return \Arbel\Model\Communication\Channel
     */
    public function initChannel(): Channel
    {
        $datetime    = new \DateTime();
        $endDate     = $datetime->modify('+2 days')->format('Y-m-d');
        $channel     = $this->getModel(Channel::class);
        $channel->setCode($this->getRandomChannelCode())
            ->setName($this->getRandomChannelCode())
            ->setFinishAt($endDate)
            ->save(true);
        return $channel;
    }

    public function getRandomChannelCode()
    {
        return $this::CHANNEL_CODE.time().rand(999999,999999);
    }

}