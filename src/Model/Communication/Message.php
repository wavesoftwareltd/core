<?php

namespace Arbel\Model\Communication;

use Arbel\Base\ModelAbstract;
use Arbel\Sms\SmsInterface;
use Arbel\Calls\CallInterface;
use Arbel\Model\Communication\Email;
use Arbel\MobileNotification\MobileNotificationInterface;

/**
 * Message model
 */
class Message extends ModelAbstract
{
    //table name
    const TABLE_NAME     = 'core_communication_message';
    //primary key
    const PRIMARY_KEY    = 'id';
    //Sent status
    const SENT_STATUS    = 'sent';
    //Sent status
    const PENDING_STATUS = 'pending';
    //Sent status
    const ERROR_STATUS   = 'error';

    /**
     * SmsInterface
     * @var SmsInterface
     */
    protected $sms;

    /**
     * Email
     * @var Email
     */
    protected $email;
    
    /**
     * Mobile Notification
     * @var Email
     */
    protected $mobileNotification;

    public function initConfig()
    {
        $this->hasOne('reciver', 'id', 'Arbel\Model\Communication\Interlocutor',
            'reciver_interlocutor_id');
        $this->hasOne('sender', 'id', 'Arbel\Model\Communication\Interlocutor',
            'sender_interlocutor_id');
    }

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    /**
     * Inject objects
     * @param SmsInterface $sms
     * @param Email $email
     */
    public function setInjections(SmsInterface $sms, Email $email,CallInterface $call,MobileNotificationInterface $mobileNotification)
    {
        $this->sms   = $sms;
        $this->call   = $call;
        $this->email = $email;
        $this->mobileNotification = $mobileNotification;
    }

    public function send()
    {
        $this->save();
        switch ($this->getPlatformCode()) {
            case Platform::SMS:
                $status = $this->sendSms();
                break;

            case Platform::EMAIL:
                $status = $this->sendEmail();

                break;

            case Platform::CALL:
                $status = $this->sendCall();

                break;
            
            case Platform::MOBILE_NOTIFICATION:
                $status = $this->sendMobileNotification();

                break;

            default:
                break;
        }
        $this->setStatus(($status ? $this::SENT_STATUS : $this::ERROR_STATUS));
        return $status;
    }

    /**
     * Send sms message
     */
    public function sendSms()
    {
        $result = $this->sms->reset()->addTo($this->getReciver()->getSource())
            ->setFrom($this->getSender()->getSource())
            ->setMessage($this->getContent())
            ->send();
        if ($result) {
            $this->setStatus($this::PENDING_STATUS)
                ->setInfo($this->sms->getInfo())
                ->setTrackingKey($this->sms->getTrackingKey());
        } else {
            $this->setStatus($this::ERROR_STATUS)
                ->setInfo($this->sms->getInfo())
                ->setTrackingKey($this->sms->getTrackingKey());
        }
        $this->save();
        return $result;
    }
    
    
    /**
     * Send sms message
     */
    public function sendMobileNotification()
    {
        $result = $this->mobileNotification->reset()->addTo($this->getReciver()->getSource())
            ->setFrom($this->getSender()->getSource())
            ->setSubject($this->getSubject())
            ->setContent($this->getContent())
            ->setMessageData($this->get('message_data',[]))
            ->send();
        if ($result) {
            $this->setStatus($this::PENDING_STATUS)
                ->setInfo($this->mobileNotification->getInfo())
                ->setTrackingKey($this->mobileNotification->getTrackingKey());
        } else {
            $this->setStatus($this::ERROR_STATUS)
                ->setInfo($this->mobileNotification->getInfo())
                ->setTrackingKey($this->mobileNotification->getTrackingKey());
        }
        $this->save();
        return $result;
    }

    /**
     * Send sms message
     */
    public function sendCall()
    {
        $result = $this->call->reset()->addTo($this->getReciver()->getSource())
            ->setFrom($this->getSender()->getSource())
            ->setMessage($this->getContent())
            ->setParams(['message_id' => $this->getId()])
            ->send();
        if ($result) {
            $this->setStatus($this::SENT_STATUS)
                ->setInfo($this->call->getInfo())
                ->setTrackingKey($this->call->getTrackingKey());
        } else {
            $this->setStatus($this::ERROR_STATUS)
                ->setInfo($this->call->getInfo())
                ->setTrackingKey($this->call->getTrackingKey());
        }
        $this->save();
        return $result;
    }

    public function sendEmail()
    {
        $result = $this->email
            ->setSubject($this->getSubject())
            ->setContent($this->getContent())
            ->setSender($this->getReciver()->getUser() && mb_detect_encoding($this->getReciver()->getUser()->getFirstName(),
                    'ASCII', true) ? $this->getReciver()->getUser()->getFirstName()
                    : null)
            ->setTo($this->getReciver()->getSource())
            ->setFrom($this->getSender()->getSource())
            ->send();
        if ($result) {
            $this->setStatus($this::SENT_STATUS)
                ->setInfo($this->sms->getInfo());
        } else {
            $this->setStatus($this::ERROR_STATUS)
                ->setInfo($this->sms->getInfo());
        }
        $this->save();
        return $result;
    }
}