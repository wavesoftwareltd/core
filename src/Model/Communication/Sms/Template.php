<?php

namespace Arbel\Model\Communication\Sms;

use Arbel\Base\ModelAbstract;

/**
 * Email Template model
 */
class Template extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_communication_sms_template';
    //primary key
    const PRIMARY_KEY  = 'id';

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    /**
     * Generate content
     * @param array $params
     * @return type
     */
    public function generateContent(array $params = null)
    {
        if(!isset($params)){
            $params = $this->getParams();
        }
        $result = $this->getContent();
        foreach ($params as $key => $val) {
            $result = str_replace('['.$key.']', $val, $result);
        }
        return $result;
    }

}