<?php

namespace Arbel\Model\Communication;

use Arbel\Base\ModelAbstract;

/**
 * Notification model
 */
class Platform extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_communication_platform';
    //primary key
    const PRIMARY_KEY  = 'id';
    //phone sms flag
    const SMS = 'phone_sms';
    //phone sms flag
    const MOBILE_NOTIFICATION = 'mobile_notification';
    //phone sms flag
    const CALL = 'call';
    //email flag
    const EMAIL = 'email';
  
    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function save($isInsertOrUpdate = false, $isInsertIfNoExist = false)
    {
        if(!$this->getCode()){
            $this->generateCode();
        }
        parent::save($isInsertOrUpdate, $isInsertIfNoExist);
    }
}