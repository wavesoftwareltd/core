<?php

namespace Arbel\Model\Communication;

use Arbel\Base\ModelAbstract;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;

/**
 * Email model
 */
class Email extends ModelAbstract
{
    //table name
    const TABLE_NAME  = 'core_communication_email';
    //primary key
    const PRIMARY_KEY = 'id';

    /**
     * SmtpOptions
     * @var SmtpOptions
     */
    protected $smtpOptions;

    /**
     * Message
     * @var Message
     */
    protected $message;

    /**
     * If the email was initialize
     * @var bool
     */
    protected $isInitilaize = false;

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    public function initConfig()
    {
        $this->hasOne('email_template', 'code', 'Arbel\Model\Email\Template',
            'template');
    }

    public function init()
    {
        $this->initMessage();
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function initMessage()
    {
        if ($this->getEmailTemplate()) {
            $this->getMessage()
                ->setSubject($this->getEmailTemplate()->getSubject())
                ->setBody($this->getEmailTemplate()->getContent());
        }else{
            $this->getMessage()
                ->setSubject($this->getSubject())
                ->setBody($this->getContent());
        }

        $this->initBcc()
            ->initCc()
            ->initFrom()
            ->initReplyTo()
            ->initTo();

        return $this;
    }

    public function setParameters(array $array)
    {
        foreach ($array as $key => $val) {
            $this->setParameter($key, $val);
        }
        return $this;
    }

    public function setParameter(string $key, string $val)
    {
        $subject = $this->getMessage()->getSubject();
        $content = $this->getMessage()->getBodyText();
        $this->getMessage()->setSubject(str_replace('|'.$key.'|', $val, $subject));
        $this->getMessage()->setBody(str_replace('|'.$key.'|', $val,
                $content));

        $this->setArrayData('parameters', $key, $val);
        return $this;
    }

    public function initBcc()
    {
        if ($this->hasBcc()) {
            $bccList = $this->getBcc();
            $bccArray = explode(',', $bccList);
            $this->getMessage()->setBcc($bccArray);
        }
        return $this;
    }

    public function initFrom()
    {
        if ($this->hasFrom()) {
            $this->getMessage()->setFrom($this->getFrom(),
                $this->getSender($this->getFrom()));
        }
        return $this;
    }

    public function initReplyTo()
    {
        if ($this->hasReplyTo()) {
            $toList = $this->getReplyTo();
            $toArray = explode(',', $toList);
            $this->getMessage()->setReplyTo($toArray);
        }
        return $this;
    }

    public function initTo()
    {
        if ($this->hasTo()) {
            $toList = $this->getTo();
            $toArray = explode(',', $toList);
            $this->getMessage()->setTo($toArray);
        }
        return $this;
    }

    public function initCc()
    {
        if ($this->hasCc()) {
            $ccList = $this->getCc();
            $ccArray = explode(',', $ccList);
            $this->getMessage()->setCc($ccArray);
        }
        return $this;
    }

    public function &getMessage()
    {
        if (!$this->message) {
            $this->message = new Message();
        }
        return $this->message;
    }

    public function sendEmail()
    {

        $message = new Message();
        $message->setTo('yarivluts@gmail.com');
        $message->setFrom('service@w.wave-software.net');
        $message->setSender('service@w.wave-software.net');
        $message->setReplyTo('service@w.wave-software.net');
        $message->setSubject('Greetings and Salutations!');
        $message->setBody("Sorry, I'm going to be late today!");
        print_r('<pre>'.$message->toString().'</pre>');
// Setup SMTP transport using LOGIN authentication
    }

    /**
     * Send the email
     */
    public function send()
    {
        if(!$this->isInitilaize){
            $this->init();
        }
        if (!$this->getIsNoHtml()) {
            $this->convertToHtml();
        }
        $transport = new SmtpTransport();
        $transport->setOptions($this->smtpOptions);
        $transport->send($this->getMessage());
        return true;
    }

    public function convertToHtml()
    {
        $content        = $this->getMessage()->getBodyText();
        $htmlPart       = new MimePart($content);
        $htmlPart->type = "text/html";

        $body = new MimeMessage();
        $body->setParts(array($htmlPart));
 
        $this->getMessage()->setBody($body);
    }

    /**
     * Inject objects
     * @param SmtpOptions $smtpOptions
     */
    public function setInjections(SmtpOptions $smtpOptions)
    {
        $this->smtpOptions = $smtpOptions;
    }
}