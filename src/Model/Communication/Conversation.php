<?php

namespace Arbel\Model\Communication;

use Arbel\Base\ModelAbstract;
use Arbel\Model\Communication\Interlocutor;

/**
 * Conversation model
 */
class Conversation extends ModelAbstract
{
    //table name
    const TABLE_NAME  = 'core_communication_conversation';
    //primary key
    const PRIMARY_KEY = 'id';

    public function initConfig()
    {
        $this->hasOne('channel', 'id', 'Arbel\Model\Channel', 'channel_id');
        $this->hasMany('interlocutors', Interlocutor::TABLE_NAME,
            'conversation_id', Interlocutor::class, 'id');
    }

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    /**
     * Get open conversation with source
     * @param array $params
     * @param string $platform
     * @return type
     */
    public function getOpenBy(array $params, string $platform = Platform::SMS)
    {
        $where = array();
        foreach ($params as $source) {
            $where[] = ['source' => $source,
                'platform_code' => $platform];
        }
        return $this->getModel(Conversation::class)->with('interlocutors',
                    $where)
                ->addWhere(array(
                    '('.$this::TABLE_NAME.'.finish_at is null or '.$this::TABLE_NAME.'.finish_at > now())'
                ))->last();
    }
}