<?php

namespace Arbel\Model\Communication;

use Arbel\Base\ModelAbstract;
use Arbel\Model\Communication\Message;
use Arbel\Model\Communication\Platform;
use Arbel\Helper;

/**
 * Interlocutor model
 */
class Interlocutor extends ModelAbstract
{
    //table name
    const TABLE_NAME  = 'core_communication_interlocutor';
    //primary key
    const PRIMARY_KEY = 'id';

    public function initConfig()
    {
        $this->hasOne('conversation', 'id',
            'Arbel\Model\Communication\Conversation', 'conversation_id');
        $this->hasOne('notification', 'id',
            'Arbel\Model\Communication\Notification', 'notification_id');
        $this->hasOne('user', 'id', 'Arbel\Model\User', 'user_id');
    }

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function send(string $content, string $subject = null,array $data = [])
    {
        $result = array();
        $others = $this->getOthers();
        $lastMessages = [];
        foreach ($others as $interlocutor) {
            $message = $this->getModel(Message::class);
            $result[] = $message->setGroupCode($this->getGroupHash())
                ->setSender($this)
                ->setReciver($interlocutor)
                ->setSubject($interlocutor->replaceFlags($subject))
                ->setContent($interlocutor->replaceFlags($content))
                ->setMessageData($data)
                ->setPlatformCode($interlocutor->getPlatformCode())
                ->send();
            $lastMessages[] = $message;
        }
        $this->setLastSendedMessages($lastMessages);
        return $result;
    }

    public function receive(Interlocutor $from, string $content,
                            string $subject = null,$trackingKey = null)
    {
        $message = $this->getModel(Message::class);
        $message->setGroupCode($this->getGroupHash())
            ->setSender($from)
            ->setReciver($this)
            ->setSubject($subject)
            ->setContent($content)
            ->setPlatformCode($this->getPlatformCode())
            ->setTrackingKey($trackingKey)
            ->save();
        $this->trrigerReceive($from, $content,$subject);
    }

    public function factory(array $data = array())
    {
        if(empty($data)){
            $data = $this->getData();
        }
        if (isset($data['class_name'])) {
            $className = $data['class_name'];
            unset($data['class_name']);
            return $this->getModel($className)->setData($data);
        } else {
            return parent::factory($data);
        }
    }

    /**
     *
     * @param \Arbel\Model\Communication\Interlocutor $from
     * @param string $content
     * @param string $subject
     */
    public function trrigerReceive(Interlocutor $from, string $content,
                                   string $subject = null)
    {

    }

    /**
     * Get group hash - generate if not exist
     * @return string
     */
    public function getGroupHash()
    {
        if (!$this->have('group_hash')) {
            $this->set('group_hash', Helper::generateRandomHash());
        }
        return $this->get('group_hash');
    }

    public function replaceFlags($text)
    {
        if (isset($text) && $this->getUser() && !$this->getIsSystem()) {
            $text = str_ireplace('|first_name|',
                $this->getUser()->getFirstName(), $text);
            $text = str_ireplace('|last_name|', $this->getUser()->getLastName(),
                $text);
            $text = str_ireplace('|full_name|',
                $this->getUser()->getFirstName().' '.$this->getUser()->getLastName(),
                $text);
        }
        return $text;
    }

    public function getOthers()
    {
        if ($this->getConversation()) {
            $object = $this->getConversation();
            $key    = 'conversation_id';
        } else {
            $object = $this->getNotification();
            $key    = 'notification_id';
        }
        $others = $this->getAll(array(
            $key => $object->getId(),
            $this->getPrimaryKey() => array('!=', $this->getId())
        ));
        return $others;
    }

    public function getTargetList()
    {
        $result = array();
        $others = $this->getOthers();
        foreach ($others as $interlocutor) {
            $result[$interlocutor->getPlatformCode()][] = $interlocutor->getSource();
        }
        return $result;
    }

}