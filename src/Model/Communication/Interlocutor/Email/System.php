<?php

namespace Arbel\Model\Communication\Interlocutor\Email;

use Arbel\Model\Communication\Interlocutor;
use Arbel\Model\Communication\Platform;
use Arbel\Model\Log;

/**
 * System Interlocutor model
 */
class System extends Interlocutor\System
{

    public function initConfig()
    {
        parent::initConfig();
        $this->setPlatformCode(Platform::EMAIL);
    }

    /**
     * Init source
     * @throws \Exception
     */
    public function initSource()
    {
        $options = $this->getOptionsForSource();
        $others  = $this->getOthers();
        $targetSource = 'empty';
        foreach ($others as $other) {
            $targetSource = $other->getSource();
            foreach ($options as $option) {
                if($this->getConversation()){
                $conversation = $this->getConversation()->getOpenBy(array($option,$other->getSource()));
                }else{
                $conversation = null;
                }
                if(!$conversation){
                    return $this->setSource($option);
                }
            }
        }
        $this->getModel(Log::class)->info('interlocutor/system/error/'.$conversation->getId(),"Can't find available source for ".$targetSource);
        throw new \Exception("Can't find available source for ".$targetSource);
    }

    public function getOptionsForSource()
    {
        return [$this->getServiceManager()->get('Arbel\Config')->get('email.from')];
    }
}