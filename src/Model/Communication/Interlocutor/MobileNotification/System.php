<?php

namespace Arbel\Model\Communication\Interlocutor\MobileNotification;

use Arbel\Model\Communication\Interlocutor;
use Arbel\Model\Communication\Platform;
use Arbel\Model\Log;

/**
 * System Interlocutor model
 */
class System extends Interlocutor\System
{

    const DEFAULT_NUMBERS_CONFIG_NAME = 'api.fcm.token';
    const DEFAULT_NAME = 'fcm_default';

    public function initConfig()
    {
        parent::initConfig();
        $this->setPlatformCode(Platform::MOBILE_NOTIFICATION);
    }

    /**
     * Init source
     * @throws \Exception
     */
    public function initSource()
    {
        return $this->setSource($this->getNotificationName());
    }

    public function getNotificationName()
    {
        return $this->getServiceManager()->get('Arbel\Config')->get('notification.fcm.from','fcm_default');
    }
}