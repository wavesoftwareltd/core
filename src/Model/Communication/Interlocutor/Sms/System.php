<?php

namespace Arbel\Model\Communication\Interlocutor\Sms;

use Arbel\Model\Communication\Interlocutor;
use Arbel\Model\Communication\Platform;
use Arbel\Model\Log;

/**
 * System Interlocutor model
 */
class System extends Interlocutor\System
{

    const DEFAULT_NUMBERS_CONFIG_NAME = 'api.plivo.numbers';

    public function initConfig()
    {
        parent::initConfig();
        $this->setPlatformCode(Platform::SMS);
    }

    /**
     * Init source
     * @throws \Exception
     */
    public function initSource()
    {
        $options      = $this->getOptionsForSource();
        $others       = $this->getOthers();
        $targetSource = 'empty';
        foreach ($others as $other) {
            $targetSource = $other->getSource();
            foreach ($options as $option) {
                if ($this->getConversation()) {
                    $conversation = $this->getConversation()->getOpenBy(array($option, $other->getSource()));
                    if(!$conversation){
                          return $this->setSource($option);
                    }
                } else {
                    if ($this->getNotificationName()) {
                        return $this->setSource($this->getNotificationName());
                    }else{
                        return $this->setSource($option);
                    }
                }
            }
        }
        $this->getModel(Log::class)->info('interlocutor/system/error/'.($conversation ? $conversation->getId() : 'unknown'),
            "Can't find available source for ".$targetSource);
        if (!$this->getSource()) {
            $this->setSource('NotFound');
        }
// throw new \Exception("Can't find available source for ".$targetSource);
    }

    public function getOptionsForSource()
    {
        return $this->getServiceManager()->get('Arbel\Config')->get($this->getOptionNumbersConfigName());
    }

    public function getOptionNumbersConfigName(){
        $configName = $this->getServiceManager()->get('Arbel\Config')->get('conversation.sms.number_config');
       return $configName ? $configName : $this::DEFAULT_NUMBERS_CONFIG_NAME;
    }

    public function getNotificationName()
    {
        return $this->getServiceManager()->get('Arbel\Config')->get('notification.sms.from');
    }
}