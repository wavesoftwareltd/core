<?php

namespace Arbel\Model\Communication\Interlocutor;

use Arbel\Model\Communication\Interlocutor;
use Arbel\Model\Communication\Platform;

/**
 * System Interlocutor model
 */
abstract class System extends Interlocutor
{

    public function initConfig()
    {
        parent::initConfig();
        $className = get_class($this);
        $this->setIsSystem(true);
        $this->setClassName($className);
        return $this;
    }

    public function save($isInsertOrUpdate = false, $isInsertIfNoExist = false)
    {
        if (!$this->getSource()) {
            $this->initSource();
        }
       return parent::save($isInsertOrUpdate, $isInsertIfNoExist);
    }

    /**
     * Init the source value
     */
    abstract public function initSource();

}