<?php

namespace Arbel\Model\Communication\Interlocutor;

use Arbel\Model\Communication\Interlocutor\Sms\System;

/**
 * Test Interlocutor model
 */
class Test extends System
{


    public function trrigerReceive(Interlocutor $from, string $content,
                                   string $subject = null)
    {
        if (is_numeric($content)) {
            $this->send("$content + 3 = ".($content + 3));
        } else {
            $this->send(' מעולה מה שלומך?');
        }
    }

}