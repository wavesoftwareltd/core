<?php

namespace Arbel\Model\Acl;

use Arbel\Base\ModelAbstract;

/**
 * Acl Resource model
 */
class Resource extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_acl_resource';
    //primary key
    const PRIMARY_KEY  = 'id';

    public function initConfig()
    {
        $this->hasOne('parent', 'code', Resource::class, 'parent_code');
        $this->hasMany('roles', Role::PIVOT,
            'id', Role::class, 'role_id');
    }
    
    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function save($isInsertOrUpdate = false, $isInsertIfNoExist = false)
    {
        if(!$this->getCode()){
            $this->generateCode();
        }elseif(!$this->getName()){
            $this->generateName('code');
        }
        parent::save($isInsertOrUpdate, $isInsertIfNoExist);
    }

    public function __toString()
    {
        return $this->getCode('');
    }

}