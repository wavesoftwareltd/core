<?php

namespace Arbel\Model\Acl;

use Arbel\Base\ModelAbstract;

/**
 * Channel model
 */
class Role extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_acl_role';
    //primary key
    const PRIMARY_KEY  = 'id';
    //pivot table
    const PIVOT  = 'core_acl_role_resource_action';
 
    public function initConfig()
    {
        $this->hasMany('resources', $this::PIVOT,
            'id', Resource::class, 'resource_id');
        $this->hasMany('parents', 'core_acl_role_parents',
            array('parent_code' => 'code'), Role::class, array('role_code'=> 'code'));
        $this->hasMany('permissions', Permission::TABLE_NAME,
            'role_code', Permission::class, 'code');
        $this->hasMany('resources', Permission::TABLE_NAME,
            array('resource_code' => 'code'),Resource::class, array('role_code' => 'code'));
    }

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function addParent($parent)
    {
        $this->addRelation('parents', $parent);
    }

    public function removeParent($parent)
    {
        $this->removeRelation('parents', $parent);
    }
    
    public function save($isInsertOrUpdate = false, $isInsertIfNoExist = false)
    {
        if(!$this->getCode()){
            $this->generateCode();
        }elseif(!$this->getName()){
            $this->generateName('code');
        }
        parent::save($isInsertOrUpdate, $isInsertIfNoExist);
    }

    public function __toString()
    {
        return $this->getCode('');
    }

}