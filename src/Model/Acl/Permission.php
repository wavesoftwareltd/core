<?php

namespace Arbel\Model\Acl;

use Arbel\Base\ModelAbstract;

/**
 * Acl Resource model
 */
class Permission extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_acl_permission';
    //primary key
    const PRIMARY_KEY  = 'id';
    /**
     * Unique table column
     * @var type
     */
    protected $uniqueTableColumn = array(
        'role_code',
        'resource_code',
        'privilege_code',
    );

    public function initConfig()
    {
        $this->hasOne('role', 'code', Role::class, 'role_code');
        $this->hasOne('resource', 'code', Resource::class, 'resource_code');
    }

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function save($isInsertOrUpdate = false, $isInsertIfNoExist = false)
    {
        parent::save($isInsertOrUpdate, $isInsertIfNoExist);
    }

}