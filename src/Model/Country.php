<?php

namespace Arbel\Model;

use Arbel\Base\ModelAbstract;

/**
 * Country model
 */
class Country extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_country';
    //primary key
    const PRIMARY_KEY  = 'id';

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

}