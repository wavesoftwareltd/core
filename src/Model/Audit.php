<?php

namespace Arbel\Model;

use Arbel\Base\ModelAbstract;

/**
 * Channel model
 */
class Audit extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_audit';
    //primary key
    const PRIMARY_KEY  = 'id';

    /**
     *
     * @var array
     */
    protected $jsonToObjectFields = ['info'];

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function addEvent($code,$value,$info = [],string $scope = 'default',
        string $previews = null ,string $identifier = null)
    {
        $newObject = $this->getModel(get_class($this));
        if(is_array($value) && empty($info)){
           $info = $value;
           $value = true;
        }else if(is_array($value)){
            $value = json_encode($value);
        }

        $newObject->setCode($code)
            ->setValue($value)
            ->setScope($scope)
            ->setInfo($info)
            ->setUserId($this->getServiceManager()->get('User') ? $this->getServiceManager()->get('User')->getId() : -1)
            ->setIdentifier($identifier)
            ->setPreviews($previews)
            ->save();
        return $newObject;
    }

    public function addNotify(){
      $emails = $this->getConfig()->getAuditNotifyEmails();
      if(!empty($emails)){
          $emailsArray = explode(',',$emails);
      }
    }


}