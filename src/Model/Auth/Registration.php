<?php

namespace Arbel\Model\Auth;

use Arbel\Base\ModelAbstract;
use Arbel\Helper;
use Arbel\Model\Communication\Message;
use Arbel\Model\User;
use Arbel\Model\Communication\Interlocutor\Email\System as EmailSystem;
use Arbel\Model\Communication\Interlocutor\Sms\System as SmsSystem;
use Arbel\Model\Channel;
use Arbel\Model\Communication\Platform;
use Arbel\Model\Communication\Notification;
use Arbel\Model\Communication\Interlocutor;
use Arbel\Model\Communication\Email\Template;

/**
 * Cache model
 */
class Registration extends ModelAbstract
{
    //table name
    const TABLE_NAME  = 'core_registration';
    //primary key
    const PRIMARY_KEY = 'id';

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    public function initConfig()
    {
        $this->hasOne('user', 'id', User::class, 'user_id');
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function save($isInsertOrUpdate = false, $isInsertIfNoExist = false)
    {
        if (!$this->getPhoneCode()) {
            $this->initPhoneCode();
        }

        if (!$this->getEmailToken()) {
            $this->initEmailToken();
        }

        if (!$this->getExpirationTime()) {
            $this->initExpirationTime();
        }
        return parent::save($isInsertOrUpdate, $isInsertIfNoExist);
    }

    public function sendEmailVerification()
    {
        $notification = $this->initNotification();
        $interlocutor = $this->getModel(Interlocutor::class)
            ->setNotification($notification)
            ->setUserId($this->getUser()->getId())
            ->setPlatformCode(Platform::EMAIL)
            ->setSource($this->getUser()->getEmail())
            ->save();

        $systemInterlocutor = $this->getModel(EmailSystem::class)
            ->setNotification($notification)
            ->save();

        $result = $systemInterlocutor->send(
            $this->getEmailContent(), $this->getEmailHeader());
    }

    public function sendPhoneVerification()
    {
        $notification = $this->initNotification();
        $interlocutor = $this->getModel(Interlocutor::class)
            ->setNotification($notification)
            ->setUserId($this->getUser()->getId())
            ->setPlatformCode(Platform::SMS)
            ->setSource($this->getUser()->getFinalPhone())
            ->save();

        $systemInterlocutor = $this->getModel(SmsSystem::class)
            ->setNotification($notification)
            ->save();

        $result = $systemInterlocutor->send(
            $this->getSmsContent());
        return $result[0] ?? false;
    }

    public function initChannel(): Channel
    {
        $datetime    = new \DateTime();
        $endDate     = $datetime->modify('+2 day')->format('Y-m-d');
        $channelCode = 'registration/'.$this->getId();
        $channel = $this->getModel(Channel::class)->getOne(['code' => $channelCode]);
        if(!$channel){
            $channel     = $this->getModel(Channel::class)
                    ->setCode($channelCode);
        }
        $channel
            ->setName($channelCode)
            ->setFinishAt($endDate)
            ->save(true);
        return $channel;
    }

    public function initNotification()
    {
        $notification = $this->getModel(Notification::class)
            ->setChannel($this->initChannel())
            ->save(true);
        $this->setNotification($notification);
        return $notification;
    }

    public function initEmailMessage()
    {
        $this->getModel(Message::class)
            ->setData([
                'subject' => $this->getEmailHeader(),
                'content' => $this->generateEmailContent([
                    'name' => $this->getUser()->getName(),
                    'verification_link' => $this->getVerificationUrl()
                ])
            ])
            ->save();
    }

    public function getSmsContent()
    {
        $content = $this->getSmsTemplate();
        return str_replace('[code]', $this->getPhoneCode(), $content);
    }

    public function getSmsTemplate()
    {
        return 'your verification code is [code]';
    }

    public function getVerificationUrl()
    {
        return Helper::getCurrentBaseHost().'/#/email_verification/'.$this->getEmailToken();
    }

    public function getEmailContentTemplate()
    {
        $emailTemplate = $this->getModel(Template::class)->loadByKey('code',
            'registration');
        if (!$emailTemplate->isExist()) {
            return
                $this->__('Welcome').' [name]'.
                '<br>'.
                $this->__('You can confirm your account through the link below').':'.
                '<br>'.
                '<a href="[verification_link]">'.$this->__('Confirm my account').'</a>'
            ;
        } else {
            return $emailTemplate->getContent();
        }
    }

    public function getEmailContent()
    {
        return $this->generateEmailContent([
                'name' => $this->getUser()->getName(),
                'verification_link' => $this->getVerificationUrl()
        ]);
    }

    public function generateEmailContent(array $params)
    {
        $result = $this->getEmailContentTemplate();
        foreach ($params as $key => $val) {
            $result = str_replace('['.$key.']', $val, $result);
        }
        return $result;
    }

    public function getEmailHeader()
    {
        return $this->__('Verification instructions');
    }

    public function initPhoneCode()
    {
        return $this->setPhoneCode(rand(1000, 9999));
    }

    public function initEmailToken()
    {
        return $this->setEmailToken(Helper::generateRandomHash());
    }

    public function initExpirationTime()
    {
        $startDate = time();
        $date      = date('Y-m-d H:i:s', strtotime('+1 day', $startDate));
        $this->setExpirationTime($date);
    }

    /**
     * Check if registration is expired
     */
    public function isExpired(): bool
    {
        $a = new \DateTime();
        $b = $this->datetime('expiration_time');

        $diff = $a->diff($b);
        return ($diff->days >= 1);
    }
}