<?php

namespace Arbel\Model\Auth;

use Arbel\Base\ModelAbstract;
use Arbel\Helper;
use Arbel\Model\Communication\Message;
use Arbel\Model\User;
use Arbel\Model\Communication\Interlocutor\Email\System as EmailSystem;
use Arbel\Model\Communication\Interlocutor\Sms\System as SmsSystem;
use Arbel\Model\Channel;
use Arbel\Model\Communication\Platform;
use Arbel\Model\Communication\Notification;
use Arbel\Model\Communication\Interlocutor;
use Arbel\Model\Communication\Email\Template;

/**
 * Cache model
 */
class ChangePassword extends ModelAbstract
{
    //table name
    const TABLE_NAME  = 'core_change_password';
    //primary key
    const PRIMARY_KEY = 'id';

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    public function initConfig()
    {
        $this->hasOne('user', 'id', User::class, 'user_id');
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function save($isInsertOrUpdate = false, $isInsertIfNoExist = false)
    {
        if (!$this->getEmailToken()) {
            $this->initEmailToken();
        }

        if (!$this->getExpirationTime()) {
            $this->initExpirationTime();
        }
        return parent::save($isInsertOrUpdate, $isInsertIfNoExist);
    }

    public function sendEmail()
    {
        $notification = $this->initNotification();
        $interlocutor = $this->getModel(Interlocutor::class)
            ->setNotification($notification)
            ->setUserId($this->getUser()->getId())
            ->setPlatformCode(Platform::EMAIL)
            ->setSource($this->getUser()->getEmail())
            ->save();

        $systemInterlocutor = $this->getModel(EmailSystem::class)
            ->setNotification($notification)
            ->save();

        $result = $systemInterlocutor->send(
            $this->getEmailContent(), $this->getEmailHeader());
    }


    public function initChannel(): Channel
    {
        $datetime    = new \DateTime();
        $endDate     = $datetime->modify('+2 day')->format('Y-m-d');
        $channelCode = 'change_password/'.$this->getId();
        $channel     = $this->getModel(Channel::class);
        $channel->setCode($channelCode)
            ->setName($channelCode)
            ->setFinishAt($endDate)
            ->save(true);
        return $channel;
    }

    public function initNotification()
    {
        $notification = $this->getModel(Notification::class)
            ->setChannel($this->initChannel())
            ->save(true);
        $this->setNotification($notification);
        return $notification;
    }

    public function initEmailMessage()
    {
        $this->getModel(Message::class)
            ->setData([
                'subject' => $this->getEmailHeader(),
                'content' => $this->generateEmailContent([
                    'name' => $this->getUser()->getName(),
                    'reset_link' => $this->getVerificationUrl()
                ])
            ])
            ->save();
    }

    public function getVerificationUrl()
    {
        return Helper::getCurrentBaseHost().'/#/change_password/'.$this->getEmailToken();
    }

    public function getEmailContentTemplate()
    {
        $emailTemplate = $this->getModel(Template::class)->loadByKey('code', 'change_password');
        if(!$emailTemplate->isExist()){
        return
            $this->__('Welcome').' [name]'.
            '<br>'.
            $this->__('Please ').':'.
            '<br>'.
            '<a href="[reset_link]">'.$this->__('Change my password').'</a>'
        ;
        }else{
            return $emailTemplate->getContent();
        }
    }

    public function getEmailContent()
    {
        return $this->generateEmailContent([
                'name' => $this->getUser()->getName(),
                'support_email' => $this->getUser()->getEmail(),
                'reset_link' => $this->getVerificationUrl()
        ]);
    }

    public function generateEmailContent(array $params)
    {
        $result = $this->getEmailContentTemplate();
        foreach ($params as $key => $val) {
            $result = str_replace('['.$key.']', $val, $result);
        }
        return $result;
    }

    public function getEmailHeader()
    {
        return $this->__('Reset password');
    }

    public function initEmailToken()
    {
        return$this->setEmailToken(Helper::generateRandomHash());
    }

    public function initExpirationTime()
    {
        $startDate = time();
        $date      = date('Y-m-d H:i:s', strtotime('+1 day', $startDate));
        $this->setExpirationTime($date);
    }

    /**
     * Check if registration is expired
     */
    public function isExpired():bool
    {
        $a = new \DateTime();
        $b = $this->datetime('expiration_time');

        $diff = $a->diff($b);
        return ($diff->days >= 1);
    }
}