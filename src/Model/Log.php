<?php

namespace Arbel\Model;

use Arbel\Base\ModelAbstract;

/**
 * Channel model
 */
class Log extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_log';
    //primary key
    const PRIMARY_KEY  = 'id';

    /**
     *
     * @var array
     */
    protected $jsonToObjectFields = ['info'];

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function info(string $scope,$params)
    {
        if(!is_array($params)){
            $params = [$params];
        }
        $this->setType('info')
            ->setScope($scope)
            ->setInfo($params)
            ->save();
        return $this;
    }


}