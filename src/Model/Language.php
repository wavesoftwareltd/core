<?php

namespace Arbel\Model;

use Arbel\Base\ModelAbstract;

/**
 * Language model
 */
class Language extends ModelAbstract
{
    //table name
    const TABLE_NAME   = 'core_language';
    //primary key
    const PRIMARY_KEY  = 'id';
    const HEBREW_CODE = 'he-il';
    //rtl language direction
    const RTL_LANGUAGES = array(
        'he'
    );

    /**
     * Get primary key
     * @return string
     */
    public static function getPrimaryKey(): string
    {
        return self::PRIMARY_KEY;
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }
    
    /**
     * Check if current language is rtl
     * @return bool
     */
    public function isRtl() :bool {
        return in_array($this->getLang(), $this::RTL_LANGUAGES);
    }

}