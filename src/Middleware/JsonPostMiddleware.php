<?php
namespace Arbel\Middleware;


use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\ServerRequest;
use Zend\Diactoros\Response;
use Zend\Psr7Bridge\Psr7Response;
use Zend\Http\Response as ZendResponse;

class JsonPostMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
      $this->initPostFromJson();
      $newRequest = $request->withParsedBody($this->fromJson());
      
        // do some work
          return $delegate->process($newRequest);
    }

//     public function after(ServerRequestInterface $request, DelegateInterface $delegate,ResponseInterface $response)
//    {
//         $res = Psr7Response::toZend($response);
//         exit('<pre>'.print_r($res->getContent(),true).'</pre>');
//      $newRequest = $request->withQueryParams(['after_res' => 22]);
//        // do some work
//          return $delegate->process($newRequest);
//    }
    
     public function render(ServerRequestInterface $request, DelegateInterface $delegate,ResponseInterface $response)
    {
         $res = Psr7Response::toZend($response);
         exit('<pre>'.print_r($response,true).'</pre>');
      $newRequest = $request->withQueryParams(['after_res' => 22]);
        // do some work
          return $delegate->process($newRequest);
    }
    
    
    public function fromJson() {
        $body = file_get_contents('php://input');
        if (!empty($body)) {
            $json = json_decode($body, true);
            if (!empty($json)) {
                return $json;
            }
        }
        return [];
    }
    
    public function initPostFromJson() {
        $params = $this->fromJson();
        if($params){
           $_POST = array_merge($_POST,$params);
        }
    }
    
    public function modifyRender($sm){
            $strategy = $sm->get('ViewJsonStrategy');
            $view     = $sm->get('ViewManager')->getView();
            $strategy->attach($view->getEventManager());
    }
}