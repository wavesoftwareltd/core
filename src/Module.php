<?php
/**
 * Arbel core app
 */

namespace Arbel;

use Zend\ServiceManager\ServiceManager;
use Arbel\ObjectManager;
use Arbel\Base\ModelAbstract;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use Zend\Session\Validator;
use Zend\Db\Adapter\Adapter;
use Zend\EventManager\EventManagerInterface;
use Arbel\Updater\ModuleUpdater;
use Illuminate\Database\Capsule\Manager as Capsule;
use Zend\Cache\Storage\StorageInterface;
use Arbel\Google\Translate;
use Arbel\Google\Storage;
use Arbel\Model\Translator;
use Arbel\Cache\CacheInterface;
use Zend\Mvc\MvcEvent;
use Arbel\Cache;
use Arbel\Cache\CacheManager;
use Zend\Mail\Transport\SmtpOptions;
use Plivo\RestClient;
use Plivo\RestAPI;
use Arbel\Sms\SmsInterface;
use Arbel\MobileNotification\MobileNotificationInterface;
use Arbel\Calls\CallInterface;
use Arbel\Sms\Plivo as PlivoSms;
use Arbel\Calls\Plivo as PlivoCall;
use Arbel\Service\Factory\PlivoFactory;
use Arbel\Listener\MiddlewareListener;
use Arbel\Listener\PermissionListener;
use Arbel\OAuth2\ServerFactoryInterface;
use Arbel\OAuth2\ServerFactory;
use Arbel\Upload\UploadInterface;
use Arbel\Helper;
use Arbel\Service\Factory\DatabaseFactory;
use Arbel\Database\DatabaseInterface;
use Arbel\MobileNotification\Fcm;

class Module
{
    const VERSION = '0.2.6';

    public function getConfig()
    {
        return include __DIR__.'/../config/module.config.php';
    }

    public function getUpdaterVersion()
    {
        return $this::VERSION;
    }

    public static function getTablePrefix()
    {
        return 'core';
    }

    public function onBootstrap($e)
    {
        $app          = $e->getTarget();
        $services     = $app->getServiceManager();
        $eventManager = $e->getApplication()->getEventManager();
        // Register the event listener method onDispatch
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [MiddlewareListener::class, 'onDispatch'],
            100);
//        $eventManager->attach(MvcEvent::EVENT_RENDER, [MiddlewareListener::class, 'onRender'],
//            100);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [PermissionListener::class, 'onDispatch'],
            99);

        // Register the event listener method onDispatch
        $eventManager->attach(MvcEvent::EVENT_FINISH, [MiddlewareListener::class, 'onFinish'], 100);

        $this->initDi($services);
        $this->runUpdater($services);
        $services->addAbstractFactory($services->get('DiStrictAbstractServiceFactory'));
        // The following line instantiates the SessionManager and automatically
        // makes the SessionManager the 'default' one.
      //  $this->bootstrapSession($e);
//        $services->get(SessionManager::class);
        
        
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $viewModel->serviceManager = $services;
        $viewModel->modelManager = $services->get('Di');
        $viewModel->lang = $services->get('User')->getLanguage()->getCode();
        $viewModel->isRtl = $services->get('User')->getLanguage()->isRtl();
        $viewModel->config = $services->get('Di')->get('Arbel\Config');
        $viewModel->translator = $services->get('Di')->get(Translator::class);
        $viewModel->helper = new Helper;
        $viewModel->isProduction = $services->get('Di')->get('Arbel\Config')->get('in_production');
        
    }

    public function bootstrapSession($e)
    {
        $session = $e->getApplication()
            ->getServiceManager()
            ->get(SessionManager::class);
        $session->start();

        $container = new Container('initialized');

        if (isset($container->init)) {
            return;
        }

        $serviceManager = $e->getApplication()->getServiceManager();
        $request        = $serviceManager->get('Request');

        $session->regenerateId(true);
        $container->init = 1;
        if (!$request instanceof \Zend\Console\Request) {

            $container->remoteAddr    = $request->getServer()->get('REMOTE_ADDR')
                    ?? null;
            $container->httpUserAgent = $request->getServer()->get('HTTP_USER_AGENT')
                    ?? null;
        }
        $config = $serviceManager->get('Config');
        if (!isset($config['session'])) {
            return;
        }

        $sessionConfig = $config['session'];

        if (!isset($sessionConfig['validators'])) {
            return;
        }

        $chain = $session->getValidatorChain();

        foreach ($sessionConfig['validators'] as $validator) {
            switch ($validator) {
                case Validator\HttpUserAgent::class:
                    $validator = new $validator($container->httpUserAgent);
                    break;
                case Validator\RemoteAddr::class:
                    $validator = new $validator($container->remoteAddr);
                    break;
                default:
                    $validator = new $validator();
            }

            $chain->attach('session.validate', array($validator, 'isValid'));
        }
    }

    public function initDi($sm)
    {
        $sm->get('Di')->instanceManager()
            ->setSharedInstance(ServiceManager::class, $sm)
            ->setSharedInstance(ObjectManager::class, 'DependencyInjector')
            ->setSharedInstance(CacheInterface::class, Cache::class)
            ->setSharedInstance(EventManagerInterface::class, 'EventManager')
            ->setSharedInstance(ServerFactoryInterface::class, ServerFactory::class)
            ->setSharedInstance(DatabaseInterface::class, 'DatabaseFactory')
            ->setSharedInstance(Log::class, 'Log')
            ->setSharedInstance(StorageInterface::class, 'Cache')
            ->setParameters(RestAPI::class,
                [
                'auth_id' => $sm->get('Arbel\Config')->get('api.plivo.auth_id'),
                'auth_token' => $sm->get('Arbel\Config')->get('api.plivo.auth_token')
            ])
            ->setParameters(RestClient::class,
                [
                'authId' => $sm->get('Arbel\Config')->get('api.plivo.auth_id'),
                'authToken' => $sm->get('Arbel\Config')->get('api.plivo.auth_token')
            ])
            ->setSharedInstance(SmsInterface::class, PlivoSms::class)
            ->setSharedInstance(MobileNotificationInterface::class, Fcm::class)
            ->setSharedInstance(CallInterface::class, PlivoCall::class)
            ->setSharedInstance(UploadInterface::class, Storage::class)
            ->setParameters('Arbel\Config',
                [
                'data' => $sm->get('Config')
            ])
            ->setSharedInstance(Translate::class, 'Api\Translate')
            ->setParameters(Adapter::class,
                [
                'driver' => $sm->get('Arbel\Config')->get('db_connection')
            ])
            ->setParameters(SmtpOptions::class,
                [
                'options' => $sm->get('Arbel\Config')->get('email.default')
            ])
            ->setSharedInstance(Capsule::class, Capsule::class)
            //Set all object db insences to not be shared by default
            ->setShared(ModelAbstract::class, false)
            ->setShared(ModuleUpdater::class, false)
            ->setShared(CacheManager::class, true)
        ;
    }

    public function runUpdater($sm)
    {
        $manager         = $sm->get('ModuleManager');
        $modules         = $manager->getLoadedModules();
        $loadedModules   = array_keys($modules);
        $skipActionsList = array('notFoundAction', 'getMethodFromAction');

        foreach ($loadedModules as $loadedModule) {
            $moduleClass  = '\\'.$loadedModule.'\Module';
            $moduleObject = new $moduleClass;
            $updater      = $sm->get('Di')->get(ModuleUpdater::class);
            $updater->loadModule($moduleObject);
            if ($updater->isUpdaterExist() && $updater->isNeedToUpdate()) {
                $updater->run();
            }
        }
    }

    /**
     * Attach listeners to an event manager
     *
     * @param  EventManagerInterface $events
     * @return void
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH,
            [$this, 'onDispatch'], 1);
    }

    /**
     * Listen to the "dispatch" event
     *
     * @param  MvcEvent $event
     * @return mixed
     */
    public function onDispatch(MvcEvent $event)
    {
        $this->initMiddlewaresDispatch($event);
    }

}