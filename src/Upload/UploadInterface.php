<?php

 namespace Arbel\Upload;
 
interface UploadInterface {
    
    public function upload(string $filePath,string $ext = null,string $prefix = '',string $newName = null) : string;
     
}