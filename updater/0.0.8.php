<?php
$updater = $this;

//Create Acl role table
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('acl_role'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('acl_role'),
        function ($table) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('code', 255)->unique();
        $table->string('scope')->default('default');
        $table->datetimes();
    });
}

//Create Acl role table
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('acl_role_parents'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('acl_role_parents'),
        function ($table) use ($updater) {
        $table->string('role_code', 255);
        $table->string('parent_code', 255);
        $table->datetimes();
        $table->unique(['role_code', 'parent_code']);
        $table->foreign('role_code')->references('code')->on($updater->getTableName('acl_role'))->onDelete('cascade');
        $table->foreign('parent_code')->references('code')->on($updater->getTableName('acl_role'))->onDelete('cascade');
    });
}


//Create Acl resource table
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('acl_resource'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('acl_resource'),
        function ($table) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('code', 255)->unique();
        $table->string('scope')->default('default');
        $table->string('parent_code', 255)->nullable();
        $table->datetimes();
    });
}

//Create Acl action table
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('acl_privilege'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('acl_privilege'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('code', 255)->unique();
        $table->string('scope')->default('default');
        $table->string('parent_code', 255)->nullable();
        $table->datetimes();
    });
}

//Create Acl role
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('acl_permission'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('acl_permission'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->string('scope')->default('default');
        $table->string('role_code', 255)->nullable();
        $table->string('resource_code', 255)->nullable();
        $table->string('privilege_code', 255)->nullable();
        $table->enum('type', array('allow', 'deny'))->default('deny');
        $table->datetimes();
        $table->foreign('role_code')->references('code')->on($updater->getTableName('acl_role'))->onDelete('cascade');
        $table->foreign('resource_code')->references('code')->on($updater->getTableName('acl_resource'))->onDelete('cascade');
        $table->foreign('privilege_code')->references('code')->on($updater->getTableName('acl_privilege'))->onDelete('cascade');
    });
}


