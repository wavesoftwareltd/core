<?php
$updater = $this;

if ($this->getDatabase()->getSchema()->hasTable('core_user')) {
    $this->getDatabase()->getSchema()->table('core_user',
        function ($table) {
        $table->string('guid',255)->nullable();
    });
}

$users = $this->getModel(\Arbel\Model\User::class)->getAll();

foreach($users as $user){
    $user->setGuid(Arbel\Helper::guid())
        ->save();
}