<?php
$updater = $this;

//Create email verification
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('change_password'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('change_password'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->integer('user_id');
        $table->string('email_token', 255);;
        $table->dateTime('expiration_time');
        $table->text('info')->nullable();
        $table->datetimes();
        $table->foreign('user_id')->references('id')->on($updater->getTableName('user'))->onDelete('cascade');
    });
}
