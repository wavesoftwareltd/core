<?php
$updater = $this;

//Create email verification
if ($this->getDatabase()->getSchema()->hasTable($this->getTableName('translate'))) {
    $this->getDatabase()->getSchema()->table($this->getTableName('translate'),
        function ($table) use ($updater) {
      //  $table->dropUnique('core_translate_name_domain_language_code_unique');
    });

     $this->getDatabase()->getSchema()->table($this->getTableName('translate'),
        function ($table) use ($updater) {
        $table->text('name')->change();
    });
}
