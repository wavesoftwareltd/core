<?php
$updater = $this;

//Delete oauth table
if ($this->getDatabase()->getSchema()->hasTable('oauth_users')) {
    $this->getDatabase()->getSchema()->drop('oauth_users');
}
 
//Create Acl role table
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('acl_role_parents'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('acl_role_parents'),
        function ($table) use ($updater) {
        $table->string('role_code', 255);
        $table->string('parent_code', 255);
        $table->datetimes();
        $table->unique(['role_code', 'parent_code']);
        $table->foreign('role_code')->references('code')->on($updater->getTableName('acl_role'))->onDelete('cascade');
        $table->foreign('parent_code')->references('code')->on($updater->getTableName('acl_role'))->onDelete('cascade');
    });
}
