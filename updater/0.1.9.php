<?php
$updater = $this;

if ($this->getDatabase()->getSchema()->hasTable($this->getTableName('user'))) {
    $this->getDatabase()->getSchema()->table($this->getTableName('user'),
        function ($table) use ($updater) {
        $table->string('company_name', 255)->nullable();
    });
}
