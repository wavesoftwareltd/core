<?php
$updater = $this;

if ($this->getDatabase()->getSchema()->hasTable($this->getTableName('change_password'))) {
    $this->getDatabase()->getSchema()->table($this->getTableName('change_password'),
        function ($table) use ($updater) {
        $table->enum('is_used', array(0, 1))->default(0);
    });
}
