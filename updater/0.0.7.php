<?php
$updater = $this;
//Add address column to user table
$this->getDatabase()->getSchema()->table($this->getTableName('user'),
    function ($table) use ($updater) {
    $table->string('address', 255)->nullable();
    $table->string('country_code', 45)->nullable();
    $table->foreign('country_code')->references('iso')->on($updater->getTableName('country'));
});

//Create email tamplate table if not exist
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('channel'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('channel'),
        function ($table) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('code', 255)->unique();
        $table->string('scope')->default('default');
        $table->datetime('finish_at');
        $table->datetimes();
    });
}

//Create email tamplate table if not exist
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('communication_platform'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('communication_platform'),
        function ($table) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('code', 255)->unique();
        $table->enum('status', array(0, 1))->default(1);
        $table->datetimes();
    });
}

//Create email tamplate table if not exist
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('communication_conversation'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('communication_conversation'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->string('scope')->default('default');
        $table->integer('channel_id', false, true)->nullable();
        $table->datetime('finish_at')->nullable();
        $table->datetimes();
        $table->foreign('channel_id')->references('id')->on($updater->getTableName('channel'));
    });
}


//Create email tamplate table if not exist
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('communication_notification'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('communication_notification'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->string('scope')->default('default');
        $table->integer('channel_id', false, true)->nullable();
        //$table->datetime('finish_at', 255);
        $table->datetimes();
        $table->foreign('channel_id')->references('id')->on($updater->getTableName('channel'));
    });
}

//Create email tamplate table if not exist
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('communication_interlocutor'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('communication_interlocutor'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->integer('conversation_id', false, true)->nullable();
        $table->integer('notification_id', false, true)->nullable();
        $table->enum('status', array(0, 1))->default(1);
        $table->string('platform_code', 255);
        $table->string('source', 255);
        $table->integer('user_id', false, true)->nullable();
        $table->enum('is_visible', array(0, 1))->default(1);
        $table->enum('is_connected', array(0, 1))->default(1);
        $table->enum('is_system', array(0, 1))->default(0);
        $table->string('class_name', 255)->nullable();
        $table->datetime('last_activity')->nullable();
        $table->datetimes();
        $table->foreign('conversation_id')->references('id')->on($updater->getTableName('communication_conversation'));
        $table->foreign('notification_id')->references('id')->on($updater->getTableName('communication_notification'));
        $table->foreign('platform_code')->references('code')->on($updater->getTableName('communication_platform'));
    });
}


if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('communication_message'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('communication_message'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->string('group_code', 255);
        $table->integer('sender_interlocutor_id', false, true);
        $table->integer('reciver_interlocutor_id', false, true);
        $table->string('platform_code', 100);
        $table->string('subject', 255)->nullable();
        $table->longText('content');
        $table->longText('info')->nullable();
        $table->enum('status', array('pending', 'sent', 'error'))->default('pending');
        $table->enum('is_open', array(0, 1))->default(0);
        $table->enum('is_read', array(0, 1))->default(0);
        $table->datetimes();
        $table->foreign('sender_interlocutor_id')->references('id')->on($updater->getTableName('communication_interlocutor'));
        $table->foreign('reciver_interlocutor_id')->references('id')->on($updater->getTableName('communication_interlocutor'));
        $table->foreign('platform_code')->references('code')->on($updater->getTableName('communication_platform'));
    });
}

$this->getModel('Arbel\Model\Communication\Platform')
    ->setName('Email')
    ->save();

$this->getModel('Arbel\Model\Communication\Platform')
    ->setName('Phone Sms')
    ->save();

$this->getModel('Arbel\Model\Communication\Platform')
    ->setName('Phone Voice')
    ->save();



