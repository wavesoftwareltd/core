<?php

use Arbel\Model\Communication\Email\Template as EmailTemplate;
use Arbel\Model\Communication\Sms\Template as SmsTemplate;

if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('communication_sms_template'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('communication_sms_template'),
        function ($table) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('code', 255)->unique();
        $table->longText('content');
        $table->datetimes();
    });
}

$updater = $this;

$this->getModel(EmailTemplate::class)
    ->setName('Manager User Registration')
    ->setCode('manager_user_registration')
    ->setSubject('New Registration')
    ->setContent('Hi [name] <br> '
        . 'You has a new account in [url] <br>'
        . 'And your login details is '
        . '[email] <br>'
        . '[password] <br>')
    ->save(false,true);

$this->getModel(SmsTemplate::class)
    ->setName('Verification code')
    ->setCode('verification_code')
    ->setContent('your verification code is [code]')
    ->save(false,true);

$this->getModel(EmailTemplate::class)
    ->setName('Manager User Registration')
    ->setCode('welcome_email')
    ->setSubject('Welcom')
    ->setContent('Hi [name] <br> '
        . 'You has a new account in [url] <br>'
        . 'And your login details is '
        . '[email] <br>'
        . '[password] <br>')
    ->save(false,true);