<?php
//Create language table if not exist
if (!$this->getDatabase()->getSchema()->hasTable('core_user')) {
    $this->getDatabase()->getSchema()->create('core_user',
        function ($table) {
        $table->increments('id');
        $table->string('first_name', 255);
        $table->string('last_name', 255);
        $table->string('email', 255)->unique();
        $table->string('password', 255)->nullable();
        $table->string('language_code', 255)->nullable();
        $table->string('scope', 100)->default('basic');
        $table->string('phone', 100)->nullable();
        $table->string('permission', 100)->nullable();
        $table->dateTime('last_login')->nullable();
        $table->text('info');
        $table->datetimes();

        $table->foreign('language_code')->references('code')->on('core_language');
    });
}
