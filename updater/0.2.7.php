<?php
$updater = $this;

//Create email verification
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('shorten_url'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('shorten_url'),
        function ($table) use ($updater) { 
        $table->index('code');
    });
}
