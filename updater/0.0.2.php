<?php

//Create language table if not exist
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('language'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('language'),
        function ($table) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('code', 255)->unique();
        $table->string('lang', 255);
        $table->string('country', 100);
        $table->datetimes();
    });
}

//Create translate table if not exist
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('translate'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('translate'),
        function ($table) use ($updter) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('text', 255);
        $table->string('domain', 255)->default('default');
        $table->string('language_code', 255);
        $table->foreign('language_code')->references('code')->on($updter->getTableName('language'));
        $table->unique(['name','domain','language_code']);
        $table->datetimes();
    });
}

// Add english language
$this->getModel('Arbel\Model\Language')
    ->setName('English')
    ->setCode('en-us')
    ->setLang('en')
    ->setCountry('us')
    ->save(false,true);

// Add hebrew language
$this->getModel('Arbel\Model\Language')
    ->setName('Hebrew')
    ->setCode('he-il')
    ->setLang('he')
    ->setCountry('il')
    ->save(false,true);
