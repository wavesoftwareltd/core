<?php
$updater = $this;
//Create email tamplate table if not exist
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('communication_email_template'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('communication_email_template'),
        function ($table) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('code', 255)->unique();
        $table->string('subject', 255);
        $table->longText('content');
        $table->datetimes();
    });
}

//Create email tamplate table if not exist
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('communication_email'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('communication_email'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('code', 255);
        $table->string('scope')->default('default');
        $table->string('template_code', 255);
        $table->string('from', 255);
        $table->string('sender', 255);
        $table->string('to', 255);
        $table->string('reply_to', 255);
        $table->string('cc', 255);
        $table->string('bcc', 255);
        $table->datetimes();
        $table->unique(['code','scope']);
        $table->foreign('template_code')->references('code')->on($updater->getTableName('communication_email_template'));
    });
}

// Add translate cache
$this->getModel('Arbel\Model\Communication\Email\Template')
    ->setName('Reset Password V1')
    ->setCode('reset_password_v1')
    ->setContent('<div>Please click this link to reset your password</br><a href=|link| >|link| </a> </div>')
    ->setSubject('Reset password for |name|')
    ->save(false,true);

// Add translate cache
$this->getModel('Arbel\Model\Communication\Email')
    ->setName('Reset Password')
    ->setCode('reset_password')
    ->setTemplate('reset_password_v1')
    ->setFrom('service@w.wave-software.net')
    ->setSender('Arbel Service')
    ->setTo('yarivluts@gmail.com')
    ->setReplyTo('service@w.wave-software.net')
    ->setCc('yarivluts@gmail.com')
    ->setBcc('yarivluts@gmail.com')
    ->save(false,true);

