<?php
$updater = $this;

if ($this->getDatabase()->getSchema()->hasTable($this->getTableName('communication_message'))) {
    $this->getDatabase()->getSchema()->table($this->getTableName('communication_message'),
        function ($table) use ($updater) {
        $table->string('tracking_key', 255)->nullable();
    });
}
