<?php
$updater = $this;

//Create email verification
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('registration'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('registration'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->integer('user_id');
        $table->string('email_token', 255);
        $table->string('phone_code', 255);
        $table->enum('is_email_verified', array(0, 1))->default(1);
        $table->enum('is_phone_verified', array(0, 1))->default(1);
        $table->dateTime('expiration_time');
        $table->text('info')->nullable();
        $table->datetimes();
        $table->foreign('user_id')->references('id')->on($updater->getTableName('user'))->onDelete('cascade');
    });
}
