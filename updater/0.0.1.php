<?php
//Create updater table if not exist
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('updater'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('updater'),
        function ($table) {
        $table->increments('id');
        $table->string('package_name', 255)->unique();
        $table->string('version', 100);
        $table->datetimes();
    });
}
