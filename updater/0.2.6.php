<?php
$updater = $this;

//Create email verification
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('shorten_url'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('shorten_url'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->string('code', 255);
        $table->string('value', 2083)->nullable();
        $table->enum('status', array(0, 1))->default(1); 
        $table->index('value');
        $table->text('info')->nullable();
        $table->datetimes();
    });
}
