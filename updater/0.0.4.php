<?php
//Create language table if not exist
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('cache'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('cache'),
        function ($table) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('code', 255)->unique();
        $table->enum('status', array(0,1))->default(1);
        $table->datetimes();
    });
}

// Add translate cache
$this->getModel('Arbel\Model\Cache')
    ->setName('General Cache')
    ->setCode('general')
    ->save(false,true);

// Add translate cache
$this->getModel('Arbel\Model\Cache')
    ->setName('Translate Cache')
    ->setCode('translate')
    ->save(false,true);

