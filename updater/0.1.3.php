<?php
$updater = $this;
 
//Create email verification
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('log'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('log'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->string('type', 255);
        $table->string('scope', 255);
        $table->json('info');
        $table->datetimes();
    });
}
