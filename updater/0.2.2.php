<?php
$updater = $this;

//Create email verification
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('config'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('config'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->string('scope', 255)->default('default');
        $table->string('code', 255);
        $table->string('value', 255);
        $table->datetimes();
    });
}
