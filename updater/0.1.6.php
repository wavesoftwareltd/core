<?php
$updater = $this;

//Create email verification
if ($this->getDatabase()->getSchema()->hasTable($this->getTableName('user'))) {
    $this->getDatabase()->getSchema()->table($this->getTableName('user'),
        function ($table) use ($updater) {
        $table->enum('is_verified', array(0, 1))->default(0);
    });
}
