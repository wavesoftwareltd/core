<?php
$updater = $this;

//Modify user table
$this->getDatabase()->getSchema()->table($this->getTableName('user'),
    function ($table) use ($updater) {
        $table->enum('status', array(0, 1))->default(1);
});

//Create email verification
if (!$this->getDatabase()->getSchema()->hasTable($this->getTableName('user_verification'))) {
    $this->getDatabase()->getSchema()->create($this->getTableName('user_verification'),
        function ($table) use ($updater) {
        $table->increments('id');
        $table->string('verification_code', 255);
        $table->string('email', 255);
        $table->integer('user_id');
        $table->dateTime('expiration_time');
        $table->enum('is_pass', array(0, 1))->default(1);
        $table->datetimes();
        $table->foreign('user_id')->references('id')->on($updater->getTableName('user'))->onDelete('cascade');
    });
}
