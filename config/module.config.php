<?php

/**
 * Arbel Core Configuration
 */
use Arbel\Service\Factory\DiFactory;
use Arbel\Service\Factory\ConfigFactory;
use Arbel\Service\Factory\LanguageFactory;
use Arbel\Service\Factory\SessionManagerFactory;
use Arbel\Service\Factory\UserFactory;
use Arbel\Service\Factory\CacheFactory;
use Arbel\Service\Factory\CapsuleFactory;
use Zend\Session\Storage\SessionArrayStorage;
use Zend\Session\Validator\RemoteAddr;
use Zend\Session\Validator\HttpUserAgent;
use Zend\Session\SessionManager;
use Illuminate\Database\Capsule\Manager as Capsule;
use Arbel\Controller\CacheController;
use Arbel\Service\Factory\Google\TranslateFactory;
use Arbel\Cache\CacheInterface;
use Arbel\Service\Factory\DiServiceFactory;
use Plivo\RestAPI;
use Arbel\Service\Factory\PlivoFactory;
use Arbel\Controller\CommunicationController;
use Arbel\Controller\ResourcesController;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Arbel\Middleware\PermissionMiddleware;
use Arbel\Controller\UserController;
use Arbel\Controller\InfoController;
use Arbel\Controller\AuthController;
use Arbel\Service\Factory\OAuth2\ServerFactoryFactory;
use Arbel\OAuth2\ServerFactory;
use Arbel\Service\Factory\OAuth2\AuthControllerFactory;
use Arbel\Controller\TranslateController;
use Arbel\Controller\UploadController;
use ZF\MvcAuth\Authentication\DefaultAuthenticationListener;
use Arbel\Controller\Manager\AuthController as ManagerAuthController;
use Arbel\Controller\ShortenController;
use Arbel\Service\Factory\DatabaseFactory;
use Arbel\Middleware\JsonPostMiddleware;

return [
    'service_manager' => [
        'factories' => [
            'Log' => 'Arbel\Service\Factory\LogFactory',
            'DependencyInjector' => DiFactory::class,
            DefaultAuthenticationListener::class => \Arbel\Service\Factory\DefaultAuthenticationListenerFactory::class,
            'Arbel\Config' => ConfigFactory::class,
            'Cache' => CacheFactory::class,
            'User' => UserFactory::class,
            'Api\Translate' => TranslateFactory::class,
            RestAPI::class => PlivoFactory::class,
            Capsule::class => CapsuleFactory::class,
            'DatabaseFactory' => DatabaseFactory::class,
            SessionManager::class => SessionManagerFactory::class,
            PermissionMiddleware::class => DiServiceFactory::class,
            JsonPostMiddleware::class => DiServiceFactory::class,
            ServerFactory ::class => ServerFactoryFactory::class,
        ],
        'di' => [
            'allowed_controllers' => [
                // this config is required, otherwise the MVC won't even attempt to ask Di for the controller!
                'Application\Controller\MainController',
            ],
            'instance' => [
                'preference' => [
                    // these allow injecting correct EventManager and ServiceManager
                    // (taken from the main ServiceManager) into the controller,
                    // because Di doesn't know how to retrieve abstract types. These
                    // dependencies are inherited from Zend\Mvc\Controller\AbstractController
                    'Zend\EventManager\EventManagerInterface' => 'EventManager',
                    'Zend\ServiceManager\ServiceLocatorInterface' => 'ServiceManager',
                ],
            ],
        ]
    ],
    'router' => [
        'routes' => [
            'communication' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/core/communication[/:action]',
                    'defaults' => [
                        'controller' => CommunicationController::class,
                        'action' => 'create',
                        'middlewares' => [PermissionMiddleware::class],
                        'is_use_acl' => true
                    ],
                ],
            ],
            'user_api' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api/user',
                    'defaults' => [
                        'controller' => UserController::class,
                    ],
                ],
            ],
            'shorten_url' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/s/:code',
                    'defaults' => [
                        'controller' => ShortenController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'auth_guid' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/guid/:guid',
                    'defaults' => [
                        'controller' => AuthController::class,
                        'action' => 'guid',
                    ],
                ],
            ],
            'upload_api' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/api/upload/:action',
                    'defaults' => [
                        'controller' => UploadController::class,
                        'is_use_acl' => true
                    ],
                ],
            ],
            'auth_api' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/api/auth/:action',
                    'defaults' => [
                        'controller' => AuthController::class,
                    ],
                ],
            ],
            'manager_auth_api' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/api/manager_auth/:action',
                    'defaults' => [
                        'controller' => ManagerAuthController::class,
                        'is_use_acl' => true
                    ],
                ],
            ],
            'auth_api_verify' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api/auth/verify_login',
                    'defaults' => [
                        'controller' => AuthController::class,
                        'action' => 'verifyLogin',
                        //'is_use_acl' => true
                    ],
                ],
            ],
            'api_info' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api/info',
                    'defaults' => [
                        'controller' => InfoController::class,
                        'action' => 'get',
                        'is_use_acl' => false
                    ],
                ],
            ],
            'api_contact' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api/contact',
                    'defaults' => [
                        'controller' => CommunicationController::class,
                        'action' => 'contact',
                        'is_use_acl' => false
                    ],
                ],
            ],
            'api_translate' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api/translate',
                    'defaults' => [
                        'controller' => TranslateController::class,
                        'action' => 'index',
                        'is_use_acl' => false
                    ],
                ],
            ],
        ]
    ],
    'controllers' => [
        'factories' => [
            CacheController::class => DiServiceFactory::class,
            ResourcesController::class => DiServiceFactory::class,
            CommunicationController::class => DiServiceFactory::class,
            UserController::class => DiServiceFactory::class,
            InfoController ::class => DiServiceFactory::class,
            AuthController ::class => AuthControllerFactory::class,
            TranslateController ::class => DiServiceFactory::class,
            UploadController ::class => DiServiceFactory::class,
            ManagerAuthController ::class => DiServiceFactory::class,
            ShortenController ::class => DiServiceFactory::class,
        ],
    ],
    'log' => [
        'dir' => 'logs',
        'permission' => 0755,
        'enable_zend_server_log' => false,
    ],
    // Session configuration.
    'session_config' => [
        // Session cookie will expire in 1 week.
        'cookie_lifetime' => 60 * 60 * 24 * 7,
        // Session data will be stored on server maximum for 30 days.
        'gc_maxlifetime' => 60 * 60 * 24 * 30,
    ],
    // Session manager configuration.
    'session_manager' => [
        // Session validators (used for security).
        'validators' => [
            RemoteAddr::class,
            HttpUserAgent::class,
        ]
    ],
    // Session storage configuration.
    'session_storage' => [
        'type' => SessionArrayStorage::class
    ],
    //Cache settings
    'cache' => [
        'adapter' => [
            'name' => 'filesystem',
            'options' => [
                'cache_dir' => 'cache',
                'key_pattern' => null,
                'dir_level' => 2
            ]
        ],
        'plugins' => [
            // Don't throw exceptions on cache errors
            'exception_handler' => [
                'throw_exceptions' => false
            ],
        ],
    ],
    //Console config
    'console' => [
        'router' => [
            'routes' => [
                'cache' => [
                    'options' => [
                        'route' => 'cache <action> [<code>]',
                        'defaults' => [
                            'controller' => CacheController::class,
                            'action' => 'list',
                        ],
                    ],
                ],
                'resources' => [
                    'options' => [
                        'route' => 'resources <action> [<code>]',
                        'defaults' => [
                            'controller' => ResourcesController::class,
                            'action' => 'list',
                        ],
                    ],
                ],
            ],
        ],
    ],
    //Default language
    'language' => [
        'default' => [
            'local' => 'he-il'
        ]
    ],
    //Translate config
    'translate' => [
        'use_google_translate' => true
    ],
    'zf-mvc-auth'        => [
        'authentication' => [
            'adapters' => [
                'user' => [
                    'adapter' => 'Arbel\OAuth2\OAuth2Adapter',
                    ],
                'client' => [
                    'adapter' => 'Arbel\OAuth2\OAuth2Adapter',
                    ],
                'named-storage' => [
                    'adapter' => 'Arbel\OAuth2\OAuth2Adapter',
                    ]
                ]
            ],
        ],

    'view_manager' => [
        'template_map' => [
            'arbel/layout/layout' =>  __DIR__ . '/../views/layout/layout.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ],
    'auth' => [
        'allow_guid_login' => false
    ]
];
